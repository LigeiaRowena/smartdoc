//
//  PDFExporter.m
//  SmartDoc
//
//  Created by Francesca Corsini on 05/09/15.
//  Copyright (c) 2015 Francesca Corsini. All rights reserved.
//

#import "PDFExporter.h"
#import "CustomPrintPageRenderer.h"
#import "DBManager.h"
#import "Prescription.h"
#import "PrescribedTherapy.h"
#import "DiagnosticTest.h"
#import "Patient.h"
#import "PatientData.h"
#import "ContactPhone.h"
#import "ContactEmail.h"
#import "Visit.h"
#import "RemoteAnamnesis.h"
#import "Measurement.h"
#import "Symptom.h"
#import "FatherDiseaseHistory.h"
#import "MotherDiseaseHistory.h"
#import "Vaccination.h"
#import "DiseaseHistory.h"
#import "Operation.h"
#import "ProximalAnamnesis.h"
#import "NSDate-Utilities.h"
#import "ActualRiskFactor.h"
#import "OtherRiskFactor.h"
#import "Allergy.h"

#define DRUG_ROW @"<tr><td style=\"width: 200px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$DRUG_NAME$]</td><td style=\"width: 120px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$DOSAGE$]</td><td style=\"width: 120px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$UNIT$]</td><td style=\"width: 120px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$FREQUENCY$]</td><td style=\"width: 120px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$ROUTE$]</td></tr>"

#define ALLERGY_ROW @"<tr><td style=\"width: 200px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$ALLERGY_TYPE$]</td><td style=\"width: 506px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$ALLERGY_NOTE$]</td></tr>"


#define TEST_ROW @"<tr><td style=\"width: 200px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$TEST_TYPE$]</td><td style=\"width: 506px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$TEST_NOTE$]</td></tr>"

#define CONTACT_TELEPHONE_ROW @"<tr><td style=\"width: 189px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$CONTACT_TYPE$]</td><td style=\"width: 189px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$TELEPHONE$]</td></tr>"

#define CONTACT_EMAIL_ROW @"<tr><td style=\"width: 189px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$CONTACT_TYPE$]</td><td style=\"width: 189px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$EMAIL$]</td></tr>"

#define CURRENT_MEASUREMENT_ROW @"<tr><td style=\"width: 130px; color: #78808B; font-family:opensans; font-size:15px; text-align:center\">[$HEIGHT$]</td><td style=\"width: 130px; color: #78808B; font-family:opensans; font-size:15px; text-align:center\">[$WEIGHT$]</td><td style=\"width: 130px; color: #78808B; font-family:opensans; font-size:15px; text-align:center\">[$WAIST$]</td><td style=\"width: 130px; color: #78808B; font-family:opensans; font-size:15px; text-align:center\">[$BLOOD_MAX$]</td><td style=\"width: 130px; color: #78808B; font-family:opensans; font-size:15px; text-align:center\">[$BLOOD_MIN$]</td><td style=\"width: 130px; color: #78808B; font-family:opensans; font-size:15px; text-align:center\">[$CARDIAC_F$]</td></tr>"

#define PREVIOUS_MEASUREMENT_ROW @"<tr><td style=\"width: 130px; color: #78808B; font-family:opensans-bold; font-size:14px; text-align:center\">[$PREV_HEIGHT$]</td><td style=\"width: 130px; color: #78808B; font-family:opensans-bold; font-size:14px; text-align:center\">[$PREV_WEIGHT$]</td><td style=\"width: 130px; color: #78808B; font-family:opensans-bold; font-size:14px; text-align:center\">[$PREV_WAIST$]</td><td style=\"width: 130px; color: #78808B; font-family:opensans-bold; font-size:14px; text-align:center\">[$PREV_BLOOD_MAX$]</td><td style=\"width: 130px; color: #78808B; font-family:opensans-bold; font-size:14px; text-align:center\">[$PREV_BLOOD_MIN$]</td><td style=\"width: 130px; color: #78808B; font-family:opensans-bold; font-size:14px; text-align:center\">[$PREV_CARDIAC_F$]</td></tr>"

#define SYMPTOM_ROW @"<tr><td style=\"width: 200px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$SYMPTOM_TYPE$]</td><td style=\"width: 200px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$SYMPTOM_DETAILS$]</td><td style=\"width: 200px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$SYMPTOM_NOTE$]</td></tr>"

#define FATHER_DISEASE_HISTORY_ROW @"<tr><td style=\"width: 223px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$TYPE$]</td><td style=\"width: 267px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$NOTE$]</td></tr>"

#define MOTHER_DISEASE_HISTORY_ROW @"<tr><td style=\"width: 223px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$TYPE$]</td><td style=\"width: 267px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$NOTE$]</td></tr>"

#define VACCINATION_ROW @"<tr><td style=\"width: 223px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$TYPE$]</td><td style=\"width: 200px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$BIRTH_DATE$]</td><td style=\"width: 200px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$NOTE$]</td></tr>"

#define PATIENT_DISEASE_ROW @"<div style=\"color: #22262C; font-family:opensans-bold; font-size:16px\">[$DISEASE$]</div>"

#define OTHER_RISK_ROW @"<div style=\"color: #22262C; font-family:opensans-bold; font-size:16px\">[$OTHER_RISK$]</div>"

#define OPERATION_ROW @"<tr><td style=\"width: 223px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$OPERATION$]</td><td style=\"width: 200px; color: #22262C; font-family:opensans-bold; font-size:17px; text-align:left\">[$NOTE$]</td></tr>"


@implementation PDFExporter

#pragma mark - NSFileManager methods

+ (void)clearDocuments
{
	NSString *documentsDirectory = [PDFExporter getDocumentsDirectory];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error = nil;
	
	NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:nil];
	for (NSString *fileName in contents)
	{
		if ([fileManager fileExistsAtPath:fileName])
			[fileManager removeItemAtPath:fileName error:&error];
	}
}

+ (NSString*)getDocumentsDirectory
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	return [paths objectAtIndex:0];
}

#pragma mark - PDF Exporter

+ (NSData*) printToPDFWithRenderer:(CustomPrintPageRenderer*)renderer
{
	NSMutableData *wrapper = [NSMutableData data];
	UIGraphicsBeginPDFContextToData(wrapper, CGRectZero, nil );
	for ( int i = 0 ; i < [renderer numberOfPages] ; i++ )
	{
		UIGraphicsBeginPDFPage();
		CGRect bounds = UIGraphicsGetPDFContextBounds();
		[renderer drawPageAtIndex:i inRect: bounds];
	}
	UIGraphicsEndPDFContext();
	
	return wrapper;
}

+ (NSData*)getPDFDataWithHTML:(NSString*)html
{
	// create page renderer and add print formatter to it
	CustomPrintPageRenderer *renderer = [[CustomPrintPageRenderer alloc] init];
	UIMarkupTextPrintFormatter *fmt = [[UIMarkupTextPrintFormatter alloc] initWithMarkupText:html];
	[renderer addPrintFormatter:fmt startingAtPageAtIndex:0];

	// generate pdf data in a4 size
	NSData *pdfData = [PDFExporter printToPDFWithRenderer: renderer];
	
	return pdfData;
}

+ (NSString*)getHTMLFromDrugs:(NSArray*)drugs
{
	NSMutableString *drugString = @"".mutableCopy;
	for (PrescribedTherapy *drug in drugs)
	{
		drugString = DRUG_ROW.mutableCopy;
		drugString = [drugString stringByReplacingOccurrencesOfString:@"[$DRUG_NAME$]" withString:(drug.drugName ? drug.drugName : @"-")].mutableCopy;
		drugString = [drugString stringByReplacingOccurrencesOfString:@"[$DOSAGE$]" withString:(drug.dosage ? drug.dosage : @"-")].mutableCopy;
		drugString = [drugString stringByReplacingOccurrencesOfString:@"[$UNIT$]" withString:(drug.dosageUnit ? drug.dosageUnit : @"-")].mutableCopy;
		drugString = [drugString stringByReplacingOccurrencesOfString:@"[$FREQUENCY$]" withString:(drug.frequency ? drug.frequency : @"-")].mutableCopy;
		drugString = [drugString stringByReplacingOccurrencesOfString:@"[$ROUTE$]" withString:(drug.route ? drug.route : @"-")].mutableCopy;
	}
	return drugString;
}

+ (NSString*)getHTMLFromAllergies:(NSArray*)allergies
{
	NSMutableString *allergyString = @"".mutableCopy;
	for (Allergy *allergy in allergies)
	{
		allergyString = ALLERGY_ROW.mutableCopy;
		allergyString = [allergyString stringByReplacingOccurrencesOfString:@"[$ALLERGY_TYPE$]" withString:(allergy.name ? allergy.name : @"-")].mutableCopy;
		allergyString = [allergyString stringByReplacingOccurrencesOfString:@"[$ALLERGY_NOTE$]" withString:(allergy.note ? allergy.note : @"-")].mutableCopy;
	}
	return allergyString;
}

+ (NSString*)getHTMLFromSymptoms:(NSArray*)symptoms
{
	NSMutableString *symptomString = @"".mutableCopy;
	for (Symptom *symptom in symptoms)
	{
		symptomString = SYMPTOM_ROW.mutableCopy;
		symptomString = [symptomString stringByReplacingOccurrencesOfString:@"[$SYMPTOM_TYPE$]" withString:(symptom.name ? symptom.name : @"-")].mutableCopy;
		symptomString = [symptomString stringByReplacingOccurrencesOfString:@"[$SYMPTOM_DETAILS$]" withString:(symptom.details ? symptom.details : @"-")].mutableCopy;
		symptomString = [symptomString stringByReplacingOccurrencesOfString:@"[$SYMPTOM_NOTE$]" withString:(symptom.note ? symptom.note : @"-")].mutableCopy;
	}
	return symptomString;
}

+ (NSString*)getHTMLFromTests:(NSArray*)tests
{
	NSMutableString *testString = @"".mutableCopy;
	for (DiagnosticTest *test in tests)
	{
		testString = TEST_ROW.mutableCopy;
		testString = [testString stringByReplacingOccurrencesOfString:@"[$TEST_TYPE$]" withString:(test.name ? test.name : @"-")].mutableCopy;
		testString = [testString stringByReplacingOccurrencesOfString:@"[$TEST_NOTE$]" withString:(test.note ? test.note : @"-")].mutableCopy;
	}
	return testString;
}

+ (NSString*)getHTMLFromTelephoneContacts:(NSArray*)contacts
{
	NSMutableString *contactString = @"".mutableCopy;
	for (ContactPhone *contact in contacts)
	{
		contactString = CONTACT_TELEPHONE_ROW.mutableCopy;
		contactString = [contactString stringByReplacingOccurrencesOfString:@"[$CONTACT_TYPE$]" withString:(contact.type ? contact.type : @"-")].mutableCopy;
		contactString = [contactString stringByReplacingOccurrencesOfString:@"[$TELEPHONE$]" withString:(contact.text ? contact.text : @"-")].mutableCopy;
	}
	return contactString;
}

+ (NSString*)getHTMLFromEmailContacts:(NSArray*)contacts
{
	NSMutableString *contactString = @"".mutableCopy;
	for (ContactEmail *contact in contacts)
	{
		contactString = CONTACT_EMAIL_ROW.mutableCopy;
		contactString = [contactString stringByReplacingOccurrencesOfString:@"[$CONTACT_TYPE$]" withString:(contact.type ? contact.type : @"-")].mutableCopy;
		contactString = [contactString stringByReplacingOccurrencesOfString:@"[$EMAIL$]" withString:(contact.text ? contact.text : @"-")].mutableCopy;
	}
	return contactString;
}

+ (NSString*)getHTMLFromMeasurements:(NSArray*)measurements
{
	NSMutableString *measurementString = @"".mutableCopy;
	measurementString = CURRENT_MEASUREMENT_ROW.mutableCopy;

	for (Measurement *measurement in measurements)
	{
		if ([measurement.name isEqualToString:@"Height"])
			measurementString = [measurementString stringByReplacingOccurrencesOfString:@"[$HEIGHT$]" withString:(measurement.value ? [getDecimalFormatter(1) stringFromNumber:measurement.value] : @"")].mutableCopy;
		else if ([measurement.name isEqualToString:@"Weight"])
			measurementString = [measurementString stringByReplacingOccurrencesOfString:@"[$WEIGHT$]" withString:(measurement.value ? [getDecimalFormatter(1) stringFromNumber:measurement.value] : @"")].mutableCopy;
		else if ([measurement.name isEqualToString:@"Waist C."])
			measurementString = [measurementString stringByReplacingOccurrencesOfString:@"[$WAIST$]" withString:(measurement.value ? [getDecimalFormatter(1) stringFromNumber:measurement.value] : @"")].mutableCopy;
		else if ([measurement.name isEqualToString:@"Blood Max"])
			measurementString = [measurementString stringByReplacingOccurrencesOfString:@"[$BLOOD_MAX$]" withString:(measurement.value ? [getDecimalFormatter(1) stringFromNumber:measurement.value] : @"")].mutableCopy;
		else if ([measurement.name isEqualToString:@"Blood Min"])
			measurementString = [measurementString stringByReplacingOccurrencesOfString:@"[$BLOOD_MIN$]" withString:(measurement.value ? [getDecimalFormatter(1) stringFromNumber:measurement.value] : @"")].mutableCopy;
		else if ([measurement.name isEqualToString:@"Cardiac F."])
			measurementString = [measurementString stringByReplacingOccurrencesOfString:@"[$CARDIAC_F$]" withString:(measurement.value ? [getDecimalFormatter(1) stringFromNumber:measurement.value] : @"")].mutableCopy;
	}
	return measurementString;
}

+ (Measurement*)getLastMeasurement:(Measurement*)measurement patient:(Patient*)patient
{
	NSArray *prevMeasurements = [[DBManager getIstance] getAllMeasurementsWithName:measurement.name patient:patient];
	return (prevMeasurements.count>0 ? (Measurement*)prevMeasurements[0] : measurement);
}

+ (NSString*)getHTMLFromPreviousMeasurementsForPatient:(Patient*)patient currentMeasurements:(NSArray*)currentMeasurements
{
	NSMutableString *measurementString = @"".mutableCopy;
	measurementString = PREVIOUS_MEASUREMENT_ROW.mutableCopy;

	for (Measurement *measurement in currentMeasurements)
	{
		if ([measurement.name isEqualToString:@"Height"])
		{
			Measurement *lastMeasurement = [PDFExporter getLastMeasurement:measurement patient:patient];
			measurementString = [measurementString stringByReplacingOccurrencesOfString:@"[$PREV_HEIGHT$]" withString:(lastMeasurement.value ? [NSString stringWithFormat:@"%@ %@", [getDecimalFormatter(1) stringFromNumber:lastMeasurement.value], measurement.unit] : @"")].mutableCopy;
		}
		
		else if ([measurement.name isEqualToString:@"Weight"])
		{
			Measurement *lastMeasurement = [PDFExporter getLastMeasurement:measurement patient:patient];
			measurementString = [measurementString stringByReplacingOccurrencesOfString:@"[$PREV_WEIGHT$]" withString:(lastMeasurement.value ? [NSString stringWithFormat:@"%@ %@", [getDecimalFormatter(1) stringFromNumber:lastMeasurement.value], measurement.unit] : @"")].mutableCopy;
		}
		else if ([measurement.name isEqualToString:@"Waist C."])
		{
			Measurement *lastMeasurement = [PDFExporter getLastMeasurement:measurement patient:patient];
			measurementString = [measurementString stringByReplacingOccurrencesOfString:@"[$PREV_WAIST$]" withString:(lastMeasurement.value ? [NSString stringWithFormat:@"%@ %@", [getDecimalFormatter(1) stringFromNumber:lastMeasurement.value], measurement.unit] : @"")].mutableCopy;
		}
		else if ([measurement.name isEqualToString:@"Blood Max"])
		{
			Measurement *lastMeasurement = [PDFExporter getLastMeasurement:measurement patient:patient];
			measurementString = [measurementString stringByReplacingOccurrencesOfString:@"[$PREV_BLOOD_MAX$]" withString:(lastMeasurement.value ? [NSString stringWithFormat:@"%@ %@", [getDecimalFormatter(1) stringFromNumber:lastMeasurement.value], measurement.unit] : @"")].mutableCopy;
		}
		else if ([measurement.name isEqualToString:@"Blood Min"])
		{
			Measurement *lastMeasurement = [PDFExporter getLastMeasurement:measurement patient:patient];
			measurementString = [measurementString stringByReplacingOccurrencesOfString:@"[$PREV_BLOOD_MIN$]" withString:(lastMeasurement.value ? [NSString stringWithFormat:@"%@ %@", [getDecimalFormatter(1) stringFromNumber:lastMeasurement.value], measurement.unit] : @"")].mutableCopy;
		}
		else if ([measurement.name isEqualToString:@"Cardiac F."])
		{
			Measurement *lastMeasurement = [PDFExporter getLastMeasurement:measurement patient:patient];
			measurementString = [measurementString stringByReplacingOccurrencesOfString:@"[$PREV_CARDIAC_F$]" withString:(lastMeasurement.value ? [NSString stringWithFormat:@"%@ %@", [getDecimalFormatter(1) stringFromNumber:lastMeasurement.value], measurement.unit] : @"")].mutableCopy;
		}
	}
	return measurementString;
}

+ (NSString*)getHTMLFromFatherDiseaseHistory:(NSArray*)diseases
{
	NSMutableString *fatherString = @"".mutableCopy;
	for (FatherDiseaseHistory *disease in diseases)
	{
		fatherString = FATHER_DISEASE_HISTORY_ROW.mutableCopy;
		fatherString = [fatherString stringByReplacingOccurrencesOfString:@"[$TYPE$]" withString:(disease.type ? disease.type : @"-")].mutableCopy;
		fatherString = [fatherString stringByReplacingOccurrencesOfString:@"[$NOTE$]" withString:(disease.note ? disease.note : @"-")].mutableCopy;
	}
	return fatherString;
}

+ (NSString*)getHTMLFromMotherDiseaseHistory:(NSArray*)diseases
{
	NSMutableString *motherString = @"".mutableCopy;
	for (MotherDiseaseHistory *disease in diseases)
	{
		motherString = MOTHER_DISEASE_HISTORY_ROW.mutableCopy;
		motherString = [motherString stringByReplacingOccurrencesOfString:@"[$TYPE$]" withString:(disease.type ? disease.type : @"-")].mutableCopy;
		motherString = [motherString stringByReplacingOccurrencesOfString:@"[$NOTE$]" withString:(disease.note ? disease.note : @"-")].mutableCopy;
	}
	return motherString;
}

+ (NSString*)getHTMLFromVaccinations:(NSArray*)vaccinations
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
	[formatter setDateFormat:@"dd/MM/yyyy"];
	
	NSMutableString *vaccinationString = @"".mutableCopy;
	for (Vaccination *vaccination in vaccinations)
	{
		vaccinationString = VACCINATION_ROW.mutableCopy;
		vaccinationString = [vaccinationString stringByReplacingOccurrencesOfString:@"[$TYPE$]" withString:(vaccination.type ? vaccination.type : @"-")].mutableCopy;
		vaccinationString = [vaccinationString stringByReplacingOccurrencesOfString:@"[$BIRTH_DATE$]" withString:(vaccination.date ? [formatter stringFromDate: vaccination.date] : @"-")].mutableCopy;
		vaccinationString = [vaccinationString stringByReplacingOccurrencesOfString:@"[$NOTE$]" withString:(vaccination.note ? vaccination.note : @"-")].mutableCopy;
	}
	return vaccinationString;
}

+ (NSString*)getHTMLFromDiseases:(NSArray*)diseases
{
	NSMutableString *diseaseString = @"".mutableCopy;
	for (DiseaseHistory *disease in diseases)
	{
		if ([disease.status boolValue])
		{
			NSString *diseaseStringTemp = [PATIENT_DISEASE_ROW stringByReplacingOccurrencesOfString:@"[$DISEASE$]" withString:(disease.name ? disease.name : @"-")];
			diseaseString = [diseaseString stringByAppendingString:diseaseStringTemp].mutableCopy;
		}
	}
	return diseaseString;
}

+ (NSString*)getHTMLFromOtherRisks:(NSArray*)otherRisks
{
	NSMutableString *otherRiskString = @"".mutableCopy;
	for (OtherRiskFactor *otherRisk in otherRisks)
	{
		if ([otherRisk.status boolValue])
		{
			NSString *otherRiskTemp = [OTHER_RISK_ROW stringByReplacingOccurrencesOfString:@"[$OTHER_RISK$]" withString:(otherRisk.name ? otherRisk.name : @"-")];
			otherRiskString = [otherRiskString stringByAppendingString:otherRiskTemp].mutableCopy;
		}
	}
	return otherRiskString;
}

+ (NSString*)getHTMLFromOperations:(NSArray*)operations
{
	NSMutableString *operationString = @"".mutableCopy;
	for (Operation *operation in operations)
	{
		operationString = OPERATION_ROW.mutableCopy;
		operationString = [operationString stringByReplacingOccurrencesOfString:@"[$OPERATION$]" withString:(operation.name ? operation.name : @"-")].mutableCopy;
		operationString = [operationString stringByReplacingOccurrencesOfString:@"[$NOTE$]" withString:(operation.note ? operation.note : @"-")].mutableCopy;
	}
	return operationString;
}

+ (NSString*)getHTMLFromSmokingFrequency:(NSString*)frequency
{
	if ([frequency isEqualToString:@"1"])
		return @"1 to 5";
	else if ([frequency isEqualToString:@"2"])
		return @"5 to 15";
	else if ([frequency isEqualToString:@"3"])
		return @"More than 20";
	else
		return @"";
}

#pragma mark - Prescription methods

+ (NSString*)getPrescriptionPDFWithTitle:(NSString*)title prescription:(Prescription*)prescription
{
	// get PDF data from HTML string using UIPrintPageRenderer
	NSData *pdfData = [PDFExporter getPDFDataWithHTML:[PDFExporter generatePrescriptionHTML:prescription title:title]];
	
	// save PDF data to a file
	NSString *documentsDirectory = [PDFExporter getDocumentsDirectory];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"Prescription.pdf"];
	if ([[NSFileManager defaultManager] fileExistsAtPath:path])
		[[NSFileManager defaultManager] removeItemAtPath:path error:nil];
	[pdfData writeToFile: path atomically: YES];
	
	return path;
}

+ (NSString*)generatePrescriptionHTML:(Prescription*)prescription title:(NSString*)title
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
	[formatter setDateFormat:@"dd/MM/yyyy"];

	
	NSMutableString *string = @"".mutableCopy;
	NSString *path = [[NSBundle mainBundle] pathForResource:@"prescription_template" ofType:@"html"];
	NSString* template = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
	
	// Title
	string = [template stringByReplacingOccurrencesOfString:@"[$HTML_TITLE$]" withString:title].mutableCopy;
	
	// Prescription title, date and details
	string = [string stringByReplacingOccurrencesOfString:@"[$PRESCRIPTION_TITLE$]" withString:(prescription.title ? prescription.title : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$PRESCRIPTION_DATE$]" withString:(prescription.startDate ? [formatter stringFromDate: prescription.startDate] : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$PRESCRIPTION_DETAILS$]" withString:(prescription.details ? prescription.details : @"-")].mutableCopy;
	
	// Drugs
	NSArray *drugs = [[DBManager getIstance] getTherapiesFromPrescription:prescription];
	NSString *drugString = [PDFExporter getHTMLFromDrugs:drugs];
	string = [string stringByReplacingOccurrencesOfString:@"[$DRUG$]" withString:drugString].mutableCopy;
	
	// Diagnostic tests
	NSArray *tests = [prescription.tests allObjects];
	NSString *testString = [PDFExporter getHTMLFromTests:tests];
	string = [string stringByReplacingOccurrencesOfString:@"[$TEST$]" withString:testString].mutableCopy;
	
	// Phisical prescriptions, referral, note
	string = [string stringByReplacingOccurrencesOfString:@"[$PHISICAL_PRESCRIPTIONS$]" withString:(prescription.physicalPrescription ? prescription.physicalPrescription : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$REFERRAL$]" withString:(prescription.referral ? prescription.referral : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$NOTE$]" withString:(prescription.referralNote ? prescription.referralNote : @"-")].mutableCopy;
	
	return string;	
}

#pragma mark - PatientData methods

+ (NSString*)getPatientDataPDFWithTitle:(NSString*)title patient:(Patient*)patient status:(NSString*)status
{
	// get PDF data from HTML string using UIPrintPageRenderer
	NSData *pdfData = [PDFExporter getPDFDataWithHTML:[PDFExporter generatePatientDataHTML:patient title:title status:status]];
	
	// save PDF data to a file
	NSString *documentsDirectory = [PDFExporter getDocumentsDirectory];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"PatientData.pdf"];
	if ([[NSFileManager defaultManager] fileExistsAtPath:path])
		[[NSFileManager defaultManager] removeItemAtPath:path error:nil];
	[pdfData writeToFile: path atomically: YES];
	
	return path;
}

+ (NSString*)generatePatientDataHTML:(Patient*)patient title:(NSString*)title status:(NSString*)status
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
	[formatter setDateFormat:@"dd/MM/yyyy"];
	
	
	NSMutableString *string = @"".mutableCopy;
	NSString *path = [[NSBundle mainBundle] pathForResource:@"patientdata_template" ofType:@"html"];
	NSString* template = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
	
	// Title
	string = [template stringByReplacingOccurrencesOfString:@"[$HTML_TITLE$]" withString:title].mutableCopy;
	
	// Patient category, patient status
	string = [string stringByReplacingOccurrencesOfString:@"[$PATIENT_CATEGORY$]" withString:(patient.category ? patient.category : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$PATIENT_STATUS$]" withString:status].mutableCopy;

	
	// Health code, Insurance type, Health note
	string = [string stringByReplacingOccurrencesOfString:@"[$HEALTH_CODE$]" withString:(patient.patientData.healthCode ? patient.patientData.healthCode : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$INSURANCE_TYPE$]" withString:(patient.patientData.insuranceType ? patient.patientData.insuranceType : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$HEALTH_NOTE$]" withString:(patient.patientData.insuranceNote ? patient.patientData.insuranceNote : @"-")].mutableCopy;

	// Patient info
	string = [string stringByReplacingOccurrencesOfString:@"[$TITLE$]" withString:(patient.patientData.title ? patient.patientData.title : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$NAME$]" withString:(patient.patientData.name ? patient.patientData.name : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$MIDDLE$]" withString:(patient.patientData.middleName ? patient.patientData.middleName : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$LAST$]" withString:(patient.patientData.lastName ? patient.patientData.lastName : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$BIRTH_DATE$]" withString:(patient.patientData.birthDate ? [formatter stringFromDate: patient.patientData.birthDate] : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$AGE$]" withString:(patient.patientData.birthDate ? [patient.patientData.birthDate getAge] : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$SEX$]" withString:(patient.patientData.sex ? patient.patientData.sex : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$BIRTH_CITY$]" withString:(patient.patientData.birthCity ? patient.patientData.birthCity : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$FISCAL_CODE$]" withString:(patient.patientData.fiscalCode ? patient.patientData.fiscalCode : @"-")].mutableCopy;
	
	// Education, work
	string = [string stringByReplacingOccurrencesOfString:@"[$LEVEL_EDUCATION$]" withString:(patient.patientData.levelEducation ? patient.patientData.levelEducation : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$PREVIOUS_WORK$]" withString:(patient.patientData.previousWork ? patient.patientData.previousWork : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$ACTUAL_WORK$]" withString:(patient.patientData.actualWork ? patient.patientData.actualWork : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$WORK_NOTE$]" withString:(patient.patientData.workNote ? patient.patientData.workNote : @"-")].mutableCopy;

	// Contacts
	NSArray *telephoneContacts = [[patient.patientData phoneContacts] allObjects];
	NSString *telephoneContactsString = [PDFExporter getHTMLFromTelephoneContacts:telephoneContacts];
	string = [string stringByReplacingOccurrencesOfString:@"[$CONTACT_TELEPHONE$]" withString:telephoneContactsString].mutableCopy;
	NSArray *emailContacts = [[patient.patientData emailContacts] allObjects];
	NSString *emailContactsString = [PDFExporter getHTMLFromEmailContacts:emailContacts];
	string = [string stringByReplacingOccurrencesOfString:@"[$CONTACT_EMAIL$]" withString:emailContactsString].mutableCopy;

	// Patient address
	string = [string stringByReplacingOccurrencesOfString:@"[$ADDRESS$]" withString:(patient.patientData.address ? patient.patientData.address : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$CITY$]" withString:(patient.patientData.city ? patient.patientData.city : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$STATE$]" withString:(patient.patientData.state ? patient.patientData.state : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$ZIP_CODE$]" withString:(patient.patientData.zipCode ? patient.patientData.zipCode : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$COUNTRY$]" withString:(patient.patientData.country ? patient.patientData.country : @"-")].mutableCopy;

	
	return string;
}

#pragma mark - Visit methods

+ (NSString*)getVisitPDFWithTitle:(NSString*)title visit:(Visit*)visit patient:(Patient*)patient
{
	// get PDF data from HTML string using UIPrintPageRenderer
	NSData *pdfData = [PDFExporter getPDFDataWithHTML:[PDFExporter generateVisitHTML:visit title:title patient:patient]];
	
	// save PDF data to a file
	NSString *documentsDirectory = [PDFExporter getDocumentsDirectory];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"Visit.pdf"];
	if ([[NSFileManager defaultManager] fileExistsAtPath:path])
		[[NSFileManager defaultManager] removeItemAtPath:path error:nil];
	[pdfData writeToFile: path atomically: YES];
	
	return path;

}

+ (NSString*)generateVisitHTML:(Visit*)visit title:(NSString*)title patient:(Patient*)patient
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
	[formatter setDateFormat:@"dd/MM/yyyy"];
	
	
	NSMutableString *string = @"".mutableCopy;
	NSString *path = [[NSBundle mainBundle] pathForResource:@"visit_template" ofType:@"html"];
	NSString* template = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
	
	// Title
	string = [template stringByReplacingOccurrencesOfString:@"[$HTML_TITLE$]" withString:title].mutableCopy;
	
	// Visit date, visit type, visit note
	string = [string stringByReplacingOccurrencesOfString:@"[$VISIT_DATE$]" withString:(visit.date ? [formatter stringFromDate: visit.date] : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$VISIT_TYPE$]" withString:(visit.type ? visit.type : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$VISIT_NOTE$]" withString:(visit.note ? visit.note : @"-")].mutableCopy;

	// Measurements
	NSArray *measurements = [[DBManager getIstance] getMeasurementsSortedWithIndexForVisit:visit];
	NSString *measurementString = [PDFExporter getHTMLFromMeasurements:measurements];
	string = [string stringByReplacingOccurrencesOfString:@"[$CURRENT_MEASUREMENT$]" withString:measurementString].mutableCopy;
	NSString *prevMeasurementString = [PDFExporter getHTMLFromPreviousMeasurementsForPatient:patient currentMeasurements:measurements];
	string = [string stringByReplacingOccurrencesOfString:@"[$PREVIOUS_MEASUREMENT$]" withString:prevMeasurementString].mutableCopy;
	
	// BMI Index
	string = [string stringByReplacingOccurrencesOfString:@"[$BMI_INDEX$]" withString:[PDFExporter getBMIIndexForVisit:visit]].mutableCopy;

	// Symptoms
	NSArray *symptoms = [visit.symptoms allObjects];
	NSString *symptomString = [PDFExporter getHTMLFromSymptoms:symptoms];
	string = [string stringByReplacingOccurrencesOfString:@"[$SYMPTOM$]" withString:symptomString].mutableCopy;

	// Objective examination
	string = [string stringByReplacingOccurrencesOfString:@"[$OBJECTIVE_EXAMINATION$]" withString:(visit.objectiveExamination ? visit.objectiveExamination : @"-")].mutableCopy;

	// Drugs
	NSArray *drugs = [[DBManager getIstance] getTherapiesFromVisit:visit];
	NSString *drugString = [PDFExporter getHTMLFromDrugs:drugs];
	string = [string stringByReplacingOccurrencesOfString:@"[$DRUG$]" withString:drugString].mutableCopy;
	
	// Final considerations
	string = [string stringByReplacingOccurrencesOfString:@"[$FINAL_CONSIDERATIONS$]" withString:(visit.finalConsideration ? visit.finalConsideration : @"-")].mutableCopy;

	// Diagnostic tests
	NSArray *tests = [visit.tests allObjects];
	NSString *testString = [PDFExporter getHTMLFromTests:tests];
	string = [string stringByReplacingOccurrencesOfString:@"[$TEST$]" withString:testString].mutableCopy;
	
	return string;
}

+ (NSString*)getBMIIndexForVisit:(Visit*)visit
{
	Measurement *weightMeasurement = [[DBManager getIstance] getMeasurementWithName:@"Weight" visit:visit];
	Measurement *heightMeasurement = [[DBManager getIstance] getMeasurementWithName:@"Height" visit:visit];
	if ([BaseViewController isEmptyNumber:weightMeasurement.value] || [BaseViewController isEmptyNumber:heightMeasurement.value])
		return @"ND";
	
	float bmiIndex = [[PDFExporter getBMIIndexWeight:weightMeasurement height:heightMeasurement] floatValue];
	if (bmiIndex != 0)
		return [NSString stringWithFormat:@"%i", (int)bmiIndex];
	else
		return @"ND";
}

+ (NSNumber*)getBMIIndexWeight:(Measurement*)weightMeasurement height:(Measurement*)heightMeasurement
{
	NSNumber *bmiIndex;
	float weight = [weightMeasurement.value floatValue];
	float height = [heightMeasurement.value floatValue]/100;
	if (weight != 0 && height != 0)
	{
		float powerHeight = powf(height, 2);
		float index = weight / powerHeight;
		if (index < 15)
			index = 15;
		else if (index > 40)
			index = 40;
		bmiIndex = [NSNumber numberWithFloat:index];
	}
	else
		bmiIndex = [NSNumber numberWithFloat:0];
	
	return bmiIndex;
}

#pragma mark - Remote Anamnesis

+ (NSString*)getRemoteAnamnesisPDFWithTitle:(NSString*)title remoteAnamnesis:(RemoteAnamnesis*)remoteAnamnesis diseases:(NSArray*)diseases
{
	// get PDF data from HTML string using UIPrintPageRenderer
	NSData *pdfData = [PDFExporter getPDFDataWithHTML:[PDFExporter generateRemoteAnamnesisHTML:remoteAnamnesis title:title diseases:diseases]];
	
	// save PDF data to a file
	NSString *documentsDirectory = [PDFExporter getDocumentsDirectory];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"RemoteAnamnesis.pdf"];
	if ([[NSFileManager defaultManager] fileExistsAtPath:path])
		[[NSFileManager defaultManager] removeItemAtPath:path error:nil];
	[pdfData writeToFile: path atomically: YES];
	
	return path;
}

+ (NSString*)generateRemoteAnamnesisHTML:(RemoteAnamnesis*)remoteAnamnesis title:(NSString*)title diseases:(NSArray*)diseases
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
	[formatter setDateFormat:@"dd/MM/yyyy"];
	
	
	NSMutableString *string = @"".mutableCopy;
	NSString *path = [[NSBundle mainBundle] pathForResource:@"remoteanamnesis_template" ofType:@"html"];
	NSString* template = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
	
	// Title
	string = [template stringByReplacingOccurrencesOfString:@"[$HTML_TITLE$]" withString:title].mutableCopy;
	
	// Delivery
	string = [string stringByReplacingOccurrencesOfString:@"[$DELIVERY_TYPE$]" withString:(remoteAnamnesis.delivery ? remoteAnamnesis.delivery : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$INSURANCE_TYPE$]" withString:(remoteAnamnesis.deliveryNote ? remoteAnamnesis.deliveryNote : @"-")].mutableCopy;
	
	// Congenital diseases
	string = [string stringByReplacingOccurrencesOfString:@"[$LIFE_BIRTHS$]" withString:(remoteAnamnesis.lifeBirds ? [remoteAnamnesis.lifeBirds stringValue] : @"-")].mutableCopy;
	
	// Father disease history
	NSArray *fatherDiseases = [remoteAnamnesis.fatherDiseases allObjects];
	NSString *fatherString = [PDFExporter getHTMLFromFatherDiseaseHistory:fatherDiseases];
	string = [string stringByReplacingOccurrencesOfString:@"[$FATHER_DISEASE_HISTORY$]" withString:fatherString].mutableCopy;
	
	// Mother disease history
	NSArray *motherDiseases = [remoteAnamnesis.motherDiseases allObjects];
	NSString *motherString = [PDFExporter getHTMLFromMotherDiseaseHistory:motherDiseases];
	string = [string stringByReplacingOccurrencesOfString:@"[$MOTHER_DISEASE_HISTORY$]" withString:motherString].mutableCopy;

	// Vaccinations
	NSArray *vaccinations = [remoteAnamnesis.vaccinations allObjects];
	NSString *vaccinationString = [PDFExporter getHTMLFromVaccinations:vaccinations];
	string = [string stringByReplacingOccurrencesOfString:@"[$VACCINATION$]" withString:vaccinationString].mutableCopy;
	
	// Patient disease history
	NSString *diseaseString = [PDFExporter getHTMLFromDiseases:diseases];
	string = [string stringByReplacingOccurrencesOfString:@"[$DISEASES$]" withString:diseaseString].mutableCopy;
	
	// Surgical operations
	NSArray *operations = [remoteAnamnesis.operations allObjects];
	NSString *operationString = [PDFExporter getHTMLFromOperations:operations];
	string = [string stringByReplacingOccurrencesOfString:@"[$OPERATION$]" withString:operationString].mutableCopy;

	// Note
	string = [string stringByReplacingOccurrencesOfString:@"[$NOTE$]" withString:(remoteAnamnesis.note ? remoteAnamnesis.note : @"-")].mutableCopy;

	return string;
}

#pragma mark - Proximal Anamnesis

+ (NSString*)getProximalAnamnesisPDFWithTitle:(NSString*)title proximalAnamnesis:(ProximalAnamnesis*)proximalAnamnesis patient:(Patient*)patient
{
	// get PDF data from HTML string using UIPrintPageRenderer
	NSData *pdfData = [PDFExporter getPDFDataWithHTML:[PDFExporter generateProximalAnamnesisHTML:proximalAnamnesis title:title patient:patient]];
	
	// save PDF data to a file
	NSString *documentsDirectory = [PDFExporter getDocumentsDirectory];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"ProximalAnamnesis.pdf"];
	if ([[NSFileManager defaultManager] fileExistsAtPath:path])
		[[NSFileManager defaultManager] removeItemAtPath:path error:nil];
	[pdfData writeToFile: path atomically: YES];
	
	return path;
}

+ (NSString*)generateProximalAnamnesisHTML:(ProximalAnamnesis*)proximalAnamnesis title:(NSString*)title patient:(Patient*)patient
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
	[formatter setDateFormat:@"dd/MM/yyyy"];
	
	
	NSMutableString *string = @"".mutableCopy;
	NSString *path = [[NSBundle mainBundle] pathForResource:@"proximalanamnesis_template" ofType:@"html"];
	NSString* template = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
	
	// Title
	string = [template stringByReplacingOccurrencesOfString:@"[$HTML_TITLE$]" withString:title].mutableCopy;
	
	// Onset date, onset place, Symptom description
	string = [string stringByReplacingOccurrencesOfString:@"[$ONSET_DATE$]" withString:(proximalAnamnesis.onSetDate ? proximalAnamnesis.onSetDate : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$ONSET_PLACE$]" withString:(proximalAnamnesis.onSetPlace ? proximalAnamnesis.onSetPlace : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$SYMPTOM_DESCRIPTION$]" withString:(proximalAnamnesis.symptomsDescription ? proximalAnamnesis.symptomsDescription : @"-")].mutableCopy;

	// Drugs
	NSArray *drugs = [[DBManager getIstance] getTherapiesFromPatient:patient suspended:NO prescribed:YES active:NO];
	NSString *drugString = [PDFExporter getHTMLFromDrugs:drugs];
	string = [string stringByReplacingOccurrencesOfString:@"[$DRUG$]" withString:drugString].mutableCopy;
	
	// Previous clinical visit
	string = [string stringByReplacingOccurrencesOfString:@"[$PREVIOUS_CLINICAL_VISIT$]" withString:(proximalAnamnesis.previousClinicalVisit ? proximalAnamnesis.previousClinicalVisit : @"-")].mutableCopy;
	
	// Previous hospitalization
	string = [string stringByReplacingOccurrencesOfString:@"[$PREVIOUS_HOSPITALIZATION$]" withString:(proximalAnamnesis.previousHospitalization ? proximalAnamnesis.previousHospitalization : @"-")].mutableCopy;

	// Recent diagnostic tests
	NSArray *tests = [proximalAnamnesis.tests allObjects];
	NSString *testString = [PDFExporter getHTMLFromTests:tests];
	string = [string stringByReplacingOccurrencesOfString:@"[$TEST$]" withString:testString].mutableCopy;
	
	// Test note
	string = [string stringByReplacingOccurrencesOfString:@"[$TEST_NOTE$]" withString:(proximalAnamnesis.note ? proximalAnamnesis.note : @"-")].mutableCopy;

	// Symptoms
	NSArray *symptoms = [proximalAnamnesis.symptomsHistory allObjects];
	NSString *symptomString = [PDFExporter getHTMLFromSymptoms:symptoms];
	string = [string stringByReplacingOccurrencesOfString:@"[$SYMPTOM$]" withString:symptomString].mutableCopy;
	
	return string;
}

#pragma mark - Actual Risk Factor

+ (NSString*)getActualRiskFactorPDFWithTitle:(NSString*)title actualRiskFactor:(ActualRiskFactor*)actualRiskFactor
{
	// get PDF data from HTML string using UIPrintPageRenderer
	NSData *pdfData = [PDFExporter getPDFDataWithHTML:[PDFExporter generateActualRiskFactorHTML:actualRiskFactor title:title]];
	
	// save PDF data to a file
	NSString *documentsDirectory = [PDFExporter getDocumentsDirectory];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"ActualRiskFactor.pdf"];
	if ([[NSFileManager defaultManager] fileExistsAtPath:path])
		[[NSFileManager defaultManager] removeItemAtPath:path error:nil];
	[pdfData writeToFile: path atomically: YES];
	
	return path;
}

+ (NSString*)generateActualRiskFactorHTML:(ActualRiskFactor*)actualRiskFactor title:(NSString*)title
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
	[formatter setDateFormat:@"dd/MM/yyyy"];
	
	
	NSMutableString *string = @"".mutableCopy;
	NSString *path = [[NSBundle mainBundle] pathForResource:@"actualriskfactors_template" ofType:@"html"];
	NSString* template = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
	
	// Title
	string = [template stringByReplacingOccurrencesOfString:@"[$HTML_TITLE$]" withString:title].mutableCopy;
	
	// Smoking
	string = [string stringByReplacingOccurrencesOfString:@"[$SMOKING$]" withString:(actualRiskFactor.smoking ? actualRiskFactor.smoking : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$HOW_MUCH$]" withString:[PDFExporter getHTMLFromSmokingFrequency:actualRiskFactor.smokingFrequency]].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$FROM_HOW_LONG$]" withString:(actualRiskFactor.smokingTime ? actualRiskFactor.smokingTime : @"-")].mutableCopy;
	string = [string stringByReplacingOccurrencesOfString:@"[$SMOKING_NOTE$]" withString:(actualRiskFactor.smokingNote ? actualRiskFactor.smokingNote : @"-")].mutableCopy;
	
	// Other risk
	NSString *otherRiskString = [PDFExporter getHTMLFromOtherRisks:[actualRiskFactor.otherRiskFactors allObjects]];
	string = [string stringByReplacingOccurrencesOfString:@"[$OTHER_RISK_FACTORS$]" withString:otherRiskString].mutableCopy;
	
	// Note
	string = [string stringByReplacingOccurrencesOfString:@"[$NOTE$]" withString:(actualRiskFactor.otherRiskFactorNote ? actualRiskFactor.otherRiskFactorNote : @"-")].mutableCopy;

	// Allergies
	NSArray *allergies = [actualRiskFactor.allergies allObjects];
	NSString *allergyString = [PDFExporter getHTMLFromAllergies:allergies];
	string = [string stringByReplacingOccurrencesOfString:@"[$ALLERGY$]" withString:allergyString].mutableCopy;
	
	// Special drugs
	string = [string stringByReplacingOccurrencesOfString:@"[$SPECIAL_DRUGS$]" withString:(actualRiskFactor.specialDrugs ? actualRiskFactor.specialDrugs : @"-")].mutableCopy;
	
	// Other
	string = [string stringByReplacingOccurrencesOfString:@"[$OTHER$]" withString:(actualRiskFactor.other ? actualRiskFactor.other : @"-")].mutableCopy;
	
	return string;
}


@end
