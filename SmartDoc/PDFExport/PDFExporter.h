//
//  PDFExporter.h
//  SmartDoc
//
//  Created by Francesca Corsini on 05/09/15.
//  Copyright (c) 2015 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Prescription;
@class Patient;
@class Visit;
@class RemoteAnamnesis;
@class ProximalAnamnesis;
@class ActualRiskFactor;

@interface PDFExporter : NSObject

+ (void)clearDocuments;
+ (NSString*)getPrescriptionPDFWithTitle:(NSString*)title prescription:(Prescription*)prescription;
+ (NSString*)getPatientDataPDFWithTitle:(NSString*)title patient:(Patient*)patient status:(NSString*)status;
+ (NSString*)getVisitPDFWithTitle:(NSString*)title visit:(Visit*)visit patient:(Patient*)patient;
+ (NSString*)getRemoteAnamnesisPDFWithTitle:(NSString*)title remoteAnamnesis:(RemoteAnamnesis*)remoteAnamnesis diseases:(NSArray*)diseases;
+ (NSString*)getProximalAnamnesisPDFWithTitle:(NSString*)title proximalAnamnesis:(ProximalAnamnesis*)proximalAnamnesis patient:(Patient*)patient;
+ (NSString*)getActualRiskFactorPDFWithTitle:(NSString*)title actualRiskFactor:(ActualRiskFactor*)actualRiskFactor;


@end
