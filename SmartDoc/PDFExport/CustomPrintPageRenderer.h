//
//  CustomPrintPageRenderer.h
//  SmartDoc
//
//  Created by Francesca Corsini on 30/03/16.
//  Copyright © 2016 Francesca Corsini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPrintPageRenderer : UIPrintPageRenderer

@end
