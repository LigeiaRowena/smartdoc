//
//  CustomPrintPageRenderer.m
//  SmartDoc
//
//  Created by Francesca Corsini on 30/03/16.
//  Copyright © 2016 Francesca Corsini. All rights reserved.
//

#import "CustomPrintPageRenderer.h"

@implementation CustomPrintPageRenderer

- (CGRect)paperRect
{
	// a4 size
	return CGRectMake(0, 0, 595.2,841.8);
}

- (CGRect)printableRect
{
	return CGRectInset(self.paperRect, 0, 0);
}


@end
