//
//  CalendarManager.m
//  SmartDoc
//
//  Created by Francesca Corsini on 05/07/15.
//  Copyright (c) 2015 Francesca Corsini. All rights reserved.
//

#import "CalendarManager.h"
#import "Patient.h"
#import "Dashboard.h"
#import "Action.h"
#import "PatientData.h"

@interface CalendarManager()
{
	EKEventStore *eventStore;
	EKCalendar *defaultCalendar;
}
@end

@implementation CalendarManager

#pragma mark - Init

+ (CalendarManager*)getIstance
{
	static dispatch_once_t onceToken = 0;
	__strong static id _sharedObject = nil;
	dispatch_once(&onceToken, ^{
		_sharedObject = [[self alloc] init];
	});
	return _sharedObject;
}

- (id)init
{
	self = [super init];
	if (self) {
		eventStore = [[EKEventStore alloc] init];
		
		[eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError * _Nullable error) {
			if (granted)
				defaultCalendar = [eventStore defaultCalendarForNewEvents];
			else
				NSLog(@"Request access to calendar failed: %@", error);
		}];
	}
	return self;
}

#pragma mark - EventKit

- (void)addEvent:(NSString*)eventInfo patient:(NSString*)patient date:(NSDate*)date confirmBlock:(CalendarManagerBlocksConfirm)confirm cancelBlock:(CalendarManagerBlocksError)error
{
	confirmBlock = [confirm copy];
	errorBlock = [error copy];
	
	[eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
	 {
		 if (granted)
		 {
			 EKEvent *event = [EKEvent eventWithEventStore:eventStore];
			 event.title = [NSString stringWithFormat:@"Appointment type: %@ Patient: %@", eventInfo, patient];
			 event.allDay = NO;
			 event.startDate = date;
			 int duration = [[[[DBManager getIstance] getSettings] appointmentDuration] intValue];
			 event.endDate = [event.startDate dateByAddingTimeInterval:duration*60];
			 [event setCalendar:defaultCalendar];
			 
			 NSError *err = nil;
			 BOOL result = [eventStore saveEvent:event span:EKSpanThisEvent error:&err];
			 if (result && !err)
			 {
				 if (confirmBlock != nil)
					 confirmBlock(event.eventIdentifier);
			 }
			 else
			 {
				 if (errorBlock != nil)
					 errorBlock(err);
			 }
		 }
		 else
		 {
			 if (errorBlock != nil)
				 errorBlock(error);
		 }
	 }];
}

- (void)editEvent:(NSString*)eventInfo patient:(NSString*)patient date:(NSDate*)date eventIdentifier:(NSString*)eventIdentifier confirmBlock:(CalendarManagerBlocksConfirm)confirm cancelBlock:(CalendarManagerBlocksError)error
{
	confirmBlock = [confirm copy];
	errorBlock = [error copy];
	
	[eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
		if (granted)
		{
			EKEvent *event = [eventStore eventWithIdentifier:eventIdentifier];
			if (event == nil)
				event = [EKEvent eventWithEventStore:eventStore];
			if (event)
			{
				NSError *err = nil;
				event.title = [NSString stringWithFormat:@"Appointment type: %@ Patient: %@", eventInfo, patient];
				event.startDate = date;
				event.endDate = [event.startDate dateByAddingTimeInterval:3600];
				BOOL result = [eventStore saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
				if (result && !err)
				{
				 if (confirmBlock != nil)
					 confirmBlock(event.eventIdentifier);
				}
				else
				{
					if (errorBlock != nil)
						errorBlock(err);
				}
			}
		}
		else
		{
			if (errorBlock != nil)
				errorBlock(error);
		}
	}];
}

- (void)deleteEvent:(NSString*)eventIdentifier confirmBlock:(CalendarManagerBlocksConfirm)confirm cancelBlock:(CalendarManagerBlocksError)error
{
	confirmBlock = [confirm copy];
	errorBlock = [error copy];
	
	[eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
		if (granted)
		{
			EKEvent* eventToRemove = [eventStore eventWithIdentifier:eventIdentifier];
			if (eventToRemove)
			{
				NSError* err = nil;
				BOOL result = [eventStore removeEvent:eventToRemove span:EKSpanThisEvent commit:YES error:&err];
				if (result && !err)
				{
				 if (confirmBlock != nil)
					 confirmBlock(eventToRemove.eventIdentifier);
				}
				else
				{
					if (errorBlock != nil)
						errorBlock(err);
				}
			}
		}
		else
		{
			if (errorBlock != nil)
				errorBlock(error);
		}
	}];
}

- (void)updateCalendarConfirmBlock:(CalendarManagerBlocksConfirm)confirm cancelBlock:(CalendarManagerBlocksError)error
{
	confirmBlock = [confirm copy];
	errorBlock = [error copy];

	NSArray *patients = [[DBManager getIstance] getPatientsFavourites:NO];
	[patients enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		Patient *patient = (Patient*)obj;
		NSArray *actions = [[patient.dashboard actions] allObjects];
		[actions enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
			Action *action = (Action*)obj;
			if ([action.type isEqualToString:ActionAppointment])
			{
				[eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
					if (granted)
					{
						EKEvent *event = [eventStore eventWithIdentifier:action.eventIdentifier];
						if (event == nil)
						{
							NSString *patientName = [NSString stringWithFormat:@"%@ %@ %@", patient.patientData.name, patient.patientData.middleName, patient.patientData.lastName];
							event = [EKEvent eventWithEventStore:eventStore];
							event.title = [NSString stringWithFormat:@"Appointment type: %@ Patient: %@", action.desc, patientName];
							event.allDay = YES;
							event.startDate = action.date;
							event.endDate = [event.startDate dateByAddingTimeInterval:3600];
							[event setCalendar:defaultCalendar];
							
							NSError *err = nil;
							BOOL result = [eventStore saveEvent:event span:EKSpanThisEvent error:&err];
							if (result && !err)
							{
								if (confirmBlock != nil)
									confirmBlock(nil);
							}
							else
							{
								if (errorBlock != nil)
									errorBlock(err);
							}
						}
					}
					else
					{
						if (errorBlock != nil)
							errorBlock(error);
					}
				}];
			}
		}];
	}];
}

@end
