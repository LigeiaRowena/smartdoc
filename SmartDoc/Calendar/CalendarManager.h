//
//  CalendarManager.h
//  SmartDoc
//
//  Created by Francesca Corsini on 05/07/15.
//  Copyright (c) 2015 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EventKit/EventKit.h>

typedef void(^CalendarManagerBlocksConfirm)(NSString*);
typedef void(^CalendarManagerBlocksError)(NSError*);

@interface CalendarManager : NSObject
{
	CalendarManagerBlocksConfirm confirmBlock;
	CalendarManagerBlocksError errorBlock;
}

+ (CalendarManager*)getIstance;

- (void)addEvent:(NSString*)eventInfo patient:(NSString*)patient date:(NSDate*)date confirmBlock:(CalendarManagerBlocksConfirm)confirm cancelBlock:(CalendarManagerBlocksError)error;
- (void)editEvent:(NSString*)eventInfo patient:(NSString*)patient date:(NSDate*)date eventIdentifier:(NSString*)eventIdentifier confirmBlock:(CalendarManagerBlocksConfirm)confirm cancelBlock:(CalendarManagerBlocksError)error;
- (void)deleteEvent:(NSString*)eventIdentifier confirmBlock:(CalendarManagerBlocksConfirm)confirm cancelBlock:(CalendarManagerBlocksError)error;
- (void)updateCalendarConfirmBlock:(CalendarManagerBlocksConfirm)confirm cancelBlock:(CalendarManagerBlocksError)error;

@end
