//
//  Macros.h
//  RadioRai
//
//  Created by Francesca Corsini on 18/03/14.
//
//

#import <UIKit/UIKit.h>

#pragma mark - Constants

static NSString * const DateField = @"Date";
static NSString * const EditableField = @"Editable";
static NSString * const StaticField = @"Static";
static NSString * const DrugNameField = @"DrugName";
static NSString * const DrugUnitField = @"DrugUnit";
static NSString * const DrugFrequencyField = @"DrugFrequency";
static NSString * const DrugRouteField = @"DrugRoute";
static NSString * const SymptomTypeField = @"SymptomTypeField";
static NSString * const SymptomDetailsField = @"SymptomDetails";
static NSString * const SymptomNoteField = @"SymptomNote";
static NSString * const ActionTodo = @"ActionTodo";
static NSString * const ActionNote = @"ActionNote";
static NSString * const ActionAppointment = @"ActionAppointment";
static NSString * const QUESTIONS_PLIST = @"Questions.plist";
static NSString * const PLIST_FILE = @"DBSetStatic.plist";
static NSString * const NOMI_COMMERCIALI = @"NomiCommerciali.plist";
static NSString * const PRINCIPI_ATTIVI = @"PrincipiAttivi.plist";
static NSString * const REQUEST = @"Request.plist";
static NSString * const LOCAL_SETTINGS = @"LocalSettings.plist";
static NSString * const REQUEST_BASEPATH = @"basepath";
static NSString * const REQUEST_LOGIN = @"login";
static NSString * const REQUEST_SENDCODE = @"sendActivationCode";
static NSString * const REQUEST_SENDPASSWORD = @"sendPassword";
static NSString * const REQUEST_REGISTER = @"register";
static NSString * const REQUEST_DRUGS= @"drugs";
static NSString * const REQUEST_UPLOAD = @"upload";
static NSString * const REQUEST_USER_FILES = @"userFiles";


#pragma mark - Colors

static inline UIColor* lightLightBlueColorBackground()
{
	return [UIColor colorWithRed:(249.0f/255.0f) green:(253.0f/255.0f) blue:(255.0f/255.0f) alpha:1.0f];
}

static inline UIColor* lightBlueColorBackground()
{
	return [UIColor colorWithRed:(79.0f/255.0f) green:(192.0f/255.0f) blue:(228.0f/255.0f) alpha:1.0f];
}

static inline UIColor* lightBlueColor()
{
	return [UIColor colorWithRed:(79.0f/255.0f) green:(193.0f/255.0f) blue:(233.0f/255.0f) alpha:1.0f];
}

static inline UIColor* bluNavBarColor()
{
	return [UIColor colorWithRed:(59.0f/255.0f) green:(175.0f/255.0f) blue:(218.0f/255.0f) alpha:1.0f];
}

static inline UIColor* tooltipGrayColor()
{
	return [UIColor colorWithRed:(67.0f/255.0f) green:(74.0f/255.0f) blue:(84.0f/255.0f) alpha:1.0f];
}

static inline UIColor* darkGrayBlueColor()
{
	return [UIColor colorWithRed:(34.0f/255.0f) green:(38.0f/255.0f) blue:(44.0f/255.0f) alpha:1.0f];
}

static inline UIColor* grayBlueColor()
{
	return [UIColor colorWithRed:(86.0f/255.0f) green:(93.0f/255.0f) blue:(103.0f/255.0f) alpha:1.0f];
}

static inline UIColor* lightGrayBlueColor()
{
	return [UIColor colorWithRed:(120.0f/255.0f) green:(128.0f/255.0f) blue:(139.0f/255.0f) alpha:1.0f];
}

static inline UIColor* lightRedColor()
{
	return [UIColor colorWithRed:(252.0f/255.0f) green:(110.0f/255.0f) blue:(81.0f/255.0f) alpha:1.0f];
}

static inline UIColor* normalRedColor()
{
	return [UIColor colorWithRed:(255.0f/255.0f) green:(60.0f/255.0f) blue:(48.0f/255.0f) alpha:1.0f];
}

static inline UIColor* greenBlueColor()
{
	return [UIColor colorWithRed:(72.0f/255.0f) green:(207.0f/255.0f) blue:(173.0f/255.0f) alpha:1.0f];
}

static inline UIColor* normalGreenColor()
{
	return [UIColor colorWithRed:(160.0f/255.0f) green:(212.0f/255.0f) blue:(104.0f/255.0f) alpha:1.0f];
}

static inline UIColor* lightPurpleColor()
{
	return [UIColor colorWithRed:(172.0f/255.0f) green:(146.0f/255.0f) blue:(236.0f/255.0f) alpha:1.0f];
}

static inline UIColor* fucsiaColor()
{
	return [UIColor colorWithRed:(237.0f/255.0f) green:(85.0f/255.0f) blue:(101.0f/255.0f) alpha:1.0f];
}

static inline UIColor* separatorGrayColor()
{
	return [UIColor colorWithRed:(180.0f/255.0f) green:(180.0f/255.0f) blue:(180.0f/255.0f) alpha:1.0f];
}

static inline UIColor* lightGrayBgColor()
{
	return [UIColor colorWithRed:(240.0f/255.0f) green:(242.0f/255.0f) blue:(245.0f/255.0f) alpha:1.0f];
}

static inline UIColor* borderGrayColor()
{
	return [UIColor colorWithRed:(196.0f/255.0f) green:(201.0f/255.0f) blue:(202.0f/255.0f) alpha:1.0f];
}
		
#pragma mark - Font

static inline UIFont* OpenSansBold(float size)
{
	return [UIFont fontWithName:@"OpenSans-Bold" size:size];
}

static inline UIFont* OpenSansRegular(float size)
{
	return [UIFont fontWithName:@"OpenSans" size:size];
}

static inline UIFont* OpenSansLight(float size)
{
	return [UIFont fontWithName:@"OpenSans-Light" size:size];
}

static inline UIFont* OpenSansSemibold(float size)
{
	return [UIFont fontWithName:@"OpenSans-Semibold" size:size];
}

#pragma mark - Interface orientation

static inline UIInterfaceOrientation getOrientation()
{
	if ([UIDevice currentDevice].orientation == UIDeviceOrientationPortrait || [UIDevice currentDevice].orientation == UIDeviceOrientationPortraitUpsideDown)
		return UIInterfaceOrientationPortrait;
	else if ([UIDevice currentDevice].orientation == UIDeviceOrientationLandscapeLeft)
		return UIInterfaceOrientationLandscapeRight;
	else if ([UIDevice currentDevice].orientation == UIDeviceOrientationLandscapeRight)
		return UIInterfaceOrientationLandscapeLeft;
	else
		return [[UIApplication sharedApplication] statusBarOrientation];
}

#pragma mark - iOS version

static inline BOOL iOS9()
{
	if ([[UIDevice currentDevice].systemVersion floatValue] >= 9)
		return YES;
	else
		return NO;
}

static inline BOOL iOS8()
{
	if ([[UIDevice currentDevice].systemVersion floatValue] >= 8)
		return YES;
	else
		return NO;
}

static inline BOOL iOS7()
{
	if ([[UIDevice currentDevice].systemVersion floatValue] >= 7)
		return YES;
	else
		return NO;
}

static inline BOOL systemVersionLessThan(NSString *version)
{
	if ([[[UIDevice currentDevice] systemVersion] compare:version options:NSNumericSearch] == NSOrderedAscending)
		return YES;
	else
		return NO;
}

#pragma mark - Device version

static inline BOOL iPad()
{
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		return YES;
	else
		return NO;
}

static inline BOOL iPhone()
{
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
		return YES;
	else
		return NO;
}

static inline NSString *mobilePrefix()
{
	if (iPad())
		return @"iPad";
	else
		return @"iPhone";
}

static inline BOOL iPhone5()
{
	if ([[UIScreen mainScreen] bounds].size.height == 568)
		return YES;
	else
		return NO;
}

#pragma mark - UILabel

static inline CGFloat textWidth(UILabel *label)
{
	return [label.text boundingRectWithSize:label.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:label.font} context:nil].size.width;
}

#pragma mark - NSNumberFormatter

static inline NSNumberFormatter *getDecimalFormatter(int digits)
{
	NSNumberFormatter *defaultFormatter = [[NSNumberFormatter alloc] init];
	defaultFormatter.locale = [NSLocale currentLocale];
	[defaultFormatter setFormatterBehavior:NSNumberFormatterBehaviorDefault];
	[defaultFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
	[defaultFormatter setRoundingMode:NSNumberFormatterRoundHalfUp];
	[defaultFormatter setMaximumFractionDigits:digits];
	[defaultFormatter setMinimumFractionDigits:digits];
	defaultFormatter.alwaysShowsDecimalSeparator = NO;
	return defaultFormatter;
}

#pragma mark - NSString

static inline BOOL stringContainsOccurenceOfString(NSString *string, NSString *occurence)
{
	NSRange range = [string rangeOfString:occurence options:NSCaseInsensitiveSearch];
	if (range.length > 0)
		return YES;
	else
		return NO;
}

