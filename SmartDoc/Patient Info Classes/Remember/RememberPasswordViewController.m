//
//  RememberPasswordViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 10/09/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "RememberPasswordViewController.h"

@interface RememberPasswordViewController ()

@property (nonatomic, weak) IBOutlet CustomTextField *username;

@end

@implementation RememberPasswordViewController

#pragma mark - Init RememberPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// set the background color
	self.view.backgroundColor = lightBlueColorBackground();
	self.mainscroll.backgroundColor = lightBlueColorBackground();
}

- (void)setup
{
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	return [super textFieldDidBeginEditing:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	[super textFieldDidEndEditing:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	return YES;
}

#pragma mark - Actions

- (IBAction)sendPassword:(id)sender
{
	if (![BaseViewController isEmptyString:self.username.text])
	{
		[AFRequests sendPassword:self.username.text success:^(NSHTTPURLResponse *response, id responseObject) {
			NSString *message = responseObject[@"MESSAGE"];
			[[AlertViewBlocks getIstance] informationAlertViewWithMessage:message confirmBlock:^{
			}];
		} failure:^(NSHTTPURLResponse *response, NSError *error) {
			NSString *message = error.userInfo[@"message"];
			[[AlertViewBlocks getIstance] warningAlertViewWithMessage:message confirmBlock:^{
			}];
		}];
	}
	else
		[[AlertViewBlocks getIstance] warningAlertViewWithMessage:@"Please insert your email" confirmBlock:^{
		}];
}

- (IBAction)back:(id)sender
{
	[self parent:self.parentViewController popViewController:self animateWithDuration:0.2];
}



@end
