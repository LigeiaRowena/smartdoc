//
//  GalleryViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 18/10/15.
//  Copyright © 2015 Francesca Corsini. All rights reserved.
//

#import "GalleryViewController.h"
#import "ThumbGalleryCollectionCell.h"
#import "DiagnosticImage.h"
#import "EditPhotoViewController.h"

@interface GalleryViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *images;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;
@property (nonatomic) BOOL selectMode;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *trashButton;

@end

@implementation GalleryViewController

#pragma mark - Init GalleryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.selectMode = NO;
	
	// images collection
	UINib *cellImage = [UINib nibWithNibName:@"ThumbGalleryCollectionCell" bundle:nil];
	[self.images registerNib:cellImage forCellWithReuseIdentifier:[ThumbGalleryCollectionCell reuseIdentifier]];
	self.images.allowsMultipleSelection = YES;
}

- (BOOL)prefersStatusBarHidden
{
	return YES;
}

- (void)setup
{
}

#pragma mark - Actions

- (IBAction)back:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)selectAction:(id)sender
{
	[self.selectButton setSelected:!self.selectMode];
	self.selectMode = !self.selectMode;
	
	self.trashButton.hidden = !self.selectMode;
	self.trashButton.enabled = self.selectMode;
	
	// reset all
	if (!self.selectMode)
		[self.images reloadData];
}

- (IBAction)trashAction:(id)sender
{
	NSArray *selectedThumbs = [self.images indexPathsForSelectedItems];
	for (NSIndexPath *indexPath in selectedThumbs)
	{
		DiagnosticImage *diagnosticImage = self.data[indexPath.row];
		if ([self.data containsObject:diagnosticImage])
			[self.data removeObject:diagnosticImage];
		[[DBManager getIstance] deleteObject:diagnosticImage];
	}
	[self.images reloadData];
}

#pragma mark - UICollectionViewDataSource

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
	return CGSizeMake(119, 119);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
	return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
	return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
	return 5;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
	return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return [self.data count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	ThumbGalleryCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[ThumbGalleryCollectionCell reuseIdentifier] forIndexPath:indexPath];
	cell.index = [NSNumber numberWithInt:(int)indexPath.row];
	DiagnosticImage *diagnosticImage = self.data[indexPath.row];
	[cell setThumbWithData:diagnosticImage.image];
	
	return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	
	// modalità normale
	if (!self.selectMode)
	{
		DiagnosticImage *diagnosticImage = self.data[indexPath.row];
		EditPhotoViewController *editPhotoViewController = [[EditPhotoViewController alloc] initWithNibName:@"EditPhotoViewController" bundle:nil];
		[self parent:self pushViewController:editPhotoViewController superView:self.view animateWithDuration:0.2];
		[editPhotoViewController savePhoto:[UIImage imageWithData:diagnosticImage.image] note:diagnosticImage.note completion:^(UIImage *savedImage) {
			DiagnosticImage *diagnosticImage = self.data[indexPath.row];
			NSData *data = UIImagePNGRepresentation(savedImage);
			diagnosticImage.image = data;
			diagnosticImage.note = savedImage.note;
			[[DBManager getIstance] saveContext];
			[self.images reloadData];
		} cancel:^{
			self.selectMode = NO;
			[self.images reloadData];
		}];
	}
}


@end
