//
//  ViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 17/10/15.
//  Copyright © 2015 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"

typedef void(^EditPhotoConfirm)(UIImage* savedImage);
typedef void(^EditPhotoCancel)();

@interface EditPhotoViewController : BaseViewController <UIGestureRecognizerDelegate>
{
	EditPhotoConfirm confirmBlock;
	EditPhotoCancel cancelBlock;
}

@property (strong, nonatomic) UIImage *image;

- (void)savePhoto:(UIImage*)image note:(NSString*)note completion:(EditPhotoConfirm)completion cancel:(EditPhotoCancel)cancel;

@end
