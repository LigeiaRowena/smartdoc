//
//  ViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 17/10/15.
//  Copyright © 2015 Francesca Corsini. All rights reserved.
//

#import "EditPhotoViewController.h"
#import <CoreImage/CoreImage.h>

@interface EditPhotoViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *pinchImage;
@property (weak, nonatomic) IBOutlet CustomTextView *note;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation EditPhotoViewController

#pragma mark - Init EditPhotoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (BOOL)prefersStatusBarHidden
{
	return YES;
}

- (void)setup
{
	if (self.imageView != nil)
		self.imageView.image = self.image;
	self.imageView.userInteractionEnabled = YES;
	self.imageView.contentMode = UIViewContentModeCenter;

	
	// pinch gesture
	UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchFrom:)];
	pinch.delegate = self;
	[self.imageView addGestureRecognizer:pinch];
	
	
	// rotation gesture
	UIRotationGestureRecognizer *rotation = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotationFrom:)];
	rotation.delegate = self;
	[self.imageView addGestureRecognizer:rotation];
}

- (void)dealloc
{
	confirmBlock = nil;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
	return YES;
}

#pragma mark - UIGestureRecognizer

- (void)handleRotationFrom:(UIRotationGestureRecognizer *)recognizer
{
	self.pinchImage.hidden = YES;
	
	recognizer.view.transform = CGAffineTransformRotate(recognizer.view.transform, recognizer.rotation);
	recognizer.rotation = 0;	
}

- (void)handlePinchFrom:(UIPinchGestureRecognizer *)recognizer
{
	self.pinchImage.hidden = YES;
	
	recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
	recognizer.scale = 1;
}


#pragma mark - Actions

- (void)savePhoto:(UIImage*)image note:(NSString*)note completion:(EditPhotoConfirm)completion cancel:(EditPhotoCancel)cancel
{
	confirmBlock = completion;
	cancelBlock = cancel;
	self.note.textView.text = note;
	self.image = image;
	if (self.imageView != nil)
		self.imageView.image = self.image;
}

- (IBAction)back:(id)sender
{
	// torna indietro senza salvare
	if (self.parentViewController != nil)
		[self parent:self.parentViewController popViewController:self animateWithDuration:0.2];
	else
		[self dismissViewControllerAnimated:YES completion:nil];
	
	if (cancelBlock)
		cancelBlock();
}

- (IBAction)save:(id)sender
{
	CGAffineTransform transform = self.imageView.transform;
	CIContext *context = [CIContext contextWithOptions:nil];
	CIImage *inputImage = [CIImage imageWithCGImage:self.imageView.image.CGImage];
	CIFilter *filter = [CIFilter filterWithName:@"CIAffineTransform"];
	[filter setValue:inputImage forKey:@"inputImage"];
	[filter setValue:[NSValue valueWithBytes:&transform objCType:@encode(CGAffineTransform)] forKey:@"inputTransform"];
	CIImage *result = [filter valueForKey:@"outputImage"];
	CGImageRef cgImage = [context createCGImage:result fromRect:[result extent]];
	UIImage *newImage =  [UIImage imageWithCGImage:cgImage scale:1.0 orientation:UIImageOrientationUp];
	CGImageRelease(cgImage);

	
	newImage.note = self.note.textView.text;
	if (confirmBlock)
		confirmBlock(newImage);
	
	// torna indietro salvando
	if (self.parentViewController != nil)
		[self parent:self.parentViewController popViewController:self animateWithDuration:0.2];
	else
		[self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)action:(id)sender
{
	UIButton *button = (UIButton*)sender;
	NSData * imageData = UIImagePNGRepresentation(self.image);
	NSArray *activityItems = @[self.note.textView.text, imageData];
	UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
	activityViewController.excludedActivityTypes = nil;
	UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:activityViewController];
	[popover presentPopoverFromRect:button.bounds inView:button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

@end
