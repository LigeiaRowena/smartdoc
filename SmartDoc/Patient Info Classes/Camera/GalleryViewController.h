//
//  GalleryViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 18/10/15.
//  Copyright © 2015 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"

@interface GalleryViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (strong, nonatomic) NSMutableArray *data;

@end
