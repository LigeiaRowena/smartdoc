//
//  PatientProximalAnamnesisViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 20/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "PatientProximalAnamnesisViewController.h"
#import "PatientProximalAnamnesisEdit.h"
#import "PatientMedicalHistoryViewController.h"
#import "PatientData.h"
#import "PrescribedTherapy.h"
#import "DiagnosticTest.h"
#import "SymptomHistory.h"
#import "ProximalAnamnesis.h"
#import "SymptomLabelCell.h"
#import "DrugLabelCell.h"
#import "TestLabelCell.h"
#import "CustomTableView.h"
#import "PDFExporter.h"

#define cellDrugsHeight 35
#define headerDrugsHeight 80

@interface PatientProximalAnamnesisViewController ()

@property (nonatomic, weak) IBOutlet RoundedLabel *onsetDate;
@property (nonatomic, weak) IBOutlet RoundedLabel *onsetPlace;
@property (nonatomic, weak) IBOutlet FitLabel *symptomDesc;
@property (nonatomic, weak) IBOutlet CustomTableView *drugsTable;
@property (nonatomic, weak) IBOutlet UIView *prevVisitView;
@property (nonatomic, weak) IBOutlet FitLabel *prevVisit;
@property (nonatomic, weak) IBOutlet UIView *prevHospitalizationView;
@property (nonatomic, weak) IBOutlet FitLabel *prevHospitalization;
@property (nonatomic, weak) IBOutlet CustomTableView *testTable;
@property (nonatomic, weak) IBOutlet UIView *testNoteView;
@property (nonatomic, weak) IBOutlet FitLabel *testNote;
@property (nonatomic, weak) IBOutlet CustomTableView *symptomsTable;

@property (nonatomic, strong) NSMutableArray *drugs;
@property (nonatomic, strong) NSMutableArray *tests;
@property (nonatomic, strong) NSMutableArray *symptoms;

@end

#pragma mark - Init PatientProximalAnamnesisViewController

@implementation PatientProximalAnamnesisViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	
	// drug table
	UINib *cellNibdrug = [UINib nibWithNibName:@"DrugLabelCell" bundle:nil];
	[self.drugsTable registerNib:cellNibdrug forCellReuseIdentifier:[DrugLabelCell reuseIdentifier]];
	
	// test table
	UINib *cellNibtest = [UINib nibWithNibName:@"TestLabelCell" bundle:nil];
	[self.testTable registerNib:cellNibtest forCellReuseIdentifier:[TestLabelCell reuseIdentifier]];
	
	// symtom table
	UINib *cellNibsymtom = [UINib nibWithNibName:@"SymptomLabelCell" bundle:nil];
	[self.symptomsTable registerNib:cellNibsymtom forCellReuseIdentifier:[SymptomLabelCell reuseIdentifier]];
}

- (void)setup
{
	self.mainscroll.frame = self.view.bounds;

	[self.onsetDate setRoundedText:self.patient.proximalAnamnesis.onSetDate];
	[self.onsetPlace setRoundedText:self.patient.proximalAnamnesis.onSetPlace];
	[self.symptomDesc fitText:self.patient.proximalAnamnesis.symptomsDescription];
	[self.prevVisit fitText:self.patient.proximalAnamnesis.previousClinicalVisit];
	[self.prevHospitalization fitText:self.patient.proximalAnamnesis.previousHospitalization];
	[self.testNote fitText:self.patient.proximalAnamnesis.note];
	
	// drugs table
	self.drugs = [[DBManager getIstance] getTherapiesFromPatient:self.patient suspended:NO prescribed:YES active:NO].mutableCopy;
	[self animateDrugsTable:(int)[self.drugs count]];
	
	// test table
	self.tests = [[self.patient.proximalAnamnesis tests] allObjects].mutableCopy;
	[self animateTestsTable:(int)[self.tests count]];

	 // symptom table
	self.symptoms = [[self.patient.proximalAnamnesis symptomsHistory] allObjects].mutableCopy;
	[self animateSymptomsTable:(int)[self.symptoms count]];
}

#pragma mark - Actions

- (IBAction)print:(id)sender
{
	NSString *title = [NSString stringWithFormat:@"Visit for Patient: %@ %@ %@", self.patient.patientData.name, self.patient.patientData.middleName, self.patient.patientData.lastName];
	[PDFExporter clearDocuments];
	NSString *pdfFileName = [PDFExporter getProximalAnamnesisPDFWithTitle:title proximalAnamnesis:self.patient.proximalAnamnesis patient:self.patient];
	[self printPDF:pdfFileName anchor:sender];
}

- (IBAction)send:(id)sender
{
	NSString *title = [NSString stringWithFormat:@"Visit for Patient: %@ %@ %@", self.patient.patientData.name, self.patient.patientData.middleName, self.patient.patientData.lastName];
	[PDFExporter clearDocuments];
	NSString *pdfFileName = [PDFExporter getProximalAnamnesisPDFWithTitle:title proximalAnamnesis:self.patient.proximalAnamnesis patient:self.patient];
	[self sendPDFtitle:title path:pdfFileName];
}

- (IBAction)edit:(id)sender
{
	PatientProximalAnamnesisEdit *vc = [[PatientProximalAnamnesisEdit alloc] initWithNibName:@"PatientProximalAnamnesisEdit" bundle:nil];
	PatientMedicalHistoryViewController *parent = (PatientMedicalHistoryViewController*)self.parentViewController;
	vc.isEditing = YES;
	vc.patient = self.patient;
	[self parent:parent fadeToViewController:vc fromViewController:self superView:parent.mainView animateWithDuration:0.2];
}

- (NSMutableString*)getProximalAnamnesisMultiText
{
	NSMutableString *multiText = @"".mutableCopy;

	NSArray *drugs = [[DBManager getIstance] getTherapiesFromPatient:self.patient suspended:NO prescribed:YES active:NO];
	[multiText appendFormat:@"Proximal Anamnesis"];
	[multiText appendFormat:@"\nOnset Date: %@ Onset Place: %@", self.patient.proximalAnamnesis.onSetDate, self.patient.proximalAnamnesis.onSetPlace];
	[multiText appendFormat:@"\nSymptom Description: %@", self.patient.proximalAnamnesis.symptomsDescription];
	[multiText appendString:@"\nDRUGS:"];
	for (PrescribedTherapy *drug in drugs)
		[multiText appendFormat:@"\nDrug name: %@ - Dosage: %@ - Frequency: %@ - Route: %@", drug.drugName, drug.dosage, drug.frequency, drug.route];
	[multiText appendFormat:@"\nPrevious Clinical Visit: %@", self.patient.proximalAnamnesis.previousClinicalVisit];
	[multiText appendFormat:@"\nPrevious Hospitalization: %@", self.patient.proximalAnamnesis.previousHospitalization];
	[multiText appendString:@"\nRECENT DIAGNOSTIC TESTS:"];
	for (DiagnosticTest *test in self.tests)
		[multiText appendFormat:@"\nTest: %@ - Note: %@", test.type, test.note];
	[multiText appendFormat:@"\nVisit Note: %@", self.patient.proximalAnamnesis.note];
	[multiText appendString:@"\nSYMPTOMS:"];
	for (SymptomHistory *symptom in self.symptoms)
		[multiText appendFormat:@"\nSymptom name: %@ - details: %@ - note: %@", symptom.name, symptom.details, symptom.note];

	return multiText;
}

#pragma mark - Animations

- (void)animateDrugsTable:(int)count
{
	int height = 0;
	int delta = 0;
	if (count != 0)
		height = count*cellDrugsHeight + headerDrugsHeight;
	delta = height - self.drugsTable.frame.size.height;
	
	if (delta != 0)
	{
		self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
		self.drugsTable.frame = CGRectMake(self.drugsTable.frame.origin.x, self.drugsTable.frame.origin.y, self.drugsTable.frame.size.width, height);
		self.prevVisitView.frame = CGRectMake(self.prevVisitView.frame.origin.x, self.prevVisitView.frame.origin.y + delta, self.prevVisitView.frame.size.width, self.prevVisitView.frame.size.height);
		self.prevHospitalizationView.frame = CGRectMake(self.prevHospitalizationView.frame.origin.x, self.prevHospitalizationView.frame.origin.y + delta, self.prevHospitalizationView.frame.size.width, self.prevHospitalizationView.frame.size.height);
		self.testTable.frame = CGRectMake(self.testTable.frame.origin.x, self.testTable.frame.origin.y + delta, self.testTable.frame.size.width, self.testTable.frame.size.height);
		self.testNoteView.frame = CGRectMake(self.testNoteView.frame.origin.x, self.testNoteView.frame.origin.y + delta, self.testNoteView.frame.size.width, self.testNoteView.frame.size.height);
		self.symptomsTable.frame = CGRectMake(self.symptomsTable.frame.origin.x, self.symptomsTable.frame.origin.y + delta, self.symptomsTable.frame.size.width, self.symptomsTable.frame.size.height);
		[self.drugsTable reloadData];
	}
}

- (void)animateSymptomsTable:(int)count
{
	int height = 0;
	int delta = 0;
	if (count != 0)
		height = count*cellDrugsHeight + headerDrugsHeight;
	delta = height - self.symptomsTable.frame.size.height;
	
	if (delta != 0)
	{
		self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
		self.symptomsTable.frame = CGRectMake(self.symptomsTable.frame.origin.x, self.symptomsTable.frame.origin.y, self.symptomsTable.frame.size.width, height);
		[self.symptomsTable reloadData];
	}
}

- (void)animateTestsTable:(int)count
{
	int height = 0;
	int delta = 0;
	if (count != 0)
		height = count*cellDrugsHeight + headerDrugsHeight;
	delta = height - self.testTable.frame.size.height;
	
	if (delta != 0)
	{
		self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
		self.testTable.frame = CGRectMake(self.testTable.frame.origin.x, self.testTable.frame.origin.y, self.testTable.frame.size.width, height);
		self.testNoteView.frame = CGRectMake(self.testNoteView.frame.origin.x, self.testNoteView.frame.origin.y + delta, self.testNoteView.frame.size.width, self.testNoteView.frame.size.height);
		self.symptomsTable.frame = CGRectMake(self.symptomsTable.frame.origin.x, self.symptomsTable.frame.origin.y + delta, self.symptomsTable.frame.size.width, self.symptomsTable.frame.size.height);
		[self.testTable reloadData];
	}
}

#pragma mark - UITableViewDataSource/UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return headerDrugsHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return cellDrugsHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (tableView == self.drugsTable)
		return [self.drugs count];
	else if (tableView == self.testTable)
		return [self.tests count];
	else
		return [self.symptoms count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 768, headerDrugsHeight)];
	view.backgroundColor = lightLightBlueColorBackground();
	
	if (tableView == self.drugsTable)
	{
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 15, 189, 21)];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = lightGrayBlueColor();
		label.font = OpenSansBold(21);
		label.text = @"DRUGS";
		[view addSubview:label];
		
		UILabel *drugname = [[UILabel alloc] initWithFrame:CGRectMake(20, 51, 200, 21)];
		drugname.backgroundColor = [UIColor clearColor];
		drugname.textColor = lightBlueColor();
		drugname.font = OpenSansBold(16);
		drugname.text = @"DRUG NAME";
		[view addSubview:drugname];
		
		UILabel *dosage = [[UILabel alloc] initWithFrame:CGRectMake(233, 51, 120, 21)];
		dosage.backgroundColor = [UIColor clearColor];
		dosage.textColor = lightBlueColor();
		dosage.font = OpenSansBold(16);
		dosage.text = @"DOSAGE";
		[view addSubview:dosage];
		
		UILabel *unit = [[UILabel alloc] initWithFrame:CGRectMake(364, 51, 120, 21)];
		unit.backgroundColor = [UIColor clearColor];
		unit.textColor = lightBlueColor();
		unit.font = OpenSansBold(16);
		unit.text = @"UNIT";
		[view addSubview:unit];
		
		UILabel *frequency = [[UILabel alloc] initWithFrame:CGRectMake(497, 51, 120, 21)];
		frequency.backgroundColor = [UIColor clearColor];
		frequency.textColor = lightBlueColor();
		frequency.font = OpenSansBold(16);
		frequency.text = @"FREQUENCY";
		[view addSubview:frequency];
		
		UILabel *route = [[UILabel alloc] initWithFrame:CGRectMake(633, 51, 120, 21)];
		route.backgroundColor = [UIColor clearColor];
		route.textColor = lightBlueColor();
		route.font = OpenSansBold(16);
		route.text = @"ROUTE";
		[view addSubview:route];
	}
	
	else if (tableView == self.symptomsTable)
	{
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 15, 189, 21)];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = lightGrayBlueColor();
		label.font = OpenSansBold(21);
		label.text = @"SYMPTOMS";
		[view addSubview:label];
		
		UILabel *type = [[UILabel alloc] initWithFrame:CGRectMake(20, 51, 200, 21)];
		type.backgroundColor = [UIColor clearColor];
		type.textColor = lightBlueColor();
		type.font = OpenSansBold(16);
		type.text = @"SYMPTOM";
		[view addSubview:type];
		
		UILabel *details = [[UILabel alloc] initWithFrame:CGRectMake(184, 51, 200, 21)];
		details.backgroundColor = [UIColor clearColor];
		details.textColor = lightBlueColor();
		details.font = OpenSansBold(16);
		details.text = @"DETAILS";
		[view addSubview:details];
		
		UILabel *note = [[UILabel alloc] initWithFrame:CGRectMake(348, 51, 200, 21)];
		note.backgroundColor = [UIColor clearColor];
		note.textColor = lightBlueColor();
		note.font = OpenSansBold(16);
		note.text = @"NOTE";
		[view addSubview:note];
	}
	
	else
	{
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 15, 300, 21)];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = lightGrayBlueColor();
		label.font = OpenSansBold(21);
		label.text = @"RECENT DIAGNOSTIC TESTS";
		[view addSubview:label];
		
		UILabel *type = [[UILabel alloc] initWithFrame:CGRectMake(20, 51, 200, 21)];
		type.backgroundColor = [UIColor clearColor];
		type.textColor = lightBlueColor();
		type.font = OpenSansBold(16);
		type.text = @"TYPE";
		[view addSubview:type];
		
		UILabel *note = [[UILabel alloc] initWithFrame:CGRectMake(248, 51, 506, 21)];
		note.backgroundColor = [UIColor clearColor];
		note.textColor = lightBlueColor();
		note.font = OpenSansBold(16);
		note.text = @"NOTE";
		[view addSubview:note];
	}
	
	return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;
	
	if (tableView == self.drugsTable)
	{
		DrugLabelCell *customcell = [tableView dequeueReusableCellWithIdentifier:[DrugLabelCell reuseIdentifier]];
		PrescribedTherapy *drug = self.drugs[indexPath.row];
		[customcell setContent:drug];
		cell = customcell;
	}
	
	else if (tableView == self.testTable)
	{
		TestLabelCell *customcell = [tableView dequeueReusableCellWithIdentifier:[TestLabelCell reuseIdentifier]];
		DiagnosticTest *test = self.tests[indexPath.row];
		[customcell setContent:test];
		cell = customcell;
	}
	
	else if (tableView == self.symptomsTable)
	{
		SymptomLabelCell *customcell = [tableView dequeueReusableCellWithIdentifier:[SymptomLabelCell reuseIdentifier]];
		customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
		SymptomHistory *symptom = self.symptoms[indexPath.row];
		[customcell setContent:symptom];
		cell = customcell;
	}
	
    return cell;
}



@end
