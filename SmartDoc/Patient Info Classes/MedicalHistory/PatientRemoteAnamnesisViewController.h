//
//  PatientRemoteAnamnesisViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 20/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "PickerDateViewController.h"
#import "BaseViewController.h"

@interface PatientRemoteAnamnesisViewController : BaseViewController <UICollectionViewDataSource, UICollectionViewDelegate>


@end
