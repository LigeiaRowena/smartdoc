//
//  PatientRemoteAnamnesisViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 20/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "PickerDateViewController.h"
#import "PickerViewController.h"
#import "TooltipViewController.h"
#import "FatherHistoryCell.h"
#import "MotherHistoryCell.h"
#import "VaccinationCell.h"
#import "OperationCell.h"
#import "DiseaseHistoryHeader.h"
#import "DiseaseHistoryCollectionCell.h"
#import "BaseViewController.h"
#import "EditItemViewController.h"

@interface PatientRemoteAnamnesisEdit : BaseViewController <UICollectionViewDataSource, UICollectionViewDelegate, CellDelegate, PickerDateViewControllerDelegate, PickerViewControllerDelegate, CollectionCellDelegate, EditItemViewControllerDelegate>

@property (nonatomic) BOOL isEditing;

@end
