//
//  PatientProximalAnamnesisViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 20/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "PatientProximalAnamnesisEdit.h"
#import "PatientProximalAnamnesisViewController.h"
#import "PatientMedicalHistoryViewController.h"
#import "PrescribedTherapy.h"
#import "DiagnosticTest.h"
#import "SymptomHistory.h"
#import "ProximalAnamnesis.h"
#import "EditPhotoViewController.h"
#import "GalleryViewController.h"

#define cellDrugsHeight 80
#define cellTestHeight 190

@interface PatientProximalAnamnesisEdit ()

@property (nonatomic, weak) IBOutlet PickerField *onsetDateText;
@property (nonatomic, weak) IBOutlet CustomTextField *onsetPlaceText;
@property (nonatomic, weak) IBOutlet CustomTextView *symptomsDescText;
@property (nonatomic, weak) IBOutlet PickerField *drugName;
@property (nonatomic, weak) IBOutlet CustomTextField *drugDosageText;
@property (nonatomic, weak) IBOutlet PickerField *drugUnitText;
@property (nonatomic, weak) IBOutlet PickerField *drugFrequencyText;
@property (nonatomic, weak) IBOutlet PickerField *drugRouteText;
@property (nonatomic, weak) IBOutlet UITableView *drugsTable;
@property (nonatomic, weak) IBOutlet CustomTextView *prevClinicalVisitText;
@property (nonatomic, weak) IBOutlet CustomTextView *prevHospitalizationText;
@property (nonatomic, weak) IBOutlet UIView *testHeader;
@property (nonatomic, weak) IBOutlet PickerField *testTypeText;
@property (nonatomic, weak) IBOutlet CustomTextField *testNoteText;
@property (nonatomic, weak) IBOutlet PickerField *testDateText;
@property (nonatomic, weak) IBOutlet UITableView *testTable;
@property (nonatomic, weak) IBOutlet CustomTextView *testNote;
@property (nonatomic, weak) IBOutlet UIView *symptomsHeader;
@property (nonatomic, weak) IBOutlet PickerField *symptomType;
@property (nonatomic, weak) IBOutlet PickerField *symptomDetail;
@property (nonatomic, weak) IBOutlet CustomTextField *symptomNote;
@property (nonatomic, weak) IBOutlet UITableView *symptomsTable;
@property (weak, nonatomic) IBOutlet UIView *saveView;

@property (nonatomic, strong) NSMutableArray *drugs;
@property (nonatomic, strong) NSMutableArray *tests;
@property (nonatomic, strong) NSMutableArray *symptoms;

@end

#pragma mark - Init PatientProximalAnamnesisEdit

@implementation PatientProximalAnamnesisEdit

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// drug table
	UINib *cellNibdrug = [UINib nibWithNibName:@"DrugCell" bundle:nil];
	[self.drugsTable registerNib:cellNibdrug forCellReuseIdentifier:[DrugCell reuseIdentifier]];
	self.drugs = [[DBManager getIstance] getTherapiesFromPatient:self.patient suspended:NO prescribed:YES active:NO].mutableCopy;
	[self animateDrugsTableWithDelta:((int)[self.drugs count]*cellDrugsHeight)];

	// test table
	UINib *cellNibtest = [UINib nibWithNibName:@"TestCell" bundle:nil];
	[self.testTable registerNib:cellNibtest forCellReuseIdentifier:[TestCell reuseIdentifier]];
	self.tests = [[self.patient.proximalAnamnesis tests] allObjects].mutableCopy;
	[self animateTestsTableWithDelta:((int)[self.tests count]*cellTestHeight)];

	// symtom table
	UINib *cellNibsymtom = [UINib nibWithNibName:@"SymptomCell" bundle:nil];
	[self.symptomsTable registerNib:cellNibsymtom forCellReuseIdentifier:[SymptomCell reuseIdentifier]];
	self.symptoms = [[self.patient.proximalAnamnesis symptomsHistory] allObjects].mutableCopy;
	[self animateSymptomsTableWithDelta:((int)[self.symptoms count]*cellDrugsHeight)];
}

- (void)setup
{
	if (self.imagePickerController.isBeingDismissed)
		return;
	
	self.mainscroll.frame = self.view.bounds;
	
	// save button mode
	if (self.isEditing) {
		self.saveView.hidden = NO;
	} else {
		self.saveView.hidden = YES;
	}

	self.onsetDateText.textField.text = self.patient.proximalAnamnesis.onSetDate;
	self.onsetPlaceText.text = self.patient.proximalAnamnesis.onSetPlace;
	self.symptomsDescText.textView.text = self.patient.proximalAnamnesis.symptomsDescription;
	self.prevClinicalVisitText.textView.text = self.patient.proximalAnamnesis.previousClinicalVisit;
	self.prevHospitalizationText.textView.text = self.patient.proximalAnamnesis.previousHospitalization;
	self.testNote.textView.text = self.patient.proximalAnamnesis.note;
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

#pragma mark - Actions


- (IBAction)save:(id)sender
{
	PatientProximalAnamnesisViewController *vc = [[PatientProximalAnamnesisViewController alloc] initWithNibName:@"PatientProximalAnamnesisViewController" bundle:nil];
	PatientMedicalHistoryViewController *parent = (PatientMedicalHistoryViewController*)self.parentViewController;
	vc.patient = self.patient;
	[self parent:parent fadeToViewController:vc fromViewController:self superView:parent.mainView animateWithDuration:0.2];
}

// add drug as a prescribed therapy
- (IBAction)addDrug:(id)sender
{
	 if (![BaseViewController isEmptyString:self.drugName.textField.text])
	 {
		 [[DBManager getIstance] createPrescribedTherapyForPatient:self.patient date:[NSDate date] drugName:self.drugName.textField.text dosage:self.drugDosageText.text frequency:self.drugFrequencyText.textField.text route:self.drugRouteText.textField.text active:NO suspended:NO prescribed:YES dosageUnit:self.drugUnitText.textField.text visit:nil prescription:nil];
		 self.drugName.textField.text = @"";
		 self.drugDosageText.text = @"";
		 self.drugUnitText.textField.text = @"";
		 self.drugFrequencyText.textField.text = @"";
		 self.drugRouteText.textField.text = @"";
		 self.drugs = [[DBManager getIstance] getTherapiesFromPatient:self.patient suspended:NO prescribed:YES active:NO].mutableCopy;
		 [self animateDrugsTableWithDelta:cellDrugsHeight];
	 }
	else
	{
		[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please provide a value for the Details filled" confirmBlock:^{
			self.drugName.textField.text = @"";
			self.drugDosageText.text = @"";
			self.drugUnitText.textField.text = @"";
			self.drugFrequencyText.textField.text = @"";
			self.drugRouteText.textField.text = @"";
		}];
	}
}

- (IBAction)addSymptom:(id)sender
{
	if (![BaseViewController isEmptyString:self.symptomType.textField.text])
	{
		[[DBManager getIstance] createSymptomHistoryForProximalAnamnesis:self.patient.proximalAnamnesis name:self.symptomType.textField.text details:self.symptomDetail.textField.text note:self.symptomNote.text];
		self.symptomType.textField.text = @"";
		self.symptomDetail.textField.text = @"";
		self.symptomNote.text = @"";
		self.symptoms = [[self.patient.proximalAnamnesis symptomsHistory] allObjects].mutableCopy;
		[self animateSymptomsTableWithDelta:cellDrugsHeight];
	}
	else
	{
		[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please provide a value for the Details filled" confirmBlock:^{
			self.symptomType.textField.text = @"";
			self.symptomDetail.textField.text = @"";
			self.symptomNote.text = @"";
		}];
	}
}

- (IBAction)addTest:(id)sender
{
	if (![BaseViewController isEmptyString:self.testTypeText.textField.text])
	{
		NSDate *date;
		if (![BaseViewController isEmptyString:self.testDateText.textField.text])
			date = [self.formatter dateFromString:self.testDateText.textField.text];
		else
			date = [NSDate date];
		[[DBManager getIstance] createDiagnosticTestForProximalAnamnesis:self.patient.proximalAnamnesis type:self.testTypeText.textField.text note:self.testNoteText.text date:date];
		self.tests = [[self.patient.proximalAnamnesis tests] allObjects].mutableCopy;
		self.testTypeText.textField.text = @"";
		self.testNoteText.text = @"";
		self.testDateText.textField.text = @"";
		[self animateTestsTableWithDelta:cellTestHeight];
	}
	else
	{
		[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please provide a value for the Details filled" confirmBlock:^{
			self.testTypeText.textField.text = @"";
			self.testNoteText.text = @"";
			self.testDateText.textField.text = @"";
		}];
	}
}


#pragma mark - Animations

- (void)animateSymptomsTableWithDelta:(int)delta
{
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.symptomsTable.frame = CGRectMake(self.symptomsTable.frame.origin.x, self.symptomsTable.frame.origin.y, self.symptomsTable.frame.size.width, self.symptomsTable.frame.size.height + delta);
	} completion:^(BOOL s){
		[self.symptomsTable reloadData];
	}];
}

- (void)animateTestsTableWithDelta:(int)delta
{
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.testTable.frame = CGRectMake(self.testTable.frame.origin.x, self.testTable.frame.origin.y, self.testTable.frame.size.width, self.testTable.frame.size.height + delta);
		self.testNote.frame = CGRectMake(self.testNote.frame.origin.x, self.testNote.frame.origin.y + delta, self.testNote.frame.size.width, self.testNote.frame.size.height);
		self.symptomsHeader.frame = CGRectMake(self.symptomsHeader.frame.origin.x, self.symptomsHeader.frame.origin.y + delta, self.symptomsHeader.frame.size.width, self.symptomsHeader.frame.size.height);
		self.symptomsTable.frame = CGRectMake(self.symptomsTable.frame.origin.x, self.symptomsTable.frame.origin.y + delta, self.symptomsTable.frame.size.width, self.symptomsTable.frame.size.height);
	} completion:^(BOOL s){
		[self.testTable reloadData];
	}];
}

- (void)animateDrugsTableWithDelta:(int)delta
{	
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.drugsTable.frame = CGRectMake(self.drugsTable.frame.origin.x, self.drugsTable.frame.origin.y, self.drugsTable.frame.size.width, self.drugsTable.frame.size.height + delta);
		self.prevClinicalVisitText.frame = CGRectMake(self.prevClinicalVisitText.frame.origin.x, self.prevClinicalVisitText.frame.origin.y + delta, self.prevClinicalVisitText.frame.size.width, self.prevClinicalVisitText.frame.size.height);
		self.prevHospitalizationText.frame = CGRectMake(self.prevHospitalizationText.frame.origin.x, self.prevHospitalizationText.frame.origin.y + delta, self.prevHospitalizationText.frame.size.width, self.prevHospitalizationText.frame.size.height);
		self.testHeader.frame = CGRectMake(self.testHeader.frame.origin.x, self.testHeader.frame.origin.y + delta, self.testHeader.frame.size.width, self.testHeader.frame.size.height);
		self.testTable.frame = CGRectMake(self.testTable.frame.origin.x, self.testTable.frame.origin.y + delta, self.testTable.frame.size.width, self.testTable.frame.size.height);
		self.testNote.frame = CGRectMake(self.testNote.frame.origin.x, self.testNote.frame.origin.y + delta, self.testNote.frame.size.width, self.testNote.frame.size.height);
		self.symptomsHeader.frame = CGRectMake(self.symptomsHeader.frame.origin.x, self.symptomsHeader.frame.origin.y + delta, self.symptomsHeader.frame.size.width, self.symptomsHeader.frame.size.height);
		self.symptomsTable.frame = CGRectMake(self.symptomsTable.frame.origin.x, self.symptomsTable.frame.origin.y + delta, self.symptomsTable.frame.size.width, self.symptomsTable.frame.size.height);
	} completion:^(BOOL s){
		[self.drugsTable reloadData];
	}];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	return [super textFieldDidBeginEditing:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	
	NSString *text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	if (textField == self.onsetPlaceText)
	{
		self.patient.proximalAnamnesis.onSetPlace = text;
		[[DBManager getIstance] saveContext];
	}
	else if ([textField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]])
	{
		int index = (int)textField.tag;
		PrescribedTherapy *therapy = self.drugs[index];
		therapy.dosage = text;
		[[DBManager getIstance] saveContext];
		self.drugs = [[DBManager getIstance] getTherapiesFromPatient:self.patient suspended:NO prescribed:YES active:NO].mutableCopy;
	}
	else if ([textField.accessibilityIdentifier isEqualToString:[TestCell reuseIdentifier]])
	{
		int index = (int)textField.tag;
		DiagnosticTest *test = self.tests[index];
		test.note = text;
		[[DBManager getIstance] saveContext];
		self.tests = [[self.patient.proximalAnamnesis tests] allObjects].mutableCopy;
	}
	
	else if ([textField.accessibilityIdentifier isEqualToString:[SymptomCell reuseIdentifier]] && [[(CustomTextField*)textField type] isEqualToString:SymptomNoteField])
	{
		int index = (int)textField.tag;
		SymptomHistory *symptom = self.symptoms[index];
		symptom.note = text;
		[[DBManager getIstance] saveContext];
		self.symptoms = [[self.patient.proximalAnamnesis symptomsHistory] allObjects].mutableCopy;
	}

	[super textFieldDidEndEditing:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	return [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
}


#pragma mark - CustomTextViewDelegate

- (void)customTextViewDidBeginEditing:(CustomTextView *)textView
{
	[super customTextViewDidBeginEditing:textView];
}

- (void)customTextViewDidEndEditing:(CustomTextView *)textView
{
	NSString *text = [textView.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

	if (textView == self.symptomsDescText)
	{
		self.patient.proximalAnamnesis.symptomsDescription = text;
		[[DBManager getIstance] saveContext];
	}
	else if (textView == self.prevClinicalVisitText)
	{
		self.patient.proximalAnamnesis.previousClinicalVisit = text;
		[[DBManager getIstance] saveContext];
	}
	else if (textView == self.prevHospitalizationText)
	{
		self.patient.proximalAnamnesis.previousHospitalization = text;
		[[DBManager getIstance] saveContext];
	}
	else if (textView == self.testNote)
	{
		self.patient.proximalAnamnesis.note = text;
		[[DBManager getIstance] saveContext];
	}
	
	[super customTextViewDidEndEditing:textView];
}

- (void)customTextViewDidChange:(CustomTextView *)textView
{
	[super customTextViewDidChange:textView];
}

#pragma mark - PickerFieldDelegate Methods

- (void)tapOnPickerFieldDrugButton:(PickerField*)pickerField
{
	PickerViewController *pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
	pickerViewController.pickerField = pickerField;
	pickerViewController.delegate = self;
	pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:DrugActiveData text:@""].mutableCopy;
	pickerViewController.pickerType = DrugNameField;
	pickerViewController.dataType = DrugActiveData;
	
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerViewController];
	[self.popover setPopoverContentSize:pickerViewController.view.frame.size];
	[pickerViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	
	/*
	PickerDrugViewController *pickerDrugViewController = [[PickerDrugViewController alloc] initWithNibName:@"PickerDrugViewController" bundle:nil];
	pickerDrugViewController.pickerField = pickerField;
	pickerDrugViewController.delegate = self;
	
	if (pickerField == self.drugName || [pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]])
	{
		pickerDrugViewController.pickerType = DrugNameField;
	}
	
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerDrugViewController];
	[self.popover setPopoverContentSize:pickerDrugViewController.view.frame.size];
	[pickerDrugViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	 */
}

- (void)tapOnPickerFieldDateButton:(PickerField*)pickerField
{
	PickerDateViewController *pickerDateViewController = [[PickerDateViewController alloc] initWithNibName:@"PickerDateViewController" bundle:nil];
	pickerDateViewController.pickerField = pickerField;
	pickerDateViewController.delegate = self;
	
	if (pickerField == self.onsetDateText)
		pickerDateViewController.date = [self.formatter dateFromString:self.onsetDateText.textField.text];
	else if (pickerField == self.testDateText || [pickerField.accessibilityIdentifier isEqualToString:[TestCell reuseIdentifier]])
		pickerDateViewController.date = [self.formatter dateFromString:self.testDateText.textField.text];

	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerDateViewController];
	[self.popover setPopoverContentSize:pickerDateViewController.view.frame.size];
	[pickerDateViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)pickerFieldDidBeginEditing:(PickerField *)pickerField
{
	[super pickerFieldDidBeginEditing:pickerField];
}

- (void)tapOnPickerFieldButton:(PickerField*)pickerField
{
	PickerViewController *pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
	pickerViewController.pickerField = pickerField;
	pickerViewController.delegate = self;
	
	if ((pickerField == self.drugUnitText) || ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugUnitField]))
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"DrugDosageUnit"];
		for (NSDictionary *item in list)
			[pickerViewController.data addObject:item[@"name"]];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = DrugUnitField;
	}
	
	else if ((pickerField == self.drugFrequencyText) || ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugFrequencyField]))
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"Frequency"];
		for(NSString *title in list)
			[pickerViewController.data addObject:title];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = DrugFrequencyField;
	}
	
	else if ((pickerField == self.drugRouteText) || ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugRouteField]))
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:RouteData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = RouteData;
	}
	
	else if (pickerField == self.testTypeText || [pickerField.accessibilityIdentifier isEqualToString:[TestCell reuseIdentifier]])
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:DiagnosticTestsData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = DiagnosticTestsData;
	}
	
	else if (pickerField == self.symptomType || ([pickerField.accessibilityIdentifier isEqualToString:[SymptomCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:SymptomTypeField]))
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:SymptomData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = SymptomData;
	}
	
	else if (pickerField == self.symptomDetail || ([pickerField.accessibilityIdentifier isEqualToString:[SymptomCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:SymptomDetailsField]))
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"SymptomType"];
		for(NSString *title in list)
			[pickerViewController.data addObject:title];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = StaticField;
	}

	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerViewController];
	[self.popover setPopoverContentSize:pickerViewController.view.frame.size];
	[pickerViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}


#pragma mark - PickerDateViewControllerDelegate Methods

- (void)pickerDateViewControllerSetDate:(NSDate *)date pickerField:(PickerField *)pickerField
{
	pickerField.textField.text = [self.formatter stringFromDate:date];
	
	if (pickerField == self.onsetDateText)
	{
		self.patient.proximalAnamnesis.onSetDate = [self.formatter stringFromDate:date];
		[[DBManager getIstance] saveContext];
	}
	else if ([pickerField.accessibilityIdentifier isEqualToString:[TestCell reuseIdentifier]])
	{
		int index = (int)pickerField.tag;
		DiagnosticTest *test = self.tests[index];
		test.date = date;
		[[DBManager getIstance] saveContext];
		self.tests = [[self.patient.proximalAnamnesis tests] allObjects].mutableCopy;
	}
}

#pragma mark - PickerDrugViewControllerDelegate Methods

/*
- (void)pickerDrugViewControllerSetText:(NSString*)text pickerField:(PickerField*)pickerField
{
	pickerField.textField.text = text;

	if ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]])
	{
		int index = (int)pickerField.tag;
		PrescribedTherapy *therapy = self.drugs[index];
		therapy.drugName = text;
		[[DBManager getIstance] saveContext];
		self.drugs = [[DBManager getIstance] getTherapiesFromPatient:self.patient suspended:NO prescribed:YES active:NO].mutableCopy;
	}
}
 */

#pragma mark - PickerViewControllerDelegate Methods

- (void)pickerViewControllerSetText:(NSString*)text pickerField:(PickerField*)pickerField
{
	pickerField.textField.text = text;
	
	if ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugNameField])
	{
		int index = (int)pickerField.tag;
		PrescribedTherapy *therapy = self.drugs[index];
		therapy.drugName = text;
		[[DBManager getIstance] saveContext];
	}
	else if ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugUnitField])
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"DrugDosageUnit"];
		pickerField.textField.text = list[pickerField.pickerIndex][@"name"];
		int index = (int)pickerField.tag;
		PrescribedTherapy *therapy = self.drugs[index];
		therapy.dosageUnit = text;
		[[DBManager getIstance] saveContext];
		self.drugs = [[DBManager getIstance] getTherapiesFromPatient:self.patient suspended:NO prescribed:YES active:NO].mutableCopy;
	}
	
	else if ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugFrequencyField])
	{
		int index = (int)pickerField.tag;
		PrescribedTherapy *therapy = self.drugs[index];
		therapy.frequency = text;
		[[DBManager getIstance] saveContext];
		self.drugs = [[DBManager getIstance] getTherapiesFromPatient:self.patient suspended:NO prescribed:YES active:NO].mutableCopy;
	}
	
	else if ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugRouteField])
	{
		int index = (int)pickerField.tag;
		PrescribedTherapy *therapy = self.drugs[index];
		therapy.route = text;
		[[DBManager getIstance] saveContext];
		self.drugs = [[DBManager getIstance] getTherapiesFromPatient:self.patient suspended:NO prescribed:YES active:NO].mutableCopy;
	}
	
	else if ([pickerField.accessibilityIdentifier isEqualToString:[TestCell reuseIdentifier]])
	{
		int index = (int)pickerField.tag;
		DiagnosticTest *test = self.tests[index];
		test.type = text;
		[[DBManager getIstance] saveContext];
		self.tests = [[self.patient.proximalAnamnesis tests] allObjects].mutableCopy;
	}
	
	else if ([pickerField.accessibilityIdentifier isEqualToString:[SymptomCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:SymptomTypeField])
	{
		int index = (int)pickerField.tag;
		SymptomHistory *symptom = self.symptoms[index];
		symptom.name = text;
		[[DBManager getIstance] saveContext];
		self.symptoms = [[self.patient.proximalAnamnesis symptomsHistory] allObjects].mutableCopy;
	}
	
	else if ([pickerField.accessibilityIdentifier isEqualToString:[SymptomCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:SymptomDetailsField])
	{
		int index = (int)pickerField.tag;
		Symptom *symptom = self.symptoms[index];
		symptom.details = text;
		[[DBManager getIstance] saveContext];
		self.symptoms = [[self.patient.proximalAnamnesis symptomsHistory] allObjects].mutableCopy;
	}
}

#pragma mark - UITableViewDataSource/UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (tableView == self.drugsTable)
		return [self.drugs count];
	else if (tableView == self.testTable)
		return [self.tests count];
	else
		return [self.symptoms count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;

	if (tableView == self.drugsTable)
	{
		DrugCell *customcell = [tableView dequeueReusableCellWithIdentifier:[DrugCell reuseIdentifier]];
		customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
		customcell.cellDelegate = self;
		PrescribedTherapy *therapy = self.drugs[indexPath.row];
		[customcell setContent:therapy];
		cell = customcell;
	}
	
	else if (tableView == self.testTable)
	{
		TestCell *customcell = [tableView dequeueReusableCellWithIdentifier:[TestCell reuseIdentifier]];
		customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
		customcell.cellDelegate = self;
		DiagnosticTest *test = self.tests[indexPath.row];
		[customcell setContent:test];
		cell = customcell;
	}
	
	else if (tableView == self.symptomsTable)
	{
		SymptomCell *customcell = [tableView dequeueReusableCellWithIdentifier:[SymptomCell reuseIdentifier]];
		customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
		customcell.cellDelegate = self;
		SymptomHistory *symptom = self.symptoms[indexPath.row];
		[customcell setContent:symptom];
		cell = customcell;
	}
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)doActionForKey:(NSString*)actionKey actionData:(id)actionData
{
	if ([actionKey isEqualToString:@"deleteDrug"])
	{
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"You are going to delete this information: are you sure?" confirmBlock:^{
			int index = [actionData[@"index"] intValue];
			PrescribedTherapy *therapy = self.drugs[index];
			[[DBManager getIstance] deleteObject:therapy];
			self.drugs = [[DBManager getIstance] getTherapiesFromPatient:self.patient suspended:NO prescribed:YES active:NO].mutableCopy;
			[self animateDrugsTableWithDelta:-cellDrugsHeight];
		} cancelBlock:^{
		}];
	}
	
	else if ([actionKey isEqualToString:@"deleteTest"])
	{
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"You are going to delete this information: are you sure?" confirmBlock:^{
			int index = [actionData[@"index"] intValue];
			DiagnosticTest *test = self.tests[index];
			[[DBManager getIstance] deleteObject:test];
			self.tests = [[self.patient.proximalAnamnesis tests] allObjects].mutableCopy;
			[self animateTestsTableWithDelta:-cellTestHeight];
		} cancelBlock:^{
		}];
	}
	
	else if ([actionKey isEqualToString:@"deleteSymptom"])
	{
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"You are going to delete this information: are you sure?" confirmBlock:^{
			int index = [actionData[@"index"] intValue];
			SymptomHistory *symptom = self.symptoms[index];
			[[DBManager getIstance] deleteObject:symptom];
			self.symptoms = [[self.patient.proximalAnamnesis symptomsHistory] allObjects].mutableCopy;
			[self animateSymptomsTableWithDelta:cellDrugsHeight];
		} cancelBlock:^{
		}];
	}
	else if ([actionKey isEqualToString:@"addPhoto"])
	{
		// disponibili sia camera che photos
		if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
		{
			// mostra alert per scelta tra camera e photos
			UIImage *firstButtonBg = [UIImage imageNamed:@"Butt_cameragrayalert"];
			UIImage *secondButtonBg = [UIImage imageNamed:@"Butt_photosgreenalert"];
			[[AlertViewBlocks getIstance] twoOptionsAlertViewWithmessage:@"Load pictures from the iPad camera roll or take a new picture" firstButtonBg:firstButtonBg secondButtonBg:secondButtonBg confirmBlock:^{
				[self showCamera:actionData[@"index"]];
			} secondConfirm:^{
				[self showPhotos:actionData[@"index"]];
			}];
		}
		
		// disponibile solo camera
		else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && ![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
		{
			[self showCamera:actionData[@"index"]];
		}
		
		// disponibile solo photos
		else if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
		{
			[self showPhotos:actionData[@"index"]];
		}
	}
	else if ([actionKey isEqualToString:@"selectGallery"])
	{
		int index = [actionData[@"index"] intValue];
		DiagnosticTest *test = self.tests[index];
		GalleryViewController *galleryViewController = [[GalleryViewController alloc] initWithNibName:@"GalleryViewController" bundle:nil];
		[self presentViewController:galleryViewController animated:YES completion:nil];
		galleryViewController.headerLabel.text = test.name;
		galleryViewController.data = [[test diagnosticImages] allObjects].mutableCopy;
	}
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
	if ([info[UIImagePickerControllerMediaType] isEqualToString:@"public.image"])
	{
		// modifica foto e salva nelle diagnostic test
		UIImage *originalImage = info[UIImagePickerControllerOriginalImage];
		EditPhotoViewController *editPhotoViewController = [[EditPhotoViewController alloc] initWithNibName:@"EditPhotoViewController" bundle:nil];
		if ([self.presentedViewController isKindOfClass:[UIImagePickerController class]])
		{
			[self dismissViewControllerAnimated:YES completion:nil];
		}
		[self presentViewController:editPhotoViewController animated:YES completion:nil];
		[editPhotoViewController savePhoto:originalImage note:originalImage.note completion:^(UIImage *savedImage) {
			int index = [picker.index intValue];
			DiagnosticTest *test = self.tests[index];
			NSData *data = UIImagePNGRepresentation(savedImage);
			if (data == nil)
				return ;
			[[DBManager getIstance] createDiagnosticImageForTest:test image:data note:savedImage.note];
			[self.testTable reloadData];
			
			// se il picker è di tipo camera chiedi se salvare la foto anche delle photos
			if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
			{
				[[AlertViewBlocks getIstance] informationConfirmAlertViewWithmessage:@"Do you want to save the image in Photos app?" confirmBlock:^{
					UIImageWriteToSavedPhotosAlbum(savedImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
				} cancelBlock:^{
				}];
			}
		} cancel:^{
		}];
	}
}

@end
