//
//  PatientActualRiskFactorsViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 20/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"
#import "PickerViewController.h"
#import "MenuView.h"
#import "DiseaseHistoryHeader.h"
#import "DiseaseHistoryCollectionCell.h"
#import "AllergyCell.h"

@interface PatientActualRiskFactorsEdit : BaseViewController <PickerViewControllerDelegate, MenuViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, CollectionCellDelegate, CellDelegate>

@property (nonatomic) BOOL isEditing;

@end
