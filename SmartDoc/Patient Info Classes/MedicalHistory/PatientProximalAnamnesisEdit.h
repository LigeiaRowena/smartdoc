//
//  PatientProximalAnamnesisViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 20/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"
#import "PickerDateViewController.h"
#import "PickerViewController.h"
#import "PickerDrugViewController.h"
#import "DrugCell.h"
#import "TestCell.h"
#import "SymptomCell.h"

@interface PatientProximalAnamnesisEdit : BaseViewController <PickerDateViewControllerDelegate, PickerViewControllerDelegate, PickerDrugViewControllerDelegate, CellDelegate>

@property (nonatomic) BOOL isEditing;

@end
