//
//  PatientMedicalHistoryViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 27/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "PatientMedicalHistoryViewController.h"
#import "PatientRemoteAnamnesisViewController.h"
#import "PatientProximalAnamnesisViewController.h"
#import "PatientActualRiskFactorsViewController.h"

@interface PatientMedicalHistoryViewController ()

@end

@implementation PatientMedicalHistoryViewController

#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self.menu selectItemAtIndex:0];
}

#pragma mark - MenuViewDelegate

- (void)menuDidSelectItemAtIndex:(int)index
{
	if (index == 0)
		[self pushRemoteAnamnesis];
	else if (index == 1)
		[self pushProximalAnamnesis];
	else if (index == 2)
		[self pushActualRiskFactors];
}

#pragma mark - Actions

- (void)pushRemoteAnamnesis
{
	PatientRemoteAnamnesisViewController *patientRemoteAnamnesisViewController = [[PatientRemoteAnamnesisViewController alloc] initWithNibName:@"PatientRemoteAnamnesisViewController" bundle:nil];
	patientRemoteAnamnesisViewController.patient = self.patient;
	BaseViewController *lastChild = (BaseViewController*)[self getLastChildFromParent:self];
	if (lastChild != nil)
		[self parent:self fadeToViewController:patientRemoteAnamnesisViewController fromViewController:lastChild superView:self.mainView animateWithDuration:0.2];
	else
		[self parent:self addChildViewController:patientRemoteAnamnesisViewController superView:self.mainView];
}

- (void)pushProximalAnamnesis
{
	PatientProximalAnamnesisViewController *patientProximalAnamnesisViewController = [[PatientProximalAnamnesisViewController alloc] initWithNibName:@"PatientProximalAnamnesisViewController" bundle:nil];
	patientProximalAnamnesisViewController.patient = self.patient;
	BaseViewController *lastChild = (BaseViewController*)[self getLastChildFromParent:self];
	[self parent:self fadeToViewController:patientProximalAnamnesisViewController fromViewController:lastChild superView:self.mainView animateWithDuration:0.2];
}

- (void)pushActualRiskFactors
{
	PatientActualRiskFactorsViewController *patientActualRiskFactorsViewController = [[PatientActualRiskFactorsViewController alloc] initWithNibName:@"PatientActualRiskFactorsViewController" bundle:nil];
	patientActualRiskFactorsViewController.patient = self.patient;
	BaseViewController *lastChild = (BaseViewController*)[self getLastChildFromParent:self];
	[self parent:self fadeToViewController:patientActualRiskFactorsViewController fromViewController:lastChild superView:self.mainView animateWithDuration:0.2];
}


- (IBAction)back:(id)sender
{
	[self parent:self.parentViewController popViewController:self animateWithDuration:0.2];
}

- (IBAction)favourite:(id)sender
{
	if ([self.patient.favourite boolValue])
	{
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"Do you want to delete the patient from the favourites?" confirmBlock:^{
			self.patient.favourite = [NSNumber numberWithBool:NO];
			[[DBManager getIstance] saveContext];
		} cancelBlock:^{
		}];
	}
	else
	{
		[[AlertViewBlocks getIstance] informationConfirmAlertViewWithmessage:@"Do you want to add the patient to the favourites?" confirmBlock:^{
			self.patient.favourite = [NSNumber numberWithBool:YES];
			[[DBManager getIstance] saveContext];
		} cancelBlock:^{
		}];
	}
}


@end
