//
//  PatientMedicalHistoryViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 07/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"

@interface PatientFirstVisitViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UIButton *remoteAnamnesisButton;
@property (nonatomic, weak) IBOutlet UIButton *proximalAnamnesisButton;
@property (nonatomic, weak) IBOutlet UIButton *actualRiskFactorButton;
@property (nonatomic, weak) IBOutlet UIButton *goOnButton;
@property (nonatomic, weak) IBOutlet UIView *mainView;
@property (nonatomic) int index;

@end
