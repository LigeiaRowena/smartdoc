//
//  PatientActualRiskFactorsViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 20/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"

@interface PatientActualRiskFactorsViewController : BaseViewController < UICollectionViewDataSource, UICollectionViewDelegate>

@end
