//
//  PatientMedicalHistoryViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 07/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "PatientFirstVisitViewController.h"
#import "SettingsViewController.h"
#import "PatientRemoteAnamnesisEdit.h"
#import "PatientProximalAnamnesisEdit.h"
#import "PatientActualRiskFactorsEdit.h"
#import "PatientListViewController.h"
#import "RemoteAnamnesis.h"

@interface PatientFirstVisitViewController ()
@end

#pragma mark - Init

@implementation PatientFirstVisitViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self pushRemoteAnamnesis];
	
	// creation date first visit
	if (self.patient.remoteAnamnesis.creationDate == nil)
	{
		self.patient.remoteAnamnesis.creationDate = [NSDate date];
		[[DBManager getIstance] saveContext];
	}
}

- (void)setup
{
}


#pragma mark - Actions

- (IBAction)tapMenuButtons:(id)sender
{
	[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please fill the fields about Medical History of the patient." confirmBlock:^{
	}];
}

- (void)pushRemoteAnamnesis
{
	self.index = 0;
	self.remoteAnamnesisButton.selected = YES;
	self.proximalAnamnesisButton.selected = NO;
	self.actualRiskFactorButton.selected = NO;
	
	PatientRemoteAnamnesisEdit *patientRemoteAnamnesisViewController = [[PatientRemoteAnamnesisEdit alloc] initWithNibName:@"PatientRemoteAnamnesisEdit" bundle:nil];
	patientRemoteAnamnesisViewController.patient = self.patient;
	[self parent:self addChildViewController:patientRemoteAnamnesisViewController superView:self.mainView];
	
	[self.goOnButton setImage:[UIImage imageNamed:@"Butt_movetosteptwo.png"] forState:UIControlStateNormal];
}

- (void)pushProximalAnamnesis
{
	self.index = 1;
	self.remoteAnamnesisButton.selected = NO;
	self.proximalAnamnesisButton.selected = YES;
	self.actualRiskFactorButton.selected = NO;
	
	PatientProximalAnamnesisEdit *patientProximalAnamnesisViewController = [[PatientProximalAnamnesisEdit alloc] initWithNibName:@"PatientProximalAnamnesisEdit" bundle:nil];
	patientProximalAnamnesisViewController.patient = self.patient;
	BaseViewController *lastChild = (BaseViewController*)[self getLastChildFromParent:self];
	[self parent:self fadeToViewController:patientProximalAnamnesisViewController fromViewController:lastChild superView:self.mainView animateWithDuration:0.2];
	
	[self.goOnButton setImage:[UIImage imageNamed:@"Butt_movetostepthree.png"] forState:UIControlStateNormal];
}

- (void)pushActualRiskFactors
{
	self.index = 2;
	self.remoteAnamnesisButton.selected = NO;
	self.proximalAnamnesisButton.selected = NO;
	self.actualRiskFactorButton.selected = YES;
	
	PatientActualRiskFactorsEdit *patientActualRiskFactorsViewController = [[PatientActualRiskFactorsEdit alloc] initWithNibName:@"PatientActualRiskFactorsEdit" bundle:nil];
	patientActualRiskFactorsViewController.patient = self.patient;
	BaseViewController *lastChild = (BaseViewController*)[self getLastChildFromParent:self];
	[self parent:self fadeToViewController:patientActualRiskFactorsViewController fromViewController:lastChild superView:self.mainView animateWithDuration:0.2];

	[self.goOnButton setImage:[UIImage imageNamed:@"Butt_confirmandcreatevisit.png"] forState:UIControlStateNormal];
}

- (IBAction)goOn:(id)sender
{
	if (self.index == 0)
		[self pushProximalAnamnesis];
	else if (self.index == 1)
		[self pushActualRiskFactors];
	else
	{
		[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"The Medical History has been created and will be available in the Visits section." confirmBlock:^{
			[self goOn];
		}];
	}
}

- (void)goOn
{
	self.patient.hasFirstVisit = [NSNumber numberWithBool:YES];
	[[DBManager getIstance] saveContext];
	PatientListViewController *patientListViewController = (PatientListViewController*)self.parentViewController;
	[patientListViewController pushPatientRoot:self patient:self.patient];
}

- (IBAction)back:(id)sender
{
	NSString *message = [NSString stringWithFormat:@"You are going to lose your data, please finish the process in Step %i in order to save your data.", self.index+1];
	[[AlertViewBlocks getIstance] warningConfirmAlertViewWithMessage:message confirmBlock:^{
		[self parent:self popViewController:self animateWithDuration:0.2];
	} cancelBlock:^{
	}];
}

- (IBAction)viewSettings:(id)sender
{
	SettingsViewController *settingsViewController = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
	[self parent:self addChildViewController:settingsViewController superView:self.view animateWithDuration:0.2];
}

@end
