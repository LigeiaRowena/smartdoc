//
//  PatientRemoteAnamnesisViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 20/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "PatientRemoteAnamnesisViewController.h"
#import "PatientRemoteAnamnesisEdit.h"
#import "PatientMedicalHistoryViewController.h"
#import "FatherDiseaseHistory.h"
#import "MotherDiseaseHistory.h"
#import "RemoteAnamnesis.h"
#import "Vaccination.h"
#import "DiseaseHistory.h"
#import "Operation.h"
#import "PatientData.h"
#import "FatherHistoryLabelCell.h"
#import "MotherHistoryLabelCell.h"
#import "VaccinationLabelCell.h"
#import "DiseaseHistoryCollectionCell.h"
#import "DiseaseHistoryHeader.h"
#import "OperationLabelCell.h"
#import "CustomTableView.h"
#import "PDFExporter.h"

#define cellDrugsHeight 35
#define headerDrugsHeight 80

#define cellDiseaseHistoryHeight 40
#define headerDiseaseHistoryHeight 30

@interface PatientRemoteAnamnesisViewController ()

@property (nonatomic, weak) IBOutlet RoundedLabel *deliveryType;
@property (nonatomic, weak) IBOutlet RoundedLabel *deliveryNote;
@property (nonatomic, weak) IBOutlet RoundedLabel *lifeBirths;
@property (nonatomic, weak) IBOutlet CustomTableView *fatherDiseaseTable;
@property (nonatomic, weak) IBOutlet CustomTableView *motherDiseaseTable;
@property (nonatomic, weak) IBOutlet CustomTableView *vaccinationTable;
@property (nonatomic, weak) IBOutlet UICollectionView *diseasesCollection;
@property (nonatomic, weak) IBOutlet CustomTableView *operationTable;
@property (nonatomic, weak) IBOutlet UIView *bottomView;
@property (nonatomic, weak) IBOutlet FitLabel *note;

@property (nonatomic, strong) NSMutableArray *fatherRiskHistories;
@property (nonatomic, strong) NSMutableArray *motherRiskHistories;
@property (nonatomic, strong) NSMutableArray *vaccinations;
@property (nonatomic, strong) NSMutableArray *diseasesHistories;
@property (nonatomic, strong) NSMutableArray *operations;

@end

#pragma mark - Init PatientRemoteAnamnesisViewController

@implementation PatientRemoteAnamnesisViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// father risks table
	UINib *cellNibfather = [UINib nibWithNibName:@"FatherHistoryLabelCell" bundle:nil];
	[self.fatherDiseaseTable registerNib:cellNibfather forCellReuseIdentifier:[FatherHistoryLabelCell reuseIdentifier]];
	
	// mother risks table
	UINib *cellNibmother = [UINib nibWithNibName:@"MotherHistoryLabelCell" bundle:nil];
	[self.motherDiseaseTable registerNib:cellNibmother forCellReuseIdentifier:[MotherHistoryLabelCell reuseIdentifier]];

	// vaccination table
	UINib *cellNibvacc = [UINib nibWithNibName:@"VaccinationLabelCell" bundle:nil];
	[self.vaccinationTable registerNib:cellNibvacc forCellReuseIdentifier:[VaccinationLabelCell reuseIdentifier]];

	// disease collection
	UINib *headerDisease = [UINib nibWithNibName:@"DiseaseHistoryHeader" bundle:nil];
	[self.diseasesCollection registerNib:headerDisease forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[DiseaseHistoryHeader reuseIdentifier]];
	UINib *cellDisease = [UINib nibWithNibName:@"DiseaseHistoryCollectionCell" bundle:nil];
	[self.diseasesCollection registerNib:cellDisease forCellWithReuseIdentifier:[DiseaseHistoryCollectionCell reuseIdentifier]];
	
	// operation table
	UINib *cellNibop = [UINib nibWithNibName:@"OperationLabelCell" bundle:nil];
	[self.operationTable registerNib:cellNibop forCellReuseIdentifier:[OperationLabelCell reuseIdentifier]];
}


- (void)setup
{
	self.mainscroll.frame = self.view.bounds;
	
	[self.deliveryType setRoundedText:self.patient.remoteAnamnesis.delivery];
	[self.deliveryNote setRoundedText:self.patient.remoteAnamnesis.deliveryNote];
	[self.lifeBirths setRoundedText:[self.patient.remoteAnamnesis.lifeBirds stringValue]];
	[self.note fitText:self.patient.remoteAnamnesis.note];
	
	// father risks table
	self.fatherRiskHistories = [[self.patient.remoteAnamnesis fatherDiseases] allObjects].mutableCopy;
	[self animateFatherRiskTable:(int)[self.fatherRiskHistories count]];

	// mother risks table
	self.motherRiskHistories = [[self.patient.remoteAnamnesis motherDiseases] allObjects].mutableCopy;
	[self animateMotherRiskTable:(int)[self.motherRiskHistories count]];

	// vaccination table
	self.vaccinations = [[self.patient.remoteAnamnesis vaccinations] allObjects].mutableCopy;
	[self animateVaccinationTable:(int)[self.vaccinations count]];
	
	// disease collection
	self.diseasesHistories = [self getDiseasesHistoriesArrayOrdered].mutableCopy;
	[self animateDiseasesTableWithHeight:[self getDiseaseCollectionHeight] + cellDiseaseHistoryHeight];

	// operation table
	self.operations = [[self.patient.remoteAnamnesis operations] allObjects].mutableCopy;
	[self animateOperationsTable:(int)[self.operations count]];
}

- (NSArray*)getDiseasesHistoriesArrayOrdered
{
	[self.diseasesHistories removeAllObjects];
	NSArray *diseases = [[self.patient.remoteAnamnesis diseaseHistory] allObjects];
	NSArray *diseasesOrdered = [diseases sortedArrayUsingComparator: ^(DiseaseHistory *d_1, DiseaseHistory *d_2) {
		NSDate *n_1 = d_1.date;
		NSDate *n_2 = d_2.date;
		return [n_1 compare: n_2];
	}];
	return diseasesOrdered;
}


#pragma mark - Animations

- (float)getDiseaseCollectionHeight
{
    int numberLines = 1;
    if ([self.diseasesHistories count] > 1)
        numberLines = (int)[self.diseasesHistories count]/5 + 1;
    return numberLines*cellDiseaseHistoryHeight + headerDiseaseHistoryHeight;
}

- (void)animateFatherRiskTable:(int)count
{
	int height = 0;
	int delta = 0;
	if (count != 0)
		height = count*cellDrugsHeight + headerDrugsHeight;
	delta = height - self.fatherDiseaseTable.frame.size.height;
	
	if (delta != 0)
	{
		self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
		self.fatherDiseaseTable.frame = CGRectMake(self.fatherDiseaseTable.frame.origin.x, self.fatherDiseaseTable.frame.origin.y, self.fatherDiseaseTable.frame.size.width, height);
		self.motherDiseaseTable.frame = CGRectMake(self.motherDiseaseTable.frame.origin.x, self.motherDiseaseTable.frame.origin.y + delta, self.motherDiseaseTable.frame.size.width, self.motherDiseaseTable.frame.size.height);
		self.vaccinationTable.frame = CGRectMake(self.vaccinationTable.frame.origin.x, self.vaccinationTable.frame.origin.y + delta, self.vaccinationTable.frame.size.width, self.vaccinationTable.frame.size.height);
		self.diseasesCollection.frame = CGRectMake(self.diseasesCollection.frame.origin.x, self.diseasesCollection.frame.origin.y + delta, self.diseasesCollection.frame.size.width, self.diseasesCollection.frame.size.height);
		self.operationTable.frame = CGRectMake(self.operationTable.frame.origin.x, self.operationTable.frame.origin.y + delta, self.operationTable.frame.size.width, self.operationTable.frame.size.height);
		self.bottomView.frame = CGRectMake(self.bottomView.frame.origin.x, self.bottomView.frame.origin.y + delta, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
		[self.fatherDiseaseTable reloadData];
	}
}

- (void)animateMotherRiskTable:(int)count
{
	int height = 0;
	int delta = 0;
	if (count != 0)
		height = count*cellDrugsHeight + headerDrugsHeight;
	delta = height - self.motherDiseaseTable.frame.size.height;
	
	if (delta != 0)
	{
		self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
		self.motherDiseaseTable.frame = CGRectMake(self.motherDiseaseTable.frame.origin.x, self.motherDiseaseTable.frame.origin.y, self.motherDiseaseTable.frame.size.width, height);
		self.vaccinationTable.frame = CGRectMake(self.vaccinationTable.frame.origin.x, self.vaccinationTable.frame.origin.y + delta, self.vaccinationTable.frame.size.width, self.vaccinationTable.frame.size.height);
		self.diseasesCollection.frame = CGRectMake(self.diseasesCollection.frame.origin.x, self.diseasesCollection.frame.origin.y + delta, self.diseasesCollection.frame.size.width, self.diseasesCollection.frame.size.height);
		self.operationTable.frame = CGRectMake(self.operationTable.frame.origin.x, self.operationTable.frame.origin.y + delta, self.operationTable.frame.size.width, self.operationTable.frame.size.height);
		self.bottomView.frame = CGRectMake(self.bottomView.frame.origin.x, self.bottomView.frame.origin.y + delta, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
		[self.motherDiseaseTable reloadData];
	}
}

- (void)animateVaccinationTable:(int)count
{
	int height = 0;
	int delta = 0;
	if (count != 0)
		height = count*cellDrugsHeight + headerDrugsHeight;
	delta = height - self.vaccinationTable.frame.size.height;
	
	if (delta != 0)
	{
		self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
		self.vaccinationTable.frame = CGRectMake(self.vaccinationTable.frame.origin.x, self.vaccinationTable.frame.origin.y, self.vaccinationTable.frame.size.width, height);
		self.diseasesCollection.frame = CGRectMake(self.diseasesCollection.frame.origin.x, self.diseasesCollection.frame.origin.y + delta, self.diseasesCollection.frame.size.width, self.diseasesCollection.frame.size.height);
		self.operationTable.frame = CGRectMake(self.operationTable.frame.origin.x, self.operationTable.frame.origin.y + delta, self.operationTable.frame.size.width, self.operationTable.frame.size.height);
		self.bottomView.frame = CGRectMake(self.bottomView.frame.origin.x, self.bottomView.frame.origin.y + delta, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
		[self.vaccinationTable reloadData];
	}
}

- (void)animateOperationsTable:(int)count
{
	int height = 0;
	int delta = 0;
	if (count != 0)
		height = count*cellDrugsHeight + headerDrugsHeight;
	delta = height - self.operationTable.frame.size.height;
	
	if (delta != 0)
	{
		self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
		self.operationTable.frame = CGRectMake(self.operationTable.frame.origin.x, self.operationTable.frame.origin.y, self.operationTable.frame.size.width, height);
		self.bottomView.frame = CGRectMake(self.bottomView.frame.origin.x, self.bottomView.frame.origin.y + delta, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
		[self.operationTable reloadData];
	}
}

- (void)animateDiseasesTableWithHeight:(float)height
{
	int delta = height - self.diseasesCollection.frame.size.height;
	
	if (delta != 0)
	{
		self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
		self.diseasesCollection.frame = CGRectMake(self.diseasesCollection.frame.origin.x, self.diseasesCollection.frame.origin.y, self.diseasesCollection.frame.size.width, height);
		self.operationTable.frame = CGRectMake(self.operationTable.frame.origin.x, self.operationTable.frame.origin.y + delta, self.operationTable.frame.size.width, self.operationTable.frame.size.height);
		self.bottomView.frame = CGRectMake(self.bottomView.frame.origin.x, self.bottomView.frame.origin.y + delta, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
	}
	[self.diseasesCollection reloadData];
}

#pragma mark - Actions

- (IBAction)print:(id)sender
{
	NSString *title = [NSString stringWithFormat:@"RemoteAnamnesis for Patient: %@ %@ %@", self.patient.patientData.name, self.patient.patientData.middleName, self.patient.patientData.lastName];
	[PDFExporter clearDocuments];
	NSString *pdfFileName = [PDFExporter getRemoteAnamnesisPDFWithTitle:title remoteAnamnesis:self.patient.remoteAnamnesis diseases:[self getDiseasesHistoriesArrayOrdered]];
	[self printPDF:pdfFileName anchor:sender];
}

- (IBAction)send:(id)sender
{
	NSString *title = [NSString stringWithFormat:@"RemoteAnamnesis for Patient: %@ %@ %@", self.patient.patientData.name, self.patient.patientData.middleName, self.patient.patientData.lastName];
	[PDFExporter clearDocuments];
	NSString *pdfFileName = [PDFExporter getRemoteAnamnesisPDFWithTitle:title remoteAnamnesis:self.patient.remoteAnamnesis diseases:[self getDiseasesHistoriesArrayOrdered]];
	[self sendPDFtitle:title path:pdfFileName];
}

- (IBAction)edit:(id)sender
{
	PatientRemoteAnamnesisEdit *vc = [[PatientRemoteAnamnesisEdit alloc] initWithNibName:@"PatientRemoteAnamnesisEdit" bundle:nil];
	PatientMedicalHistoryViewController *parent = (PatientMedicalHistoryViewController*)self.parentViewController;
	vc.isEditing = YES;
	vc.patient = self.patient;
	[self parent:parent fadeToViewController:vc fromViewController:self superView:parent.mainView animateWithDuration:0.2];
}

- (NSMutableString*)getRemoteAnamnesisMultiText
{
	NSMutableString *multiText = @"".mutableCopy;
	[multiText appendFormat:@"Remote Anamnesis"];
	[multiText appendFormat:@"\nDelivery: %@ note: %@", self.patient.remoteAnamnesis.delivery, self.patient.remoteAnamnesis.deliveryNote];
	[multiText appendFormat:@"\nCongenital Diseases"];
	[multiText appendFormat:@"\nNumber of life births in the family: %@", self.patient.remoteAnamnesis.lifeBirds];
	for (FatherDiseaseHistory *father in self.fatherRiskHistories)
		[multiText appendFormat:@"\nFather Disease History name: %@ - type: %@", father.type, father.note];
	for (MotherDiseaseHistory *mother in self.motherRiskHistories)
		[multiText appendFormat:@"\nMother Disease History name: %@ - type: %@", mother.type, mother.note];
	[multiText appendFormat:@"\nVaccinations"];
	for (Vaccination *vaccination in self.vaccinations)
		[multiText appendFormat:@"\nVaccination name: %@ - date: %@ - note: %@", vaccination.type, [self.formatter stringFromDate:vaccination.date], vaccination.note];
	[multiText appendFormat:@"\nDisease History"];
	for (DiseaseHistory *disease in self.diseasesHistories)
		if ([disease.status boolValue])
			[multiText appendFormat:@"\nDisease name: %@", disease.name];
	[multiText appendFormat:@"\nSurgical Operation"];
	for (Operation *operation in self.operations)
		[multiText appendFormat:@"\nOperation name: %@ - note: %@", operation.name, operation.note];
	[multiText appendFormat:@"\nNote"];
	[multiText appendFormat:@"%@", self.patient.remoteAnamnesis.note];

	return multiText;
}

#pragma mark - UICollectionViewDataSource

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
	UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader)
	{
        DiseaseHistoryHeader *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[DiseaseHistoryHeader reuseIdentifier] forIndexPath:indexPath];
		header.backgroundColor = lightLightBlueColorBackground();
		header.header.textColor = lightGrayBlueColor();
		header.header.font = OpenSansBold(21);
		header.header.text = @"PATIENT DISEASE HISTORY";
		header.header.frame = CGRectMake(15, 15, header.header.frame.size.width, header.header.frame.size.height);
        reusableview = header;
    }
	
	return reusableview;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
	return CGSizeMake(768, 50);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
	return CGSizeMake(145, 40);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
	return UIEdgeInsetsMake(0, 20, 0, 20);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
	return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
	return 0;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return [self.diseasesHistories count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	DiseaseHistoryCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[DiseaseHistoryCollectionCell reuseIdentifier] forIndexPath:indexPath];
	cell.index = [NSNumber numberWithInt:(int)indexPath.row];
	DiseaseHistory *disease = self.diseasesHistories[indexPath.row];
	[cell setDiseaseHistoryNotEditable:disease];
	cell.backgroundColor = lightLightBlueColorBackground();
	
	return cell;
}

#pragma mark - UITableViewDataSource/UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return headerDrugsHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return cellDrugsHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 768, headerDrugsHeight)];
	view.backgroundColor = lightLightBlueColorBackground();
	
	if (tableView == self.fatherDiseaseTable)
	{
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 300, 21)];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = lightGrayBlueColor();
		label.font = OpenSansBold(21);
		label.text = @"FATHER DISEASE HISTORY";
		[view addSubview:label];
		
		UILabel *type = [[UILabel alloc] initWithFrame:CGRectMake(15, 51, 223, 21)];
		type.backgroundColor = [UIColor clearColor];
		type.textColor = lightBlueColor();
		type.font = OpenSansBold(16);
		type.text = @"TYPE";
		[view addSubview:type];
		
		UILabel *details = [[UILabel alloc] initWithFrame:CGRectMake(267, 51, 200, 21)];
		details.backgroundColor = [UIColor clearColor];
		details.textColor = lightBlueColor();
		details.font = OpenSansBold(16);
		details.text = @"NOTE";
		[view addSubview:details];
	}

	else if (tableView == self.motherDiseaseTable)
	{
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 300, 21)];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = lightGrayBlueColor();
		label.font = OpenSansBold(21);
		label.text = @"MOTHER DISEASE HISTORY";
		[view addSubview:label];
		
		UILabel *type = [[UILabel alloc] initWithFrame:CGRectMake(15, 51, 223, 21)];
		type.backgroundColor = [UIColor clearColor];
		type.textColor = lightBlueColor();
		type.font = OpenSansBold(16);
		type.text = @"TYPE";
		[view addSubview:type];
		
		UILabel *details = [[UILabel alloc] initWithFrame:CGRectMake(267, 51, 200, 21)];
		details.backgroundColor = [UIColor clearColor];
		details.textColor = lightBlueColor();
		details.font = OpenSansBold(16);
		details.text = @"NOTE";
		[view addSubview:details];
	}
	
	else if (tableView == self.vaccinationTable)
	{
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 189, 21)];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = lightGrayBlueColor();
		label.font = OpenSansBold(21);
		label.text = @"VACCINATIONS";
		[view addSubview:label];
		
		UILabel *type = [[UILabel alloc] initWithFrame:CGRectMake(15, 51, 223, 21)];
		type.backgroundColor = [UIColor clearColor];
		type.textColor = lightBlueColor();
		type.font = OpenSansBold(16);
		type.text = @"TYPE";
		[view addSubview:type];
		
		UILabel *date = [[UILabel alloc] initWithFrame:CGRectMake(184, 51, 200, 21)];
		date.backgroundColor = [UIColor clearColor];
		date.textColor = lightBlueColor();
		date.font = OpenSansBold(16);
		date.text = @"BIRTH DATE";
		[view addSubview:date];

		UILabel *note = [[UILabel alloc] initWithFrame:CGRectMake(348, 51, 200, 21)];
		note.backgroundColor = [UIColor clearColor];
		note.textColor = lightBlueColor();
		note.font = OpenSansBold(16);
		note.text = @"NOTE";
		[view addSubview:note];
	}
	
	else if (tableView == self.operationTable)
	{
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 300, 21)];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = lightBlueColor();
		label.font = OpenSansBold(21);
		label.text = @"SURGICAL OPERATION";
		[view addSubview:label];
		
		UILabel *type = [[UILabel alloc] initWithFrame:CGRectMake(15, 51, 223, 21)];
		type.backgroundColor = [UIColor clearColor];
		type.textColor = lightBlueColor();
		type.font = OpenSansBold(16);
		type.text = @"OPERATION";
		[view addSubview:type];
		
		UILabel *details = [[UILabel alloc] initWithFrame:CGRectMake(348, 51, 200, 21)];
		details.backgroundColor = [UIColor clearColor];
		details.textColor = lightBlueColor();
		details.font = OpenSansBold(16);
		details.text = @"NOTE";
		[view addSubview:details];
	}
		
	return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (tableView == self.fatherDiseaseTable)
		return [self.fatherRiskHistories count];
	else if (tableView == self.motherDiseaseTable)
		return [self.motherRiskHistories count];
	else if (tableView == self.vaccinationTable)
		return [self.vaccinations count];
	else
		return [self.operations count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;
	
	if (tableView == self.fatherDiseaseTable)
	{
		FatherHistoryLabelCell *customcell = [tableView dequeueReusableCellWithIdentifier:[FatherHistoryLabelCell reuseIdentifier]];
		FatherDiseaseHistory *father = self.fatherRiskHistories[indexPath.row];
		[customcell setContent:father];
		cell = customcell;
	}
	
	else if (tableView == self.motherDiseaseTable)
	{
		MotherHistoryLabelCell *customcell = [tableView dequeueReusableCellWithIdentifier:[MotherHistoryLabelCell reuseIdentifier]];
		MotherDiseaseHistory *mother = self.motherRiskHistories[indexPath.row];
		[customcell setContent:mother];
		cell = customcell;
	}
	
	else if (tableView == self.vaccinationTable)
	{
		VaccinationLabelCell *customcell = [tableView dequeueReusableCellWithIdentifier:[VaccinationLabelCell reuseIdentifier]];
		Vaccination *vaccination = self.vaccinations[indexPath.row];
		[customcell setContent:vaccination];
		cell = customcell;
	}

	else if (tableView == self.operationTable)
	{
		OperationLabelCell *customcell = [tableView dequeueReusableCellWithIdentifier:[OperationLabelCell reuseIdentifier]];
		Operation *operation = self.operations[indexPath.row];
		[customcell setContent:operation];
		cell = customcell;
	}
	
    return cell;
}

@end
