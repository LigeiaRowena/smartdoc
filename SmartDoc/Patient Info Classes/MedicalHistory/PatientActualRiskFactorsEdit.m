//
//  PatientActualRiskFactorsViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 20/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "PatientActualRiskFactorsEdit.h"
#import "PatientActualRiskFactorsViewController.h"
#import "PatientMedicalHistoryViewController.h"
#import "ActualRiskFactor.h"
#import "OtherRiskFactor.h"
#import "Allergy.h"

#define cellAllergiesHeight 80
#define headerOtherRiskHeight 30
#define cellOtherRiskHistoryHeight 40

@interface PatientActualRiskFactorsEdit ()

@property (nonatomic, weak) IBOutlet PickerField *smokingText;
@property (nonatomic, weak) IBOutlet MenuView *smokingMenu;
@property (nonatomic, weak) IBOutlet PickerField *smokingTimeText;
@property (nonatomic, weak) IBOutlet CustomTextField *smokingNoteText;
@property (nonatomic, weak) IBOutlet UICollectionView *otherRiskCollection;
@property (nonatomic, weak) IBOutlet UIView *otherRiskHeader;
@property (nonatomic, weak) IBOutlet CustomTextField *otherRiskNote;
@property (nonatomic, weak) IBOutlet UIView *allergyHeader;
@property (nonatomic, weak) IBOutlet PickerField *allergyText;
@property (nonatomic, weak) IBOutlet CustomTextField *allergyNoteText;
@property (nonatomic, weak) IBOutlet UITableView *allergiesTable;
@property (nonatomic, weak) IBOutlet CustomTextView *specialDrugsText;
@property (nonatomic, weak) IBOutlet CustomTextView *otherText;
@property (weak, nonatomic) IBOutlet UIView *saveView;

@property (nonatomic, strong) NSMutableArray *otherRisks;
@property (nonatomic, strong) NSMutableArray *allergies;

@end

#pragma mark - Init PatientActualRiskFactorsEdit

@implementation PatientActualRiskFactorsEdit

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// other risks collection
	UINib *headerCollection = [UINib nibWithNibName:@"DiseaseHistoryHeader" bundle:nil];
	[self.otherRiskCollection registerNib:headerCollection forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[DiseaseHistoryHeader reuseIdentifier]];
	UINib *cellDisease = [UINib nibWithNibName:@"DiseaseHistoryCollectionCell" bundle:nil];
	[self.otherRiskCollection registerNib:cellDisease forCellWithReuseIdentifier:[DiseaseHistoryCollectionCell reuseIdentifier]];
	
	// allergy table
	UINib *cellNiballergy = [UINib nibWithNibName:@"AllergyCell" bundle:nil];
	[self.allergiesTable registerNib:cellNiballergy forCellReuseIdentifier:[AllergyCell reuseIdentifier]];
}

- (void)setup
{
	self.mainscroll.frame = self.view.bounds;
	
	// save button mode
	if (self.isEditing) {
		self.saveView.hidden = NO;
	} else {
		self.saveView.hidden = YES;
	}

	self.smokingText.textField.text = self.patient.actualRiskFactor.smoking;
	self.smokingTimeText.textField.text = self.patient.actualRiskFactor.smokingTime;
	self.smokingNoteText.text = self.patient.actualRiskFactor.smokingNote;
	self.otherRiskNote.text = self.patient.actualRiskFactor.otherRiskFactorNote;
	self.specialDrugsText.textView.text = self.patient.actualRiskFactor.specialDrugs;
	self.otherText.textView.text = self.patient.actualRiskFactor.other;

	// smoking time menu
	int index = [self.patient.actualRiskFactor.smokingFrequency intValue];
	if (index > 0)
		[self.smokingMenu selectItemAtIndex:index-1];
	
	// other risks collection
	self.otherRisks = [self getOtherRisksArrayOrdered].mutableCopy;
	self.otherRiskCollection.frame = CGRectMake(self.otherRiskCollection.frame.origin.x, self.otherRiskCollection.frame.origin.y, self.otherRiskCollection.frame.size.width, 0);
	[self animateOtherRiskTableWithHeight:[self getOtherRiskCollectionHeight] + cellOtherRiskHistoryHeight];
	
	// allergies table
	self.allergies = [[self.patient.actualRiskFactor allergies] allObjects].mutableCopy;
	[self animateAllergyTableWithDelta:((int)[self.allergies count]*cellAllergiesHeight)];
}

- (NSArray*)getOtherRisksArrayOrdered
{
	NSArray *array = [[self.patient.actualRiskFactor otherRiskFactors] allObjects];
	NSArray *arrayOrdered = [array sortedArrayUsingComparator: ^(id id_1, id id_2) {
		OtherRiskFactor *m_1 = (OtherRiskFactor*) id_1;
		OtherRiskFactor *m_2 = (OtherRiskFactor*) id_2;
		NSString *d_1 = m_1.name;
		NSString *d_2 = m_2.name;
		return [d_1 compare: d_2];
	}];
	return arrayOrdered;
}


#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	return [super textFieldDidBeginEditing:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	
	NSString *text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	if (textField == self.smokingNoteText)
	{
		self.patient.actualRiskFactor.smokingNote = text;
		[[DBManager getIstance] saveContext];
	}
	else if (textField == self.otherRiskNote)
	{
		self.patient.actualRiskFactor.otherRiskFactorNote = text;
		[[DBManager getIstance] saveContext];
	}
	else if ([textField.accessibilityIdentifier isEqualToString:[AllergyCell reuseIdentifier]])
	{
		int index = (int)textField.tag;
		Allergy *allergy = self.allergies[index];
		allergy.note = text;
		[[DBManager getIstance] saveContext];
		self.allergies = [[self.patient.actualRiskFactor allergies] allObjects].mutableCopy;
	}
	
	[super textFieldDidEndEditing:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	return [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
}


#pragma mark - CustomTextViewDelegate

- (void)customTextViewDidBeginEditing:(CustomTextView *)textView
{
	[super customTextViewDidBeginEditing:textView];
}

- (void)customTextViewDidEndEditing:(CustomTextView *)textView
{
	NSString *text = [textView.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

	if (textView == self.specialDrugsText)
	{
		self.patient.actualRiskFactor.specialDrugs = text;
		[[DBManager getIstance] saveContext];
	}
	else if (textView == self.otherText)
	{
		self.patient.actualRiskFactor.other = text;
		[[DBManager getIstance] saveContext];
	}
	
	[super customTextViewDidEndEditing:textView];
}

- (void)customTextViewDidChange:(CustomTextView *)textView
{
	[super customTextViewDidChange:textView];
}

#pragma mark - PickerViewControllerDelegate Methods

- (void)pickerViewControllerSetText:(NSString*)text pickerField:(PickerField*)pickerField
{
	pickerField.textField.text = text;

	if (pickerField == self.smokingTimeText)
	{
		self.patient.actualRiskFactor.smokingTime = text;
		[[DBManager getIstance] saveContext];
	}
	
	else if (pickerField == self.smokingText)
	{
		self.patient.actualRiskFactor.smoking = text;
		[[DBManager getIstance] saveContext];
	}
	
	else if ([pickerField.accessibilityIdentifier isEqualToString:[AllergyCell reuseIdentifier]])
	{
		int index = (int)pickerField.tag;
		Allergy *allergy = self.allergies[index];
		allergy.name = text;
		[[DBManager getIstance] saveContext];
		self.allergies = [[self.patient.actualRiskFactor allergies] allObjects].mutableCopy;
	}
}

#pragma mark - PickerFieldDelegate Methods

- (void)tapOnPickerFieldButton:(PickerField*)pickerField
{
	PickerViewController *pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
	pickerViewController.pickerField = pickerField;
	pickerViewController.delegate = self;
	
	if (pickerField == self.smokingText)
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:SmokingData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = SmokingData;
	}
	
	else if (pickerField == self.smokingTimeText)
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"FromHowLong"];
		for(NSString *title in list)
			[pickerViewController.data addObject:title];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = StaticField;
	}
	
	else if (pickerField == self.allergyText || [pickerField.accessibilityIdentifier isEqualToString:[AllergyCell reuseIdentifier]])
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:AllergyData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = AllergyData;
	}
	
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerViewController];
	[self.popover setPopoverContentSize:pickerViewController.view.frame.size];
	[pickerViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)pickerFieldDidBeginEditing:(PickerField *)pickerField
{
	[super pickerFieldDidBeginEditing:pickerField];
}


#pragma mark - MenuViewDelegate

- (void)menuDidSelectItemAtIndex:(int)index
{
	self.patient.actualRiskFactor.smokingFrequency = [NSString stringWithFormat:@"%i", index+1];
	[[DBManager getIstance] saveContext];
}

#pragma mark - Animations

- (float)getOtherRiskCollectionHeight
{
	CGSize size = self.otherRiskCollection.collectionViewLayout.collectionViewContentSize;
	if (size.height != 0)
		return size.height + headerOtherRiskHeight;
	else
	{
		int numberLines = 1;
		if ([self.otherRisks count] > 1)
			numberLines = (int)[self.otherRisks count]/5 + 1;
		return numberLines*cellOtherRiskHistoryHeight + headerOtherRiskHeight;
	}
}

- (void)animateOtherRiskTableWithHeight:(float)height
{
	float delta = height - self.otherRiskCollection.frame.size.height;
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.otherRiskCollection.frame = CGRectMake(self.otherRiskCollection.frame.origin.x, self.otherRiskCollection.frame.origin.y, self.otherRiskCollection.frame.size.width, height);
		self.otherRiskHeader.frame = CGRectMake(self.otherRiskHeader.frame.origin.x, self.otherRiskHeader.frame.origin.y + delta, self.otherRiskHeader.frame.size.width, self.otherRiskHeader.frame.size.height);
		self.allergyHeader.frame = CGRectMake(self.allergyHeader.frame.origin.x, self.allergyHeader.frame.origin.y + delta, self.allergyHeader.frame.size.width, self.allergyHeader.frame.size.height);
		self.allergiesTable.frame = CGRectMake(self.allergiesTable.frame.origin.x, self.allergiesTable.frame.origin.y + delta, self.allergiesTable.frame.size.width, self.allergiesTable.frame.size.height);
		self.specialDrugsText.frame = CGRectMake(self.specialDrugsText.frame.origin.x, self.specialDrugsText.frame.origin.y + delta, self.specialDrugsText.frame.size.width, self.specialDrugsText.frame.size.height);
		self.otherText.frame = CGRectMake(self.otherText.frame.origin.x, self.otherText.frame.origin.y + delta, self.otherText.frame.size.width, self.otherText.frame.size.height);
	} completion:^(BOOL s){
		[self.otherRiskCollection reloadData];
	}];
}

- (void)animateAllergyTableWithDelta:(int)delta
{
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.allergiesTable.frame = CGRectMake(self.allergiesTable.frame.origin.x, self.allergiesTable.frame.origin.y, self.allergiesTable.frame.size.width, self.allergiesTable.frame.size.height + delta);
		self.specialDrugsText.frame = CGRectMake(self.specialDrugsText.frame.origin.x, self.specialDrugsText.frame.origin.y + delta, self.specialDrugsText.frame.size.width, self.specialDrugsText.frame.size.height);
		self.otherText.frame = CGRectMake(self.otherText.frame.origin.x, self.otherText.frame.origin.y + delta, self.otherText.frame.size.width, self.otherText.frame.size.height);
		} completion:^(BOOL s){
		[self.allergiesTable reloadData];
	}];
}

#pragma mark - Actions


- (IBAction)save:(id)sender
{
	PatientActualRiskFactorsViewController *vc = [[PatientActualRiskFactorsViewController alloc] initWithNibName:@"PatientActualRiskFactorsViewController" bundle:nil];
	PatientMedicalHistoryViewController *parent = (PatientMedicalHistoryViewController*)self.parentViewController;
	vc.patient = self.patient;
	[self parent:parent fadeToViewController:vc fromViewController:self superView:parent.mainView animateWithDuration:0.2];
}


- (IBAction)addAllergy:(id)sender
{
	if (![BaseViewController isEmptyString:self.allergyText.textField.text])
	{
		[[DBManager getIstance] createAllergyForActualRiskFactor:self.patient.actualRiskFactor name:self.allergyText.textField.text note:self.allergyNoteText.text];
		self.allergies = [self.patient.actualRiskFactor.allergies allObjects].mutableCopy;
		self.allergyText.textField.text = @"";
		self.allergyNoteText.text = @"";
		[self animateAllergyTableWithDelta:cellAllergiesHeight];
	}
	else
	{
		[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please provide a value for the Details filled" confirmBlock:^{
			self.allergyText.textField.text = @"";
			self.allergyNoteText.text = @"";
		}];
	}
}

#pragma mark - UITableViewDataSource/UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.allergies count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;
	
	AllergyCell *customcell = [tableView dequeueReusableCellWithIdentifier:[AllergyCell reuseIdentifier]];
	customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
	customcell.cellDelegate = self;
	Allergy *allergy = self.allergies[indexPath.row];
	[customcell setContent:allergy];
	cell = customcell;
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)doActionForKey:(NSString*)actionKey actionData:(id)actionData
{
	if ([actionKey isEqualToString:@"deleteAllergy"])
	{
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"You are going to delete this information: are you sure?" confirmBlock:^{
			int index = [actionData[@"index"] intValue];
			Allergy *allergy = self.allergies[index];
			[[DBManager getIstance] deleteObject:allergy];
			self.allergies = [[self.patient.actualRiskFactor allergies] allObjects].mutableCopy;
			[self animateAllergyTableWithDelta:-cellAllergiesHeight];
		} cancelBlock:^{
		}];
	}
}

#pragma mark - UICollectionViewDataSource

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
	UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader)
	{
        DiseaseHistoryHeader *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[DiseaseHistoryHeader reuseIdentifier] forIndexPath:indexPath];
		header.header.text = @"OTHER RISK FACTORS";
        reusableview = header;
    }
	
	return reusableview;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
	return CGSizeMake(728, 30);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
	return CGSizeMake([self getCollectionWidth:indexPath], 40);
}

- (float)getCollectionWidth:(NSIndexPath*)indexPath
{
	OtherRiskFactor *risk = self.otherRisks[indexPath.row];
	NSDictionary *attributes = @{NSFontAttributeName: OpenSansRegular(16)};
	CGSize size = [risk.name boundingRectWithSize:CGSizeMake(145-32, 40) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
	return size.width + 50;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
	return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
	return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
	return 0;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return [self.otherRisks count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	DiseaseHistoryCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[DiseaseHistoryCollectionCell reuseIdentifier] forIndexPath:indexPath];
	cell.index = [NSNumber numberWithInt:(int)indexPath.row];
	cell.cellDelegate = self;
	OtherRiskFactor *risk = self.otherRisks[indexPath.row];
	[cell selOtherRiskFactor:risk];
	
	return cell;
}

- (void)collectionDoActionForKey:(NSString*)actionKey actionData:(id)actionData
{
	if ([actionKey isEqualToString:@"changeStatus"])
	{
		int index = [actionData[@"index"] intValue];
		OtherRiskFactor *risk = self.otherRisks[index];
		BOOL status = ![risk.status boolValue];
		risk.status = [NSNumber numberWithBool:status];
		[[DBManager getIstance] saveContext];
		self.otherRisks = [self getOtherRisksArrayOrdered].mutableCopy;
	}
}

@end
