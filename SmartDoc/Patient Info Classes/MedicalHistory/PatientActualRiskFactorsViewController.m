//
//  PatientActualRiskFactorsViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 20/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "PatientActualRiskFactorsViewController.h"
#import "PatientActualRiskFactorsEdit.h"
#import "PatientMedicalHistoryViewController.h"
#import "ActualRiskFactor.h"
#import "OtherRiskFactor.h"
#import "Allergy.h"
#import "PatientData.h"
#import "DiseaseHistoryHeader.h"
#import "DiseaseHistoryCollectionCell.h"
#import "AllergyLabelCell.h"
#import "CustomTableView.h"
#import "PDFExporter.h"

#define cellDrugsHeight 35
#define headerDrugsHeight 80

#define headerOtherRiskHeight 30
#define cellOtherRiskHistoryHeight 40

@interface PatientActualRiskFactorsViewController ()

@property (nonatomic, weak) IBOutlet RoundedLabel *smoking;
@property (nonatomic, weak) IBOutlet RoundedLabel *smokingFrequency;
@property (nonatomic, weak) IBOutlet RoundedLabel *smokingTime;
@property (nonatomic, weak) IBOutlet RoundedLabel *smokingNote;
@property (nonatomic, weak) IBOutlet UICollectionView *otherRiskCollection;
@property (nonatomic, weak) IBOutlet UIView *noteView;
@property (nonatomic, weak) IBOutlet FitLabel *note;
@property (nonatomic, weak) IBOutlet CustomTableView *allergiesTable;
@property (nonatomic, weak) IBOutlet UIView *specialDrugsView;
@property (nonatomic, weak) IBOutlet FitLabel *specialDrugs;
@property (nonatomic, weak) IBOutlet UIView *otherView;
@property (nonatomic, weak) IBOutlet FitLabel *other;

@property (nonatomic, strong) NSMutableArray *otherRisks;
@property (nonatomic, strong) NSMutableArray *allergies;

@end

#pragma mark - Init PatientActualRiskFactorsViewController

@implementation PatientActualRiskFactorsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// other risks collection
	UINib *headerCollection = [UINib nibWithNibName:@"DiseaseHistoryHeader" bundle:nil];
	[self.otherRiskCollection registerNib:headerCollection forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[DiseaseHistoryHeader reuseIdentifier]];
	UINib *cellDisease = [UINib nibWithNibName:@"DiseaseHistoryCollectionCell" bundle:nil];
	[self.otherRiskCollection registerNib:cellDisease forCellWithReuseIdentifier:[DiseaseHistoryCollectionCell reuseIdentifier]];
	
	
	// allergy table
	UINib *cellNiballergy = [UINib nibWithNibName:@"AllergyLabelCell" bundle:nil];
	[self.allergiesTable registerNib:cellNiballergy forCellReuseIdentifier:[AllergyLabelCell reuseIdentifier]];
}

- (void)setup
{
	self.mainscroll.frame = self.view.bounds;
	
	[self.smoking setRoundedText:self.patient.actualRiskFactor.smoking];
	if ([self.patient.actualRiskFactor.smokingFrequency isEqualToString:@"1"])
		[self.smokingFrequency setRoundedText:@"1 to 5"];
	else if ([self.patient.actualRiskFactor.smokingFrequency isEqualToString:@"2"])
		[self.smokingFrequency setRoundedText:@"5 to 15"];
	else if ([self.patient.actualRiskFactor.smokingFrequency isEqualToString:@"3"])
		[self.smokingFrequency setRoundedText:@"More than 20"];
	else
		[self.smokingFrequency setRoundedText:@""];
	[self.smokingTime setRoundedText:self.patient.actualRiskFactor.smokingTime];
	[self.smokingNote setRoundedText:self.patient.actualRiskFactor.smokingNote];
	[self.note fitText:self.patient.actualRiskFactor.otherRiskFactorNote];
	[self.specialDrugs fitText:self.patient.actualRiskFactor.specialDrugs];
	[self.other fitText:self.patient.actualRiskFactor.other];
	
	// other risks collection
	self.otherRisks = [self getOtherRisksArrayOrdered].mutableCopy;
	[self.otherRiskCollection reloadData];

	// allergies table
	self.allergies = [[self.patient.actualRiskFactor allergies] allObjects].mutableCopy;
	[self animateAllergyTable:(int)[self.allergies count]];
}

- (NSArray*)getOtherRisksArrayOrdered
{
	NSArray *array = [[self.patient.actualRiskFactor otherRiskFactors] allObjects];
	NSArray *arrayOrdered = [array sortedArrayUsingComparator: ^(id id_1, id id_2) {
		OtherRiskFactor *m_1 = (OtherRiskFactor*) id_1;
		OtherRiskFactor *m_2 = (OtherRiskFactor*) id_2;
		NSString *d_1 = m_1.name;
		NSString *d_2 = m_2.name;
		return [d_1 compare: d_2];
	}];
	return arrayOrdered;
}


#pragma mark - Animations

- (void)animateAllergyTable:(int)count
{
	int height = 0;
	int delta = 0;
	if (count != 0)
		height = count*cellDrugsHeight + headerDrugsHeight;
	delta = height - self.allergiesTable.frame.size.height;
	
	if (delta != 0)
	{
		self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
		self.allergiesTable.frame = CGRectMake(self.allergiesTable.frame.origin.x, self.allergiesTable.frame.origin.y, self.allergiesTable.frame.size.width, height);
		self.specialDrugsView.frame = CGRectMake(self.specialDrugsView.frame.origin.x, self.specialDrugsView.frame.origin.y + delta, self.specialDrugsView.frame.size.width, self.specialDrugsView.frame.size.height);
		self.otherView.frame = CGRectMake(self.otherView.frame.origin.x, self.otherView.frame.origin.y + delta, self.otherView.frame.size.width, self.otherView.frame.size.height);
		[self.allergiesTable reloadData];
	}
}

#pragma mark - Actions

- (IBAction)print:(id)sender
{
	NSString *title = [NSString stringWithFormat:@"Actual Risk Factors for Patient: %@ %@ %@", self.patient.patientData.name, self.patient.patientData.middleName, self.patient.patientData.lastName];
	[PDFExporter clearDocuments];
	NSString *pdfFileName = [PDFExporter getActualRiskFactorPDFWithTitle:title actualRiskFactor:self.patient.actualRiskFactor];
	[self printPDF:pdfFileName anchor:sender];
}

- (IBAction)send:(id)sender
{
	NSString *title = [NSString stringWithFormat:@"Actual Risk Factors for Patient: %@ %@ %@", self.patient.patientData.name, self.patient.patientData.middleName, self.patient.patientData.lastName];
	[PDFExporter clearDocuments];
	NSString *pdfFileName = [PDFExporter getActualRiskFactorPDFWithTitle:title actualRiskFactor:self.patient.actualRiskFactor];
	[self sendPDFtitle:title path:pdfFileName];
}

- (IBAction)edit:(id)sender
{
	PatientActualRiskFactorsEdit *vc = [[PatientActualRiskFactorsEdit alloc] initWithNibName:@"PatientActualRiskFactorsEdit" bundle:nil];
	PatientMedicalHistoryViewController *parent = (PatientMedicalHistoryViewController*)self.parentViewController;
	vc.patient = self.patient;
	vc.isEditing = YES;
	[self parent:parent fadeToViewController:vc fromViewController:self superView:parent.mainView animateWithDuration:0.2];
}

- (NSMutableString*)getActualRiskFactorsMultiText
{
	NSMutableString *multiText = @"".mutableCopy;
	[multiText appendFormat:@"\nSmoking: %@", self.patient.actualRiskFactor.smoking];
	[multiText appendFormat:@"\nSmoking Frequency: %@", self.patient.actualRiskFactor.smokingFrequency];
	[multiText appendFormat:@"\nSmoking Time: %@", self.patient.actualRiskFactor.smokingTime];
	[multiText appendFormat:@"\nSmoking Note: %@", self.patient.actualRiskFactor.smokingNote];
	[multiText appendString:@"\nOTHER RISK FACTORS:"];
	for (OtherRiskFactor *risk in self.otherRisks)
		if ([risk.status boolValue])
			[multiText appendFormat:@"\n%@", risk.name];
	[multiText appendFormat:@"\nOther Risk Note: %@", self.patient.actualRiskFactor.otherRiskFactorNote];
	[multiText appendString:@"\nALLERGIES:"];
	for (Allergy *allergy in self.allergies)
		[multiText appendFormat:@"\nAllergy name: %@ - note: %@", allergy.name, allergy.note];
	[multiText appendFormat:@"\nSpecial Drugs: %@", self.patient.actualRiskFactor.specialDrugs];
	[multiText appendFormat:@"\nOther: %@", self.patient.actualRiskFactor.other];

	
	return multiText;
}

#pragma mark - UITableViewDataSource/UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return headerDrugsHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return cellDrugsHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.allergies count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 768, headerDrugsHeight)];
	view.backgroundColor = lightLightBlueColorBackground();
	
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 230, 21)];
	label.backgroundColor = [UIColor clearColor];
	label.textColor = lightGrayBlueColor();
	label.font = OpenSansBold(21);
	label.text = @"ALLERGIES";
	[view addSubview:label];
	
	UILabel *type = [[UILabel alloc] initWithFrame:CGRectMake(15, 51, 200, 21)];
	type.backgroundColor = [UIColor clearColor];
	type.textColor = lightBlueColor();
	type.font = OpenSansBold(16);
	type.text = @"TYPE";
	[view addSubview:type];
	
	UILabel *note = [[UILabel alloc] initWithFrame:CGRectMake(348, 51, 506, 21)];
	note.backgroundColor = [UIColor clearColor];
	note.textColor = lightBlueColor();
	note.font = OpenSansBold(16);
	note.text = @"NOTE";
	[view addSubview:note];
	
	return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;
	
	AllergyLabelCell *customcell = [tableView dequeueReusableCellWithIdentifier:[AllergyLabelCell reuseIdentifier]];
	Allergy *allergy = self.allergies[indexPath.row];
	[customcell setContent:allergy];
	cell = customcell;
	
    return cell;
}


#pragma mark - UICollectionViewDataSource

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
	UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader)
	{
        DiseaseHistoryHeader *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[DiseaseHistoryHeader reuseIdentifier] forIndexPath:indexPath];
		header.header.text = @"OTHER RISK FACTORS";
		header.backgroundColor = lightLightBlueColorBackground();
		header.header.textColor = lightGrayBlueColor();
		header.header.font = OpenSansBold(21);
		header.header.frame = CGRectMake(15, 15, header.header.frame.size.width, header.header.frame.size.height);
        reusableview = header;
    }
	
	return reusableview;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
	return CGSizeMake(768, 50);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
	return CGSizeMake([self getCollectionWidth:indexPath], 40);
}

- (float)getCollectionWidth:(NSIndexPath*)indexPath
{
	OtherRiskFactor *risk = self.otherRisks[indexPath.row];
	NSDictionary *attributes = @{NSFontAttributeName: OpenSansBold(17)};
	CGSize size = [risk.name boundingRectWithSize:CGSizeMake(192-32, 40) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
	return size.width + 50;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
	return UIEdgeInsetsMake(0, 20, 0, 20);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
	return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
	return 0;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return [self.otherRisks count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	DiseaseHistoryCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[DiseaseHistoryCollectionCell reuseIdentifier] forIndexPath:indexPath];
	OtherRiskFactor *risk = self.otherRisks[indexPath.row];
	[cell selOtherRiskFactorNotEditable:risk];
	cell.backgroundColor = lightLightBlueColorBackground();
	
	return cell;
}

@end
