//
//  PatientRemoteAnamnesisViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 20/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "PatientRemoteAnamnesisEdit.h"
#import "PatientRemoteAnamnesisViewController.h"
#import "PatientMedicalHistoryViewController.h"
#import "FatherDiseaseHistory.h"
#import "MotherDiseaseHistory.h"
#import "RemoteAnamnesis.h"
#import "Vaccination.h"
#import "DiseaseHistory.h"
#import "Operation.h"

#define cellFatherRiskHeight 80
#define cellDiseaseHistoryHeight 40
#define headerDiseaseHistoryHeight 30

@interface PatientRemoteAnamnesisEdit ()

@property (nonatomic, weak) IBOutlet PickerField *deliveryText;
@property (nonatomic, weak) IBOutlet CustomTextField *deliveryNoteText;
@property (nonatomic, weak) IBOutlet CustomTextField *lifeBirdsText;
@property (nonatomic, weak) IBOutlet PickerField *fatherRiskText;
@property (nonatomic, weak) IBOutlet CustomTextField *fatherRiskNoteText;
@property (nonatomic, weak) IBOutlet UITableView *fatherRiskTable;
@property (nonatomic, weak) IBOutlet UIView *motherRiskHeader;
@property (nonatomic, weak) IBOutlet PickerField *motherRiskText;
@property (nonatomic, weak) IBOutlet CustomTextField *motherRiskNoteText;
@property (nonatomic, weak) IBOutlet UITableView *motherRiskTable;
@property (nonatomic, weak) IBOutlet UIView *vaccinationHeader;
@property (nonatomic, weak) IBOutlet PickerField *vaccinationTypeText;
@property (nonatomic, weak) IBOutlet PickerField *vaccinationDateText;
@property (nonatomic, weak) IBOutlet CustomTextField *vaccinationNoteText;
@property (nonatomic, weak) IBOutlet UITableView *vaccinationTable;
@property (nonatomic, weak) IBOutlet UICollectionView *diseaseCollection;
@property (nonatomic, weak) IBOutlet UIView *diseaseHeader;
@property (nonatomic, weak) IBOutlet CustomTextField *otherDiseaseText;
@property (nonatomic, weak) IBOutlet UIView *operationHeader;
@property (nonatomic, weak) IBOutlet PickerField *otherOperationText;
@property (nonatomic, weak) IBOutlet CustomTextField *otherOperationNoteText;
@property (nonatomic, weak) IBOutlet UITableView *operationsTable;
@property (nonatomic, weak) IBOutlet UIView *bottomView;
@property (nonatomic, weak) IBOutlet CustomTextView *noteText;
@property (weak, nonatomic) IBOutlet UIView *saveView;

@property (nonatomic, strong) NSMutableArray *fatherRiskHistories;
@property (nonatomic, strong) NSMutableArray *motherRiskHistories;
@property (nonatomic, strong) NSMutableArray *vaccinations;
@property (nonatomic, strong) NSMutableArray *diseasesHistories;
@property (nonatomic, strong) NSMutableArray *operations;

@end

#pragma mark - Init PatientRemoteAnamnesisEdit

@implementation PatientRemoteAnamnesisEdit

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// father risks table
	UINib *cellNibfather = [UINib nibWithNibName:@"FatherHistoryCell" bundle:nil];
	[self.fatherRiskTable registerNib:cellNibfather forCellReuseIdentifier:[FatherHistoryCell reuseIdentifier]];
	
	// mother risks table
	UINib *cellNibmother = [UINib nibWithNibName:@"MotherHistoryCell" bundle:nil];
	[self.motherRiskTable registerNib:cellNibmother forCellReuseIdentifier:[MotherHistoryCell reuseIdentifier]];
	
	// vaccination table
	UINib *cellNibvaccination = [UINib nibWithNibName:@"VaccinationCell" bundle:nil];
	[self.vaccinationTable registerNib:cellNibvaccination forCellReuseIdentifier:[VaccinationCell reuseIdentifier]];
	
	// disease collection
	UINib *headerDisease = [UINib nibWithNibName:@"DiseaseHistoryHeader" bundle:nil];
	[self.diseaseCollection registerNib:headerDisease forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[DiseaseHistoryHeader reuseIdentifier]];
	UINib *cellDisease = [UINib nibWithNibName:@"DiseaseHistoryCollectionCell" bundle:nil];
	[self.diseaseCollection registerNib:cellDisease forCellWithReuseIdentifier:[DiseaseHistoryCollectionCell reuseIdentifier]];
	
	// operation table
	UINib *cellNibop = [UINib nibWithNibName:@"OperationCell" bundle:nil];
	[self.operationsTable registerNib:cellNibop forCellReuseIdentifier:[OperationCell reuseIdentifier]];
}


- (void)setup
{
	self.mainscroll.frame = self.view.bounds;
	
	// save button mode
	if (self.isEditing) {
		self.saveView.hidden = NO;
	} else {
		self.saveView.hidden = YES;
	}

	self.deliveryText.textField.text = self.patient.remoteAnamnesis.delivery;
	self.deliveryNoteText.text = self.patient.remoteAnamnesis.deliveryNote;
	self.lifeBirdsText.text = [self.patient.remoteAnamnesis.lifeBirds stringValue];
	self.lifeBirdsText.textAlignment = 1;
	self.noteText.textView.text = self.patient.remoteAnamnesis.note;
	
	// father risks table
	self.fatherRiskHistories = [[self.patient.remoteAnamnesis fatherDiseases] allObjects].mutableCopy;
	[self animateFatherRiskTableWithDelta:((int)[self.fatherRiskHistories count]*cellFatherRiskHeight)];
	
	// mother risks table
	self.motherRiskHistories = [[self.patient.remoteAnamnesis motherDiseases] allObjects].mutableCopy;
	[self animateMotherRiskTableWithDelta:((int)[self.motherRiskHistories count]*cellFatherRiskHeight)];
	
	// vaccination table
	self.vaccinations = [[self.patient.remoteAnamnesis vaccinations] allObjects].mutableCopy;
	[self animateVaccinationTableWithDelta:((int)[self.vaccinations count]*cellFatherRiskHeight)];
	
	// disease collection
	self.diseasesHistories = [self getDiseasesHistoriesArrayOrdered].mutableCopy;
	[self animateDiseasesTableWithHeight:[self getDiseaseCollectionHeight] + cellDiseaseHistoryHeight];
	
	// operation table
	self.operations = [[self.patient.remoteAnamnesis operations] allObjects].mutableCopy;
	[self animateOperationsTableWithDelta:((int)[self.operations count]*cellFatherRiskHeight)];
}

- (NSArray*)getDiseasesHistoriesArrayOrdered
{
	[self.diseasesHistories removeAllObjects];
	NSArray *diseases = [[self.patient.remoteAnamnesis diseaseHistory] allObjects];
	NSArray *diseasesOrdered = [diseases sortedArrayUsingComparator: ^(DiseaseHistory *d_1, DiseaseHistory *d_2) {
		NSDate *n_1 = d_1.date;
		NSDate *n_2 = d_2.date;
		return [n_1 compare: n_2];
	}];
	return diseasesOrdered;
}


#pragma mark - Animations

- (float)getDiseaseCollectionHeight
{
	CGSize size = self.diseaseCollection.collectionViewLayout.collectionViewContentSize;
	if (size.height != 0)
		return size.height + headerDiseaseHistoryHeight;
	else
	{
		int numberLines = 1;
		if ([self.diseasesHistories count] > 1)
			numberLines = (int)[self.diseasesHistories count]/5 + 1;
		return numberLines*cellDiseaseHistoryHeight + headerDiseaseHistoryHeight;
	}
}

- (void)animateDiseasesTableWithHeight:(float)height
{
	float delta = height - self.diseaseCollection.frame.size.height;
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.diseaseCollection.frame = CGRectMake(self.diseaseCollection.frame.origin.x, self.diseaseCollection.frame.origin.y, self.diseaseCollection.frame.size.width, height);
		self.diseaseHeader.frame = CGRectMake(self.diseaseHeader.frame.origin.x, self.diseaseHeader.frame.origin.y + delta, self.diseaseHeader.frame.size.width, self.diseaseHeader.frame.size.height);
		self.operationHeader.frame = CGRectMake(self.operationHeader.frame.origin.x, self.operationHeader.frame.origin.y + delta, self.operationHeader.frame.size.width, self.operationHeader.frame.size.height);
		self.operationsTable.frame = CGRectMake(self.operationsTable.frame.origin.x, self.operationsTable.frame.origin.y + delta, self.operationsTable.frame.size.width, self.operationsTable.frame.size.height);
		self.bottomView.frame = CGRectMake(self.bottomView.frame.origin.x, self.bottomView.frame.origin.y + delta, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
	} completion:^(BOOL s){
		[self.diseaseCollection reloadData];
	}];
}

- (void)animateVaccinationTableWithDelta:(int)delta
{
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.vaccinationTable.frame = CGRectMake(self.vaccinationTable.frame.origin.x, self.vaccinationTable.frame.origin.y, self.vaccinationTable.frame.size.width, self.vaccinationTable.frame.size.height + delta);
		self.diseaseCollection.frame = CGRectMake(self.diseaseCollection.frame.origin.x, self.diseaseCollection.frame.origin.y + delta, self.diseaseCollection.frame.size.width, self.diseaseCollection.frame.size.height);
		self.diseaseHeader.frame = CGRectMake(self.diseaseHeader.frame.origin.x, self.diseaseHeader.frame.origin.y + delta, self.diseaseHeader.frame.size.width, self.diseaseHeader.frame.size.height);
		self.operationHeader.frame = CGRectMake(self.operationHeader.frame.origin.x, self.operationHeader.frame.origin.y + delta, self.operationHeader.frame.size.width, self.operationHeader.frame.size.height);
		self.operationsTable.frame = CGRectMake(self.operationsTable.frame.origin.x, self.operationsTable.frame.origin.y + delta, self.operationsTable.frame.size.width, self.operationsTable.frame.size.height);
		self.bottomView.frame = CGRectMake(self.bottomView.frame.origin.x, self.bottomView.frame.origin.y + delta, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
	} completion:^(BOOL s){
		[self.vaccinationTable reloadData];
	}];
}

- (void)animateMotherRiskTableWithDelta:(int)delta
{
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.motherRiskTable.frame = CGRectMake(self.motherRiskTable.frame.origin.x, self.motherRiskTable.frame.origin.y, self.motherRiskTable.frame.size.width, self.motherRiskTable.frame.size.height + delta);
		self.vaccinationHeader.frame = CGRectMake(self.vaccinationHeader.frame.origin.x, self.vaccinationHeader.frame.origin.y + delta, self.vaccinationHeader.frame.size.width, self.vaccinationHeader.frame.size.height);
		self.vaccinationTable.frame = CGRectMake(self.vaccinationTable.frame.origin.x, self.vaccinationTable.frame.origin.y + delta, self.vaccinationTable.frame.size.width, self.vaccinationTable.frame.size.height);
		self.diseaseCollection.frame = CGRectMake(self.diseaseCollection.frame.origin.x, self.diseaseCollection.frame.origin.y + delta, self.diseaseCollection.frame.size.width, self.diseaseCollection.frame.size.height);
		self.diseaseHeader.frame = CGRectMake(self.diseaseHeader.frame.origin.x, self.diseaseHeader.frame.origin.y + delta, self.diseaseHeader.frame.size.width, self.diseaseHeader.frame.size.height);
		self.operationHeader.frame = CGRectMake(self.operationHeader.frame.origin.x, self.operationHeader.frame.origin.y + delta, self.operationHeader.frame.size.width, self.operationHeader.frame.size.height);
		self.operationsTable.frame = CGRectMake(self.operationsTable.frame.origin.x, self.operationsTable.frame.origin.y + delta, self.operationsTable.frame.size.width, self.operationsTable.frame.size.height);
		self.bottomView.frame = CGRectMake(self.bottomView.frame.origin.x, self.bottomView.frame.origin.y + delta, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
	} completion:^(BOOL s){
		[self.motherRiskTable reloadData];
	}];
}

- (void)animateFatherRiskTableWithDelta:(int)delta
{
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.fatherRiskTable.frame = CGRectMake(self.fatherRiskTable.frame.origin.x, self.fatherRiskTable.frame.origin.y, self.fatherRiskTable.frame.size.width, self.fatherRiskTable.frame.size.height + delta);
		self.motherRiskHeader.frame = CGRectMake(self.motherRiskHeader.frame.origin.x, self.motherRiskHeader.frame.origin.y + delta, self.motherRiskHeader.frame.size.width, self.motherRiskHeader.frame.size.height);
		self.motherRiskTable.frame = CGRectMake(self.motherRiskTable.frame.origin.x, self.motherRiskTable.frame.origin.y + delta, self.motherRiskTable.frame.size.width, self.motherRiskTable.frame.size.height);
		self.vaccinationHeader.frame = CGRectMake(self.vaccinationHeader.frame.origin.x, self.vaccinationHeader.frame.origin.y + delta, self.vaccinationHeader.frame.size.width, self.vaccinationHeader.frame.size.height);
		self.vaccinationTable.frame = CGRectMake(self.vaccinationTable.frame.origin.x, self.vaccinationTable.frame.origin.y + delta, self.vaccinationTable.frame.size.width, self.vaccinationTable.frame.size.height);
		self.diseaseCollection.frame = CGRectMake(self.diseaseCollection.frame.origin.x, self.diseaseCollection.frame.origin.y + delta, self.diseaseCollection.frame.size.width, self.diseaseCollection.frame.size.height);
		self.diseaseHeader.frame = CGRectMake(self.diseaseHeader.frame.origin.x, self.diseaseHeader.frame.origin.y + delta, self.diseaseHeader.frame.size.width, self.diseaseHeader.frame.size.height);
		self.operationHeader.frame = CGRectMake(self.operationHeader.frame.origin.x, self.operationHeader.frame.origin.y + delta, self.operationHeader.frame.size.width, self.operationHeader.frame.size.height);
		self.operationsTable.frame = CGRectMake(self.operationsTable.frame.origin.x, self.operationsTable.frame.origin.y + delta, self.operationsTable.frame.size.width, self.operationsTable.frame.size.height);
		self.bottomView.frame = CGRectMake(self.bottomView.frame.origin.x, self.bottomView.frame.origin.y + delta, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
	} completion:^(BOOL s){
		[self.fatherRiskTable reloadData];
	}];
}

- (void)animateOperationsTableWithDelta:(int)delta {
	
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.operationsTable.frame = CGRectMake(self.operationsTable.frame.origin.x, self.operationsTable.frame.origin.y, self.operationsTable.frame.size.width, self.operationsTable.frame.size.height + delta);
		self.bottomView.frame = CGRectMake(self.bottomView.frame.origin.x, self.bottomView.frame.origin.y + delta, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
	} completion:^(BOOL s){
		[self.operationsTable reloadData];
	}];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
	if (textField == self.lifeBirdsText)
		return NO;
	else
		return [super textFieldShouldBeginEditing:textField];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	return [super textFieldDidBeginEditing:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	
	NSString *text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	if (textField == self.deliveryNoteText)
	{
		self.patient.remoteAnamnesis.deliveryNote = text;
		[[DBManager getIstance] saveContext];
	}
	else if ([textField.accessibilityIdentifier isEqualToString:[FatherHistoryCell reuseIdentifier]])
	{
		int index = (int)textField.tag;
		FatherDiseaseHistory *father = self.fatherRiskHistories[index];
		father.note = text;
		[[DBManager getIstance] saveContext];
		self.fatherRiskHistories = [[self.patient.remoteAnamnesis fatherDiseases] allObjects].mutableCopy;
	}
	else if ([textField.accessibilityIdentifier isEqualToString:[MotherHistoryCell reuseIdentifier]])
	{
		int index = (int)textField.tag;
		MotherDiseaseHistory *mother = self.motherRiskHistories[index];
		mother.note = text;
		[[DBManager getIstance] saveContext];
		self.motherRiskHistories = [[self.patient.remoteAnamnesis motherDiseases] allObjects].mutableCopy;
	}
	else if ([textField.accessibilityIdentifier isEqualToString:[VaccinationCell reuseIdentifier]])
	{
		int index = (int)textField.tag;
		Vaccination *vaccination = self.vaccinations[index];
		vaccination.note = text;
		[[DBManager getIstance] saveContext];
		self.vaccinations = [[self.patient.remoteAnamnesis vaccinations] allObjects].mutableCopy;
	}
	else if ([textField.accessibilityIdentifier isEqualToString:[OperationCell reuseIdentifier]])
	{
		int index = (int)textField.tag;
		Operation *operation = self.operations[index];
		operation.note = text;
		[[DBManager getIstance] saveContext];
		self.operations = [[self.patient.remoteAnamnesis operations] allObjects].mutableCopy;
	}
	
	[super textFieldDidEndEditing:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	return [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
}


#pragma mark - CustomTextViewDelegate

- (void)customTextViewDidBeginEditing:(CustomTextView *)textView
{
	[super customTextViewDidBeginEditing:textView];
}

- (void)customTextViewDidEndEditing:(CustomTextView *)textView
{
	NSString *text = [textView.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	 if (textView == self.noteText)
	 {
		 self.patient.remoteAnamnesis.note = text;
		 [[DBManager getIstance] saveContext];
	 }

	[super customTextViewDidEndEditing:textView];
}

- (void)customTextViewDidChange:(CustomTextView *)textView
{
	[super customTextViewDidChange:textView];
}

#pragma mark - PickerDateViewControllerDelegate Methods

- (void)pickerDateViewControllerSetDate:(NSDate *)date pickerField:(PickerField *)pickerField
{
	pickerField.textField.text = [self.formatter stringFromDate:date];
	
	if ([pickerField.accessibilityIdentifier isEqualToString:[VaccinationCell reuseIdentifier]])
	{
		int index = (int)pickerField.tag;
		Vaccination *vaccination = self.vaccinations[index];
		vaccination.date = date;
		[[DBManager getIstance] saveContext];
		self.vaccinations = [[self.patient.remoteAnamnesis vaccinations] allObjects].mutableCopy;
	}
}

#pragma mark - PickerViewControllerDelegate Methods

- (void)pickerViewControllerSetText:(NSString*)text pickerField:(PickerField*)pickerField
{
	pickerField.textField.text = text;
		
	if (pickerField == self.deliveryText)
	{
		self.patient.remoteAnamnesis.delivery = text;
		[[DBManager getIstance] saveContext];
	}
	else if ([pickerField.accessibilityIdentifier isEqualToString:[FatherHistoryCell reuseIdentifier]])
	{
		int index = (int)pickerField.tag;
		FatherDiseaseHistory *father = self.fatherRiskHistories[index];
		father.type = text;
		[[DBManager getIstance] saveContext];
		self.fatherRiskHistories = [[self.patient.remoteAnamnesis fatherDiseases] allObjects].mutableCopy;
	}
	else if ([pickerField.accessibilityIdentifier isEqualToString:[MotherHistoryCell reuseIdentifier]])
	{
		int index = (int)pickerField.tag;
		MotherDiseaseHistory *mother = self.motherRiskHistories[index];
		mother.type = text;
		[[DBManager getIstance] saveContext];
		self.motherRiskHistories = [[self.patient.remoteAnamnesis motherDiseases] allObjects].mutableCopy;
	}
	else if ([pickerField.accessibilityIdentifier isEqualToString:[VaccinationCell reuseIdentifier]])
	{
		int index = (int)pickerField.tag;
		Vaccination *vaccination = self.vaccinations[index];
		vaccination.type = text;
		[[DBManager getIstance] saveContext];
		self.vaccinations = [[self.patient.remoteAnamnesis vaccinations] allObjects].mutableCopy;
	}
	else if ([pickerField.accessibilityIdentifier isEqualToString:[OperationCell reuseIdentifier]])
	{
		int index = (int)pickerField.tag;
		Operation *operation = self.operations[index];
		operation.name = text;
		[[DBManager getIstance] saveContext];
		self.operations = [[self.patient.remoteAnamnesis operations] allObjects].mutableCopy;
	}
}

#pragma mark - PickerFieldDelegate Methods

- (void)tapOnPickerFieldDateButton:(PickerField*)pickerField
{
	PickerDateViewController *pickerDateViewController = [[PickerDateViewController alloc] initWithNibName:@"PickerDateViewController" bundle:nil];
	pickerDateViewController.pickerField = pickerField;
	pickerDateViewController.delegate = self;
	
	if (pickerField == self.vaccinationDateText)
		pickerDateViewController.date = [self.formatter dateFromString:self.vaccinationDateText.textField.text];
	else if ([pickerField.accessibilityIdentifier isEqualToString:[VaccinationCell reuseIdentifier]])
		pickerDateViewController.date = [self.formatter dateFromString:pickerField.textField.text];

	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerDateViewController];
	[self.popover setPopoverContentSize:pickerDateViewController.view.frame.size];
	[pickerDateViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)tapOnPickerFieldButton:(PickerField*)pickerField
{
	PickerViewController *pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
	pickerViewController.pickerField = pickerField;
	pickerViewController.delegate = self;
	
	if (pickerField == self.deliveryText)
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:DeliveryData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = DeliveryData;
	}
	else if (pickerField == self.motherRiskText || pickerField == self.fatherRiskText || [pickerField.accessibilityIdentifier isEqualToString:[FatherHistoryCell reuseIdentifier]] || [pickerField.accessibilityIdentifier isEqualToString:[MotherHistoryCell reuseIdentifier]])
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:RiskFactorData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = RiskFactorData;
	}
	if (pickerField == self.vaccinationTypeText || [pickerField.accessibilityIdentifier isEqualToString:[VaccinationCell reuseIdentifier]])
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:VaccinationData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = VaccinationData;
	}
	else if (pickerField == self.otherOperationText || [pickerField.accessibilityIdentifier isEqualToString:[OperationCell reuseIdentifier]])
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:OperationData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = OperationData;
	}

	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerViewController];
	[self.popover setPopoverContentSize:pickerViewController.view.frame.size];
	[pickerViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)pickerFieldDidBeginEditing:(PickerField *)pickerField
{
	[super pickerFieldDidBeginEditing:pickerField];
}

#pragma mark - Actions


- (IBAction)save:(id)sender
{
	PatientRemoteAnamnesisViewController *vc = [[PatientRemoteAnamnesisViewController alloc] initWithNibName:@"PatientRemoteAnamnesisViewController" bundle:nil];
	PatientMedicalHistoryViewController *parent = (PatientMedicalHistoryViewController*)self.parentViewController;
	vc.patient = self.patient;
	[self parent:parent fadeToViewController:vc fromViewController:self superView:parent.mainView animateWithDuration:0.2];
}

- (IBAction)addDisease:(id)sender
{
	if (![BaseViewController isEmptyString:self.otherDiseaseText.text])
	{
		[[DBManager getIstance] createDiseaseHistoryForRemoteAnamnesis:self.patient.remoteAnamnesis name:self.otherDiseaseText.text status:[NSNumber numberWithBool:YES]];
		self.diseasesHistories = [self getDiseasesHistoriesArrayOrdered].mutableCopy;
		self.otherDiseaseText.text = @"";
		[self animateDiseasesTableWithHeight:[self getDiseaseCollectionHeight]];
	}
	else
	{
		[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please provide a value for the Details filled" confirmBlock:^{
			self.otherDiseaseText.text = @"";
		}];
	}
}

- (IBAction)showToolTip:(id)sender
{
	UIButton *button = (UIButton*)sender;
	
	TooltipViewController *tooltipViewController = [[TooltipViewController alloc] initWithNibName:@"TooltipViewController" bundle:nil];
	tooltipViewController.info = @"You can add other diseases here.";
	self.popover = [[UIPopoverController alloc] initWithContentViewController:tooltipViewController];
	[self.popover setPopoverContentSize:tooltipViewController.view.frame.size];
	[tooltipViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:button.bounds inView:button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (IBAction)addVaccination:(id)sender
{
	if (![BaseViewController isEmptyString:self.vaccinationTypeText.textField.text])
	{
		NSDate *date;
		if (![BaseViewController isEmptyString:self.vaccinationDateText.textField.text])
			date = [self.formatter dateFromString:self.vaccinationDateText.textField.text];
		else
			date = [NSDate date];
		[[DBManager getIstance] createVaccinationForRemoteAnamnesis:self.patient.remoteAnamnesis note:self.vaccinationNoteText.text type:self.vaccinationTypeText.textField.text date:date];
		self.vaccinations = [self.patient.remoteAnamnesis.vaccinations allObjects].mutableCopy;
		self.vaccinationTypeText.textField.text = @"";
		self.vaccinationDateText.textField.text = @"";
		self.vaccinationNoteText.text = @"";
		[self animateVaccinationTableWithDelta:cellFatherRiskHeight];
	}
	else
	{
		[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please provide a value for the Details filled" confirmBlock:^{
			self.vaccinationTypeText.textField.text = @"";
			self.vaccinationDateText.textField.text = @"";
			self.vaccinationNoteText.text = @"";
		}];
	}
}

- (IBAction)addMotherRisk:(id)sender
{
	if (![BaseViewController isEmptyString:self.motherRiskText.textField.text])
	{
		[[DBManager getIstance] createMotherDiseaseHistoryForRemoteAnamnesis:self.patient.remoteAnamnesis note:self.motherRiskNoteText.text type:self.motherRiskText.textField.text];
		self.motherRiskHistories = [self.patient.remoteAnamnesis.motherDiseases allObjects].mutableCopy;
		self.motherRiskText.textField.text = @"";
		self.motherRiskNoteText.text = @"";
		[self animateMotherRiskTableWithDelta:cellFatherRiskHeight];
	}
	else
	{
		[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please provide a value for the Details filled" confirmBlock:^{
			self.motherRiskText.textField.text = @"";
			self.motherRiskNoteText.text = @"";
		}];
	}
}

- (IBAction)addFatherRisk:(id)sender
{
	if (![BaseViewController isEmptyString:self.fatherRiskText.textField.text])
	{
		[[DBManager getIstance] createFatherDiseaseHistoryForRemoteAnamnesis:self.patient.remoteAnamnesis note:self.fatherRiskNoteText.text type:self.fatherRiskText.textField.text];
		self.fatherRiskHistories = [self.patient.remoteAnamnesis.fatherDiseases allObjects].mutableCopy;
		self.fatherRiskText.textField.text = @"";
		self.fatherRiskNoteText.text = @"";
		[self animateFatherRiskTableWithDelta:cellFatherRiskHeight];
	}
	else
	{
		[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please provide a value for the Details filled" confirmBlock:^{
			self.fatherRiskText.textField.text = @"";
			self.fatherRiskNoteText.text = @"";
		}];
	}
}

- (IBAction)addOperation:(id)sender
{
	if (![BaseViewController isEmptyString:self.otherOperationText.textField.text])
	{
		[[DBManager getIstance] createOperationForRemoteAnamnesis:self.patient.remoteAnamnesis name:self.otherOperationText.textField.text note:self.otherOperationNoteText.text];
		self.operations = [self.patient.remoteAnamnesis.operations allObjects].mutableCopy;
		self.otherOperationText.textField.text = @"";
		self.otherOperationNoteText.text = @"";
		[self animateOperationsTableWithDelta:cellFatherRiskHeight];
	}
	else
	{
		[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please provide a value for the Details filled" confirmBlock:^{
			self.otherOperationText.textField.text = @"";
			self.otherOperationNoteText.text = @"";
		}];
	}
}

- (IBAction)stepperMinus:(id)sender
{
	int lifeBirds = [self.lifeBirdsText.text intValue];
	if (lifeBirds > 0)
		lifeBirds--;
	
	self.lifeBirdsText.text = [NSString stringWithFormat:@"%i", lifeBirds];
	self.patient.remoteAnamnesis.lifeBirds = [NSNumber numberWithInt:lifeBirds];
	[[DBManager getIstance] saveContext];
}

- (IBAction)stepperPlus:(id)sender
{
	int lifeBirds = [self.lifeBirdsText.text intValue];
	lifeBirds++;
	
	self.lifeBirdsText.text = [NSString stringWithFormat:@"%i", lifeBirds];
	self.patient.remoteAnamnesis.lifeBirds = [NSNumber numberWithInt:lifeBirds];
	[[DBManager getIstance] saveContext];
}

#pragma mark - UICollectionViewDataSource

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
	UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader)
	{
        DiseaseHistoryHeader *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[DiseaseHistoryHeader reuseIdentifier] forIndexPath:indexPath];
		header.header.text = @"PATIENT DISEASE HISTORY";
        reusableview = header;
    }
	
	return reusableview;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
	return CGSizeMake(728, 30);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
	return CGSizeMake(145, 40);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
	return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
	return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
	return 0;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return [self.diseasesHistories count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	DiseaseHistoryCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[DiseaseHistoryCollectionCell reuseIdentifier] forIndexPath:indexPath];
	cell.index = [NSNumber numberWithInt:(int)indexPath.row];
	cell.cellDelegate = self;
	DiseaseHistory *disease = self.diseasesHistories[indexPath.row];
	[cell setDiseaseHistory:disease];
	
	return cell;
}

- (void)collectionDoActionForKey:(NSString*)actionKey actionData:(id)actionData
{
	if ([actionKey isEqualToString:@"changeStatus"])
	{
		int index = [actionData[@"index"] intValue];
		DiseaseHistory *disease = self.diseasesHistories[index];
		BOOL status = ![disease.status boolValue];
		disease.status = [NSNumber numberWithBool:status];
		[[DBManager getIstance] saveContext];
		self.diseasesHistories = [self getDiseasesHistoriesArrayOrdered].mutableCopy;
	} else if ([actionKey isEqualToString:@"longPress"])
	{
		int index = [actionData[@"index"] intValue];
		DiseaseHistory *disease = self.diseasesHistories[index];
		[[AlertViewBlocks getIstance] twoOptionsAlertViewWithmessage:@"Do you want to edit or to delete the selected disease?" firstButtonTitle:@"Edit" secondButtonTitle:@"Delete" confirmBlock:^{
			// edit the disease
            dispatch_async(dispatch_get_main_queue(), ^{
                [[AlertViewBlocks getIstance] showEditViewWithItem:disease.name delegate:self object:disease];
            });
			
		} secondConfirm:^{
			// delete the disease
			[[DBManager getIstance] deleteObject:disease];
			self.diseasesHistories = [self getDiseasesHistoriesArrayOrdered].mutableCopy;
			self.otherDiseaseText.text = @"";
			[self animateDiseasesTableWithHeight:[self getDiseaseCollectionHeight]];
		} cancelBlock:^{
		}];
	}
}

#pragma mark - EditItemViewControllerDelegate Methods

- (void)dismissEditView:(NSString*)text object:(id)object
{
    if ([object isKindOfClass:[DiseaseHistory class]])
    {
        DiseaseHistory *disease = (DiseaseHistory *)object;
        disease.name = text;
        [[DBManager getIstance] saveContext];
        self.diseasesHistories = [self getDiseasesHistoriesArrayOrdered].mutableCopy;
        [self.diseaseCollection reloadData];
    }
}

#pragma mark - UITableViewDataSource/UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (tableView == self.fatherRiskTable)
		return [self.fatherRiskHistories count];
	else if (tableView == self.motherRiskTable)
		return [self.motherRiskHistories count];
	else if (tableView == self.vaccinationTable)
		return [self.vaccinations count];
	else
		return [self.operations count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;
	
	if (tableView == self.fatherRiskTable)
	{
		FatherHistoryCell *customcell = [tableView dequeueReusableCellWithIdentifier:[FatherHistoryCell reuseIdentifier]];
		customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
		customcell.cellDelegate = self;
		FatherDiseaseHistory *father = self.fatherRiskHistories[indexPath.row];
		[customcell setContent:father];
		cell = customcell;
	}
	
	else if (tableView == self.motherRiskTable)
	{
		MotherHistoryCell *customcell = [tableView dequeueReusableCellWithIdentifier:[MotherHistoryCell reuseIdentifier]];
		customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
		customcell.cellDelegate = self;
		MotherDiseaseHistory *mother = self.motherRiskHistories[indexPath.row];
		[customcell setContent:mother];
		cell = customcell;
	}
	
	else if (tableView == self.vaccinationTable)
	{
		VaccinationCell *customcell = [tableView dequeueReusableCellWithIdentifier:[VaccinationCell reuseIdentifier]];
		customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
		customcell.cellDelegate = self;
		Vaccination *vaccination = self.vaccinations[indexPath.row];
		[customcell setContent:vaccination];
		cell = customcell;
	}
	
	else if (tableView == self.operationsTable)
	{
		OperationCell *customcell = [tableView dequeueReusableCellWithIdentifier:[OperationCell reuseIdentifier]];
		customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
		customcell.cellDelegate = self;
		Operation *operation = self.operations[indexPath.row];
		[customcell setContent:operation];
		cell = customcell;
	}
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)doActionForKey:(NSString*)actionKey actionData:(id)actionData
{
	if ([actionKey isEqualToString:@"deleteFatherRisk"])
	{
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"You are going to delete this information: are you sure?" confirmBlock:^{
			int index = [actionData[@"index"] intValue];
			FatherDiseaseHistory *fatherHistory = self.fatherRiskHistories[index];
			[[DBManager getIstance] deleteObject:fatherHistory];
			self.fatherRiskHistories = [[self.patient.remoteAnamnesis fatherDiseases] allObjects].mutableCopy;
			[self animateFatherRiskTableWithDelta:-cellFatherRiskHeight];
		} cancelBlock:^{
		}];
	}
	
	else if ([actionKey isEqualToString:@"deleteMotherRisk"])
	{
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"You are going to delete this information: are you sure?" confirmBlock:^{
			int index = [actionData[@"index"] intValue];
			MotherDiseaseHistory *motherHistory = self.motherRiskHistories[index];
			[[DBManager getIstance] deleteObject:motherHistory];
			self.motherRiskHistories = [[self.patient.remoteAnamnesis motherDiseases] allObjects].mutableCopy;
			[self animateMotherRiskTableWithDelta:-cellFatherRiskHeight];
		} cancelBlock:^{
		}];
	}
	
	else if ([actionKey isEqualToString:@"deleteVaccination"])
	{
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"You are going to delete this information: are you sure?" confirmBlock:^{
			int index = [actionData[@"index"] intValue];
			Vaccination *vaccination = self.vaccinations[index];
			[[DBManager getIstance] deleteObject:vaccination];
			self.vaccinations = [[self.patient.remoteAnamnesis vaccinations] allObjects].mutableCopy;
			[self animateVaccinationTableWithDelta:-cellFatherRiskHeight];

		} cancelBlock:^{
		}];
	}
	else if ([actionKey isEqualToString:@"deleteOperation"])
	{
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"You are going to delete this information: are you sure?" confirmBlock:^{
			int index = [actionData[@"index"] intValue];
			Operation *operation = self.operations[index];
			[[DBManager getIstance] deleteObject:operation];
			self.operations = [[self.patient.remoteAnamnesis operations] allObjects].mutableCopy;
			[self animateOperationsTableWithDelta:-cellFatherRiskHeight];
		} cancelBlock:^{
		}];
	}
}


@end
