//
//  PatientMedicalHistoryViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 27/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"
#import "MenuView.h"

@interface PatientMedicalHistoryViewController : BaseViewController <MenuViewDelegate>

@property (nonatomic, weak) IBOutlet MenuView *menu;
@property (nonatomic, weak) IBOutlet UIView *mainView;

@end
