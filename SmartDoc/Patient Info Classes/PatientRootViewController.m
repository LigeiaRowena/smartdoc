//
//  PatientDetailViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 04/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "PatientRootViewController.h"
#import "PatientDashboardViewController.h"
#import "PatientDataViewController.h"
#import "PatientVisitsViewController.h"
#import "PatientHomeTherapyViewController.h"
#import "PatientPrescriptionsController.h"
#import "PatientListViewController.h"
#import "PatientMedicalHistoryViewController.h"
#import "DBManager.h"

@interface PatientRootViewController ()

@end

#pragma mark - Init PatientRootViewController

@implementation PatientRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self.menu selectItemAtIndex:0];	
}

#pragma mark - MenuViewDelegate

- (void)menuDidSelectItemAtIndex:(int)index
{
	if (index == 0)
		[self pushDashboard];
	else if (index == 1)
		[self pushPatientData];
	else if (index == 2)
		[self pushVisits];
	else if (index == 3)
		[self pushHomeTherapy];
	else if (index == 4)
		[self pushPrescription];
}

- (void)setup
{
}

#pragma mark - Actions

- (void)pushMedicalHistory:(BaseViewController*)sourceViewController patient:(Patient *)patient
{
	PatientMedicalHistoryViewController *vc = [[PatientMedicalHistoryViewController alloc] initWithNibName:@"PatientMedicalHistoryViewController" bundle:nil];
	vc.patient = self.patient;
	[self parent:self addChildViewController:vc superView:self.view animateWithDuration:0.2];
}

- (void)pushDashboard
{
	PatientDashboardViewController *vc = [[PatientDashboardViewController alloc] initWithNibName:@"PatientDashboardViewController" bundle:nil];
	vc.patient = self.patient;
	BaseViewController *lastChild = (BaseViewController*)[self getLastChildFromParent:self];
	if (lastChild != nil)
		[self parent:self fadeToViewController:vc fromViewController:lastChild superView:self.mainView animateWithDuration:0.2];
	else
		[self parent:self addChildViewController:vc superView:self.mainView];
}

- (void)pushPatientData
{
	PatientDataViewController *vc = [[PatientDataViewController alloc] initWithNibName:@"PatientDataViewController" bundle:nil];
	vc.patient = self.patient;
	BaseViewController *lastChild = (BaseViewController*)[self getLastChildFromParent:self];
	[self parent:self fadeToViewController:vc fromViewController:lastChild superView:self.mainView animateWithDuration:0.2];
}

- (void)pushVisits
{
	PatientVisitsViewController *vc = [[PatientVisitsViewController alloc] initWithNibName:@"PatientVisitsViewController" bundle:nil];
	vc.patient = self.patient;
	BaseViewController *lastChild = (BaseViewController*)[self getLastChildFromParent:self];
	[self parent:self fadeToViewController:vc fromViewController:lastChild superView:self.mainView animateWithDuration:0.2];
}

- (void)pushHomeTherapy
{
	PatientHomeTherapyViewController *vc = [[PatientHomeTherapyViewController alloc] initWithNibName:@"PatientHomeTherapyViewController" bundle:nil];
	vc.patient = self.patient;
	BaseViewController *lastChild = (BaseViewController*)[self getLastChildFromParent:self];
	[self parent:self fadeToViewController:vc fromViewController:lastChild superView:self.mainView animateWithDuration:0.2];
}

- (void)pushPrescription
{
	PatientPrescriptionsController *vc = [[PatientPrescriptionsController alloc] initWithNibName:@"PatientPrescriptionsController" bundle:nil];
	vc.patient = self.patient;
	BaseViewController *lastChild = (BaseViewController*)[self getLastChildFromParent:self];
	[self parent:self fadeToViewController:vc fromViewController:lastChild superView:self.mainView animateWithDuration:0.2];}

- (IBAction)back:(id)sender
{
	UIViewController *child = [self getLastChildFromParent:self];
	if ([self getLastChildFromParent:child] != nil)
	{
		UIViewController *lastChild = [self getLastChildFromParent:child];
		[self parent:child popViewController:lastChild animateWithDuration:0.2];
	}
	else if ([self.parentViewController isKindOfClass:[PatientListViewController class]])
	{
		PatientListViewController *parent = (PatientListViewController*)self.parentViewController;
		[parent.tableView reloadData];
		[self parent:parent popViewController:self animateWithDuration:0.2];
	}
}

- (IBAction)favourite:(id)sender
{
	if ([self.patient.favourite boolValue])
	{
		[[AlertViewBlocks getIstance] informationConfirmAlertViewWithmessage:@"Do you want to delete the patient from the favourites?" confirmBlock:^{
			self.patient.favourite = [NSNumber numberWithBool:NO];
			[[DBManager getIstance] saveContext];
		} cancelBlock:^{
		}];
	}
	else
	{
		[[AlertViewBlocks getIstance] informationConfirmAlertViewWithmessage:@"Do you want to add the patient to the favourites?" confirmBlock:^{
			self.patient.favourite = [NSNumber numberWithBool:YES];
			[[DBManager getIstance] saveContext];
		} cancelBlock:^{
		}];
	}
}


@end
