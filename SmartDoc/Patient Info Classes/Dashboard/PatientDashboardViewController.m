
//
//  PatientReminderViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 07/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "PatientDashboardViewController.h"
#import "Action.h"
#import "Dashboard.h"
#import "Reminder.h"
#import "PatientData.h"
#import "PatientRootViewController.h"
#import "NSDate-Utilities.h"
#import "CalendarManager.h"

#define cellActionHeight 105
#define cellActionEditHeight 155
#define cellActionEditNoteHeight 270


@interface PatientDashboardViewController ()

@property (nonatomic, weak) IBOutlet UIButton *patientStatusButton;
@property (nonatomic, weak) IBOutlet UIButton *homeTherapyButton;
@property (nonatomic, weak) IBOutlet UILabel *category;
@property (nonatomic, weak) IBOutlet UITableView *actionTable;
@property (nonatomic, weak) IBOutlet UILabel *actionLabel;
@property (nonatomic, weak) IBOutlet MenuView *actionMenu;
@property (nonatomic, weak) IBOutlet UIView *actionTypeView;
@property (weak, nonatomic) IBOutlet UILabel *actionTypeLabel;
@property (nonatomic, weak) IBOutlet PickerField *actionType;
@property (nonatomic, weak) IBOutlet CustomTextView *actionNote;
@property (nonatomic, weak) IBOutlet UIView *actionDateTextView;
@property (nonatomic, weak) IBOutlet PickerField *actionDateText;
@property (nonatomic, weak) IBOutlet UIView *actionDateView;
@property (nonatomic, weak) IBOutlet PickerField *actionDate;
@property (nonatomic, weak) IBOutlet UIView *actionTimeView;
@property (nonatomic, weak) IBOutlet PickerField *actionTime;
@property (nonatomic, weak) IBOutlet UIButton *cancelButt;
@property (nonatomic, weak) IBOutlet UIButton *submitButt;
@property (weak, nonatomic) IBOutlet UIButton *enlargeButt;

@property (nonatomic, strong) NSMutableArray *actions;
@property (nonatomic, strong) NSIndexPath *indexPathEditing;

@end

#pragma mark - Init PatientDashboardViewController

@implementation PatientDashboardViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// action table
	UINib *cellNibaction = [UINib nibWithNibName:@"ActionCell" bundle:nil];
	[self.actionTable registerNib:cellNibaction forCellReuseIdentifier:[ActionCell reuseIdentifier]];
	
	// action edit table
	UINib *cellNibactionEdit = [UINib nibWithNibName:@"ActionEditCell" bundle:nil];
	[self.actionTable registerNib:cellNibactionEdit forCellReuseIdentifier:[ActionEditCell reuseIdentifier]];
	
	// action edit note table
	UINib *cellNibactionEditNote = [UINib nibWithNibName:@"ActionEditNoteCell" bundle:nil];
	[self.actionTable registerNib:cellNibactionEditNote forCellReuseIdentifier:[ActionEditNoteCell reuseIdentifier]];
}

- (void)setup
{
	self.patientStatusButton.selected = [self.patient.dashboard.active boolValue];
	self.homeTherapyButton.selected = NO;
	self.category.text = self.patient.category;
	[self.actionMenu selectItemAtIndex:0];
	
	// action table
	self.actions = [[self.patient.dashboard actions] allObjects].mutableCopy;
	self.indexPathEditing = nil;
	[self reloadData];
	
}

- (void)reloadData
{
	// action table
	[self.actionTable reloadData];
	
	// no action label
	if ([self.actions count] == 0)
		self.actionLabel.hidden = NO;
	else
		self.actionLabel.hidden = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	if ([[[[DBManager getIstance] getSettings] firstDashboardLaunch] boolValue])
		[NSTimer scheduledTimerWithTimeInterval:0.8f target:self selector:@selector(showWelcomeAlert) userInfo:nil repeats:NO];
}

- (void)showWelcomeAlert
{
	// metti Dashboard come titolo
	[[AlertViewBlocks getIstance] informationAlertViewWithTitle:@"Dashboard" message: @"Please add your first Appointment, To Do or Note here." confirmBlock:^{
		[[DBManager getIstance] getSettings].firstDashboardLaunch = @NO;
		[[DBManager getIstance] saveContext];
	}];
}

#pragma mark - MenuViewDelegate

- (void)menuDidSelectItemAtIndex:(int)index
{
	// appointment
	if (index == 0)
	{
		self.actionNote.hidden = YES;
		self.actionTypeView.hidden = NO;
		self.actionDateTextView.hidden = YES;
		self.actionDateView.hidden = NO;
		self.actionTimeView.hidden = NO;
		self.actionTypeLabel.text = @"APPOINTMENT TYPE";
		self.actionDateView.frame = CGRectMake(10, 216, self.actionDateView.frame.size.width, self.actionDateView.frame.size.height);
		self.actionTimeView.frame = CGRectMake(175, 216, self.actionTimeView.frame.size.width, self.actionTimeView.frame.size.height);
		self.enlargeButt.hidden = YES;
	}
	
	// todo
	else if (index == 1)
	{
		self.actionNote.hidden = YES;
		self.actionTypeView.hidden = NO;
		self.actionDateTextView.hidden = NO;
		self.actionDateView.hidden = NO;
		self.actionTimeView.hidden = YES;
		self.actionTypeLabel.text = @"TO DO";
		self.actionDateView.frame = CGRectMake(237, 216, self.actionDateView.frame.size.width, self.actionDateView.frame.size.height);
		self.actionTimeView.frame = CGRectMake(402, 216, self.actionTimeView.frame.size.width, self.actionTimeView.frame.size.height);
		self.enlargeButt.hidden = YES;
	}
	
	// note
	else if (index == 2)
	{
		self.actionNote.hidden = NO;
		self.actionTypeView.hidden = YES;
		self.actionDateTextView.hidden = YES;
		self.actionDateView.hidden = YES;
		self.actionTimeView.hidden = YES;
		self.actionDateView.frame = CGRectMake(237, 216, self.actionDateView.frame.size.width, self.actionDateView.frame.size.height);
		self.actionTimeView.frame = CGRectMake(402, 216, self.actionTimeView.frame.size.width, self.actionTimeView.frame.size.height);
		self.enlargeButt.hidden = NO;
	}
}

#pragma mark - Actions

- (IBAction)submitAction:(id)sender
{
	NSString *type;
	NSString *action;
	NSDate *date;
	NSString *dateString;
	
	if (self.actionMenu.index == 0)
	{
		type = ActionAppointment;
		action = self.actionType.textField.text;
		dateString = [NSString stringWithFormat:@"%@ %@", self.actionDate.textField.text, self.actionTime.textField.text];
		NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
		[formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
		[formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
		date = [formatter dateFromString:dateString];
		if ([BaseViewController isEmptyString:action] || [BaseViewController isEmptyString:self.actionDate.textField.text] || [BaseViewController isEmptyString:self.actionTime.textField.text] || date == nil)
		{
			[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please provide a value for the Details filled" confirmBlock:^{
				[self cancelAction:nil];
				return;
			}];
		}
		else
		{
			// add appointment to the local calendar
			NSString *patientName = [NSString stringWithFormat:@"%@ %@ %@", self.patient.patientData.name, self.patient.patientData.middleName, self.patient.patientData.lastName];
			[[CalendarManager getIstance] addEvent:action patient: patientName date:date confirmBlock:^(NSString *eventIdentifier) {
				[self saveActionInDBWithDateString:dateString date:date type:type desc:action time:self.actionTime.textField.text eventIdentifier:eventIdentifier];
			} cancelBlock:^(NSError *error) {
				[self saveActionInDBWithDateString:dateString date:date type:type desc:action time:self.actionTime.textField.text eventIdentifier:@""];
				dispatch_async(dispatch_get_main_queue(), ^{
					[[AlertViewBlocks getIstance] warningAlertViewWithMessage:@"Failed to add the event to Calendar. Please enable access to Calendar in your General Settings > Privacy" confirmBlock:^{
					}];
				});
			}];
		}
	}
	
	else if (self.actionMenu.index == 1)
	{
		type = ActionTodo;
		action = self.actionType.textField.text;
		
		if (![BaseViewController isEmptyString:self.actionDate.textField.text])
			date = [self.formatter dateFromString:self.actionDate.textField.text];
		else if (![BaseViewController isEmptyString:self.actionDateText.textField.text])
			dateString = self.actionDateText.textField.text;

		if ([BaseViewController isEmptyString:action] || ([BaseViewController isEmptyString:dateString] && date == nil))
		{
			[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please provide a value for the Details filled" confirmBlock:^{
				[self cancelAction:nil];
				return;
			}];
		}
		else
		{
			[self saveActionInDBWithDateString:dateString date:date type:type desc:action time:self.actionTime.textField.text eventIdentifier:nil];
		}
	}
	else
	{
		[self didCreateNote:self.actionNote.textView.text];
	}
}

- (void)saveActionInDBWithDateString:(NSString*)dateString date:(NSDate*)date type:(NSString*)type desc:(NSString*)desc time:(NSString*)time eventIdentifier:(NSString*)eventIdentifier
{
	[[DBManager getIstance] createActionForDashboard:self.patient.dashboard dateString:dateString date:date type:type desc:desc time:time eventIdentifier:eventIdentifier];
	self.actions = [self.patient.dashboard.actions allObjects].mutableCopy;
	
	dispatch_async(dispatch_get_main_queue(), ^{
		self.actionType.textField.text = @"";
		self.actionNote.textView.text = @"";
		self.actionDateText.textField.text = @"";
		self.actionDate.textField.text = @"";
		self.actionTime.textField.text = @"";
		[self reloadData];
	});
}


- (IBAction)cancelAction:(id)sender
{
	[self.actionMenu selectItemAtIndex:0];
	self.actionType.textField.text = @"";
	self.actionNote.textView.text = @"";
	self.actionDateText.textField.text = @"";
	self.actionDate.textField.text = @"";
	self.actionTime.textField.text = @"";
}

- (IBAction)enlargeNewNote:(id)sender
{
	NoteViewController *noteViewController = [[NoteViewController alloc] initWithNibName:@"NoteViewController" delegate:self type:NewNote string:self.actionNote.textView.text action:nil];
	[self parent:self.parentViewController addChildViewController:noteViewController superView:self.parentViewController.view animateWithDuration:0.2];
}

#pragma mark - NoteViewControllerDelegate

- (void)didEditNote
{
	[self.actionTable reloadData];
}

- (void)didCreateNote:(NSString *)string
{
	NSString *type = ActionNote;
	NSDate *date = [NSDate date];
	NSString *dateString = [self.formatter stringFromDate:date];
	if ([BaseViewController isEmptyString:string])
	{
		[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please enter \"Note\" in order to create a Note" confirmBlock:^{
			[self cancelAction:nil];
			return;
		}];
	}
	else
	{
		[self.actionNote.textView resignFirstResponder];
		[self saveActionInDBWithDateString:dateString date:date type:type desc:string time:self.actionTime.textField.text eventIdentifier:nil];
	}
}

#pragma mark - CustomTextViewDelegate

- (void)customTextViewDidChange:(CustomTextView *)textView
{
	/*
	 // limite di 150 caratteri alla CustomTextView
	NSInteger restrictedLength = 150;
	NSString *string = textView.textView.text;
	if ([[textView.textView text] length] > restrictedLength)
		textView.textView.text = [string substringToIndex:[string length]-1];
	 */
	[super customTextViewDidChange:textView];
}

- (void)customViewDidBeginEditing:(UIView *)superView
{
	[super customViewDidBeginEditing:superView];
}

- (void)customViewDidEndEditing:(UIView *)superView textView:(CustomTextView*)textView
{
	[super customViewDidEndEditing:superView textView:textView];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	
	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	return [super textFieldDidBeginEditing:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	
	[super textFieldDidEndEditing:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	return [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
}

#pragma mark - PickerFieldDelegate Methods

- (void)tapOnPickerFieldDateButton:(PickerField*)pickerField
{
	PickerDateViewController *pickerDateViewController = [[PickerDateViewController alloc] initWithNibName:@"PickerDateViewController" bundle:nil];
	pickerDateViewController.pickerField = pickerField;
	pickerDateViewController.delegate = self;
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerDateViewController];
	[self.popover setPopoverContentSize:pickerDateViewController.view.frame.size];
	[pickerDateViewController setPopover:self.popover];
	
	if (pickerField == self.actionDate)
		pickerDateViewController.date = [self.formatter dateFromString:self.actionDate.textField.text];
	else if (pickerField == self.actionTime)
	{
		pickerDateViewController.date = [self.formatter dateFromString:self.actionTime.textField.text];
		pickerDateViewController.picker.datePickerMode = UIDatePickerModeTime;
	}
	
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)tapOnPickerFieldButton:(PickerField*)pickerField
{
	PickerViewController *pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
	pickerViewController.pickerField = pickerField;
	pickerViewController.delegate = self;
	
	if (pickerField == self.actionDateText)
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"SpecificDate"];
		for(NSString *title in list)
			[pickerViewController.data addObject:title];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = StaticField;
	}
	
	// Appointment non di tipo To Do
	else if (pickerField == self.actionType && self.actionMenu.index != 1)
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:AppointmentData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = AppointmentData;
	}
	
	// Appointment di tipo To Do
	else if (pickerField == self.actionType && self.actionMenu.index == 1)
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:ToDoData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = ToDoData;
	}
	
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerViewController];
	[self.popover setPopoverContentSize:pickerViewController.view.frame.size];
	[pickerViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)pickerFieldDidBeginEditing:(PickerField *)pickerField
{
	[super pickerFieldDidBeginEditing:pickerField];
}

#pragma mark - PickerDateViewControllerDelegate Methods

- (void)pickerDateViewControllerSetDate:(NSDate *)date pickerField:(PickerField *)pickerField
{
	// in caso di Todo la valorizzazione del campo Date annulla il campo When
	if (self.actionMenu.index == 1 && pickerField == self.actionDate && date != nil)
	{
		self.actionDateText.textField.text = @"";
		pickerField.textField.text = [self.formatter stringFromDate:date];
	}
	
	else if (pickerField == self.actionTime)
	{
		NSDateFormatter *timeformatter = [[NSDateFormatter alloc] init] ;
		[timeformatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
		[timeformatter setDateFormat:@"HH:mm"];
		pickerField.textField.text = [timeformatter stringFromDate:date];
	}
	else
		pickerField.textField.text = [self.formatter stringFromDate:date];
}

#pragma mark - PickerViewControllerDelegate Methods

- (void)pickerViewControllerSetText:(NSString*)text pickerField:(PickerField*)pickerField
{
	pickerField.textField.text = text;
	
	// in caso di Todo la valorizzazione del campo When annulla il campo Date
	if (self.actionMenu.index == 1 && pickerField == self.actionDateText)
	{
		self.actionDate.textField.text = @"";
	}
}

#pragma mark - UITableViewDataSource/UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if ([self.indexPathEditing isEqual:indexPath])
	{
		Action *action = self.actions[indexPath.row];
		if ([action.type isEqualToString:ActionNote])
			return cellActionEditNoteHeight;
		else
			return cellActionEditHeight;
	}
	else
		return cellActionHeight;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.actions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;
	
	if ([self.indexPathEditing isEqual:indexPath])
	{
		Action *action = self.actions[indexPath.row];
		if ([action.type isEqualToString:ActionNote])
		{
			ActionEditNoteCell *customcell = [tableView dequeueReusableCellWithIdentifier:[ActionEditNoteCell reuseIdentifier]];
			customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
			customcell.cellDelegate = self;
			[customcell setContent:action];
			cell = customcell;
		}
		else
		{
			ActionEditCell *customcell = [tableView dequeueReusableCellWithIdentifier:[ActionEditCell reuseIdentifier]];
			customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
			customcell.cellDelegate = self;
			[customcell setContent:action];
			cell = customcell;
		}
	}
	else
	{
		ActionCell *customcell = [tableView dequeueReusableCellWithIdentifier:[ActionCell reuseIdentifier]];
		customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
		customcell.cellDelegate = self;
		Action *action = self.actions[indexPath.row];
		[customcell setContent:action];
		cell = customcell;
	}
	
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)doActionForKey:(NSString*)actionKey actionData:(id)actionData
{
	if ([actionKey isEqualToString:@"deleteAction"])
	{
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"You are going to delete this information: are you sure?" confirmBlock:^{
			[self deleteAction:actionData];
		} cancelBlock:^{
		}];
	}
	
	else if ([actionKey isEqualToString:@"editAction"])
	{
		int index = [actionData[@"index"] intValue];
		self.indexPathEditing = [NSIndexPath indexPathForRow:index inSection:0];
		[self.actionTable reloadData];
	}
	
	else if ([actionKey isEqualToString:@"editNoteAction"])
	{
		int index = [actionData[@"index"] intValue];
		Action *action = self.actions[index];
		NoteViewController *noteViewController = [[NoteViewController alloc] initWithNibName:@"NoteViewController" delegate:self type:EditNote string:nil action:action];
		[self parent:self.parentViewController addChildViewController:noteViewController superView:self.parentViewController.view animateWithDuration:0.2];
	}
	
	else if ([actionKey isEqualToString:@"saveAction"])
	{
		Action *action = (Action*)actionData;
		// edit appointment info in the local Calendar
		if ([action.type isEqualToString:ActionAppointment])
		{
			NSString *patientName = [NSString stringWithFormat:@"%@ %@ %@", self.patient.patientData.name, self.patient.patientData.middleName, self.patient.patientData.lastName];
			[[CalendarManager getIstance] editEvent:action.desc patient:patientName date:action.date eventIdentifier:action.eventIdentifier confirmBlock:^(NSString *eventIdentifier) {
				action.eventIdentifier = eventIdentifier;
				[[DBManager getIstance] saveContext];
			} cancelBlock:^(NSError *error) {
				NSLog(@"Failed to edit the event to Calendar with error: %@", error.localizedDescription);
				dispatch_async(dispatch_get_main_queue(), ^{
					[[AlertViewBlocks getIstance] warningAlertViewWithMessage:@"Failed to edit the event to Calendar. Please enable access to Calendar in your General Settings > Privacy" confirmBlock:^{
					}];
				});
			}];
		}
		self.indexPathEditing = nil;
		self.actions = [[self.patient.dashboard actions] allObjects].mutableCopy;
		[self.actionTable reloadData];
	}
	
	else if ([actionKey isEqualToString:@"saveActionNote"])
	{
		self.indexPathEditing = nil;
		self.actions = [[self.patient.dashboard actions] allObjects].mutableCopy;
		[self.actionTable reloadData];
	}
}

- (void)deleteAction:(id)actionData
{
	Action *action = actionData[@"action"];

	// delete appointment from the local Calendar
	if ([action.type isEqualToString:ActionAppointment])
	{
		[[CalendarManager getIstance] deleteEvent:action.eventIdentifier confirmBlock:^(NSString *result) {
			//
		} cancelBlock:^(NSError *error) {
			NSLog(@"Failed to delete the event from Calendar with error: %@", error.localizedDescription);
			dispatch_async(dispatch_get_main_queue(), ^{
				[[AlertViewBlocks getIstance] warningAlertViewWithMessage:@"Failed to remove the event from Calendar. Please enable access to Calendar in your General Settings" confirmBlock:^{
				}];
			});
		}];
	}
	
	// delete action from the DB
	dispatch_async(dispatch_get_main_queue(), ^{
		[[DBManager getIstance] deleteObject:action];
		self.actions = [[self.patient.dashboard actions] allObjects].mutableCopy;
		[self reloadData];		
	});
}


@end
