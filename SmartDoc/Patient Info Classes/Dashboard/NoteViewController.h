//
//  InfoViewController.h
//  SmartDoc
//
//  Created by Enrico on 11/09/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"

@class Action;

typedef enum{
	EditNote,
	NewNote,
}NoteType;

@protocol NoteViewControllerDelegate <NSObject>
- (void)didEditNote;
- (void)didCreateNote:(NSString*)string;
@end

@interface NoteViewController : BaseViewController

@property (nonatomic, strong) NSString *string;
@property (nonatomic, strong) Action *action;
@property (nonatomic, assign) id <NoteViewControllerDelegate> delegate;
@property (nonatomic) NoteType type;

- (id)initWithNibName:(NSString *)nibNameOrNil delegate:(id <NoteViewControllerDelegate>)delegate type:(NoteType)type string:(NSString*)string action:(Action*)action;

@end
