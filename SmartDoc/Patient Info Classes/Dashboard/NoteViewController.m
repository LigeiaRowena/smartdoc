//
//  InfoViewController.m
//  SmartDoc
//
//  Created by Enrico on 11/09/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "NoteViewController.h"
#import "Action.h"

@interface NoteViewController ()

@property (weak, nonatomic) IBOutlet CustomTextView *note;

@end

#pragma mark - Init NoteViewController

@implementation NoteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil delegate:(id <NoteViewControllerDelegate>)delegate type:(NoteType)type string:(NSString*)string action:(Action*)action
{
	self = [super initWithNibName:nibNameOrNil bundle:nil];
	if (self) {
		self.delegate = delegate;
		self.type = type;
		self.string = string;
		self.action = action;
	}
	return self;

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	// data
	if (self.type == EditNote)
		self.note.textView.text = self.action.desc;
	else
		self.note.textView.text = self.string;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
}

- (void)dealloc
{
	self.delegate = nil;
}

#pragma mark - CustomTextViewDelegate

- (void)customTextViewDidBeginEditing:(CustomTextView *)textView
{
	[super customTextViewDidBeginEditing:textView];
}

- (void)customTextViewDidEndEditing:(CustomTextView *)textView
{
	[super customTextViewDidEndEditing:textView];
}

- (void)customTextViewDidChange:(CustomTextView *)textView
{
	[super customTextViewDidChange:textView];
}

#pragma mark - Actions

- (IBAction)save:(id)sender
{
	if (self.type == EditNote)
	{
		self.action.desc = self.note.textView.text;
		[[DBManager getIstance] saveContext];
		
		if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didEditNote)])
			[self.delegate didEditNote];
	}
	
	else
	{
		if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didCreateNote:)])
			[self.delegate didCreateNote:self.note.textView.text];
	}
	
	
	[self parent:self.parentViewController removeViewController:self animateWithDuration:0.2];
}

- (IBAction)cancel:(id)sender
{
	[self parent:self.parentViewController removeViewController:self animateWithDuration:0.2];
}


@end
