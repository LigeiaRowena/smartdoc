//
//  PatientReminderViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 07/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"
#import "MenuView.h"
#import "ActionCell.h"
#import "ActionEditCell.h"
#import "ActionEditNoteCell.h"
#import "PickerDateViewController.h"
#import "PickerViewController.h"
#import "NoteViewController.h"

@interface PatientDashboardViewController : BaseViewController <MenuViewDelegate, CellDelegate, PickerDateViewControllerDelegate, PickerViewControllerDelegate, NoteViewControllerDelegate>

@end
