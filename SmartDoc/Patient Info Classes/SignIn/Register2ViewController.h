//
//  SignIn2ViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 09/09/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"
#import "PickerViewController.h"

@interface Register2ViewController : BaseViewController <PickerViewControllerDelegate>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *pass;
@property (nonatomic, strong) NSString *confPass;

@end
