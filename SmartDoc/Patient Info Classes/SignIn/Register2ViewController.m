//
//  SignIn2ViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 09/09/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "Register2ViewController.h"
#import "PatientListViewController.h"
#import "Utils.h"

@interface Register2ViewController ()

@property (nonatomic, weak) IBOutlet UILabel *spec;
@property (nonatomic, weak) IBOutlet CustomTextField *number;
@property (nonatomic, weak) IBOutlet CustomTextField *address;
@property (nonatomic, weak) IBOutlet CustomTextField *zipcode;
@property (nonatomic, weak) IBOutlet CustomTextField *city;
@property (nonatomic, weak) IBOutlet CustomTextField *country;

@end

@implementation Register2ViewController

#pragma mark - Init Register2ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// set the background color
	self.view.backgroundColor = lightBlueColorBackground();
	self.mainscroll.backgroundColor = lightBlueColorBackground();
}

- (void)setup
{
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	return [super textFieldDidBeginEditing:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	[super textFieldDidEndEditing:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	return [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
}

#pragma mark - PickerViewControllerDelegate Methods

- (void)pickerViewControllerSetText:(NSString*)text pickerField:(PickerField*)pickerField
{
	self.spec.text = text;
}

#pragma mark - Actions

- (IBAction)tapSpec:(id)sender
{
	UIButton *button = (UIButton*)sender;
	
	PickerViewController *pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
	pickerViewController.delegate = self;
	
	NSDictionary *dict = [Utils getDictionaryFromPlist:QUESTIONS_PLIST];
	NSArray *list = [[dict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
	for(NSString *title in list)
		[pickerViewController.data addObject:title];
	[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
	pickerViewController.pickerType = StaticField;
	
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerViewController];
	[self.popover setPopoverContentSize:pickerViewController.view.frame.size];
	[pickerViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:button.bounds inView:button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	[pickerViewController selectItem:self.spec.text];
}

- (IBAction)back:(id)sender
{
	[self parent:self.parentViewController popViewController:self animateWithDuration:0.2];
}


- (IBAction)signUp:(id)sender
{
	[AFRequests registerUser:self.name email:self.email password:self.pass passwordConfirm:self.confPass specialization:self.spec.text number:self.number.text address:self.address.text zipcode:self.zipcode.text city:self.city.text country:self.country.text success:^(NSHTTPURLResponse *response, id responseObject) {
		NSString *message = responseObject[@"MESSAGE"];
		[[AlertViewBlocks getIstance] informationConfirmAlertViewWithmessage:message confirmBlock:^{
			[self removeSubchildsFrom:self.parentViewController.parentViewController];
		} cancelBlock:^{
			[self removeSubchildsFrom:self.parentViewController.parentViewController];
		}];
	} failure:^(NSHTTPURLResponse *response, NSError *error) {
		NSString *message = error.userInfo[@"message"];
		[[AlertViewBlocks getIstance] warningAlertViewWithMessage:message confirmBlock:^{
		}];
	}];
}

@end
