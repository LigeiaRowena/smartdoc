//
//  SignIn1ViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 09/09/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "Register1ViewController.h"
#import "Register2ViewController.h"

@interface Register1ViewController ()

@property (nonatomic, weak) IBOutlet CustomTextField *nome;
@property (nonatomic, weak) IBOutlet CustomTextField *email;
@property (nonatomic, weak) IBOutlet CustomTextField *password;
@property (nonatomic, weak) IBOutlet CustomTextField *confPassword;

@end

@implementation Register1ViewController

#pragma mark - Init Register1ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// set the background color
	self.view.backgroundColor = lightBlueColorBackground();
	self.mainscroll.backgroundColor = lightBlueColorBackground();
}

- (void)setup
{
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	return [super textFieldDidBeginEditing:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	[super textFieldDidEndEditing:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	if (textField == self.email || textField == self.password || textField == self.confPassword)
		return YES;
	else
		return [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
}

#pragma mark - Actions

- (IBAction)back:(id)sender
{
	[self parent:self.parentViewController popViewController:self animateWithDuration:0.2];
}


- (IBAction)signUp:(id)sender
{
	if (![BaseViewController isEmptyString:self.nome.text] && ![BaseViewController isEmptyString:self.password.text] && ![BaseViewController isEmptyString:self.email.text] && ![BaseViewController isEmptyString:self.confPassword.text])
	{
		Register2ViewController *vc = [[Register2ViewController alloc] initWithNibName:@"Register2ViewController" bundle:nil];
		vc.name = self.nome.text;
		vc.email = self.email.text;
		vc.pass = self.password.text;
		vc.confPass = self.confPassword.text;
		[self parent:self pushViewController:vc superView:self.view animateWithDuration:0.2];
	}
	else
		[[AlertViewBlocks getIstance] warningAlertViewWithMessage:@"Please fill all the fields." confirmBlock:^{
		}];
}


@end
