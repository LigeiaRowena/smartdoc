//
//  PatientPrescriptionsController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 17/11/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "PatientPrescriptionsController.h"
#import "PatientPrescriptionDetailEditController.h"
#import "PatientPrescriptionDetailController.h"
#import "Prescription.h"
#import "PatientNewPrescriptionViewController.h"

#define cellPrescriptionsHeight 50

@interface PatientPrescriptionsController ()

@property (nonatomic, strong) NSMutableArray *prescriptions;

@end

#pragma mark - Init PatientPrescriptionsController

@implementation PatientPrescriptionsController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// prescriptions table
	UINib *cellNib = [UINib nibWithNibName:@"PrescriptionCell" bundle:nil];
	[self.prescriptionsTable registerNib:cellNib forCellReuseIdentifier:[PrescriptionCell reuseIdentifier]];
}

- (void)checkBlankPatients
{
	for (Prescription *prescription in [[self.patient prescriptions] allObjects])
	{
		if ([BaseViewController isEmptyString:prescription.title])
			[[DBManager getIstance] deleteObject:prescription];
	}
}

- (void)setup
{
	// prescriptions table
	[self checkBlankPatients];
	self.prescriptions = [[self.patient prescriptions] allObjects].mutableCopy;
	[self.prescriptionsTable reloadData];
}

- (void)reloadData
{
	[self checkBlankPatients];
	self.prescriptions = [[self.patient prescriptions] allObjects].mutableCopy;
	[self.prescriptionsTable reloadData];
}


#pragma mark - Actions

- (IBAction)createNewPrescription:(id)sender
{
	Prescription *prescription = [[DBManager getIstance] createPrescriptionForPatient:self.patient date:[NSDate date]];
	PatientNewPrescriptionViewController *vc = [[PatientNewPrescriptionViewController alloc] initWithNibName:@"PatientNewPrescriptionViewController" bundle:nil];
	vc.prescription = prescription;
	vc.patient = self.patient;
	vc.prescriptionTitle = @"CREATE NEW PRESCRIPTION";
	[self parent:self pushViewController:vc superView:self.view animateWithDuration:0.2];
}

#pragma mark - UITableViewDataSource/UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.prescriptions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;
	
	PrescriptionCell *customcell = [tableView dequeueReusableCellWithIdentifier:[PrescriptionCell reuseIdentifier]];
	customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
	customcell.cellDelegate = self;
	Prescription *prescription = self.prescriptions[indexPath.row];
	[customcell setContent:prescription];
	cell = customcell;
	
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	Prescription *prescription = self.prescriptions[indexPath.row];
	PatientPrescriptionDetailController *vc = [[PatientPrescriptionDetailController alloc] initWithNibName:@"PatientPrescriptionDetailController" bundle:nil];
	vc.prescription = prescription;
	vc.patient = self.patient;
	[self parent:self pushViewController:vc superView:self.view animateWithDuration:0.2];
}



@end
