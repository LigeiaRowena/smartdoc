//
//  PatientNewPrescriptionViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 31/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "PatientNewPrescriptionViewController.h"
#import "PatientPrescriptionDetailEditController.h"
#import "PatientPrescriptionsController.h"

@interface PatientNewPrescriptionViewController ()
@end

@implementation PatientNewPrescriptionViewController

#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	
	PatientPrescriptionDetailEditController *vc = [[PatientPrescriptionDetailEditController alloc] initWithNibName:@"PatientPrescriptionDetailEditController" bundle:nil];
	vc.prescription = self.prescription;
	vc.patient = self.patient;
	vc.prescriptionTitle = self.prescriptionTitle;
	[self parent:self addChildViewController:vc superView:self.mainView];
}


#pragma mark - Actions

- (IBAction)cancel:(id)sender
{
	[[DBManager getIstance] deleteObject:self.prescription];
	PatientPrescriptionsController *parent = (PatientPrescriptionsController*)self.parentViewController;
	[parent reloadData];
	[self parent:parent popViewController:self animateWithDuration:0.2];
}

- (IBAction)save:(id)sender
{
	if (![BaseViewController isEmptyString:self.prescription.title])
	{
		PatientPrescriptionsController *parent = (PatientPrescriptionsController*)self.parentViewController;
		[parent reloadData];
		[self parent:parent popViewController:self animateWithDuration:0.2];
	}
	else
	{
		[[AlertViewBlocks getIstance] warningConfirmAlertViewWithMessage:@"Please insert prescrition name" confirmBlock:^{
		} cancelBlock:^{
			[[DBManager getIstance] deleteObject:self.prescription];
			PatientPrescriptionsController *parent = (PatientPrescriptionsController*)self.parentViewController;
			[parent reloadData];
			[self parent:parent popViewController:self animateWithDuration:0.2];
		}];
	}
}




@end
