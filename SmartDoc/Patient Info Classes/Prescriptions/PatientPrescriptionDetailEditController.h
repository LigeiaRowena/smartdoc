//
//  PatientPrescriptionDetailController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 17/11/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "PatientPrescriptionsController.h"
#import "Prescription.h"
#import "BaseViewController.h"
#import "PickerViewController.h"
#import "PickerDrugViewController.h"
#import "PickerDateViewController.h"
#import "DrugCell.h"
#import "DiagnosticTestCell.h"

@interface PatientPrescriptionDetailEditController : BaseViewController < PickerViewControllerDelegate, PickerDrugViewControllerDelegate, PickerDateViewControllerDelegate, CellDelegate>

@property (nonatomic, strong) Prescription *prescription;
@property (nonatomic, strong) NSString *prescriptionTitle;
@property (nonatomic) BOOL isEditing;

@end
