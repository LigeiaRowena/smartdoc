//
//  PatientPrescriptionDetailController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 03/09/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"
#import "Prescription.h"

@interface PatientPrescriptionDetailController : BaseViewController 

@property (nonatomic, strong) Prescription *prescription;

@end
