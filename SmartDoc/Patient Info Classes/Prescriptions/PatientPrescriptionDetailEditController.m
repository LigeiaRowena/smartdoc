//
//  PatientPrescriptionDetailController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 17/11/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "PatientPrescriptionDetailEditController.h"
#import "PatientPrescriptionDetailController.h"
#import "PatientPrescriptionsController.h"
#import "DiagnosticTest.h"
#import "PatientData.h"
#import "PrescribedTherapy.h"
#import "EditPhotoViewController.h"
#import "GalleryViewController.h"

#define cellDrugsHeight 80
#define cellTestHeight 190

@interface PatientPrescriptionDetailEditController ()

@property (nonatomic, weak) IBOutlet UILabel *prescriptionTitleLabel;
@property (nonatomic, weak) IBOutlet CustomTextField *titleText;
@property (nonatomic, weak) IBOutlet PickerField *dateText;
@property (nonatomic, weak) IBOutlet CustomTextView *detailsText;
@property (nonatomic, weak) IBOutlet PickerField *drugName;
@property (nonatomic, weak) IBOutlet CustomTextField *drugDosageText;
@property (nonatomic, weak) IBOutlet PickerField *drugUnitText;
@property (nonatomic, weak) IBOutlet PickerField *drugFrequencyText;
@property (nonatomic, weak) IBOutlet PickerField *drugRouteText;
@property (nonatomic, weak) IBOutlet UITableView *drugsTable;
@property (nonatomic, weak) IBOutlet UIView *testHeader;
@property (nonatomic, weak) IBOutlet PickerField *testType;
@property (nonatomic, weak) IBOutlet CustomTextField *testNote;
@property (nonatomic, weak) IBOutlet UITableView *testTable;
@property (nonatomic, weak) IBOutlet CustomTextView *physicalPrescription;
@property (nonatomic, weak) IBOutlet UIView *referralHeader;
@property (nonatomic, weak) IBOutlet PickerField *referralText;
@property (nonatomic, weak) IBOutlet CustomTextField *referralNote;
@property (weak, nonatomic) IBOutlet UIView *saveView;

@property (nonatomic, strong) NSMutableArray *tests;
@property (nonatomic, strong) NSMutableArray *drugs;

@end

#pragma mark - Init PatientPrescriptionDetailEditController

@implementation PatientPrescriptionDetailEditController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		self.isEditing = NO;
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// drug table
	UINib *cellNibdrug = [UINib nibWithNibName:@"DrugCell" bundle:nil];
	[self.drugsTable registerNib:cellNibdrug forCellReuseIdentifier:[DrugCell reuseIdentifier]];
	
	// test table
	UINib *cellNibtest = [UINib nibWithNibName:@"DiagnosticTestCell" bundle:nil];
	[self.testTable registerNib:cellNibtest forCellReuseIdentifier:[DiagnosticTestCell reuseIdentifier]];
}

- (void)setup
{
	if (self.imagePickerController.isBeingDismissed)
		return;
	
	self.mainscroll.frame = self.view.bounds;
	
	// save button mode
	if (self.isEditing)
	{
		self.saveView.hidden = NO;
	}
	else
	{
		self.saveView.hidden = YES;
	}

	self.prescriptionTitleLabel.text = self.prescriptionTitle;
	self.titleText.text = self.prescription.title;
	self.dateText.textField.text = [self.formatter stringFromDate: self.prescription.startDate];
	self.detailsText.textView.text = self.prescription.details;
	self.physicalPrescription.textView.text = self.prescription.physicalPrescription;
	self.referralText.textField.text = self.prescription.referral;
	self.referralNote.text = self.prescription.referralNote;
	
	// drugs table
	self.drugs = [[DBManager getIstance] getTherapiesFromPrescription:self.prescription].mutableCopy;
	[self animateDrugsTableWithDelta:((int)[self.drugs count]*cellDrugsHeight)];
	
	// test table
	self.tests = [[self.prescription tests] allObjects].mutableCopy;
	[self animateTestsTableWithDelta:((int)[self.tests count]*cellTestHeight)];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
	if ([self.parentViewController.parentViewController isKindOfClass:[PatientPrescriptionsController class]])
	{
		PatientPrescriptionsController *parent = (PatientPrescriptionsController*)self.parentViewController.parentViewController;
		[parent reloadData];
	}
	else if ([self.parentViewController isKindOfClass:[PatientPrescriptionsController class]])
	{
		PatientPrescriptionsController *parent = (PatientPrescriptionsController*)self.parentViewController;
		[parent reloadData];
	}
}


#pragma mark - Animations

- (void)animateDrugsTableWithDelta:(int)delta
{
	if (self.drugsTable.contentSize.height >= delta && delta > 0)
		return;
	
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.drugsTable.frame = CGRectMake(self.drugsTable.frame.origin.x, self.drugsTable.frame.origin.y, self.drugsTable.frame.size.width, self.drugsTable.frame.size.height + delta);
		self.testHeader.frame = CGRectMake(self.testHeader.frame.origin.x, self.testHeader.frame.origin.y + delta, self.testHeader.frame.size.width, self.testHeader.frame.size.height);
		self.testTable.frame = CGRectMake(self.testTable.frame.origin.x, self.testTable.frame.origin.y + delta, self.testTable.frame.size.width, self.testTable.frame.size.height);
		self.physicalPrescription.frame = CGRectMake(self.physicalPrescription.frame.origin.x, self.physicalPrescription.frame.origin.y + delta, self.physicalPrescription.frame.size.width, self.physicalPrescription.frame.size.height);
		self.referralHeader.frame = CGRectMake(self.referralHeader.frame.origin.x, self.referralHeader.frame.origin.y + delta, self.referralHeader.frame.size.width, self.referralHeader.frame.size.height);
		//self.saveButton.frame = CGRectMake(self.saveButton.frame.origin.x, self.saveButton.frame.origin.y + delta, self.saveButton.frame.size.width, self.saveButton.frame.size.height);
	} completion:^(BOOL s){
		[self.drugsTable reloadData];
	}];
}

- (void)animateTestsTableWithDelta:(int)delta
{
	if (self.testTable.contentSize.height >= delta && delta > 0)
		return;
	
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.testTable.frame = CGRectMake(self.testTable.frame.origin.x, self.testTable.frame.origin.y, self.testTable.frame.size.width, self.testTable.frame.size.height + delta);
		self.physicalPrescription.frame = CGRectMake(self.physicalPrescription.frame.origin.x, self.physicalPrescription.frame.origin.y + delta, self.physicalPrescription.frame.size.width, self.physicalPrescription.frame.size.height);
		self.referralHeader.frame = CGRectMake(self.referralHeader.frame.origin.x, self.referralHeader.frame.origin.y + delta, self.referralHeader.frame.size.width, self.referralHeader.frame.size.height);
		//self.saveButton.frame = CGRectMake(self.saveButton.frame.origin.x, self.saveButton.frame.origin.y + delta, self.saveButton.frame.size.width, self.saveButton.frame.size.height);
	} completion:^(BOOL s){
		[self.testTable reloadData];
	}];
}


#pragma mark - CustomTextViewDelegate

- (void)customTextViewDidBeginEditing:(CustomTextView *)textView
{
	[super customTextViewDidBeginEditing:textView];
}

- (void)customTextViewDidEndEditing:(CustomTextView *)textView
{
	NSString *text = [textView.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	if (textView == self.detailsText)
	{
		self.prescription.details = text;
		[[DBManager getIstance] saveContext];
	}
		
	else if (textView == self.physicalPrescription)
	{
		self.prescription.physicalPrescription = text;
		[[DBManager getIstance] saveContext];
	}
	
	[super customTextViewDidEndEditing:textView];
}

- (void)customTextViewDidChange:(CustomTextView *)textView
{
	[super customTextViewDidChange:textView];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	return [super textFieldDidBeginEditing:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	
	NSString *text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	if (textField == self.titleText)
	{
		self.prescription.title = text;
		[[DBManager getIstance] saveContext];
	}
	else if ([textField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]])
	{
		int index = (int)textField.tag;
		PrescribedTherapy *therapy = self.drugs[index];
		therapy.dosage = text;
		[[DBManager getIstance] saveContext];
		self.drugs = [[DBManager getIstance] getTherapiesFromPrescription:self.prescription].mutableCopy;
	}
	else if ([textField.accessibilityIdentifier isEqualToString:[DiagnosticTestCell reuseIdentifier]])
	{
		int index = (int)textField.tag;
		DiagnosticTest *test = self.tests[index];
		test.note = text;
		[[DBManager getIstance] saveContext];
		self.tests = [[self.prescription tests] allObjects].mutableCopy;
	}

	else if (textField == self.referralNote)
	{
		self.prescription.referralNote = text;
		[[DBManager getIstance] saveContext];
	}

	[super textFieldDidEndEditing:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	return [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
}

#pragma mark - PickerViewControllerDelegate Methods

- (void)pickerViewControllerSetText:(NSString*)text pickerField:(PickerField*)pickerField
{
	pickerField.textField.text = text;

	if ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugNameField])
	{
		int index = (int)pickerField.tag;
		PrescribedTherapy *therapy = self.drugs[index];
		therapy.drugName = text;
		[[DBManager getIstance] saveContext];
	}
	else if ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugUnitField])
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"DrugDosageUnit"];
		pickerField.textField.text = list[pickerField.pickerIndex][@"name"];
		int index = (int)pickerField.tag;
		PrescribedTherapy *therapy = self.drugs[index];
		therapy.dosageUnit = text;
		[[DBManager getIstance] saveContext];
		self.drugs = [[DBManager getIstance] getTherapiesFromPrescription:self.prescription].mutableCopy;
	}
	
	else if ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugFrequencyField])
	{
		int index = (int)pickerField.tag;
		PrescribedTherapy *therapy = self.drugs[index];
		therapy.frequency = text;
		[[DBManager getIstance] saveContext];
		self.drugs = [[DBManager getIstance] getTherapiesFromPrescription:self.prescription].mutableCopy;
	}
	
	else if ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugRouteField])
	{
		int index = (int)pickerField.tag;
		PrescribedTherapy *therapy = self.drugs[index];
		therapy.route = text;
		[[DBManager getIstance] saveContext];
		self.drugs = [[DBManager getIstance] getTherapiesFromPrescription:self.prescription].mutableCopy;
	}

	else if ([pickerField.accessibilityIdentifier isEqualToString:[DiagnosticTestCell reuseIdentifier]])
	{
		int index = (int)pickerField.tag;
		DiagnosticTest *test = self.tests[index];
		test.name = text;
		[[DBManager getIstance] saveContext];
		self.tests = [[self.prescription tests] allObjects].mutableCopy;
	}
	
	else if (pickerField == self.referralText)
	{
		self.prescription.referral = text;
		[[DBManager getIstance] saveContext];
	}
}

#pragma mark - PickerDrugViewControllerDelegate Methods

/*
- (void)pickerDrugViewControllerSetText:(NSString*)text pickerField:(PickerField*)pickerField
{
	pickerField.textField.text = text;
	
	if ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]])
	{
		int index = (int)pickerField.tag;
		PrescribedTherapy *therapy = self.drugs[index];
		therapy.drugName = text;
		[[DBManager getIstance] saveContext];
		self.drugs = [[DBManager getIstance] getTherapiesFromPrescription:self.prescription].mutableCopy;
	}
}
*/

#pragma mark - PickerDateViewControllerDelegate Methods

- (void)pickerDateViewControllerSetDate:(NSDate *)date pickerField:(PickerField *)pickerField
{
	pickerField.textField.text = [self.formatter stringFromDate:date];
	
	if (pickerField == self.dateText)
	{
		self.prescription.startDate = date;
		[[DBManager getIstance] saveContext];
	}
}

#pragma mark - PickerFieldDelegate Methods

- (void)tapOnPickerFieldDrugButton:(PickerField*)pickerField
{
	PickerViewController *pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
	pickerViewController.pickerField = pickerField;
	pickerViewController.delegate = self;
	pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:DrugActiveData text:@""].mutableCopy;
	pickerViewController.pickerType = DrugNameField;
	pickerViewController.dataType = DrugActiveData;
	
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerViewController];
	[self.popover setPopoverContentSize:pickerViewController.view.frame.size];
	[pickerViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	
	/*
	PickerDrugViewController *pickerDrugViewController = [[PickerDrugViewController alloc] initWithNibName:@"PickerDrugViewController" bundle:nil];
	pickerDrugViewController.pickerField = pickerField;
	pickerDrugViewController.delegate = self;
	
	if (pickerField == self.drugName || [pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]])
	{
		pickerDrugViewController.pickerType = DrugNameField;
	}
	
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerDrugViewController];
	[self.popover setPopoverContentSize:pickerDrugViewController.view.frame.size];
	[pickerDrugViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	 */
}

- (void)tapOnPickerFieldDateButton:(PickerField*)pickerField
{
	PickerDateViewController *pickerDateViewController = [[PickerDateViewController alloc] initWithNibName:@"PickerDateViewController" bundle:nil];
	pickerDateViewController.pickerField = pickerField;
	pickerDateViewController.delegate = self;
	
	if (pickerField == self.dateText)
		pickerDateViewController.date = [self.formatter dateFromString:self.dateText.textField.text];
	
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerDateViewController];
	[self.popover setPopoverContentSize:pickerDateViewController.view.frame.size];
	[pickerDateViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

}

- (void)tapOnPickerFieldButton:(PickerField*)pickerField
{
	PickerViewController *pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
	pickerViewController.pickerField = pickerField;
	pickerViewController.delegate = self;
	
	if ((pickerField == self.drugUnitText) || ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugUnitField]))
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"DrugDosageUnit"];
		for(NSDictionary *item in list)
			[pickerViewController.data addObject:item[@"name"]];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = DrugUnitField;
	}
	
	else if ((pickerField == self.drugFrequencyText) || ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugFrequencyField]))
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"Frequency"];
		for(NSString *title in list)
			[pickerViewController.data addObject:title];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = DrugFrequencyField;
	}
	
	else if ((pickerField == self.drugRouteText) || ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugRouteField]))
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:RouteData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = RouteData;
	}
	
	else if (pickerField == self.testType || [pickerField.accessibilityIdentifier isEqualToString:[DiagnosticTestCell reuseIdentifier]])
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:DiagnosticTestsData];
		for(NSString *title in list)
			[pickerViewController.data addObject:title];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = StaticField;
	}
	
	else if (pickerField == self.referralText)
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:ReferrerData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = ReferrerData;
	}
	
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerViewController];
	[self.popover setPopoverContentSize:pickerViewController.view.frame.size];
	[pickerViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)pickerFieldDidBeginEditing:(PickerField *)pickerField
{
	[super pickerFieldDidBeginEditing:pickerField];
}

#pragma mark - Actions

- (IBAction)save:(id)sender
{
	PatientPrescriptionDetailController *vc = [[PatientPrescriptionDetailController alloc] initWithNibName:@"PatientPrescriptionDetailController" bundle:nil];
	PatientPrescriptionsController *parent = (PatientPrescriptionsController*)self.parentViewController;
	vc.patient = self.patient;
	vc.prescription = self.prescription;
	[self parent:parent fadeToViewController:vc fromViewController:self superView:parent.view animateWithDuration:0.2];
}


- (IBAction)newDrug:(id)sender
{
	if (![BaseViewController isEmptyString:self.drugName.textField.text])
	{
		[[DBManager getIstance] createPrescribedTherapyForPatient:self.patient date:[NSDate date] drugName:self.drugName.textField.text dosage:self.drugDosageText.text frequency:self.drugFrequencyText.textField.text route:self.drugRouteText.textField.text active:YES suspended:NO prescribed:NO dosageUnit:self.drugUnitText.textField.text visit:nil prescription:self.prescription];
		self.drugName.textField.text = @"";
		self.drugDosageText.text = @"";
		self.drugFrequencyText.textField.text = @"";
		self.drugRouteText.textField.text = @"";
		self.drugUnitText.textField.text = @"";
		self.drugs = [[DBManager getIstance] getTherapiesFromPrescription:self.prescription].mutableCopy;
		[self animateDrugsTableWithDelta:cellDrugsHeight];
	}
	else
	{
		[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please provide a value for the Details filled" confirmBlock:^{
			self.drugName.textField.text = @"";
			self.drugDosageText.text = @"";
			self.drugFrequencyText.textField.text = @"";
			self.drugRouteText.textField.text = @"";
			self.drugUnitText.textField.text = @"";
		}];
	}
}

- (IBAction)newDiagnosticTest:(id)sender
{
	if ([self.testType.textField.text isNotBlank])
	{
		[[DBManager getIstance] createDiagnosticTestForPrescription:self.prescription name:self.testType.textField.text note:self.testNote.text];
		self.tests = [[self.prescription tests] allObjects].mutableCopy;
		self.testType.textField.text = @"";
		self.testNote.text = @"";
		[self animateTestsTableWithDelta:cellTestHeight];
	}
	else
	{
		[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please provide a value for the Details filled" confirmBlock:^{
			self.testType.textField.text = @"";
			self.testNote.text = @"";
		}];
	}
}


#pragma mark - UITableViewDataSource/UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (tableView == self.testTable)
		return [self.tests count];
	else
		return [self.drugs count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;
	
	if (tableView == self.drugsTable)
	{
		DrugCell *customcell = [tableView dequeueReusableCellWithIdentifier:[DrugCell reuseIdentifier]];
		customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
		customcell.cellDelegate = self;
		PrescribedTherapy *therapy = self.drugs[indexPath.row];
		[customcell setContent:therapy];
		cell = customcell;
	}
	
	else if (tableView == self.testTable)
	{
		DiagnosticTestCell *customcell = [tableView dequeueReusableCellWithIdentifier:[DiagnosticTestCell reuseIdentifier]];
		customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
		customcell.cellDelegate = self;
		DiagnosticTest *test = self.tests[indexPath.row];
		[customcell setDiagnosticTest:test];
		cell = customcell;
	}
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)doActionForKey:(NSString*)actionKey actionData:(id)actionData
{
	if ([actionKey isEqualToString:@"deleteDrug"])
	{
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"You are going to delete this information: are you sure?" confirmBlock:^{
			int index = [actionData[@"index"] intValue];
			PrescribedTherapy *therapy = self.drugs[index];
			[[DBManager getIstance] deleteObject:therapy];
			self.drugs = [[DBManager getIstance] getTherapiesFromPrescription:self.prescription].mutableCopy;
			[self animateDrugsTableWithDelta:-cellDrugsHeight];
		} cancelBlock:^{
		}];
	}
	
	else if ([actionKey isEqualToString:@"deleteTest"])
	{
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"You are going to delete this information: are you sure?" confirmBlock:^{
			int index = [actionData[@"index"] intValue];
			DiagnosticTest *test = self.tests[index];
			[[DBManager getIstance] deleteObject:test];
			self.tests = [[self.prescription tests] allObjects].mutableCopy;
			[self animateTestsTableWithDelta:-cellTestHeight];
		} cancelBlock:^{
		}];
		

	}
	
	else if ([actionKey isEqualToString:@"addPhoto"])
	{
		// disponibili sia camera che photos
		if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
		{
			// mostra alert per scelta tra camera e photos
			UIImage *firstButtonBg = [UIImage imageNamed:@"Butt_cameragrayalert"];
			UIImage *secondButtonBg = [UIImage imageNamed:@"Butt_photosgreenalert"];
			[[AlertViewBlocks getIstance] twoOptionsAlertViewWithmessage:@"Load pictures from the iPad camera roll or take a new picture" firstButtonBg:firstButtonBg secondButtonBg:secondButtonBg confirmBlock:^{
				[self showCamera:actionData[@"index"]];
			} secondConfirm:^{
				[self showPhotos:actionData[@"index"]];
			}];
		}
		
		// disponibile solo camera
		else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && ![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
		{
			[self showCamera:actionData[@"index"]];
		}
		
		// disponibile solo photos
		else if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
		{
			[self showPhotos:actionData[@"index"]];
		}
	}
	else if ([actionKey isEqualToString:@"selectGallery"])
	{
		int index = [actionData[@"index"] intValue];
		DiagnosticTest *test = self.tests[index];
		GalleryViewController *galleryViewController = [[GalleryViewController alloc] initWithNibName:@"GalleryViewController" bundle:nil];
		[self presentViewController:galleryViewController animated:YES completion:nil];
		galleryViewController.headerLabel.text = test.name;
		galleryViewController.data = [[test diagnosticImages] allObjects].mutableCopy;
	}
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
	if ([info[UIImagePickerControllerMediaType] isEqualToString:@"public.image"])
	{
		// modifica foto e salva nelle diagnostic test
		UIImage *originalImage = info[UIImagePickerControllerOriginalImage];
		EditPhotoViewController *editPhotoViewController = [[EditPhotoViewController alloc] initWithNibName:@"EditPhotoViewController" bundle:nil];
		if ([self.presentedViewController isKindOfClass:[UIImagePickerController class]])
		{
			[self dismissViewControllerAnimated:YES completion:nil];
		}
		[self presentViewController:editPhotoViewController animated:YES completion:nil];
		[editPhotoViewController savePhoto:originalImage note:originalImage.note completion:^(UIImage *savedImage) {
			int index = [picker.index intValue];
			DiagnosticTest *test = self.tests[index];
			NSData *data = UIImagePNGRepresentation(savedImage);
			if (data == nil)
				return ;
			[[DBManager getIstance] createDiagnosticImageForTest:test image:data note:savedImage.note];
			[self.testTable reloadData];
			
			// se il picker è di tipo camera chiedi se salvare la foto anche delle photos
			if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
			{
				[[AlertViewBlocks getIstance] informationConfirmAlertViewWithmessage:@"Do you want to save the image in Photos app?" confirmBlock:^{
					UIImageWriteToSavedPhotosAlbum(savedImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
				} cancelBlock:^{
				}];
			}
		} cancel:^{
		}];
	}
}

@end
