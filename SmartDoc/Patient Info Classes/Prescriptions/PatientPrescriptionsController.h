//
//  PatientPrescriptionsController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 17/11/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"
#import "PrescriptionCell.h"

@interface PatientPrescriptionsController : BaseViewController <CellDelegate>

@property (nonatomic, weak) IBOutlet UITableView *prescriptionsTable;

- (void)reloadData;

@end
