//
//  PatientPrescriptionDetailController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 03/09/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "PatientPrescriptionDetailController.h"
#import "PatientPrescriptionsController.h"
#import "PatientPrescriptionDetailEditController.h"
#import "DiagnosticTest.h"
#import "PatientData.h"
#import "PrescribedTherapy.h"
#import "DrugLabelCell.h"
#import "TestLabelCell.h"
#import "CustomTableView.h"
#import "PDFExporter.h"

#define cellDrugsHeight 35
#define headerDrugsHeight 62

@interface PatientPrescriptionDetailController ()

@property (nonatomic, weak) IBOutlet RoundedLabel *prescriptionTitle;
@property (nonatomic, weak) IBOutlet RoundedLabel *date;
@property (nonatomic, weak) IBOutlet FitLabel *details;
@property (nonatomic, weak) IBOutlet CustomTableView *drugTable;
@property (nonatomic, weak) IBOutlet CustomTableView *diagnosticTestTable;
@property (nonatomic, weak) IBOutlet FitLabel *phisicalPrescriptions;
@property (nonatomic, weak) IBOutlet UIView *bottomView;
@property (nonatomic, weak) IBOutlet RoundedLabel *referral;
@property (nonatomic, weak) IBOutlet RoundedLabel *note;

@property (nonatomic, strong) NSMutableArray *drugs;
@property (nonatomic, strong) NSMutableArray *tests;

@end

@implementation PatientPrescriptionDetailController

#pragma mark - Init PatientPrescriptionDetailController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// table drugs
	UINib *cellNibdrugs = [UINib nibWithNibName:@"DrugLabelCell" bundle:nil];
	[self.drugTable registerNib:cellNibdrugs forCellReuseIdentifier:[DrugLabelCell reuseIdentifier]];

	// table test
	UINib *cellNibTest = [UINib nibWithNibName:@"TestLabelCell" bundle:nil];
	[self.diagnosticTestTable registerNib:cellNibTest forCellReuseIdentifier:[TestLabelCell reuseIdentifier]];
}

- (void)setup
{
	self.mainscroll.frame = self.view.bounds;
	
	[self.prescriptionTitle setRoundedText:self.prescription.title];
	[self.date setRoundedText:[self.formatter stringFromDate: self.prescription.startDate]];
	[self.details fitText:self.prescription.details];
	[self.phisicalPrescriptions fitText:self.prescription.physicalPrescription];
	[self.referral setRoundedText:self.prescription.referral];
	[self.note setRoundedText:self.prescription.referralNote];
	
	// table drugs
	self.drugs = [[DBManager getIstance] getTherapiesFromPrescription:self.prescription].mutableCopy;
	[self animateDrugsTableWithDrugs:((int)[self.drugs count]*cellDrugsHeight)];
	
	// test table
	self.tests = [[self.prescription tests] allObjects].mutableCopy;
	[self animateTestsTableWithTests:((int)[self.tests count]*cellDrugsHeight)];
}


#pragma mark - Animations

- (void)animateDrugsTableWithDrugs:(int)drugs
{
	int delta = 0;
	int height = 0;
	if (drugs != 0)
		height = drugs + headerDrugsHeight;
	delta = height - self.drugTable.frame.size.height;

	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.drugTable.frame = CGRectMake(self.drugTable.frame.origin.x, self.drugTable.frame.origin.y, self.drugTable.frame.size.width, height);
		self.diagnosticTestTable.frame = CGRectMake(self.diagnosticTestTable.frame.origin.x, self.diagnosticTestTable.frame.origin.y + delta, self.diagnosticTestTable.frame.size.width, self.diagnosticTestTable.frame.size.height);
		self.bottomView.frame = CGRectMake(self.bottomView.frame.origin.x, self.bottomView.frame.origin.y + delta, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
	} completion:^(BOOL s){
		[self.drugTable reloadData];
	}];
}

- (void)animateTestsTableWithTests:(int)tests
{
	int delta = 0;
	int height = 0;
	if (tests != 0)
		height = tests + headerDrugsHeight;
	delta = height - self.diagnosticTestTable.frame.size.height;
	
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.diagnosticTestTable.frame = CGRectMake(self.diagnosticTestTable.frame.origin.x, self.diagnosticTestTable.frame.origin.y, self.diagnosticTestTable.frame.size.width, height);
		self.bottomView.frame = CGRectMake(self.bottomView.frame.origin.x, self.bottomView.frame.origin.y + delta, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
	} completion:^(BOOL s){
		[self.diagnosticTestTable reloadData];
	}];
}

#pragma mark - Actions

- (IBAction)print:(id)sender
{	
	NSString *title = [NSString stringWithFormat:@"Prescription for Patient: %@ %@ %@", self.patient.patientData.name, self.patient.patientData.middleName, self.patient.patientData.lastName];
	[PDFExporter clearDocuments];
	NSString *pdfFileName = [PDFExporter getPrescriptionPDFWithTitle:title prescription:self.prescription];
	[self printPDF:pdfFileName anchor:sender];
}

- (IBAction)send:(id)sender
{
	NSString *title = [NSString stringWithFormat:@"Prescription for Patient: %@ %@ %@", self.patient.patientData.name, self.patient.patientData.middleName, self.patient.patientData.lastName];
	[PDFExporter clearDocuments];
	NSString *pdfFileName = [PDFExporter getPrescriptionPDFWithTitle:title prescription:self.prescription];
	[self sendPDFtitle:title path:pdfFileName];
}

- (IBAction)edit:(id)sender
{
	PatientPrescriptionDetailEditController *vc = [[PatientPrescriptionDetailEditController alloc] initWithNibName:@"PatientPrescriptionDetailEditController" bundle:nil];
	PatientPrescriptionsController *parent = (PatientPrescriptionsController*)self.parentViewController;
	vc.prescription = self.prescription;
	vc.patient = self.patient;
	vc.isEditing = YES;
	vc.prescriptionTitle = @"EDIT PRESCRIPTION";
	[self parent:parent fadeToViewController:vc fromViewController:self superView:parent.view animateWithDuration:0.2];
}

- (NSMutableString*)getPrescriptionMultiText
{
	NSArray *drugs = [[DBManager getIstance] getTherapiesFromPrescription:self.prescription];
	NSMutableString *multiText = @"".mutableCopy;
	
	[multiText appendFormat:@"Prescription Title:\n%@", self.prescription.title];
	[multiText appendFormat:@"\nStart Date:\n%@", [self.formatter stringFromDate: self.prescription.startDate]];
	[multiText appendFormat:@"\nPrescription Details:\n%@", self.prescription.details];
	[multiText appendString:@"\nDRUGS:"];
	for (PrescribedTherapy *drug in drugs)
		[multiText appendFormat:@"\nDrug name: %@ - Dosage: %@ - Frequency: %@ - Route: %@", drug.drugName, drug.dosage, drug.frequency, drug.route];
	[multiText appendString:@"\nDIAGNOSTIC TESTS:"];
	for (DiagnosticTest *test in self.tests)
		[multiText appendFormat:@"\nTest: %@ - Note: %@", test.name, test.note];
	[multiText appendFormat:@"\nPhysical Prescriptions:\n%@", self.prescription.physicalPrescription];
	[multiText appendFormat:@"\nReferral:\n%@", self.prescription.referral];
	[multiText appendFormat:@"\nReferral Note:\n%@", self.prescription.referralNote];
	
	return multiText;
}

#pragma mark - UITableViewDataSource/UITableViewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return headerDrugsHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 768, headerDrugsHeight)];
	view.backgroundColor = lightLightBlueColorBackground();
	
	if (tableView == self.drugTable)
	{
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 15, 189, 21)];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = lightGrayBlueColor();
		label.font = OpenSansBold(15);
		label.text = @"DRUGS";
		[view addSubview:label];
		
		UILabel *drugname = [[UILabel alloc] initWithFrame:CGRectMake(20, 36, 200, 21)];
		drugname.backgroundColor = [UIColor clearColor];
		drugname.textColor = lightBlueColor();
		drugname.font = OpenSansBold(15);
		drugname.text = @"DRUG NAME";
		[view addSubview:drugname];
		
		UILabel *dosage = [[UILabel alloc] initWithFrame:CGRectMake(233, 36, 120, 21)];
		dosage.backgroundColor = [UIColor clearColor];
		dosage.textColor = lightBlueColor();
		dosage.font = OpenSansBold(15);
		dosage.text = @"DOSAGE";
		[view addSubview:dosage];
		
		UILabel *unit = [[UILabel alloc] initWithFrame:CGRectMake(364, 36, 120, 21)];
		unit.backgroundColor = [UIColor clearColor];
		unit.textColor = lightBlueColor();
		unit.font = OpenSansBold(15);
		unit.text = @"UNIT";
		[view addSubview:unit];
		
		UILabel *frequency = [[UILabel alloc] initWithFrame:CGRectMake(497, 36, 120, 21)];
		frequency.backgroundColor = [UIColor clearColor];
		frequency.textColor = lightBlueColor();
		frequency.font = OpenSansBold(15);
		frequency.text = @"FREQUENCY";
		[view addSubview:frequency];
		
		UILabel *route = [[UILabel alloc] initWithFrame:CGRectMake(633, 36, 120, 21)];
		route.backgroundColor = [UIColor clearColor];
		route.textColor = lightBlueColor();
		route.font = OpenSansBold(15);
		route.text = @"ROUTE";
		[view addSubview:route];
	}
	
	else
	{
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 15, 189, 21)];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = lightGrayBlueColor();
		label.font = OpenSansBold(15);
		label.text = @"DIAGNOSTIC TESTS";
		[view addSubview:label];
		
		UILabel *type = [[UILabel alloc] initWithFrame:CGRectMake(20, 36, 200, 21)];
		type.backgroundColor = [UIColor clearColor];
		type.textColor = lightBlueColor();
		type.font = OpenSansBold(15);
		type.text = @"TYPE";
		[view addSubview:type];
		
		UILabel *note = [[UILabel alloc] initWithFrame:CGRectMake(248, 36, 506, 21)];
		note.backgroundColor = [UIColor clearColor];
		note.textColor = lightBlueColor();
		note.font = OpenSansBold(15);
		note.text = @"NOTE";
		[view addSubview:note];
	}
			
	return view;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return cellDrugsHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (tableView == self.drugTable)
		return [self.drugs count];
	else
		return [self.tests count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;
	
	if (tableView == self.drugTable)
	{
		DrugLabelCell *customcell = [tableView dequeueReusableCellWithIdentifier:[DrugLabelCell reuseIdentifier]];
		PrescribedTherapy *drug = self.drugs[indexPath.row];
		[customcell setContent:drug];
		cell = customcell;
	}
	else
	{
		TestLabelCell *customcell = [tableView dequeueReusableCellWithIdentifier:[TestLabelCell reuseIdentifier]];
		DiagnosticTest *test = self.tests[indexPath.row];
		[customcell setContent:test];
		cell = customcell;
	}
	
	return cell;
}


@end
