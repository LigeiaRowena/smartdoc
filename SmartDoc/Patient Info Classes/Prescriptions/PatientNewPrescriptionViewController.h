//
//  PatientNewPrescriptionViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 31/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"
#import "Prescription.h"

@interface PatientNewPrescriptionViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UIView *mainView;
@property (nonatomic, strong) Prescription *prescription;
@property (nonatomic, strong) NSString *prescriptionTitle;

@end
