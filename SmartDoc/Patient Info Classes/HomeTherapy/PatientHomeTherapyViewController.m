//
//  PatientHomeTherapyViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 17/11/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "PatientHomeTherapyViewController.h"
#import "PrescribedTherapy.h"

#define cellTherapiesHeight 50
#define cellTherapyOpenHeight 150

@interface PatientHomeTherapyViewController ()

@property (nonatomic, strong) NSMutableArray *therapies;
@property (nonatomic, strong) NSIndexPath *indexPath;

@end

#pragma mark - Init PatientPrescriptionsController

@implementation PatientHomeTherapyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

	// therapy table
	UINib *cellNibtherapy = [UINib nibWithNibName:@"TherapyCell" bundle:nil];
	[self.therapiesTable registerNib:cellNibtherapy forCellReuseIdentifier:[TherapyCell reuseIdentifier]];
	UINib *cellNibtherapyOpen = [UINib nibWithNibName:@"TherapyOpenCell" bundle:nil];
	[self.therapiesTable registerNib:cellNibtherapyOpen forCellReuseIdentifier:[TherapyOpenCell reuseIdentifier]];
}
	 

- (void)setup
{
	// therapy table
	self.indexPath = nil;
	self.therapies = @[].mutableCopy;
	[self.therapies addObject:[[DBManager getIstance] getPrescribedTherapiesFromPatient:self.patient]];
	[self.therapies addObject:[[DBManager getIstance] getSuspendedTherapiesFromPatient:self.patient]];
	UIView *whiteBg = [[UIView alloc] initWithFrame:self.therapiesTable.bounds];
	whiteBg.backgroundColor = [UIColor whiteColor];
	self.therapiesTable.backgroundView = whiteBg;
	self.therapiesTable.backgroundColor = [UIColor whiteColor];
	[self.therapiesTable reloadData];
}

#pragma mark - Actions

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + KEYBOARD_HEIGHT);
	return [super textFieldDidBeginEditing:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height - KEYBOARD_HEIGHT);
	NSString *text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

	if ([textField.accessibilityIdentifier isEqualToString:[TherapyOpenCell reuseIdentifier]])
	{
		int index = (int)textField.tag;
		int section = [(CustomTextField*)textField section];
		PrescribedTherapy *therapy = self.therapies[section][index];
		therapy.dosage = text;
		[[DBManager getIstance] saveContext];
	}
	
	[self.therapies removeAllObjects];
	[self.therapies addObject:[[DBManager getIstance] getPrescribedTherapiesFromPatient:self.patient]];
	[self.therapies addObject:[[DBManager getIstance] getSuspendedTherapiesFromPatient:self.patient]];
	[super textFieldDidEndEditing:textField];
}

#pragma mark - PickerViewControllerDelegate Methods

- (void)pickerViewControllerSetText:(NSString*)text pickerField:(PickerField*)pickerField
{
	pickerField.textField.text = text;
	
	if ([pickerField.accessibilityIdentifier isEqualToString:[TherapyOpenCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugNameField])
	{
		int index = (int)pickerField.tag;
		int section = pickerField.section;
		PrescribedTherapy *therapy = self.therapies[section][index];
		therapy.drugName = text;
		[[DBManager getIstance] saveContext];
	}
	else if ([pickerField.accessibilityIdentifier isEqualToString:[TherapyOpenCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugUnitField])
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"DrugDosageUnit"];
		pickerField.textField.text = list[pickerField.pickerIndex][@"name"];
		int index = (int)pickerField.tag;
		int section = pickerField.section;
		PrescribedTherapy *therapy = self.therapies[section][index];
		therapy.dosageUnit = text;
		[[DBManager getIstance] saveContext];
	}
	
	else if ([pickerField.accessibilityIdentifier isEqualToString:[TherapyOpenCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugFrequencyField])
	{
		int index = (int)pickerField.tag;
		int section = pickerField.section;
		PrescribedTherapy *therapy = self.therapies[section][index];
		therapy.frequency = text;
		[[DBManager getIstance] saveContext];
	}
	
	else if ([pickerField.accessibilityIdentifier isEqualToString:[TherapyOpenCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugRouteField])
	{
		int index = (int)pickerField.tag;
		int section = pickerField.section;
		PrescribedTherapy *therapy = self.therapies[section][index];
		therapy.route = text;
		[[DBManager getIstance] saveContext];
	}

	[self.therapies removeAllObjects];
	[self.therapies addObject:[[DBManager getIstance] getPrescribedTherapiesFromPatient:self.patient]];
	[self.therapies addObject:[[DBManager getIstance] getSuspendedTherapiesFromPatient:self.patient]];
}

#pragma mark - PickerDrugViewControllerDelegate Methods

/*
- (void)pickerDrugViewControllerSetText:(NSString*)text pickerField:(PickerField*)pickerField
{
	pickerField.textField.text = text;
	
	if ([pickerField.accessibilityIdentifier isEqualToString:[TherapyOpenCell reuseIdentifier]])
	{
		int index = (int)pickerField.tag;
		int section = pickerField.section;
		PrescribedTherapy *therapy = self.therapies[section][index];
		therapy.drugName = text;
		[[DBManager getIstance] saveContext];
	}
	
	[self.therapies removeAllObjects];
	[self.therapies addObject:[[DBManager getIstance] getPrescribedTherapiesFromPatient:self.patient]];
	[self.therapies addObject:[[DBManager getIstance] getSuspendedTherapiesFromPatient:self.patient]];
}
 */

#pragma mark - PickerFieldDelegate Methods

- (void)tapOnPickerFieldDrugButton:(PickerField*)pickerField
{
	PickerViewController *pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
	pickerViewController.pickerField = pickerField;
	pickerViewController.delegate = self;
	pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:DrugActiveData text:@""].mutableCopy;
	pickerViewController.pickerType = DrugNameField;
	pickerViewController.dataType = DrugActiveData;
	
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerViewController];
	[self.popover setPopoverContentSize:pickerViewController.view.frame.size];
	[pickerViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	/*
	PickerDrugViewController *pickerDrugViewController = [[PickerDrugViewController alloc] initWithNibName:@"PickerDrugViewController" bundle:nil];
	pickerDrugViewController.pickerField = pickerField;
	pickerDrugViewController.delegate = self;
	
	if ([pickerField.accessibilityIdentifier isEqualToString:[TherapyOpenCell reuseIdentifier]])
	{
		pickerDrugViewController.pickerType = DrugNameField;
	}
	
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerDrugViewController];
	[self.popover setPopoverContentSize:pickerDrugViewController.view.frame.size];
	[pickerDrugViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	 */
}

- (void)pickerFieldDidBeginEditing:(PickerField *)pickerField
{
	[super pickerFieldDidBeginEditing:pickerField];
}

- (void)tapOnPickerFieldButton:(PickerField*)pickerField
{
	PickerViewController *pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
	pickerViewController.pickerField = pickerField;
	pickerViewController.delegate = self;

	if (([pickerField.accessibilityIdentifier isEqualToString:[TherapyOpenCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugUnitField]))
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"DrugDosageUnit"];
		for(NSDictionary *item in list)
			[pickerViewController.data addObject:item[@"name"]];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = DrugUnitField;
	}
	
	else if (([pickerField.accessibilityIdentifier isEqualToString:[TherapyOpenCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugFrequencyField]))
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"Frequency"];
		for(NSString *title in list)
			[pickerViewController.data addObject:title];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = DrugFrequencyField;
	}
	
	else if (([pickerField.accessibilityIdentifier isEqualToString:[TherapyOpenCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugRouteField]))
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:RouteData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = RouteData;
	}

	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerViewController];
	[self.popover setPopoverContentSize:pickerViewController.view.frame.size];
	[pickerViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma mark - UITableViewDataSource/UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.therapies[section] count];
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if ([indexPath isEqual:self.indexPath])
		return cellTherapyOpenHeight;
	else
		return cellTherapiesHeight;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;
	
	if ([indexPath isEqual:self.indexPath])
	{
		TherapyOpenCell *customcell = [tableView dequeueReusableCellWithIdentifier:[TherapyOpenCell reuseIdentifier]];
		customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
		customcell.cellDelegate = self;
		customcell.section = [NSNumber numberWithInt:(int)indexPath.section];
		PrescribedTherapy *therapy = self.therapies[indexPath.section][indexPath.row];
		[customcell setContent:therapy];
		cell = customcell;
	}
	
	else
	{
		TherapyCell *customcell = [tableView dequeueReusableCellWithIdentifier:[TherapyCell reuseIdentifier]];
		customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
		customcell.cellDelegate = self;
		PrescribedTherapy *therapy = self.therapies[indexPath.section][indexPath.row];
		[customcell setContent:therapy];
		cell = customcell;
	}

	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if ([indexPath isEqual:self.indexPath])
	{
		self.indexPath = nil;
		[self.therapiesTable reloadData];
	}
	else
	{
		self.indexPath = indexPath;
		[self.therapiesTable reloadData];
	}
}

- (void)doActionForKey:(NSString*)actionKey actionData:(id)actionData
{
	if ([actionKey isEqualToString:@"changeStatusTherapy"])
	{
		int index = [actionData[@"index"] intValue];
		int section = [actionData[@"section"] intValue];
		PrescribedTherapy *prescribedTherapy = self.therapies[section][index];
		BOOL suspended = ![prescribedTherapy.suspended boolValue];
		prescribedTherapy.suspended = [NSNumber numberWithBool:suspended];
		self.indexPath = nil;
	}
	
	else if ([actionKey isEqualToString:@"deleteTherapy"])
	{
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"You are going to delete this information: are you sure?" confirmBlock:^{
			int index = [actionData[@"index"] intValue];
			int section = [actionData[@"section"] intValue];
			PrescribedTherapy *prescribedTherapy = self.therapies[section][index];
			[[DBManager getIstance] deleteObject:prescribedTherapy];
		} cancelBlock:^{
		}];
	}
	
	[[DBManager getIstance] saveContext];
	[self.therapies removeAllObjects];
	[self.therapies addObject:[[DBManager getIstance] getPrescribedTherapiesFromPatient:self.patient]];
	[self.therapies addObject:[[DBManager getIstance] getSuspendedTherapiesFromPatient:self.patient]];
	[self.therapiesTable reloadData];
}

@end
