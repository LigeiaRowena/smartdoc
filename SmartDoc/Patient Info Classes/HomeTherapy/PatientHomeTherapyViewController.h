//
//  PatientHomeTherapyViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 17/11/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"
#import "TherapyCell.h"
#import "TherapyOpenCell.h"
#import "PickerDrugViewController.h"
#import "PickerViewController.h"

@interface PatientHomeTherapyViewController : BaseViewController <CellDelegate, PickerDrugViewControllerDelegate, PickerViewControllerDelegate>

@property (nonatomic, weak) IBOutlet UITableView *therapiesTable;

@end
