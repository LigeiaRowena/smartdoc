//
//  PatientDetailViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 04/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"
#import "MenuView.h"

@interface PatientRootViewController : BaseViewController <MenuViewDelegate>

@property (nonatomic, weak) IBOutlet UIView *mainView;
@property (nonatomic, weak) IBOutlet MenuView *menu;

- (void)pushMedicalHistory:(BaseViewController*)sourceViewController patient:(Patient *)patient;
	
@end
