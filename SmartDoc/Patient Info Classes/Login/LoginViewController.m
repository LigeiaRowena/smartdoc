//
//  LoginViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 09/09/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "LoginViewController.h"
#import "RememberPasswordViewController.h"
#import "Register1ViewController.h"
#import "PatientListViewController.h"

@interface LoginViewController ()

@property (nonatomic, weak) IBOutlet CustomTextField *username;
@property (nonatomic, weak) IBOutlet CustomTextField *password;
@property (weak, nonatomic) IBOutlet UIButton *rememberButton;

@end

@implementation LoginViewController

#pragma mark - Init LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// set the background color
	self.view.backgroundColor = lightBlueColorBackground();
	self.mainscroll.backgroundColor = lightBlueColorBackground();
}

- (void)setup
{
	self.username.text = [[DBManager getIstance] getUsername];
	self.password.text = [[DBManager getIstance] getPassword];
	self.rememberButton.selected = [[DBManager getIstance] getRemember];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	return [super textFieldDidBeginEditing:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	[super textFieldDidEndEditing:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	if (textField == self.password)
		return YES;
	else
		return [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
}

#pragma mark - Actions


- (IBAction)forgotPassord:(id)sender
{
	RememberPasswordViewController *vc = [[RememberPasswordViewController alloc] initWithNibName:@"RememberPasswordViewController" bundle:nil];
	[self parent:self pushViewController:vc superView:self.view animateWithDuration:0.2];
}

- (IBAction)singIn:(id)sender
{
	if (![BaseViewController isEmptyString:self.username.text] && ![BaseViewController isEmptyString:self.password.text])
	{
		[AFRequests login:self.username.text password:self.password.text success:^(NSHTTPURLResponse *response, id responseObject) {
			PatientListViewController *vc = [[PatientListViewController alloc] initWithNibName:@"PatientListViewController" bundle:nil];
			[self parent:self pushViewController:vc superView:self.view animateWithDuration:0.2];
		} failure:^(NSHTTPURLResponse *response, NSError *error) {
			NSString *message = error.userInfo[@"message"];
			[[AlertViewBlocks getIstance] warningAlertViewWithMessage:message confirmBlock:^{
			}];
		}];
	}
	else
		[[AlertViewBlocks getIstance] warningAlertViewWithMessage:@"Please insert username and password" confirmBlock:^{
		}];
}

- (IBAction)singUp:(id)sender
{
	Register1ViewController *vc = [[Register1ViewController alloc] initWithNibName:@"Register1ViewController" bundle:nil];
	[self parent:self pushViewController:vc superView:self.view animateWithDuration:0.2];
}

- (IBAction)rememberMe:(UIButton *)sender
{
	sender.selected = !sender.selected;
	[[DBManager getIstance] setRemember:sender.selected];
	
	// remember credentials
	if (sender.selected)
	{
		[[DBManager getIstance] setUsername:self.username.text];
		[[DBManager getIstance] setPassword:self.password.text];
	}
	// don't remember credentials and delete them locally
	else
	{
		[[DBManager getIstance] setUsername:@""];
		[[DBManager getIstance] setPassword:@""];
	}
}


@end
