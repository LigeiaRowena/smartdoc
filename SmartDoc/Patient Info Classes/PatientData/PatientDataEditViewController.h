//
//  PatientDataViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 07/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"
#import "PickerDateViewController.h"
#import "PickerViewController.h"
#import "ContactPhoneCell.h"
#import "ContactEmailCell.h"
#import "ToggleView.h"

@interface PatientDataEditViewController : BaseViewController <PickerDateViewControllerDelegate, PickerViewControllerDelegate, CellDelegate, ToggleViewDelegate>

@property (nonatomic) BOOL isEditing;

@end
