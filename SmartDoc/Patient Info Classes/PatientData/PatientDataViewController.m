//
//  PatientDataViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 27/08/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "PatientDataEditViewController.h"
#import "PatientDataViewController.h"
#import "PatientData.h"
#import "ContactPhone.h"
#import "ContactEmail.h"
#import "Dashboard.h"
#import "ContactLabelCell.h"
#import "PatientRootViewController.h"
#import "CustomTableView.h"
#import "PDFExporter.h"
#import "NSDate-Utilities.h"

#define cellContactsHeight 35
#define headerContactsHeight 21

@interface PatientDataViewController ()

@property (nonatomic, weak) IBOutlet RoundedLabel *category;
@property (nonatomic, weak) IBOutlet UILabel *status;
@property (nonatomic, weak) IBOutlet RoundedLabel *healthcode;
@property (nonatomic, weak) IBOutlet RoundedLabel *insurancetype;
@property (nonatomic, weak) IBOutlet FitLabel *insurancenote;
@property (nonatomic, weak) IBOutlet RoundedLabel *titleName;
@property (nonatomic, weak) IBOutlet RoundedLabel *name;
@property (nonatomic, weak) IBOutlet RoundedLabel *middlename;
@property (nonatomic, weak) IBOutlet RoundedLabel *lastname;
@property (nonatomic, weak) IBOutlet RoundedLabel *birthdate;
@property (nonatomic, weak) IBOutlet RoundedLabel *age;
@property (nonatomic, weak) IBOutlet RoundedLabel *sex;
@property (nonatomic, weak) IBOutlet RoundedLabel *birthcity;
@property (nonatomic, weak) IBOutlet RoundedLabel *fiscalcode;
@property (nonatomic, weak) IBOutlet RoundedLabel *education;
@property (nonatomic, weak) IBOutlet RoundedLabel *previousWork;
@property (nonatomic, weak) IBOutlet RoundedLabel *actualwork;
@property (nonatomic, weak) IBOutlet FitLabel *workNote;
@property (nonatomic, weak) IBOutlet CustomTableView *contactTable;
@property (nonatomic, weak) IBOutlet UIView *contactView;
@property (nonatomic, weak) IBOutlet RoundedLabel *address;
@property (nonatomic, weak) IBOutlet RoundedLabel *city;
@property (nonatomic, weak) IBOutlet RoundedLabel *state;
@property (nonatomic, weak) IBOutlet RoundedLabel *zipcode;
@property (nonatomic, weak) IBOutlet RoundedLabel *country;

@property (nonatomic, strong) NSMutableArray *phoneContacts;
@property (nonatomic, strong) NSMutableArray *emailContacts;

@end

#pragma mark - Init PatientDataViewController

@implementation PatientDataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// table contacts
	UINib *cellNibcontact = [UINib nibWithNibName:@"ContactLabelCell" bundle:nil];
	[self.contactTable registerNib:cellNibcontact forCellReuseIdentifier:[ContactLabelCell reuseIdentifier]];
}

- (void)setup
{
	[self.category setRoundedText:self.patient.category];
	if ([self.patient.dashboard.active boolValue])
		self.status.text = @"ON";
	else
		self.status.text = @"OFF";
	[self.healthcode setRoundedText:self.patient.patientData.healthCode];
	[self.insurancetype setRoundedText:self.patient.patientData.insuranceType];
	[self.insurancenote fitText:self.patient.patientData.insuranceNote];
	[self.titleName setRoundedText:self.patient.patientData.title];
	[self.name setRoundedText:self.patient.patientData.name];
	[self.middlename setRoundedText:self.patient.patientData.middleName];
	[self.lastname setRoundedText:self.patient.patientData.lastName];
	[self.birthdate setRoundedText:[self.formatter stringFromDate:self.patient.patientData.birthDate]];
	[self.age setRoundedText:[self.patient.patientData.birthDate getAge]];
	[self.sex setRoundedText:self.patient.patientData.sex];
	[self.birthcity setRoundedText:self.patient.patientData.birthCity];
	[self.fiscalcode setRoundedText:self.patient.patientData.fiscalCode];
	[self.education setRoundedText:self.patient.patientData.levelEducation];
	[self.previousWork setRoundedText:self.patient.patientData.previousWork];
	[self.actualwork setRoundedText:self.patient.patientData.actualWork];
	[self.workNote fitText:self.patient.patientData.workNote];
	[self.address setRoundedText:self.patient.patientData.address];
	[self.city setRoundedText:self.patient.patientData.city];
	[self.state setRoundedText:self.patient.patientData.state];
	[self.zipcode setRoundedText:self.patient.patientData.zipCode];
	[self.country setRoundedText:self.patient.patientData.country];

	// table contacts
	self.phoneContacts = [[self.patient.patientData phoneContacts] allObjects].mutableCopy;
	self.emailContacts = [[self.patient.patientData emailContacts] allObjects].mutableCopy;
	[self animateContactTableWithPhones:((int)[self.phoneContacts count]*cellContactsHeight) emails:((int)[self.emailContacts count]*cellContactsHeight)];
}

#pragma mark - Animations

- (void)animateContactTableWithPhones:(int)phones emails:(int)emails
{
	int delta = 0;
	int height = 0;
	if (phones == 0 && emails != 0)
		height = emails + headerContactsHeight*2;
	else if (phones != 0 && emails == 0)
		height = phones + headerContactsHeight*2;
	else if (phones != 0 && emails != 0)
		height = phones + emails + headerContactsHeight*2;
	delta = height - self.contactTable.frame.size.height;

	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.contactTable.frame = CGRectMake(self.contactTable.frame.origin.x, self.contactTable.frame.origin.y, self.contactTable.frame.size.width, height);
		self.contactView.frame = CGRectMake(self.contactView.frame.origin.x, self.contactView.frame.origin.y + delta, self.contactView.frame.size.width, self.contactView.frame.size.height);
	} completion:^(BOOL s){
		[self.contactTable reloadData];
	}];
}


#pragma mark - Actions

- (IBAction)print:(id)sender
{
	NSString *title = [NSString stringWithFormat:@"Patient Data for Patient: %@ %@ %@", self.patient.patientData.name, self.patient.patientData.middleName, self.patient.patientData.lastName];
	[PDFExporter clearDocuments];
	NSString *pdfFileName = [PDFExporter getPatientDataPDFWithTitle:title patient:self.patient status:[self.patient.dashboard.active boolValue] ? @"ON" : @"OFF"];
	[self printPDF:pdfFileName anchor:sender];
}

- (IBAction)send:(id)sender
{
	NSString *title = [NSString stringWithFormat:@"Patient Data for Patient: %@ %@ %@", self.patient.patientData.name, self.patient.patientData.middleName, self.patient.patientData.lastName];
	[PDFExporter clearDocuments];
	NSString *pdfFileName = [PDFExporter getPatientDataPDFWithTitle:title patient:self.patient status:[self.patient.dashboard.active boolValue] ? @"ON" : @"OFF"];
	[self sendPDFtitle:title path:pdfFileName];
}

- (IBAction)edit:(id)sender
{
	PatientDataEditViewController *vc = [[PatientDataEditViewController alloc] initWithNibName:@"PatientDataEditViewController" bundle:nil];
	PatientRootViewController *parent = (PatientRootViewController*)self.parentViewController;
	vc.patient = self.patient;
	vc.isEditing = YES;
	[self parent:parent fadeToViewController:vc fromViewController:self superView:parent.mainView animateWithDuration:0.2];
}

- (NSMutableString*)getPatientDataMultiText
{
	NSMutableString *multiText = @"".mutableCopy;
	
	[multiText appendFormat:@"Patient Category:\n%@", self.patient.category];
	[multiText appendFormat:@"Patient Status:\n%@", self.status.text];
	[multiText appendFormat:@"Health System Info:\n"];
	[multiText appendFormat:@"Health Code:\n%@", self.patient.patientData.healthCode];
	[multiText appendFormat:@"Insurance Type:\n%@", self.patient.patientData.insuranceType];
	[multiText appendFormat:@"Insurance Note:\n%@", self.patient.patientData.insuranceNote];
	[multiText appendFormat:@"Patient Info:\n"];
	[multiText appendFormat:@"Title:\n%@", self.patient.patientData.title];
	[multiText appendFormat:@"First Name:\n%@", self.patient.patientData.name];
	[multiText appendFormat:@"Middle Name:\n%@", self.patient.patientData.middleName];
	[multiText appendFormat:@"Last Name:\n%@", self.patient.patientData.lastName];
	[multiText appendFormat:@"Birth Date\n%@", [self.formatter stringFromDate:self.patient.patientData.birthDate]];
	[multiText appendFormat:@"Age\n%@", [self.patient.patientData.birthDate getAge]];
	[multiText appendFormat:@"Sex:\n%@", self.patient.patientData.sex];
	[multiText appendFormat:@"Birth City:\n%@", self.patient.patientData.birthCity];
	[multiText appendFormat:@"Fiscal Code:\n%@", self.patient.patientData.fiscalCode];
	[multiText appendFormat:@"Other:\n"];
	[multiText appendFormat:@"Level of Education:\n%@", self.patient.patientData.levelEducation];
	[multiText appendFormat:@"Previous Work:\n%@", self.patient.patientData.previousWork];
	[multiText appendFormat:@"Actual Work:\n%@", self.patient.patientData.actualWork];
	[multiText appendFormat:@"Work Note:\n%@", self.patient.patientData.workNote];
	[multiText appendFormat:@"Contact Information:\n"];
	for (ContactPhone *contact in self.phoneContacts)
		[multiText appendFormat:@"\nContact: %@ - Phone: %@", contact.type, contact.text];
	for (ContactEmail *contact in self.emailContacts)
		[multiText appendFormat:@"\nContact: %@ - Email: %@", contact.type, contact.text];
	[multiText appendFormat:@"Address:\n%@", self.patient.patientData.address];
	[multiText appendFormat:@"City:\n%@", self.patient.patientData.city];
	[multiText appendFormat:@"State:\n%@", self.patient.patientData.state];
	[multiText appendFormat:@"Zip Code:\n%@", self.patient.patientData.zipCode];
	[multiText appendFormat:@"Country:\n%@", self.patient.patientData.country];
	
	return multiText;
}

#pragma mark - UITableViewDataSource/UITableViewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 21;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 768, 21)];
	view.backgroundColor = lightLightBlueColorBackground();
	
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 189, 21)];
	label.backgroundColor = [UIColor clearColor];
	label.textColor = lightBlueColor();
	label.font = OpenSansBold(16);
	label.text = @"CONTACT DETAILS";
	[view addSubview:label];
	
	UILabel *infolabel = [[UILabel alloc] initWithFrame:CGRectMake(353, 0, 189, 21)];
	infolabel.backgroundColor = [UIColor clearColor];
	infolabel.textColor = lightBlueColor();
	infolabel.font = OpenSansBold(16);
	if (section == 0)
		infolabel.text = @"TELEPHONE";
	else
		infolabel.text = @"EMAIL";
	[view addSubview:infolabel];

	return view;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return cellContactsHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (section == 0)
		return [self.phoneContacts count];
	else
		return [self.emailContacts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	ContactLabelCell *customcell = [tableView dequeueReusableCellWithIdentifier:[ContactLabelCell reuseIdentifier]];

	if (indexPath.section == 0)
	{
		ContactPhone *contact = self.phoneContacts[indexPath.row];
		[customcell setContactPhone:contact];
	}
	else
	{
		ContactEmail *contact = self.emailContacts[indexPath.row];
		[customcell setContactEmail:contact];
	}
	
	return customcell;
}




@end
