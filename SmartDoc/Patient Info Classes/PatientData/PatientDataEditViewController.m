//
//  PatientDataViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 07/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "PatientDataEditViewController.h"
#import "PatientDataViewController.h"
#import "PatientData.h"
#import "ContactPhone.h"
#import "ContactEmail.h"
#import "Dashboard.h"
#import "PatientRootViewController.h"
#import "NSDate-Utilities.h"

#define cellContactsHeight 80

@interface PatientDataEditViewController ()

@property (nonatomic, weak) IBOutlet ToggleView *statusSwitch;
@property (nonatomic, weak) IBOutlet PickerField *categoryText;
@property (nonatomic, weak) IBOutlet CustomTextField *healthcodeText;
@property (nonatomic, weak) IBOutlet PickerField *insurancetypeText;
@property (nonatomic, weak) IBOutlet CustomTextView *insurancenoteText;
@property (nonatomic, weak) IBOutlet PickerField *titleText;
@property (nonatomic, weak) IBOutlet CustomTextField *nameText;
@property (nonatomic, weak) IBOutlet CustomTextField *middlenameText;
@property (nonatomic, weak) IBOutlet CustomTextField *lastnameText;
@property (nonatomic, weak) IBOutlet PickerField *birthdateText;
@property (nonatomic, weak) IBOutlet CustomTextField *ageText;
@property (nonatomic, weak) IBOutlet PickerField *sexText;
@property (nonatomic, weak) IBOutlet PickerField *birthcityText;
@property (nonatomic, weak) IBOutlet CustomTextField *fiscalCodeText;
@property (nonatomic, weak) IBOutlet PickerField *educationText;
@property (nonatomic, weak) IBOutlet PickerField *previousWorkText;
@property (nonatomic, weak) IBOutlet PickerField *actualworkText;
@property (nonatomic, weak) IBOutlet CustomTextView *workNoteText;
@property (nonatomic, weak) IBOutlet PickerField *contactTypePhoneText;
@property (weak, nonatomic) IBOutlet CustomTextField *contactPhoneText;
@property (nonatomic, weak) IBOutlet UITableView *contactPhoneTable;
@property (nonatomic, weak) IBOutlet UIView *contactEmailHeader;
@property (nonatomic, weak) IBOutlet PickerField *contactTypeEmailText;
@property (weak, nonatomic) IBOutlet CustomTextField *contactEmailText;
@property (nonatomic, weak) IBOutlet UITableView *contactEmailTable;
@property (nonatomic, weak) IBOutlet UIView *bottomView;
@property (nonatomic, weak) IBOutlet CustomTextField *addressText;
@property (nonatomic, weak) IBOutlet PickerField *cityText;
@property (nonatomic, weak) IBOutlet PickerField *stateText;
@property (nonatomic, weak) IBOutlet PickerField *zipcodeText;
@property (nonatomic, weak) IBOutlet PickerField *countryText;
@property (nonatomic, weak) IBOutlet UIView *saveView;

@property (nonatomic, strong) NSMutableArray *phoneContacts;
@property (nonatomic, strong) NSMutableArray *emailContacts;

@end

#pragma mark - Init PatientDataViewController

@implementation PatientDataEditViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		self.isEditing = NO;
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// table contacts phone
	UINib *cellNibphone = [UINib nibWithNibName:@"ContactPhoneCell" bundle:nil];
	[self.contactPhoneTable registerNib:cellNibphone forCellReuseIdentifier:[ContactPhoneCell reuseIdentifier]];
	
	// table contacts email
	UINib *cellNibemail = [UINib nibWithNibName:@"ContactEmailCell" bundle:nil];
	[self.contactEmailTable registerNib:cellNibemail forCellReuseIdentifier:[ContactEmailCell reuseIdentifier]];
}

- (void)setup
{
	// save button mode
	if (self.isEditing) {
		self.saveView.hidden = NO;
	} else {
		self.saveView.hidden = YES;
	}
	
	self.healthcodeText.text = self.patient.patientData.healthCode;
	self.insurancenoteText.textView.text = self.patient.patientData.insuranceNote;
	[self.statusSwitch commotInitWithFrame:self.statusSwitch.frame toggleBaseType:ToggleBaseTypeChangeImage toggleButtonType:ToggleButtonTypeDefault];
	if ([self.patient.dashboard.active boolValue])
		self.statusSwitch.selectedButton = ToggleButtonSelectedRight;
	else
		self.statusSwitch.selectedButton = ToggleButtonSelectedLeft;
	self.categoryText.textField.text = self.patient.category;
	self.insurancetypeText.textField.text = self.patient.patientData.insuranceType;
	self.titleText.textField.text = self.patient.patientData.title;
	self.nameText.text = self.patient.patientData.name;
	self.middlenameText.text = self.patient.patientData.middleName;
	self.lastnameText.text = self.patient.patientData.lastName;
	self.birthdateText.textField.text = [self.formatter stringFromDate: self.patient.patientData.birthDate];
	self.ageText.text = [self.patient.patientData.birthDate getAge];
	self.sexText.textField.text = self.patient.patientData.sex;
	self.birthcityText.textField.text = self.patient.patientData.birthCity;
	self.fiscalCodeText.text = self.patient.patientData.fiscalCode;
	self.educationText.textField.text = self.patient.patientData.levelEducation;
	self.previousWorkText.textField.text = self.patient.patientData.previousWork;
	self.actualworkText.textField.text = self.patient.patientData.actualWork;
	self.workNoteText.textView.text = self.patient.patientData.workNote;
	self.addressText.text = self.patient.patientData.address;
	self.cityText.textField.text = self.patient.patientData.city;
	self.stateText.textField.text = self.patient.patientData.state;
	self.zipcodeText.textField.text = self.patient.patientData.zipCode;
	self.countryText.textField.text = self.patient.patientData.country;
	
	// table contacts phone
	self.phoneContacts = [[self.patient.patientData phoneContacts] allObjects].mutableCopy;
	[self animateContactPhoneTableWithDelta:((int)[self.phoneContacts count]*cellContactsHeight)];
	
	// table contacts email
	self.emailContacts = [[self.patient.patientData emailContacts] allObjects].mutableCopy;
	[self animateContactEmailTableWithDelta:((int)[self.emailContacts count]*cellContactsHeight)];
}

#pragma mark - Actions

- (IBAction)save:(id)sender
{
	PatientDataViewController *vc = [[PatientDataViewController alloc] initWithNibName:@"PatientDataViewController" bundle:nil];
	PatientRootViewController *parent = (PatientRootViewController*)self.parentViewController;
	vc.patient = self.patient;
	[self parent:parent fadeToViewController:vc fromViewController:self superView:parent.mainView animateWithDuration:0.2];
}

#pragma mark - ToggleViewDelegate

- (void)selectToggle:(BOOL)on
{
	self.patient.dashboard.active = [NSNumber numberWithBool: on];
	[[DBManager getIstance] saveContext];
}

#pragma mark - Animations

- (void)animateContactPhoneTableWithDelta:(int)delta
{
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.contactPhoneTable.frame = CGRectMake(self.contactPhoneTable.frame.origin.x, self.contactPhoneTable.frame.origin.y, self.contactPhoneTable.frame.size.width, self.contactPhoneTable.frame.size.height + delta);
		self.contactEmailHeader.frame = CGRectMake(self.contactEmailHeader.frame.origin.x, self.contactEmailHeader.frame.origin.y + delta, self.contactEmailHeader.frame.size.width, self.contactEmailHeader.frame.size.height);
		self.contactEmailTable.frame = CGRectMake(self.contactEmailTable.frame.origin.x, self.contactEmailTable.frame.origin.y + delta, self.contactEmailTable.frame.size.width, self.contactEmailTable.frame.size.height);
		self.bottomView.frame = CGRectMake(self.bottomView.frame.origin.x, self.bottomView.frame.origin.y + delta, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
	} completion:^(BOOL s){
		[self.contactPhoneTable reloadData];
	}];
}

- (void)animateContactEmailTableWithDelta:(int)delta
{
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);

	[UIView animateWithDuration:0.2 animations:^{
		self.contactEmailTable.frame = CGRectMake(self.contactEmailTable.frame.origin.x, self.contactEmailTable.frame.origin.y, self.contactEmailTable.frame.size.width, self.contactEmailTable.frame.size.height + delta);
		self.bottomView.frame = CGRectMake(self.bottomView.frame.origin.x, self.bottomView.frame.origin.y + delta, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
	} completion:^(BOOL s){
		[self.contactEmailTable reloadData];
	}];
}



- (IBAction)addContactPhone:(id)sender
{
	if ([self.contactPhoneText.text isNotBlank] && [self.contactTypePhoneText.textField.text isNotBlank])
	{
		[[DBManager getIstance] createContactPhoneForPatientData:self.patient.patientData text:self.contactPhoneText.text type:self.contactTypePhoneText.textField.text];
		self.phoneContacts = [[self.patient.patientData phoneContacts] allObjects].mutableCopy;
		self.contactTypePhoneText.textField.text = @"";
		self.contactPhoneText.text = @"";
		[self animateContactPhoneTableWithDelta:cellContactsHeight];
	}
	else
	{
		[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please provide a value for the Details filled" confirmBlock:^{
			self.contactTypePhoneText.textField.text = @"";
			self.contactPhoneText.text = @"";
		}];
	}
}

- (IBAction)addContactEmail:(id)sender
{
	if ([self.contactEmailText.text isNotBlank] && [self.contactTypeEmailText.textField.text isNotBlank])
	{
		[[DBManager getIstance] createContactEmailForPatientData:self.patient.patientData text:self.contactEmailText.text type:self.contactTypeEmailText.textField.text];
		self.emailContacts = [[self.patient.patientData emailContacts] allObjects].mutableCopy;
		self.contactTypeEmailText.textField.text = @"";
		self.contactEmailText.text = @"";
		[self animateContactEmailTableWithDelta:cellContactsHeight];
	}
	else
	{
		[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please provide a value for the Details filled" confirmBlock:^{
			self.contactTypeEmailText.textField.text = @"";
			self.contactEmailText.text = @"";
		}];
	}
}


#pragma mark - CustomTextViewDelegate

- (void)customTextViewDidBeginEditing:(CustomTextView *)textView
{
	[super customTextViewDidBeginEditing:textView];
}

- (void)customTextViewDidEndEditing:(CustomTextView *)textView
{	
	NSString *text = [textView.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	if (textView == self.insurancenoteText)
	{
		self.patient.patientData.insuranceNote = text;
		[[DBManager getIstance] saveContext];
	}
	else if (textView == self.workNoteText)
	{
		self.patient.patientData.workNote = text;
		[[DBManager getIstance] saveContext];
	}
	
	[super customTextViewDidEndEditing:textView];
}

- (void)customTextViewDidChange:(CustomTextView *)textView
{
	[super customTextViewDidChange:textView];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	return [super textFieldDidBeginEditing:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	NSString *text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	if (textField == self.healthcodeText)
	{
		self.patient.patientData.healthCode = text;
		[[DBManager getIstance] saveContext];
	}
	else if (textField == self.nameText)
	{
		self.patient.patientData.name = text;
		[[DBManager getIstance] saveContext];
	}
	else if (textField == self.middlenameText)
	{
		self.patient.patientData.middleName = text;
		[[DBManager getIstance] saveContext];
	}
	else if (textField == self.lastnameText)
	{
		self.patient.patientData.lastName = text;
		[[DBManager getIstance] saveContext];
	}
	else if (textField == self.fiscalCodeText)
	{
		self.patient.patientData.fiscalCode = text;
		[[DBManager getIstance] saveContext];
	}
	else if (textField == self.addressText)
	{
		self.patient.patientData.address = text;
		[[DBManager getIstance] saveContext];
	}
	else if ([textField.accessibilityIdentifier isEqualToString:[ContactPhoneCell reuseIdentifier]])
	{
		int index = (int)textField.tag;
		ContactPhone *contact = self.phoneContacts[index];
		contact.text = text;
		[[DBManager getIstance] saveContext];
		self.phoneContacts = [[self.patient.patientData phoneContacts] allObjects].mutableCopy;
	}
	[super textFieldDidEndEditing:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	if (textField == self.contactEmailText || [textField.accessibilityIdentifier isEqualToString:[ContactEmailCell reuseIdentifier]])
		return YES;
	else
		return [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
}


#pragma mark - PickerDateViewControllerDelegate Methods

- (void)pickerDateViewControllerSetDate:(NSDate *)date pickerField:(PickerField *)pickerField
{
	pickerField.textField.text = [self.formatter stringFromDate:date];
	
	if (pickerField == self.birthdateText)
	{
		self.patient.patientData.birthDate = date;
		[[DBManager getIstance] saveContext];
		self.ageText.text = [self.patient.patientData.birthDate getAge];
	}
}

#pragma mark - PickerViewControllerDelegate Methods

- (void)pickerViewControllerSetText:(NSString*)text pickerField:(PickerField*)pickerField
{
	pickerField.textField.text = text;

	if (pickerField == self.categoryText)
	{
		self.patient.category = text;
		[[DBManager getIstance] saveContext];
	}
	else if (pickerField == self.insurancetypeText)
	{
		self.patient.patientData.insuranceType = text;
		[[DBManager getIstance] saveContext];
	}
	else if (pickerField == self.titleText)
	{
		self.patient.patientData.title = text;
		[[DBManager getIstance] saveContext];
	}
	else if (pickerField == self.sexText)
	{
		self.patient.patientData.sex = text;
		[[DBManager getIstance] saveContext];
	}
	else if (pickerField == self.educationText)
	{
		self.patient.patientData.levelEducation = text;
		[[DBManager getIstance] saveContext];
	}
	if (pickerField == self.actualworkText)
	{
		self.patient.patientData.actualWork = text;
		[[DBManager getIstance] saveContext];
	}
	if (pickerField == self.previousWorkText)
	{
		self.patient.patientData.previousWork = text;
		[[DBManager getIstance] saveContext];
	}
	if (pickerField == self.birthcityText)
	{
		self.patient.patientData.birthCity = text;
		[[DBManager getIstance] saveContext];
	}
	if (pickerField == self.cityText)
	{
		self.patient.patientData.city = text;
		[[DBManager getIstance] saveContext];
	}
	if (pickerField == self.countryText)
	{
		self.patient.patientData.country = text;
		[[DBManager getIstance] saveContext];
	}
	if (pickerField == self.stateText)
	{
		self.patient.patientData.state = text;
		[[DBManager getIstance] saveContext];
	}
	if (pickerField == self.zipcodeText)
	{
		self.patient.patientData.zipCode = text;
		[[DBManager getIstance] saveContext];
	}
	else if ([pickerField.accessibilityIdentifier isEqualToString:[ContactPhoneCell reuseIdentifier]])
	{
		int index = (int)pickerField.tag;
		ContactPhone *contact = self.phoneContacts[index];
		contact.type = text;
		[[DBManager getIstance] saveContext];
		self.phoneContacts = [[self.patient.patientData phoneContacts] allObjects].mutableCopy;
	}
}

#pragma mark - PickerFieldDelegate Methods

- (void)tapOnPickerFieldDateButton:(PickerField*)pickerField
{
	PickerDateViewController *pickerDateViewController = [[PickerDateViewController alloc] initWithNibName:@"PickerDateViewController" bundle:nil];
	pickerDateViewController.pickerField = pickerField;
	pickerDateViewController.delegate = self;
	
	if (pickerField == self.birthdateText)
		pickerDateViewController.date = [self.formatter dateFromString:self.birthdateText.textField.text];
	
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerDateViewController];
	[self.popover setPopoverContentSize:pickerDateViewController.view.frame.size];
	[pickerDateViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)tapOnPickerFieldButton:(PickerField*)pickerField
{
	PickerViewController *pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
	pickerViewController.pickerField = pickerField;
	pickerViewController.delegate = self;

	if (pickerField == self.categoryText)
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:CategoryData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = CategoryData;
	}
	else if (pickerField == self.insurancetypeText)
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:InsuranceTypeData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = InsuranceTypeData;
	}
	else if (pickerField == self.titleText)
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"Title"];
		for(NSString *title in list)
			[pickerViewController.data addObject:title];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = StaticField;
	}
	else if (pickerField == self.sexText)
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"Sex"];
		for(NSString *title in list)
			[pickerViewController.data addObject:title];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = StaticField;
	}
	else if (pickerField == self.educationText)
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"LevelEducation"];
		for(NSString *title in list)
			[pickerViewController.data addObject:title];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = StaticField;
	}
	if (pickerField == self.previousWorkText || pickerField == self.actualworkText)
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:WorkData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = WorkData;
	}
	else if (pickerField == self.contactTypePhoneText || [pickerField.accessibilityIdentifier isEqualToString:[ContactPhoneCell reuseIdentifier]])
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"ContactType"];
		for(NSString *title in list)
			[pickerViewController.data addObject:title];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = StaticField;
	}
	else if (pickerField == self.contactTypeEmailText || [pickerField.accessibilityIdentifier isEqualToString:[ContactEmailCell reuseIdentifier]])
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"ContactType"];
		for(NSString *title in list)
			[pickerViewController.data addObject:title];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = StaticField;
	}
	else if (pickerField == self.birthcityText || pickerField == self.cityText)
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:CittaData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = CittaData;
	}
	else if (pickerField == self.countryText)
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:CountryData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = CountryData;
	}
	else if (pickerField == self.stateText)
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:StateData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = StateData;
	}
	else if (pickerField == self.zipcodeText)
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:ZipcodeData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = ZipcodeData;
	}

	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerViewController];
	[self.popover setPopoverContentSize:pickerViewController.view.frame.size];
	[pickerViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)pickerFieldDidBeginEditing:(PickerField *)pickerField
{
	[super pickerFieldDidBeginEditing:pickerField];
}

#pragma mark - UITableViewDataSource/UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return cellContactsHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (tableView == self.contactPhoneTable)
		return [self.phoneContacts count];
	else
		return [self.emailContacts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;
	
	if (tableView == self.contactPhoneTable)
	{
		ContactPhoneCell *customcell = [tableView dequeueReusableCellWithIdentifier:[ContactPhoneCell reuseIdentifier]];
		customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
		customcell.cellDelegate = self;
		ContactPhone *contact = self.phoneContacts[indexPath.row];
		[customcell setContent:contact];
		cell = customcell;
	}
	else
	{
		ContactEmailCell *customcell = [tableView dequeueReusableCellWithIdentifier:[ContactEmailCell reuseIdentifier]];
		customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
		customcell.cellDelegate = self;
		ContactEmail *contact = self.emailContacts[indexPath.row];
		[customcell setContent:contact];
		cell = customcell;
	}

	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)doActionForKey:(NSString*)actionKey actionData:(id)actionData
{
	if ([actionKey isEqualToString:@"deletePhone"])
	{
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"You are going to delete this information: are you sure?" confirmBlock:^{
			int index = [actionData[@"index"] intValue];
			ContactPhone *contact = self.phoneContacts[index];
			[[DBManager getIstance] deleteObject:contact];
			self.phoneContacts = [[self.patient.patientData phoneContacts] allObjects].mutableCopy;
			[self animateContactPhoneTableWithDelta:-cellContactsHeight];
		} cancelBlock:^{
		}];
	}
	else if ([actionKey isEqualToString:@"deleteEmail"])
	{
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"You are going to delete this information: are you sure?" confirmBlock:^{
			int index = [actionData[@"index"] intValue];
			ContactEmail *contact = self.emailContacts[index];
			[[DBManager getIstance] deleteObject:contact];
			self.emailContacts = [[self.patient.patientData emailContacts] allObjects].mutableCopy;
			[self animateContactEmailTableWithDelta:-cellContactsHeight];
		} cancelBlock:^{
		}];
	}
}

@end
