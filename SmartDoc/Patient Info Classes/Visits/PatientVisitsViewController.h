//
//  PatientVisitsViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 07/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//


#import "BaseViewController.h"
#import "MenuView.h"
#import "VisitCell.h"
#import "GKLineGraph.h"


@interface PatientVisitsViewController : BaseViewController <MenuViewDelegate, CellDelegate, GKLineGraphDataSource>

@property (nonatomic, weak) IBOutlet UIButton *paginatorVisits;
@property (nonatomic, weak) IBOutlet UITableView *visitTable;
@property (nonatomic, weak) IBOutlet UIImageView *bg;
@property (nonatomic, weak) IBOutlet GKLineGraph *graph;
@property (nonatomic, weak) IBOutlet UIScrollView *graphScroll;
@property (nonatomic, weak) IBOutlet MenuView *menuView;

- (void)reloadData;

@end
