//
//  PatientVisitDetailViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 25/09/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"
#import "Visit.h"

@interface PatientVisitDetailViewController : BaseViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) Visit *visit;


@end
