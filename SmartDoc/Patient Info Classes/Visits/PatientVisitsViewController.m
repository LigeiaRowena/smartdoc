//
//  PatientVisitsViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 07/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "PatientVisitsViewController.h"
#import "Visit.h"
#import "RemoteAnamnesis.h"
#import "PatientVisitDetailEditViewController.h"
#import "PatientVisitDetailViewController.h"
#import "PatientNewVisitViewController.h"
#import "PatientRootViewController.h"

@interface PatientVisitsViewController ()

@property (nonatomic, strong) NSMutableArray *visits;
@property (nonatomic, strong) NSMutableArray *data;
@property (nonatomic, strong) NSMutableArray *labels;

@end

#pragma mark - Init PatientVisitsViewController

@implementation PatientVisitsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// visits table
	UINib *cellNibvisit = [UINib nibWithNibName:@"VisitCell" bundle:nil];
	[self.visitTable registerNib:cellNibvisit forCellReuseIdentifier:[VisitCell reuseIdentifier]];

	// init chart
	self.data = @[@[].mutableCopy].mutableCopy;
	self.labels = @[].mutableCopy;
	self.graph.lineWidth = 2.0;
	self.graph.valueLabelCount = 5;
	self.graph.margin = 10;
	self.graph.optionalLabel = YES;
}

- (void)setup
{
	// visits table
	self.visits = [self getVisitsOrdered].mutableCopy;
	[self.visitTable reloadData];
	
	// set measurements
	self.graphScroll.contentSize = CGSizeMake(self.graphScroll.frame.size.width, self.graphScroll.frame.size.height);
	[self.menuView selectItemAtIndex:0];
}

- (void)setupPaginator
{
	int totalPages = (int)[self.visits count] / 3 + 1;
	NSArray *indexPathsForVisibleRows = [self.visitTable indexPathsForVisibleRows];
	
	if ([indexPathsForVisibleRows count] >= 3)
	{
		NSIndexPath *lastVisibleIndexPath = (NSIndexPath*)[indexPathsForVisibleRows lastObject];
		int lastVisibleIndex = (int)[lastVisibleIndexPath row] + 1;
		int currentPage = lastVisibleIndex/3 + 1;
		NSString *paginator = [NSString stringWithFormat:@"%i/%i", currentPage, totalPages];
		[self.paginatorVisits setTitle:paginator forState:UIControlStateNormal];
	}
	else
	{
		NSString *paginator = [NSString stringWithFormat:@"1/%i",totalPages];
		[self.paginatorVisits setTitle:paginator forState:UIControlStateNormal];
	}
}

- (NSArray*)getVisitsOrdered
{
	[self.visits removeAllObjects];
	NSArray *array = [self.patient.visits allObjects];
	NSArray *visitsOrdered = [array sortedArrayUsingComparator: ^(Visit *d_1, Visit *d_2) {
		NSDate *n_1 = d_1.date;
		NSDate *n_2 = d_2.date;
		return [n_1 compare: n_2];
	}];
	return visitsOrdered;
}

- (void)reloadData
{
	[self setup];
}

#pragma mark - GKLineGraphDataSource

- (NSInteger)numberOfLines
{
	return [self.data count];
}

- (UIColor *)colorForLineAtIndex:(NSInteger)index
{
	return [UIColor whiteColor];
}

- (NSArray *)valuesForLineAtIndex:(NSInteger)index
{
	return [self.data objectAtIndex:index];
}

- (CFTimeInterval)animationDurationForLineAtIndex:(NSInteger)index
{
	return 1.0;
}

- (NSString *)titleForLineAtIndex:(NSInteger)index
{
	return [self.labels objectAtIndex:index];
}

#pragma mark - MenuViewDelegate

- (void)menuDidSelectItemAtIndex:(int)index
{
	// cleanup
	[[self.data objectAtIndex:0] removeAllObjects];
	[self.labels removeAllObjects];
	
	// list all measurement for the patient
	NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"MeasurementData"];
	NSString *measurementName = list[index];
	NSArray *allMeasurements = [[DBManager getIstance] getAllMeasurementsWithName:measurementName patient:self.patient];
	[self setGraphUnit:measurementName];
	
	// set the datasource
	for (Measurement *m in allMeasurements)
	{
		if (![BaseViewController isEmptyNumber:m.value])
		{
			NSDecimalNumber *number;
			NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
			[formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
			[formatter setDateFormat:@"dd/MM/yy"];
			NSString *date = [formatter stringFromDate:m.date];
			if ([[DBManager getIstance] isImperialSystem])
				number = [[DBManager getIstance] getValueImperialSystemMeasurement:m];
			else
				number = m.value;
			[[self.data objectAtIndex:0] addObject:number];
			[self.labels addObject:date];
		}
	}
	
	// set the contentsize of the scroll
	if ([allMeasurements count] > 5)
	{
		int measurementsCount = (int)[allMeasurements count];
		int pages = measurementsCount / 5 + 1;
		self.graphScroll.contentSize = CGSizeMake(self.graphScroll.frame.size.width*pages, self.graphScroll.frame.size.height);
		self.graph.frame = CGRectMake(self.graph.frame.origin.x, self.graph.frame.origin.y, self.graphScroll.frame.size.width*pages, self.graphScroll.frame.size.height);
	}
	else
	{
		self.graphScroll.contentSize = CGSizeMake(self.graphScroll.frame.size.width, self.graphScroll.frame.size.height);
		self.graph.frame = CGRectMake(self.graph.frame.origin.x, self.graph.frame.origin.y, self.graphScroll.frame.size.width, self.graphScroll.frame.size.height);
	}
	
	// draw
	[self.graph reset];
	if ([[self.data objectAtIndex:0] count] > 0 && [self.labels count] > 0)
		[self.graph draw];
}

- (void)setGraphUnit:(NSString*)measurementName
{
	if ([[DBManager getIstance] isImperialSystem])
		self.graph.unit = [[DBManager getIstance] getUnitImperialSystemMeasurement:nil measurementName:measurementName];
	else
		self.graph.unit = [[DBManager getIstance] getUnitMetricSystemMeasurement:nil measurementName:measurementName];
}


#pragma mark - Actions

- (IBAction)createNewVisit:(id)sender
{
	Visit *visit = [[DBManager getIstance] createVisitForPatient:self.patient date:[NSDate date] finalConsideration:@"" info:@"" type:@""];
	PatientNewVisitViewController *vc = [[PatientNewVisitViewController alloc] initWithNibName:@"PatientNewVisitViewController" bundle:nil];
	vc.visit = visit;
	vc.patient = self.patient;
	vc.visitTitle = @"CREATE NEW VISIT";
	[self parent:self pushViewController:vc superView:self.view animateWithDuration:0.2];
}

#pragma mark - UITableViewDataSource/UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 2;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete)
	{
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"You are going to delete this visit: are you sure?"  confirmBlock:^{
			Visit *visit = self.visits[indexPath.row];
			[[DBManager getIstance] deleteObject:visit];
			[self reloadData];
		} cancelBlock:^{
		}];
	}
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (section == 0)
		return 1;
	else
		return [self.visits count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 728, 30)];
	label.backgroundColor = [UIColor clearColor];
	label.text = @"OTHER VISITS";
	label.font = OpenSansRegular(16);
	label.textColor = lightGrayBlueColor();
	
	if (section == 0)
		return nil;
	else
		return label;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	if (section == 0)
		return 0;
	else
		return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;
	
	VisitCell *customcell = [tableView dequeueReusableCellWithIdentifier:[VisitCell reuseIdentifier]];
	customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
	customcell.cellDelegate = self;
	
	if (indexPath.section == 0)
	{
		[customcell setFirstVisit:self.patient.remoteAnamnesis.creationDate];
		cell = customcell;
	}
	else if (indexPath.section == 1)
	{
		Visit *visit = self.visits[indexPath.row];
		[customcell setVisit:visit];
		cell = customcell;
	}

	return cell;
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
	[self setupPaginator];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 0)
	{
		PatientRootViewController *parent = (PatientRootViewController*)self.parentViewController;
		[parent pushMedicalHistory:self patient:self.patient];
	}
	else
	{
		Visit *visit = self.visits[indexPath.row];
		PatientVisitDetailViewController *vc = [[PatientVisitDetailViewController alloc] initWithNibName:@"PatientVisitDetailViewController" bundle:nil];
		vc.visit = visit;
		vc.patient = self.patient;
		//vc.visitTitle = @"EDIT VISIT";
		[self parent:self pushViewController:vc superView:self.view animateWithDuration:0.2];
	}
}


@end
