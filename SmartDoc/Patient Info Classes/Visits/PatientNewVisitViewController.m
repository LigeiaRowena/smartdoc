//
//  PatientNewVisitViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 29/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "PatientNewVisitViewController.h"
#import "PatientVisitDetailEditViewController.h"
#import "PatientVisitsViewController.h"

@interface PatientNewVisitViewController ()
@end

@implementation PatientNewVisitViewController

#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	PatientVisitDetailEditViewController *vc = [[PatientVisitDetailEditViewController alloc] initWithNibName:@"PatientVisitDetailEditViewController" bundle:nil];
	vc.visit = self.visit;
	vc.patient = self.patient;
	vc.visitTitle = self.visitTitle;
	[self parent:self addChildViewController:vc superView:self.mainView];
}

#pragma mark - Actions

- (IBAction)saveAndGoBack:(id)sender
{
	PatientVisitsViewController *parent = (PatientVisitsViewController*)self.parentViewController;
	[parent reloadData];
	[self parent:parent popViewController:self animateWithDuration:0.2];
}

@end
