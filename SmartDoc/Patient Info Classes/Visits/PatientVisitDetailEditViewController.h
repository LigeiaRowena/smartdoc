//
//  PatientNewVisitViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 28/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"
#import "Visit.h"
#import "PickerDateViewController.h"
#import "PickerViewController.h"
#import "PickerDrugViewController.h"
#import "DrugCell.h"
#import "DiagnosticTestCell.h"
#import "SymptomCell.h"
#import "MeasurementCollectionCell.h"

@interface PatientVisitDetailEditViewController : BaseViewController <PickerDateViewControllerDelegate, PickerViewControllerDelegate, PickerDrugViewControllerDelegate, CellDelegate, UICollectionViewDataSource, UICollectionViewDelegate, CollectionCellDelegate>

@property (nonatomic, strong) Visit *visit;
@property (nonatomic, strong) NSString *visitTitle;
@property (nonatomic) BOOL isEditing;

@end
