//
//  PatientNewVisitViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 29/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"
#import "Visit.h"

@interface PatientNewVisitViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UIView *mainView;
@property (nonatomic, strong) Visit *visit;
@property (nonatomic, weak) IBOutlet UIView *actionView;
@property (nonatomic, strong) NSString *visitTitle;

@end
