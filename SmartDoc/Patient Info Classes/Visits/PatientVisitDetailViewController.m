//
//  PatientVisitDetailViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 25/09/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "PatientVisitDetailViewController.h"
#import "PatientVisitDetailEditViewController.h"
#import "PatientVisitsViewController.h"
#import "PatientData.h"
#import "PrescribedTherapy.h"
#import "DiagnosticTest.h"
#import "Symptom.h"
#import "MeasurementCollectionCell.h"
#import "SymptomLabelCell.h"
#import "DrugLabelCell.h"
#import "TestLabelCell.h"
#import "CustomTableView.h"
#import "PDFExporter.h"

#define cellDrugsHeight 35
#define headerDrugsHeight 80

@interface PatientVisitDetailViewController ()

@property (nonatomic, weak) IBOutlet RoundedLabel *visitDate;
@property (nonatomic, weak) IBOutlet RoundedLabel *visitType;
@property (nonatomic, weak) IBOutlet RoundedLabel *visitNote;
@property (nonatomic, weak) IBOutlet UICollectionView *measurementCollection;
@property (nonatomic, weak) IBOutlet UIView *bmiView;
@property (nonatomic, weak) IBOutlet UILabel *bmiLabel;
@property (nonatomic, weak) IBOutlet UILabel *slimLabel;
@property (nonatomic, weak) IBOutlet UILabel *fatLabel;
@property (nonatomic, weak) IBOutlet UIView *bmiIndexViewBg;
@property (nonatomic, weak) IBOutlet UIView *bmiIndexView;
@property (nonatomic, weak) IBOutlet CustomTableView *symptomTable;
@property (nonatomic, weak) IBOutlet UIView *objectiveExaminationView;
@property (nonatomic, weak) IBOutlet FitLabel *objectiveExamination;
@property (nonatomic, weak) IBOutlet CustomTableView *drugTable;
@property (nonatomic, weak) IBOutlet UIView *finalConsiderationsView;
@property (nonatomic, weak) IBOutlet FitLabel *finalConsiderations;
@property (nonatomic, weak) IBOutlet CustomTableView *testTable;

@property (nonatomic, strong) NSMutableArray *measurements;
@property (nonatomic, strong) NSMutableArray *symptoms;
@property (nonatomic, strong) NSMutableArray *drugs;
@property (nonatomic, strong) NSMutableArray *tests;

@end

@implementation PatientVisitDetailViewController

#pragma mark - Init PatientVisitDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// measurement collection
	UINib *cellmeasurement = [UINib nibWithNibName:@"MeasurementCollectionCell" bundle:nil];
	[self.measurementCollection registerNib:cellmeasurement forCellWithReuseIdentifier:[MeasurementCollectionCell reuseIdentifier]];

	// symtom table
	UINib *cellNibsymtom = [UINib nibWithNibName:@"SymptomLabelCell" bundle:nil];
	[self.symptomTable registerNib:cellNibsymtom forCellReuseIdentifier:[SymptomLabelCell reuseIdentifier]];
	
	// table drugs
	UINib *cellNibdrugs = [UINib nibWithNibName:@"DrugLabelCell" bundle:nil];
	[self.drugTable registerNib:cellNibdrugs forCellReuseIdentifier:[DrugLabelCell reuseIdentifier]];
	
	// table test
	UINib *cellNibTest = [UINib nibWithNibName:@"TestLabelCell" bundle:nil];
	[self.testTable registerNib:cellNibTest forCellReuseIdentifier:[TestLabelCell reuseIdentifier]];
}

- (void)setup
{
	self.mainscroll.frame = self.view.bounds;
	
	[self.visitDate setRoundedText:[self.formatter stringFromDate: self.visit.date]];
	[self.visitType setRoundedText:self.visit.type];
	[self.visitNote setRoundedText:self.visit.note];
	[self.objectiveExamination fitText:self.visit.objectiveExamination];
	[self.finalConsiderations fitText:self.visit.finalConsideration];
	
	// measurements collection
	self.measurements = [[DBManager getIstance] getMeasurementsSortedWithIndexForVisit:self.visit].mutableCopy;
	[self.measurementCollection reloadData];
	
	// BMI index view
	self.bmiIndexView.layer.backgroundColor = bluNavBarColor().CGColor;
	self.bmiIndexView.layer.borderColor = [UIColor clearColor].CGColor;
	self.bmiIndexView.layer.borderWidth = 0.0f;
	self.bmiIndexView.layer.cornerRadius = 5.0f;
	self.bmiIndexViewBg.backgroundColor = separatorGrayColor();
	self.bmiIndexViewBg.alpha = 0.7;
	self.bmiIndexViewBg.layer.backgroundColor = separatorGrayColor().CGColor;
	self.bmiIndexViewBg.layer.borderColor = [UIColor clearColor].CGColor;
	self.bmiIndexViewBg.layer.borderWidth = 0.0f;
	self.bmiIndexViewBg.layer.cornerRadius = 5.0f;
	[self setBMIIndex];
	
	// symptom table
	self.symptoms = [[self.visit symptoms] allObjects].mutableCopy;
	[self animateSymptomsTable:(int)[self.symptoms count]];
	
	// table drugs
	self.drugs = [[DBManager getIstance] getTherapiesFromVisit:self.visit].mutableCopy;
	[self animateDrugsTable:(int)[self.drugs count]];
	
	// test table
	self.tests = [[self.visit tests] allObjects].mutableCopy;
	[self animateTestsTable:(int)[self.tests count]];
}

- (void)setBMIIndex
{
	Measurement *weightMeasurement = [[DBManager getIstance] getMeasurementWithName:@"Weight" visit:self.visit];
	Measurement *heightMeasurement = [[DBManager getIstance] getMeasurementWithName:@"Height" visit:self.visit];
	if ([BaseViewController isEmptyNumber:weightMeasurement.value] || [BaseViewController isEmptyNumber:heightMeasurement.value])
	{
		[self setBMIIndexInvalid];
		return;
	}
	
	float bmiIndex = [[self getBMIIndexWeight:weightMeasurement height:heightMeasurement] floatValue];
	if (bmiIndex != 0)
	{
		self.bmiLabel.text = [NSString stringWithFormat:@"%i", (int)bmiIndex];
		float bmiIndexViewWidth = (bmiIndex - 15)*10;
		self.bmiIndexView.frame = CGRectMake(self.bmiIndexView.frame.origin.x, self.bmiIndexView.frame.origin.y, bmiIndexViewWidth, self.bmiIndexView.frame.size.height);
		self.slimLabel.hidden = NO;
		self.fatLabel.hidden = NO;
	}
	else
		[self setBMIIndexInvalid];
}

- (NSNumber*)getBMIIndexWeight:(Measurement*)weightMeasurement height:(Measurement*)heightMeasurement
{
	NSNumber *bmiIndex;
	float weight = [weightMeasurement.value floatValue];
	float height = [heightMeasurement.value floatValue]/100;
	if (weight != 0 && height != 0)
	{
		float powerHeight = powf(height, 2);
		float index = weight / powerHeight;
		if (index < 15)
			index = 15;
		else if (index > 40)
			index = 40;
		bmiIndex = [NSNumber numberWithFloat:index];
	}
	else
		bmiIndex = [NSNumber numberWithFloat:0];

	return bmiIndex;
}

- (void)setBMIIndexInvalid
{
	self.bmiLabel.text = @"ND";
	self.bmiIndexView.frame = CGRectMake(self.bmiIndexView.frame.origin.x, self.bmiIndexView.frame.origin.y, 0, self.bmiIndexView.frame.size.height);
	self.slimLabel.hidden = YES;
	self.fatLabel.hidden = YES;
}


#pragma mark - Animations

- (void)animateSymptomsTable:(int)count
{
	int height = 0;
	int delta = 0;
	if (count != 0)
		height = count*cellDrugsHeight + headerDrugsHeight;
	delta = height - self.symptomTable.frame.size.height;
	
	if (delta != 0)
	{
		self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
		self.symptomTable.frame = CGRectMake(self.symptomTable.frame.origin.x, self.symptomTable.frame.origin.y, self.symptomTable.frame.size.width, height);
		self.objectiveExaminationView.frame = CGRectMake(self.objectiveExaminationView.frame.origin.x, self.objectiveExaminationView.frame.origin.y + delta, self.objectiveExaminationView.frame.size.width, self.objectiveExaminationView.frame.size.height);
		self.drugTable.frame = CGRectMake(self.drugTable.frame.origin.x, self.drugTable.frame.origin.y + delta, self.drugTable.frame.size.width, self.drugTable.frame.size.height);
		self.finalConsiderationsView.frame = CGRectMake(self.finalConsiderationsView.frame.origin.x, self.finalConsiderationsView.frame.origin.y + delta, self.finalConsiderationsView.frame.size.width, self.finalConsiderationsView.frame.size.height);
		self.testTable.frame = CGRectMake(self.testTable.frame.origin.x, self.testTable.frame.origin.y + delta, self.testTable.frame.size.width, self.testTable.frame.size.height);
		[self.symptomTable reloadData];
	}
}

- (void)animateDrugsTable:(int)count
{
	int height = 0;
	int delta = 0;
	if (count != 0)
		height = count*cellDrugsHeight + headerDrugsHeight;
	delta = height - self.drugTable.frame.size.height;
	
	if (delta != 0)
	{
		self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
		self.drugTable.frame = CGRectMake(self.drugTable.frame.origin.x, self.drugTable.frame.origin.y, self.drugTable.frame.size.width, height);
		self.finalConsiderationsView.frame = CGRectMake(self.finalConsiderationsView.frame.origin.x, self.finalConsiderationsView.frame.origin.y + delta, self.finalConsiderationsView.frame.size.width, self.finalConsiderationsView.frame.size.height);
		self.testTable.frame = CGRectMake(self.testTable.frame.origin.x, self.testTable.frame.origin.y + delta, self.testTable.frame.size.width, self.testTable.frame.size.height);
		[self.drugTable reloadData];
	}
}

- (void)animateTestsTable:(int)count
{
	int height = 0;
	int delta = 0;
	if (count != 0)
		height = count*cellDrugsHeight + headerDrugsHeight;
	delta = height - self.testTable.frame.size.height;
	
	if (delta != 0)
	{
		self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
		self.testTable.frame = CGRectMake(self.testTable.frame.origin.x, self.testTable.frame.origin.y, self.testTable.frame.size.width, height);
		[self.testTable reloadData];
	}
}

#pragma mark - Actions

- (IBAction)print:(id)sender
{
	NSString *title = [NSString stringWithFormat:@"Visit for Patient: %@ %@ %@", self.patient.patientData.name, self.patient.patientData.middleName, self.patient.patientData.lastName];
	[PDFExporter clearDocuments];
	NSString *pdfFileName = [PDFExporter getVisitPDFWithTitle:title visit:self.visit patient:self.patient];
	[self printPDF:pdfFileName anchor:sender];
}

- (IBAction)send:(id)sender
{
	NSString *title = [NSString stringWithFormat:@"Visit for Patient: %@ %@ %@", self.patient.patientData.name, self.patient.patientData.middleName, self.patient.patientData.lastName];
	[PDFExporter clearDocuments];
	NSString *pdfFileName = [PDFExporter getVisitPDFWithTitle:title visit:self.visit patient:self.patient];
	[self sendPDFtitle:title path:pdfFileName];
}

- (IBAction)edit:(id)sender
{
	PatientVisitDetailEditViewController *vc = [[PatientVisitDetailEditViewController alloc] initWithNibName:@"PatientVisitDetailEditViewController" bundle:nil];
	PatientVisitsViewController *parent = (PatientVisitsViewController*)self.parentViewController;
	vc.visit = self.visit;
	vc.patient = self.patient;
	vc.isEditing = YES;
	vc.visitTitle = @"EDIT VISIT";
	[self parent:parent fadeToViewController:vc fromViewController:self superView:parent.view animateWithDuration:0.2];
}

- (NSMutableString*)getVisitMultiText
{
	NSArray *drugs = [[DBManager getIstance] getTherapiesFromVisit:self.visit];
	Measurement *weightMeasurement = [[DBManager getIstance] getMeasurementWithName:@"Weight" visit:self.visit];
	Measurement *heightMeasurement = [[DBManager getIstance] getMeasurementWithName:@"Height" visit:self.visit];
	NSMutableString *multiText = @"".mutableCopy;

	[multiText appendFormat:@"Visit Date:\n%@", [self.formatter stringFromDate:self.visit.date]];
	[multiText appendFormat:@"Visit Type:\n%@", self.visit.type];
	[multiText appendFormat:@"Visit Note:\n%@", self.visit.note];
	[multiText appendString:@"\nMEASUREMENTS:"];
	for (Measurement *measurement in self.measurements)
		[multiText appendFormat:@"\nMeasurement name: %@ - value: %@ %@", measurement.name, measurement.value, measurement.unit];
	[multiText appendFormat:@"BMI index:\n%@", [self getBMIIndexWeight:weightMeasurement height:heightMeasurement]];
	[multiText appendString:@"\nSYMPTOMS:"];
	for (Symptom *symptom in self.symptoms)
		[multiText appendFormat:@"\nSymptom name: %@ - details: %@ - note: %@", symptom.name, symptom.details, symptom.note];
	[multiText appendFormat:@"\nObjective Examination:\n%@", self.visit.objectiveExamination];
	[multiText appendString:@"\nDRUGS:"];
	for (PrescribedTherapy *drug in drugs)
		[multiText appendFormat:@"\nDrug name: %@ - Dosage: %@ - Frequency: %@ - Route: %@", drug.drugName, drug.dosage, drug.frequency, drug.route];
	[multiText appendFormat:@"\nFinal Consideration:\n%@", self.visit.finalConsideration];
	[multiText appendString:@"\nDIAGNOSTIC TESTS:"];
	for (DiagnosticTest *test in self.tests)
		[multiText appendFormat:@"\nTest: %@ - Note: %@", test.name, test.note];
	
	return multiText;
}

#pragma mark - UICollectionViewDataSource

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
	return CGSizeMake(121, 130);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
	return UIEdgeInsetsMake(0, 20, 0, 20);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
	return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
	return 0;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return [self.measurements count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	MeasurementCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[MeasurementCollectionCell reuseIdentifier] forIndexPath:indexPath];
	cell.index = [NSNumber numberWithInt:(int)indexPath.row];
	Measurement *measurement = self.measurements[indexPath.row];
	NSArray *prevMeasurements = [[DBManager getIstance] getAllMeasurementsWithName:measurement.name patient:self.patient];
	[cell setMeasurement:measurement previousMeasurements:prevMeasurements];
	[cell setContentNotEditable];
	cell.backgroundColor = lightLightBlueColorBackground();
	
	return cell;
}

#pragma mark - UITableViewDataSource/UITableViewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return headerDrugsHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return cellDrugsHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 768, headerDrugsHeight)];
	view.backgroundColor = lightLightBlueColorBackground();
	
	if (tableView == self.drugTable)
	{
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 15, 189, 21)];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = lightGrayBlueColor();
		label.font = OpenSansBold(21);
		label.text = @"DRUGS";
		[view addSubview:label];
		
		UILabel *drugname = [[UILabel alloc] initWithFrame:CGRectMake(20, 51, 200, 21)];
		drugname.backgroundColor = [UIColor clearColor];
		drugname.textColor = lightBlueColor();
		drugname.font = OpenSansBold(16);
		drugname.text = @"DRUG NAME";
		[view addSubview:drugname];
		
		UILabel *dosage = [[UILabel alloc] initWithFrame:CGRectMake(233, 51, 120, 21)];
		dosage.backgroundColor = [UIColor clearColor];
		dosage.textColor = lightBlueColor();
		dosage.font = OpenSansBold(16);
		dosage.text = @"DOSAGE";
		[view addSubview:dosage];
		
		UILabel *unit = [[UILabel alloc] initWithFrame:CGRectMake(364, 51, 120, 21)];
		unit.backgroundColor = [UIColor clearColor];
		unit.textColor = lightBlueColor();
		unit.font = OpenSansBold(16);
		unit.text = @"UNIT";
		[view addSubview:unit];
		
		UILabel *frequency = [[UILabel alloc] initWithFrame:CGRectMake(497, 51, 120, 21)];
		frequency.backgroundColor = [UIColor clearColor];
		frequency.textColor = lightBlueColor();
		frequency.font = OpenSansBold(16);
		frequency.text = @"FREQUENCY";
		[view addSubview:frequency];
		
		UILabel *route = [[UILabel alloc] initWithFrame:CGRectMake(633, 51, 120, 21)];
		route.backgroundColor = [UIColor clearColor];
		route.textColor = lightBlueColor();
		route.font = OpenSansBold(16);
		route.text = @"ROUTE";
		[view addSubview:route];
	}
	
	else if (tableView == self.symptomTable)
	{
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 15, 189, 21)];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = lightGrayBlueColor();
		label.font = OpenSansBold(21);
		label.text = @"SYMPTOMS";
		[view addSubview:label];

		UILabel *type = [[UILabel alloc] initWithFrame:CGRectMake(20, 51, 200, 21)];
		type.backgroundColor = [UIColor clearColor];
		type.textColor = lightBlueColor();
		type.font = OpenSansBold(16);
		type.text = @"SYMPTOM";
		[view addSubview:type];

		UILabel *details = [[UILabel alloc] initWithFrame:CGRectMake(184, 51, 200, 21)];
		details.backgroundColor = [UIColor clearColor];
		details.textColor = lightBlueColor();
		details.font = OpenSansBold(16);
		details.text = @"DETAILS";
		[view addSubview:details];
		
		UILabel *note = [[UILabel alloc] initWithFrame:CGRectMake(348, 51, 200, 21)];
		note.backgroundColor = [UIColor clearColor];
		note.textColor = lightBlueColor();
		note.font = OpenSansBold(16);
		note.text = @"NOTE";
		[view addSubview:note];
	}
	
	else
	{
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 15, 270, 21)];
		label.backgroundColor = [UIColor clearColor];
		label.textColor = lightGrayBlueColor();
		label.font = OpenSansBold(21);
		label.text = @"DIAGNOSTIC TESTS";
		[view addSubview:label];
		
		UILabel *type = [[UILabel alloc] initWithFrame:CGRectMake(20, 51, 200, 21)];
		type.backgroundColor = [UIColor clearColor];
		type.textColor = lightBlueColor();
		type.font = OpenSansBold(16);
		type.text = @"TYPE";
		[view addSubview:type];
		
		UILabel *note = [[UILabel alloc] initWithFrame:CGRectMake(248, 51, 506, 21)];
		note.backgroundColor = [UIColor clearColor];
		note.textColor = lightBlueColor();
		note.font = OpenSansBold(16);
		note.text = @"NOTE";
		[view addSubview:note];
	}
	
	return view;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (tableView == self.drugTable)
		return [self.drugs count];
	else if (tableView == self.testTable)
		return [self.tests count];
	else
		return [self.symptoms count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;
	
	if (tableView == self.drugTable)
	{
		DrugLabelCell *customcell = [tableView dequeueReusableCellWithIdentifier:[DrugLabelCell reuseIdentifier]];
		PrescribedTherapy *drug = self.drugs[indexPath.row];
		[customcell setContent:drug];
		cell = customcell;
	}
	
	else if (tableView == self.testTable)
	{
		TestLabelCell *customcell = [tableView dequeueReusableCellWithIdentifier:[TestLabelCell reuseIdentifier]];
		DiagnosticTest *test = self.tests[indexPath.row];
		[customcell setContent:test];
		cell = customcell;
	}
	
	else if (tableView == self.symptomTable)
	{
		SymptomLabelCell *customcell = [tableView dequeueReusableCellWithIdentifier:[SymptomLabelCell reuseIdentifier]];
		customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
		Symptom *symptom = self.symptoms[indexPath.row];
		[customcell setSymptomVisit:symptom];
		cell = customcell;
	}
	
    return cell;
}



@end
