//
//  PatientNewVisitViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 28/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "PatientVisitDetailEditViewController.h"
#import "PatientVisitDetailViewController.h"
#import "PatientVisitsViewController.h"
#import "PrescribedTherapy.h"
#import "DiagnosticTest.h"
#import "Symptom.h"
#import "CustomTableView.h"
#import "EditPhotoViewController.h"
#import "GalleryViewController.h"

#define cellDrugsHeight 80
#define cellMeasurementHeight 130
#define cellTestHeight 190

@interface PatientVisitDetailEditViewController ()

@property (nonatomic, weak) IBOutlet UILabel *visitTitleLabel;
@property (nonatomic, weak) IBOutlet PickerField *visitDate;
@property (nonatomic, weak) IBOutlet PickerField *visitType;
@property (nonatomic, weak) IBOutlet CustomTextField *visitNote;
@property (nonatomic, weak) IBOutlet UICollectionView *measurementCollection;
@property (nonatomic, weak) IBOutlet UIView *bmiView;
@property (nonatomic, weak) IBOutlet UILabel *bmiLabel;
@property (nonatomic, weak) IBOutlet UILabel *slimLabel;
@property (nonatomic, weak) IBOutlet UILabel *fatLabel;
@property (nonatomic, weak) IBOutlet UIView *bmiIndexViewBg;
@property (nonatomic, weak) IBOutlet UIView *bmiIndexView;
@property (nonatomic, weak) IBOutlet UIView *symptomHeader;
@property (nonatomic, weak) IBOutlet PickerField *symptomType;
@property (nonatomic, weak) IBOutlet PickerField *symptomDetail;
@property (nonatomic, weak) IBOutlet CustomTextField *symptomNote;
@property (nonatomic, weak) IBOutlet CustomTableView *symptomTable;
@property (nonatomic, weak) IBOutlet CustomTextView *objectiveExamination;
@property (nonatomic, weak) IBOutlet UIView *drugHeader;
@property (nonatomic, weak) IBOutlet PickerField *drugName;
@property (nonatomic, weak) IBOutlet CustomTextField *drugDosageText;
@property (nonatomic, weak) IBOutlet PickerField *drugUnitText;
@property (nonatomic, weak) IBOutlet PickerField *drugFrequencyText;
@property (nonatomic, weak) IBOutlet PickerField *drugRouteText;
@property (nonatomic, weak) IBOutlet CustomTableView *drugsTable;
@property (nonatomic, weak) IBOutlet CustomTextView *finalConsiderations;
@property (nonatomic, weak) IBOutlet UIView *testHeader;
@property (nonatomic, weak) IBOutlet PickerField *testType;
@property (nonatomic, weak) IBOutlet CustomTextField *testNote;
@property (nonatomic, weak) IBOutlet CustomTableView *testTable;
@property (nonatomic, weak) IBOutlet UIButton *saveButton;

@property (nonatomic, strong) NSMutableArray *measurements;
@property (nonatomic, strong) NSMutableArray *symptoms;
@property (nonatomic, strong) NSMutableArray *drugs;
@property (nonatomic, strong) NSMutableArray *tests;

@end

@implementation PatientVisitDetailEditViewController

#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		self.isEditing = NO;
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// measurement collection
	UINib *cellmeasurement = [UINib nibWithNibName:@"MeasurementCollectionCell" bundle:nil];
	[self.measurementCollection registerNib:cellmeasurement forCellWithReuseIdentifier:[MeasurementCollectionCell reuseIdentifier]];
	
	// symtom table
	UINib *cellNibsymtom = [UINib nibWithNibName:@"SymptomCell" bundle:nil];
	[self.symptomTable registerNib:cellNibsymtom forCellReuseIdentifier:[SymptomCell reuseIdentifier]];
	
	// drug table
	UINib *cellNibdrug = [UINib nibWithNibName:@"DrugCell" bundle:nil];
	[self.drugsTable registerNib:cellNibdrug forCellReuseIdentifier:[DrugCell reuseIdentifier]];
	
	// test table
	UINib *cellNibtest = [UINib nibWithNibName:@"DiagnosticTestCell" bundle:nil];
	[self.testTable registerNib:cellNibtest forCellReuseIdentifier:[DiagnosticTestCell reuseIdentifier]];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}


- (void)setup
{
	if (self.imagePickerController.isBeingDismissed)
		return;
	
	self.mainscroll.frame = self.view.bounds;
	
	// save button mode
	if (self.isEditing)
	{
		self.saveButton.enabled = YES;
		self.saveButton.hidden = NO;
	}
	else
	{
		self.saveButton.enabled = NO;
		self.saveButton.hidden = YES;
	}
	
	self.visitTitleLabel.text = self.visitTitle;
	self.visitDate.textField.text = [self.formatter stringFromDate: self.visit.date];
	self.visitType.textField.text = self.visit.type;
	self.visitNote.text = self.visit.note;
	self.objectiveExamination.textView.text = self.visit.objectiveExamination;
	self.finalConsiderations.textView.text = self.visit.finalConsideration;

	
	// measurements collection
	self.measurements = [[DBManager getIstance] getMeasurementsSortedWithIndexForVisit:self.visit].mutableCopy;
	[self.measurementCollection reloadData];


	// BMI index view
	self.bmiIndexView.layer.backgroundColor = bluNavBarColor().CGColor;
	self.bmiIndexView.layer.borderColor = [UIColor clearColor].CGColor;
	self.bmiIndexView.layer.borderWidth = 0.0f;
	self.bmiIndexView.layer.cornerRadius = 5.0f;
	self.bmiIndexViewBg.backgroundColor = separatorGrayColor();
	self.bmiIndexViewBg.alpha = 0.7;
	self.bmiIndexViewBg.layer.backgroundColor = separatorGrayColor().CGColor;
	self.bmiIndexViewBg.layer.borderColor = [UIColor clearColor].CGColor;
	self.bmiIndexViewBg.layer.borderWidth = 0.0f;
	self.bmiIndexViewBg.layer.cornerRadius = 5.0f;
	[self setBMIIndex];
	
	// symptom table
	self.symptoms = [[self.visit symptoms] allObjects].mutableCopy;
	[self animateSymptomsTableWithDelta:((int)[self.symptoms count]*cellDrugsHeight)];
	
	// drugs table
	self.drugs = [[DBManager getIstance] getTherapiesFromVisit:self.visit].mutableCopy;
	[self animateDrugsTableWithDelta:((int)[self.drugs count]*cellDrugsHeight)];
	
	// test table
	self.tests = [[self.visit tests] allObjects].mutableCopy;
	[self animateTestsTableWithDelta:((int)[self.tests count]*cellTestHeight)];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
	if ([self.parentViewController.parentViewController isKindOfClass:[PatientVisitsViewController class]])
	{
		PatientVisitsViewController *parent = (PatientVisitsViewController*)self.parentViewController.parentViewController;
		[parent reloadData];
	}
	else if ([self.parentViewController isKindOfClass:[PatientVisitsViewController class]])
	{
		PatientVisitsViewController *parent = (PatientVisitsViewController*)self.parentViewController;
		[parent reloadData];
	}
}

- (void)setBMIIndex
{
	Measurement *weightMeasurement = [[DBManager getIstance] getMeasurementWithName:@"Weight" visit:self.visit];
	Measurement *heightMeasurement = [[DBManager getIstance] getMeasurementWithName:@"Height" visit:self.visit];
	if ([BaseViewController isEmptyNumber:weightMeasurement.value] || [BaseViewController isEmptyNumber:heightMeasurement.value])
	{
		[self setBMIIndexInvalid];
		return;
	}
	
	float weight = [weightMeasurement.value floatValue];
	float height = [heightMeasurement.value floatValue]/100;
	if (weight != 0 && height != 0)
	{
		float powerHeight = powf(height, 2);
		float bmiIndex = weight / powerHeight;
		if (bmiIndex < 15)
			bmiIndex = 15;
		else if (bmiIndex > 40)
			bmiIndex = 40;
		self.bmiLabel.text = [NSString stringWithFormat:@"%i", (int)bmiIndex];
		float bmiIndexViewWidth = (bmiIndex - 15)*10;
		self.bmiIndexView.frame = CGRectMake(self.bmiIndexView.frame.origin.x, self.bmiIndexView.frame.origin.y, bmiIndexViewWidth, self.bmiIndexView.frame.size.height);
		self.slimLabel.hidden = NO;
		self.fatLabel.hidden = NO;
	}
	else
		[self setBMIIndexInvalid];
}

- (void)setBMIIndexInvalid
{
	self.bmiLabel.text = @"ND";
	self.bmiIndexView.frame = CGRectMake(self.bmiIndexView.frame.origin.x, self.bmiIndexView.frame.origin.y, 0, self.bmiIndexView.frame.size.height);
	self.slimLabel.hidden = YES;
	self.fatLabel.hidden = YES;
}

#pragma mark - Animations

- (void)animateTestsTableWithDelta:(int)delta
{
	if (self.testTable.contentSize.height >= delta && delta > 0)
		return;
	
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.testTable.frame = CGRectMake(self.testTable.frame.origin.x, self.testTable.frame.origin.y, self.testTable.frame.size.width, self.testTable.frame.size.height + delta);
	} completion:^(BOOL s){
		[self.testTable reloadData];
	}];
}

- (void)animateSymptomsTableWithDelta:(int)delta
{
	if (self.symptomTable.contentSize.height >= delta && delta > 0)
		return;

	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.symptomTable.frame = CGRectMake(self.symptomTable.frame.origin.x, self.symptomTable.frame.origin.y, self.symptomTable.frame.size.width, self.symptomTable.frame.size.height + delta);
		self.objectiveExamination.frame = CGRectMake(self.objectiveExamination.frame.origin.x, self.objectiveExamination.frame.origin.y + delta, self.objectiveExamination.frame.size.width, self.objectiveExamination.frame.size.height);
		self.drugHeader.frame = CGRectMake(self.drugHeader.frame.origin.x, self.drugHeader.frame.origin.y + delta, self.drugHeader.frame.size.width, self.drugHeader.frame.size.height);
		self.drugsTable.frame = CGRectMake(self.drugsTable.frame.origin.x, self.drugsTable.frame.origin.y + delta, self.drugsTable.frame.size.width, self.drugsTable.frame.size.height);
		self.finalConsiderations.frame = CGRectMake(self.finalConsiderations.frame.origin.x, self.finalConsiderations.frame.origin.y + delta, self.finalConsiderations.frame.size.width, self.finalConsiderations.frame.size.height);
		self.testHeader.frame = CGRectMake(self.testHeader.frame.origin.x, self.testHeader.frame.origin.y + delta, self.testHeader.frame.size.width, self.testHeader.frame.size.height);
		self.testTable.frame = CGRectMake(self.testTable.frame.origin.x, self.testTable.frame.origin.y + delta, self.testTable.frame.size.width, self.testTable.frame.size.height);
	} completion:^(BOOL s){
		[self.symptomTable reloadData];
	}];
}

- (void)animateDrugsTableWithDelta:(int)delta
{
	if (self.drugsTable.contentSize.height >= delta && delta > 0)
		return;
	
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + delta);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.drugsTable.frame = CGRectMake(self.drugsTable.frame.origin.x, self.drugsTable.frame.origin.y, self.drugsTable.frame.size.width, self.drugsTable.frame.size.height + delta);
		self.finalConsiderations.frame = CGRectMake(self.finalConsiderations.frame.origin.x, self.finalConsiderations.frame.origin.y + delta, self.finalConsiderations.frame.size.width, self.finalConsiderations.frame.size.height);
		self.testHeader.frame = CGRectMake(self.testHeader.frame.origin.x, self.testHeader.frame.origin.y + delta, self.testHeader.frame.size.width, self.testHeader.frame.size.height);
		self.testTable.frame = CGRectMake(self.testTable.frame.origin.x, self.testTable.frame.origin.y + delta, self.testTable.frame.size.width, self.testTable.frame.size.height);
	} completion:^(BOOL s){
		[self.drugsTable reloadData];
	}];
}


#pragma mark - Actions

- (IBAction)save:(id)sender
{
	PatientVisitDetailViewController *vc = [[PatientVisitDetailViewController alloc] initWithNibName:@"PatientVisitDetailViewController" bundle:nil];
	PatientVisitsViewController *parent = (PatientVisitsViewController*)self.parentViewController;
	vc.patient = self.patient;
	vc.visit = self.visit;
	[self parent:parent fadeToViewController:vc fromViewController:self superView:parent.view animateWithDuration:0.2];
}

- (IBAction)addTest:(id)sender
{
	if (![BaseViewController isEmptyString:self.testType.textField.text])
	{
		[[DBManager getIstance] createDiagnosticTestVisitForVisit:self.visit name:self.testType.textField.text note:self.testNote.text];
		self.tests = [[self.visit tests] allObjects].mutableCopy;
		self.testType.textField.text = @"";
		self.testNote.text = @"";
		[self animateTestsTableWithDelta:cellTestHeight];
	}
	else
	{
		[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please provide a value for the Details filled" confirmBlock:^{
			self.testType.textField.text = @"";
			self.testNote.text = @"";
		}];
	}
}

// add drug as active therapy
- (IBAction)addDrug:(id)sender
{
	if (![BaseViewController isEmptyString:self.drugName.textField.text])
	{
		[[DBManager getIstance] createPrescribedTherapyForPatient:self.patient date:[NSDate date] drugName:self.drugName.textField.text dosage:self.drugDosageText.text frequency:self.drugFrequencyText.textField.text route:self.drugRouteText.textField.text active:YES suspended:NO prescribed:NO dosageUnit:self.drugUnitText.textField.text visit:self.visit prescription:nil];
		self.drugName.textField.text = @"";
		self.drugDosageText.text = @"";
		self.drugFrequencyText.textField.text = @"";
		self.drugUnitText.textField.text = @"";
		self.drugRouteText.textField.text = @"";
		self.drugs = [[DBManager getIstance] getTherapiesFromVisit:self.visit].mutableCopy;
		[self animateDrugsTableWithDelta:cellDrugsHeight];
	}
	else
	{
		[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please provide a value for the Details filled" confirmBlock:^{
			self.drugName.textField.text = @"";
			self.drugDosageText.text = @"";
			self.drugFrequencyText.textField.text = @"";
			self.drugUnitText.textField.text = @"";
			self.drugRouteText.textField.text = @"";
		}];
	}
}

- (IBAction)addSymptom:(id)sender
{
	if (![BaseViewController isEmptyString:self.symptomType.textField.text] && ![BaseViewController isEmptyString:self.symptomDetail.textField.text])
	{
		[[DBManager getIstance] createSymptomForVisit:self.visit name:self.symptomType.textField.text details:self.symptomDetail.textField.text note:self.symptomNote.text];
		self.symptomType.textField.text = @"";
		self.symptomDetail.textField.text = @"";
		self.symptomNote.text = @"";
		self.symptoms = [[self.visit symptoms] allObjects].mutableCopy;
		[self animateSymptomsTableWithDelta:cellDrugsHeight];
	}
	else
	{
		[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Please provide a value for the Details filled" confirmBlock:^{
			self.symptomType.textField.text = @"";
			self.symptomDetail.textField.text = @"";
			self.symptomNote.text = @"";
		}];
	}
}

- (IBAction)addNewFormula:(id)sender
{
	[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"More formulas available soon" confirmBlock:^{
	}];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	return [super textFieldDidBeginEditing:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	
	NSString *text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	if (textField == self.visitNote)
	{
		self.visit.note = text;
		[[DBManager getIstance] saveContext];
	}
	else if ([textField.accessibilityIdentifier isEqualToString:[MeasurementCollectionCell reuseIdentifier]])
	{
		int index = (int)textField.tag;
		Measurement *measurement = self.measurements[index];
		NSString *measurementText = [[text stringByReplacingOccurrencesOfString:@"," withString:@"."] stringByReplacingOccurrencesOfString:@" " withString:@""];
		if ([BaseViewController isEmptyString:measurementText])
			return;
		NSDecimalNumber *measurementNumber = [NSDecimalNumber decimalNumberWithString:measurementText];
		
		if ([[DBManager getIstance] isImperialSystem])
			measurement.value = [[DBManager getIstance] getValueMetricSystemMeasurementName:measurement.name measurementValue:measurementNumber];
		else
			measurement.value = measurementNumber;
		
		if ([[DBManager getIstance] isImperialSystem])
			textField.text = [[[DBManager getIstance] getValueImperialSystemMeasurement:measurement] stringValue];
		else
			textField.text = [measurement.value stringValue];
		
		measurement.date = self.visit.date;
		[[DBManager getIstance] saveContext];
		self.measurements = [[DBManager getIstance] getMeasurementsSortedWithIndexForVisit:self.visit].mutableCopy;
		[self.measurementCollection reloadData];
		[self setBMIIndex];
	}
	
	else if ([textField.accessibilityIdentifier isEqualToString:[SymptomCell reuseIdentifier]] && [[(CustomTextField*)textField type] isEqualToString:SymptomNoteField])
	{
		int index = (int)textField.tag;
		Symptom *symptom = self.symptoms[index];
		symptom.note = text;
		[[DBManager getIstance] saveContext];
		self.symptoms = [self.visit.symptoms allObjects].mutableCopy;
	}
	
	else if ([textField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]])
	{
		int index = (int)textField.tag;
		PrescribedTherapy *therapy = self.drugs[index];
		therapy.dosage = text;
		[[DBManager getIstance] saveContext];
		self.drugs = [[DBManager getIstance] getTherapiesFromVisit:self.visit].mutableCopy;
	}
	
	else if ([textField.accessibilityIdentifier isEqualToString:[DiagnosticTestCell reuseIdentifier]])
	{
		int index = (int)textField.tag;
		DiagnosticTest *test = self.tests[index];
		test.note = text;
		[[DBManager getIstance] saveContext];
		self.tests = [[self.visit tests] allObjects].mutableCopy;
	}
	
	[super textFieldDidEndEditing:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	return [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
}


#pragma mark - CustomTextViewDelegate

- (void)customTextViewDidBeginEditing:(CustomTextView *)textView
{
	[super customTextViewDidBeginEditing:textView];
}

- (void)customTextViewDidEndEditing:(CustomTextView *)textView
{
	NSString *text = [textView.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	if (textView == self.objectiveExamination)
	{
		self.visit.objectiveExamination = text;
		[[DBManager getIstance] saveContext];
	}
	else if (textView == self.finalConsiderations)
	{
		self.visit.finalConsideration = text;
		[[DBManager getIstance] saveContext];
	}
	
	[super customTextViewDidEndEditing:textView];
}

- (void)customTextViewDidChange:(CustomTextView *)textView
{
	[super customTextViewDidChange:textView];
}

#pragma mark - PickerFieldDelegate Methods

- (void)tapOnPickerFieldDrugButton:(PickerField*)pickerField
{
	PickerViewController *pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
	pickerViewController.pickerField = pickerField;
	pickerViewController.delegate = self;
	pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:DrugActiveData text:@""].mutableCopy;
	pickerViewController.pickerType = DrugNameField;
	pickerViewController.dataType = DrugActiveData;
	
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerViewController];
	[self.popover setPopoverContentSize:pickerViewController.view.frame.size];
	[pickerViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	/*
	PickerDrugViewController *pickerDrugViewController = [[PickerDrugViewController alloc] initWithNibName:@"PickerDrugViewController" bundle:nil];
	pickerDrugViewController.pickerField = pickerField;
	pickerDrugViewController.delegate = self;
	
	if (pickerField == self.drugName || [pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]])
	{
		pickerDrugViewController.pickerType = DrugNameField;
	}
	
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerDrugViewController];
	[self.popover setPopoverContentSize:pickerDrugViewController.view.frame.size];
	[pickerDrugViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	 */
}

- (void)tapOnPickerFieldDateButton:(PickerField*)pickerField
{
	PickerDateViewController *pickerDateViewController = [[PickerDateViewController alloc] initWithNibName:@"PickerDateViewController" bundle:nil];
	pickerDateViewController.pickerField = pickerField;
	pickerDateViewController.delegate = self;
	
	if (pickerField == self.visitDate)
		pickerDateViewController.date = [self.formatter dateFromString:self.visitDate.textField.text];
		
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerDateViewController];
	[self.popover setPopoverContentSize:pickerDateViewController.view.frame.size];
	[pickerDateViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)pickerFieldDidBeginEditing:(PickerField *)pickerField
{
	[super pickerFieldDidBeginEditing:pickerField];
}

- (void)tapOnPickerFieldButton:(PickerField*)pickerField
{
	PickerViewController *pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
	pickerViewController.pickerField = pickerField;
	pickerViewController.delegate = self;
	
	if (pickerField == self.visitType)
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:VisitTypeData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = VisitTypeData;
	}
	
	else if (pickerField == self.symptomType || ([pickerField.accessibilityIdentifier isEqualToString:[SymptomCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:SymptomTypeField]))
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:SymptomData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = SymptomData;
	}
	
	else if (pickerField == self.symptomDetail || ([pickerField.accessibilityIdentifier isEqualToString:[SymptomCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:SymptomDetailsField]))
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"SymptomType"];
		for(NSString *title in list)
			[pickerViewController.data addObject:title];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = StaticField;
	}
	
	else if ((pickerField == self.drugUnitText) || ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugUnitField]))
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"DrugDosageUnit"];
		for (NSDictionary *item in list)
			[pickerViewController.data addObject:item[@"name"]];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = DrugUnitField;
	}
	
	else if ((pickerField == self.drugFrequencyText) || ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugFrequencyField]))
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"Frequency"];
		for(NSString *title in list)
			[pickerViewController.data addObject:title];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = DrugFrequencyField;
	}
	
	else if ((pickerField == self.drugRouteText) || ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugRouteField]))
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:RouteData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = RouteData;
	}
	
	else if (pickerField == self.testType || [pickerField.accessibilityIdentifier isEqualToString:[DiagnosticTestCell reuseIdentifier]])
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:DiagnosticTestsData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = DiagnosticTestsData;
	}
	
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerViewController];
	[self.popover setPopoverContentSize:pickerViewController.view.frame.size];
	[pickerViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma mark - PickerDateViewControllerDelegate Methods

- (void)pickerDateViewControllerSetDate:(NSDate *)date pickerField:(PickerField *)pickerField
{
	pickerField.textField.text = [self.formatter stringFromDate:date];
	
	if (pickerField == self.visitDate)
	{
		self.visit.date = date;
		[[DBManager getIstance] saveContext];
	}
}

#pragma mark - PickerDrugViewControllerDelegate Methods

/*
- (void)pickerDrugViewControllerSetText:(NSString*)text pickerField:(PickerField*)pickerField
{
	pickerField.textField.text = text;
	
	if ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]])
	{
		int index = (int)pickerField.tag;
		PrescribedTherapy *therapy = self.drugs[index];
		therapy.drugName = text;
		[[DBManager getIstance] saveContext];
		self.drugs = [[DBManager getIstance] getTherapiesFromVisit:self.visit].mutableCopy;
	}
}
 */

#pragma mark - PickerViewControllerDelegate Methods

- (void)pickerViewControllerSetText:(NSString*)text pickerField:(PickerField*)pickerField
{
	pickerField.textField.text = text;
	
	if (pickerField == self.visitType)
	{
		self.visit.type = text;
		[[DBManager getIstance] saveContext];
	}
	
	else if ([pickerField.accessibilityIdentifier isEqualToString:[SymptomCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:SymptomTypeField])
	{
		int index = (int)pickerField.tag;
		Symptom *symptom = self.symptoms[index];
		symptom.name = text;
		[[DBManager getIstance] saveContext];
		self.symptoms = [[self.visit symptoms] allObjects].mutableCopy;
	}
	
	else if ([pickerField.accessibilityIdentifier isEqualToString:[SymptomCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:SymptomDetailsField])
	{
		int index = (int)pickerField.tag;
		Symptom *symptom = self.symptoms[index];
		symptom.details = text;
		[[DBManager getIstance] saveContext];
		self.symptoms = [[self.visit symptoms] allObjects].mutableCopy;
	}
	
	else if ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugNameField])
	{
		int index = (int)pickerField.tag;
		PrescribedTherapy *therapy = self.drugs[index];
		therapy.drugName = text;
		[[DBManager getIstance] saveContext];
	}
	
	else if ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugUnitField])
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"DrugDosageUnit"];
		pickerField.textField.text = list[pickerField.pickerIndex][@"name"];
		int index = (int)pickerField.tag;
		PrescribedTherapy *therapy = self.drugs[index];
		therapy.dosageUnit = text;
		[[DBManager getIstance] saveContext];
		self.drugs = [[DBManager getIstance] getTherapiesFromVisit:self.visit].mutableCopy;
	}
	
	else if ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugFrequencyField])
	{
		int index = (int)pickerField.tag;
		PrescribedTherapy *therapy = self.drugs[index];
		therapy.frequency = text;
		[[DBManager getIstance] saveContext];
		self.drugs = [[DBManager getIstance] getTherapiesFromVisit:self.visit].mutableCopy;
	}
	
	else if ([pickerField.accessibilityIdentifier isEqualToString:[DrugCell reuseIdentifier]] && [pickerField.fieldType isEqualToString:DrugRouteField])
	{
		int index = (int)pickerField.tag;
		PrescribedTherapy *therapy = self.drugs[index];
		therapy.route = text;
		[[DBManager getIstance] saveContext];
		self.drugs = [[DBManager getIstance] getTherapiesFromVisit:self.visit].mutableCopy;
	}
	
	else if ([pickerField.accessibilityIdentifier isEqualToString:[DiagnosticTestCell reuseIdentifier]])
	{
		int index = (int)pickerField.tag;
		DiagnosticTest *test = self.tests[index];
		test.name = text;
		[[DBManager getIstance] saveContext];
		self.tests = [[self.visit tests] allObjects].mutableCopy;
	}
}

#pragma mark - UICollectionViewDataSource

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
	return CGSizeMake(102, 130);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
	return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
	return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
	return 23;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return [self.measurements count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	MeasurementCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[MeasurementCollectionCell reuseIdentifier] forIndexPath:indexPath];
	cell.index = [NSNumber numberWithInt:(int)indexPath.row];
	cell.cellDelegate = self;
	Measurement *measurement = self.measurements[indexPath.row];
	NSArray *prevMeasurements = [[DBManager getIstance] getAllMeasurementsWithName:measurement.name patient:self.patient];
	[cell setMeasurement:measurement previousMeasurements:prevMeasurements];

	return cell;
}


#pragma mark - UITableViewDataSource/UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (tableView == self.drugsTable)
		return [self.drugs count];
	else if (tableView == self.testTable)
		return [self.tests count];
	else
		return [self.symptoms count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;
	
	if (tableView == self.drugsTable)
	{
		DrugCell *customcell = [tableView dequeueReusableCellWithIdentifier:[DrugCell reuseIdentifier]];
		customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
		customcell.cellDelegate = self;
		PrescribedTherapy *therapy = self.drugs[indexPath.row];
		[customcell setContent:therapy];
		cell = customcell;
	}
	
	else if (tableView == self.testTable)
	{
		DiagnosticTestCell *customcell = [tableView dequeueReusableCellWithIdentifier:[DiagnosticTestCell reuseIdentifier]];
		customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
		customcell.cellDelegate = self;
		DiagnosticTest *test = self.tests[indexPath.row];
		[customcell setDiagnosticTest:test];
		cell = customcell;
	}
	
	else if (tableView == self.symptomTable)
	{
		SymptomCell *customcell = [tableView dequeueReusableCellWithIdentifier:[SymptomCell reuseIdentifier]];
		customcell.index = [NSNumber numberWithInt:(int)indexPath.row];
		customcell.cellDelegate = self;
		Symptom *symptom = self.symptoms[indexPath.row];
		[customcell setSymptomVisit:symptom];
		cell = customcell;
	}
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)doActionForKey:(NSString*)actionKey actionData:(id)actionData
{
	if ([actionKey isEqualToString:@"deleteDrug"])
	{
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"You are going to delete this information: are you sure?" confirmBlock:^{
			int index = [actionData[@"index"] intValue];
			PrescribedTherapy *therapy = self.drugs[index];
			[[DBManager getIstance] deleteObject:therapy];
			self.drugs = [[DBManager getIstance] getTherapiesFromVisit:self.visit].mutableCopy;
			[self animateDrugsTableWithDelta:-cellDrugsHeight];
		} cancelBlock:^{
		}];
	}
	
	else if ([actionKey isEqualToString:@"deleteTest"])
	{
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"You are going to delete this information: are you sure?" confirmBlock:^{
			int index = [actionData[@"index"] intValue];
			DiagnosticTest *test = self.tests[index];
			[[DBManager getIstance] deleteObject:test];
			self.tests = [[self.visit tests] allObjects].mutableCopy;
			[self animateTestsTableWithDelta:-cellTestHeight];
		} cancelBlock:^{
		}];
	}
	
	else if ([actionKey isEqualToString:@"deleteSymptom"])
	{
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"You are going to delete this information: are you sure?" confirmBlock:^{
			int index = [actionData[@"index"] intValue];
			Symptom *symptom = self.symptoms[index];
			[[DBManager getIstance] deleteObject:symptom];
			self.symptoms = [[self.visit symptoms] allObjects].mutableCopy;
			[self animateSymptomsTableWithDelta:-cellDrugsHeight];
		} cancelBlock:^{
		}];
	}
	
	else if ([actionKey isEqualToString:@"addPhoto"])
	{
		// disponibili sia camera che photos
		if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
		{
			// mostra alert per scelta tra camera e photos
			UIImage *firstButtonBg = [UIImage imageNamed:@"Butt_cameragrayalert"];
			UIImage *secondButtonBg = [UIImage imageNamed:@"Butt_photosgreenalert"];
			[[AlertViewBlocks getIstance] twoOptionsAlertViewWithmessage:@"Load pictures from the iPad camera roll or take a new picture" firstButtonBg:firstButtonBg secondButtonBg:secondButtonBg confirmBlock:^{
				[self showCamera:actionData[@"index"]];
			} secondConfirm:^{
				[self showPhotos:actionData[@"index"]];
			}];
		}

		// disponibile solo camera
		else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && ![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
		{
			[self showCamera:actionData[@"index"]];
		}
		
		// disponibile solo photos
		else if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
		{
			[self showPhotos:actionData[@"index"]];
		}
	}
	else if ([actionKey isEqualToString:@"selectGallery"])
	{
		int index = [actionData[@"index"] intValue];
		DiagnosticTest *test = self.tests[index];
		GalleryViewController *galleryViewController = [[GalleryViewController alloc] initWithNibName:@"GalleryViewController" bundle:nil];
		[self presentViewController:galleryViewController animated:YES completion:nil];
		galleryViewController.headerLabel.text = test.name;
		galleryViewController.data = [[test diagnosticImages] allObjects].mutableCopy;
	}
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
	if ([info[UIImagePickerControllerMediaType] isEqualToString:@"public.image"])
	{
		// modifica foto e salva nelle diagnostic test
		UIImage *originalImage = info[UIImagePickerControllerOriginalImage];
		EditPhotoViewController *editPhotoViewController = [[EditPhotoViewController alloc] initWithNibName:@"EditPhotoViewController" bundle:nil];
		if ([self.presentedViewController isKindOfClass:[UIImagePickerController class]])
		{
			[self dismissViewControllerAnimated:YES completion:nil];
		}
		[self presentViewController:editPhotoViewController animated:YES completion:nil];
		[editPhotoViewController savePhoto:originalImage note:originalImage.note completion:^(UIImage *savedImage) {
			int index = [picker.index intValue];
			DiagnosticTest *test = self.tests[index];
			NSData *data = UIImagePNGRepresentation(savedImage);
			if (data == nil)
				return ;
			[[DBManager getIstance] createDiagnosticImageForTest:test image:data note:savedImage.note];
			[self.testTable reloadData];
			
			// se il picker è di tipo camera chiedi se salvare la foto anche delle photos
			if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
			{
				[[AlertViewBlocks getIstance] informationConfirmAlertViewWithmessage:@"Do you want to save the image in Photos app?" confirmBlock:^{
					UIImageWriteToSavedPhotosAlbum(savedImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
				} cancelBlock:^{
				}];
			}
		} cancel:^{
		}];
	}
}


@end
