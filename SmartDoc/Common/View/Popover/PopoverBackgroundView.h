//
//  PopoverBackgroundView.h
//  Demo
//
//  Created by Francesca Corsini on 27/04/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopoverBackgroundView : UIPopoverBackgroundView
{
    CGFloat                     _arrowOffset;
    UIPopoverArrowDirection     _arrowDirection;
}

@property (nonatomic, readwrite) CGFloat arrowOffset;
@property (nonatomic, readwrite) UIPopoverArrowDirection arrowDirection;

+ (CGFloat)arrowHeight;
+ (CGFloat)arrowBase;
+ (UIEdgeInsets)contentViewInsets;

@end
