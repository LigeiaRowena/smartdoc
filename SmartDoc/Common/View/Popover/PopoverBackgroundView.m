//
//  PopoverBackgroundView.m
//  Demo
//
//  Created by Francesca Corsini on 27/04/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "PopoverBackgroundView.h"

@implementation PopoverBackgroundView

@synthesize arrowOffset = _arrowOffset, arrowDirection = _arrowDirection;


- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self)
	{
		self.backgroundColor = [UIColor clearColor];
		self.alpha = 0.0f;
	}
	return self;
}

+ (CGFloat)arrowBase
{
    return 0;
}

+ (CGFloat)arrowHeight
{
    return 0;
}

+ (UIEdgeInsets)contentViewInsets
{
    return UIEdgeInsetsZero;
}

-(void) setArrowOffset:(CGFloat)arrowOffset
{
    _arrowOffset = arrowOffset;
    [self setNeedsLayout];
}

-(void) setArrowDirection:(UIPopoverArrowDirection)arrowDirection
{
    _arrowDirection = arrowDirection;
    [self setNeedsLayout];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
}

@end
