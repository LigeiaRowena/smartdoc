//
//  PickerCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 20/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "PickerCell.h"
#import "DinamicData.h"

@implementation PickerCell

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.name.font = OpenSansRegular(14);
	self.status.hidden = YES;
}

- (void)setContent:(id)obj
{
	[super setContent:obj];
	
	if ([obj isKindOfClass:[DinamicData class]])
		self.name.text = [(DinamicData*)obj text];
	else if ([obj isKindOfClass:[NSString class]])
		self.name.text = obj;
}

@end
