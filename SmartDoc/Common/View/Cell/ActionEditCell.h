//
//  ContactEmailCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "Action.h"
#import "PickerDateViewController.h"
#import "PickerViewController.h"
#import "PickerField.h"

@interface ActionEditCell : BaseTableViewCell <PickerDateViewControllerDelegate, PickerViewControllerDelegate, PickerFieldDelegate>

@property (nonatomic, weak) IBOutlet UIImageView *type;
@property (nonatomic, weak) IBOutlet PickerField *typetext;
@property (nonatomic, weak) IBOutlet UIView *actionWhenView;
@property (nonatomic, weak) IBOutlet PickerField *actionWhen;
@property (nonatomic, weak) IBOutlet UIView *actionDateView;
@property (nonatomic, weak) IBOutlet PickerField *actionDate;
@property (nonatomic, weak) IBOutlet UIView *actionTimeView;
@property (nonatomic, weak) IBOutlet PickerField *actionTime;
@property (nonatomic, strong) UIPopoverController *popover;

@property (nonatomic, strong) NSDateFormatter *formatter;

@property (nonatomic, strong) Action *action;

@end
