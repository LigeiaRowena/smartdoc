//
//  BaseTableViewCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 20/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickerField.h"
#import "CustomTextView.h"
#import "CustomTextField.h"

@protocol CellDelegate <NSObject>
@optional
- (void)doActionForKey:(NSString*)actionKey actionData:(id)actionData;
@end

@interface BaseTableViewCell : UITableViewCell

@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, weak) id<CellDelegate> cellDelegate;
@property (nonatomic, strong) NSNumber *index;

- (void)setContent:(id)obj;
+ (NSString *)reuseIdentifier;

@end
