//
//  FatherHistoryCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "DrugCell.h"
#import "PrescribedTherapy.h"
#import "BaseViewController.h"

@implementation DrugCell

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.name.accessibilityIdentifier = [self reuseIdentifier];
	self.dosage.accessibilityIdentifier = [self reuseIdentifier];
	self.unit.accessibilityIdentifier = [self reuseIdentifier];
	self.frequency.accessibilityIdentifier = [self reuseIdentifier];
	self.route.accessibilityIdentifier = [self reuseIdentifier];
}

- (void)setContent:(id)obj
{
	[super setContent:obj];
	
	PrescribedTherapy *therapy = (PrescribedTherapy*)obj;
	
	self.name.tag = [self.index intValue];
	self.name.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.name.textField.text = therapy.drugName;
	
	self.dosage.tag = [self.index intValue];
	self.dosage.delegate = (BaseViewController*)self.cellDelegate;
	self.dosage.text = therapy.dosage;
	
	self.unit.tag = [self.index intValue];
	self.unit.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.unit.textField.text = therapy.dosageUnit;
	
	self.frequency.tag = [self.index intValue];
	self.frequency.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.frequency.textField.text = therapy.frequency;

	self.route.tag = [self.index intValue];
	self.route.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.route.textField.text = therapy.route;
}

- (IBAction)remove:(id)sender
{
	if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(doActionForKey:actionData:)])
		[self.cellDelegate doActionForKey:@"deleteDrug" actionData:@{@"index": self.index}];
}

@end
