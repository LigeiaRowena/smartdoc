//
//  FatherHistoryCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface MotherHistoryCell : BaseTableViewCell

@property (nonatomic, weak) IBOutlet PickerField *fatherTypeText;
@property (nonatomic, weak) IBOutlet CustomTextField *fatherText;

@end
