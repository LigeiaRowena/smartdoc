//
//  ContactEmailCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "ActionEditCell.h"
#import "BaseViewController.h"
#import "NSDate-Utilities.h"

@implementation ActionEditCell

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.formatter = [[NSDateFormatter alloc] init];
	[self.formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
	[self.formatter setDateFormat:@"dd/MM/yyyy"];
}

- (void)setContent:(id)obj
{
	[super setContent:obj];
	
	self.action = (Action*)obj;
	self.typetext.textField.text = self.action.desc;
	
	if ([self.action.type isEqualToString:ActionAppointment])
	{
		self.actionDate.textField.text = [self.formatter stringFromDate:self.action.date];
		self.actionTime.textField.text = self.action.time;
		self.actionWhen.textField.text = self.action.dateString;
		self.type.image = [UIImage imageNamed:@"Icon_todoappointment.png"];
		self.actionTimeView.hidden = NO;
		self.actionWhenView.hidden = YES;
		
		self.actionDateView.frame = CGRectMake(10, 81, self.actionDateView.frame.size.width, self.actionDateView.frame.size.height);
		self.actionTimeView.frame = CGRectMake(178, 81, self.actionTimeView.frame.size.width, self.actionTimeView.frame.size.height);
	}
	
	else if ([self.action.type isEqualToString:ActionTodo])
	{
		self.actionDate.textField.text = [self.formatter stringFromDate:self.action.date];
		self.actionWhen.textField.text = self.action.dateString;
		self.type.image = [UIImage imageNamed:@"Icon_todoalert.png"];
		self.actionTimeView.hidden = YES;
		self.actionWhenView.hidden = NO;
		
		self.actionDateView.frame = CGRectMake(235, 81, self.actionDateView.frame.size.width, self.actionDateView.frame.size.height);
		self.actionTimeView.frame = CGRectMake(403, 81, self.actionTimeView.frame.size.width, self.actionTimeView.frame.size.height);
	}
}

- (IBAction)save:(id)sender
{
	if ([self.action.type isEqualToString:ActionAppointment])
	{
		NSString *dateString = [NSString stringWithFormat:@"%@ %@", self.actionDate.textField.text, self.actionTime.textField.text];
		NSDateFormatter *completeformatter = [[NSDateFormatter alloc] init] ;
		[completeformatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
		[completeformatter setDateFormat:@"dd/MM/yyyy HH:mm"];
		self.action.date = [completeformatter dateFromString:dateString];
		self.action.time = self.actionTime.textField.text;
		self.action.desc = self.typetext.textField.text;
		self.action.dateString = dateString;
		[[DBManager getIstance] saveContext];
	}
	else if ([self.action.type isEqualToString:ActionTodo])
	{
		if (![BaseViewController isEmptyString:self.actionDate.textField.text])
			self.action.date = [self.formatter dateFromString:self.actionDate.textField.text];
		else
			self.action.date = nil;
		if (![BaseViewController isEmptyString:self.actionWhen.textField.text])
			self.action.dateString = self.actionWhen.textField.text;
		else
			self.action.dateString = nil;
		self.action.desc = self.typetext.textField.text;
		[[DBManager getIstance] saveContext];
	}
	
	if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(doActionForKey:actionData:)])
		[self.cellDelegate doActionForKey:@"saveAction" actionData:self.action];
}

#pragma mark - PickerFieldDelegate Methods

- (void)tapOnPickerFieldDateButton:(PickerField*)pickerField
{
	PickerDateViewController *pickerDateViewController = [[PickerDateViewController alloc] initWithNibName:@"PickerDateViewController" bundle:nil];
	pickerDateViewController.pickerField = pickerField;
	pickerDateViewController.delegate = self;
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerDateViewController];
	[self.popover setPopoverContentSize:pickerDateViewController.view.frame.size];
	[pickerDateViewController setPopover:self.popover];
	
	if (pickerField == self.actionDate)
		pickerDateViewController.date = [self.formatter dateFromString:self.actionDate.textField.text];
	else if (pickerField == self.actionTime)
	{
		pickerDateViewController.date = [self.formatter dateFromString:self.actionTime.textField.text];
		pickerDateViewController.picker.datePickerMode = UIDatePickerModeTime;
	}
	
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)tapOnPickerFieldButton:(PickerField*)pickerField
{
	PickerViewController *pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
	pickerViewController.pickerField = pickerField;
	pickerViewController.delegate = self;
	
	if (pickerField == self.actionWhen)
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"SpecificDate"];
		for(NSString *title in list)
			[pickerViewController.data addObject:title];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = StaticField;
	}
	
	else if (pickerField == self.typetext)
	{
		if ([self.action.type isEqualToString:ActionAppointment])
		{
			pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:AppointmentData text:@""].mutableCopy;
			pickerViewController.pickerType = EditableField;
			pickerViewController.dataType = AppointmentData;
		}
		else if ([self.action.type isEqualToString:ActionTodo])
		{
			pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:ToDoData text:@""].mutableCopy;
			pickerViewController.pickerType = EditableField;
			pickerViewController.dataType = ToDoData;
		}
	}
	
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerViewController];
	[self.popover setPopoverContentSize:pickerViewController.view.frame.size];
	[pickerViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)pickerFieldDidBeginEditing:(PickerField *)pickerField
{
}

- (NSArray*)getArrayFromPlist:(NSString*)plist key:(NSString*)key
{
	NSString *path = [[NSBundle mainBundle] pathForResource:[plist stringByDeletingPathExtension] ofType:[plist pathExtension]];
	return [NSDictionary dictionaryWithContentsOfFile:path][key];
}

#pragma mark - PickerDateViewControllerDelegate Methods

- (void)pickerDateViewControllerSetDate:(NSDate *)date pickerField:(PickerField *)pickerField
{
	if (pickerField == self.actionTime)
	{
		NSDateFormatter *timeformatter = [[NSDateFormatter alloc] init] ;
		[timeformatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
		[timeformatter setDateFormat:@"HH:mm"];
		pickerField.textField.text = [timeformatter stringFromDate:date];
	}
	// in caso di Todo la valorizzazione del campo Date annulla il campo When
	else if ([self.action.type isEqualToString:ActionTodo] && pickerField == self.actionDate && date != nil)
	{
		self.actionWhen.textField.text = @"";
		pickerField.textField.text = [self.formatter stringFromDate:date];
	}
	else
		pickerField.textField.text = [self.formatter stringFromDate:date];
}

#pragma mark - PickerViewControllerDelegate Methods

- (void)pickerViewControllerSetText:(NSString*)text pickerField:(PickerField*)pickerField
{
	pickerField.textField.text = text;
	
	// in caso di Todo la valorizzazione del campo When annulla il campo Date
	if ([self.action.type isEqualToString:ActionTodo] && pickerField == self.actionWhen)
		self.actionDate.textField.text = @"";
}

@end
