//
//  FatherHistoryCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface DrugCell : BaseTableViewCell

@property (nonatomic, weak) IBOutlet PickerField *name;
@property (nonatomic, weak) IBOutlet CustomTextField *dosage;
@property (nonatomic, weak) IBOutlet PickerField *unit;
@property (nonatomic, weak) IBOutlet PickerField *frequency;
@property (nonatomic, weak) IBOutlet PickerField *route;

@end
