//
//  FatherHistoryCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "DiagnosticTestCell.h"
#import "BaseViewController.h"
#import "DiagnosticImage.h"

@implementation DiagnosticTestCell

#pragma mark - Init

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.typeText.accessibilityIdentifier = [self reuseIdentifier];
	self.note.accessibilityIdentifier = [self reuseIdentifier];
	self.diagnosticImages = @[].mutableCopy;
	
	// images collection
	UINib *cellImage = [UINib nibWithNibName:@"ThumbCollectionCell" bundle:nil];
	[self.images registerNib:cellImage forCellWithReuseIdentifier:[ThumbCollectionCell reuseIdentifier]];
}

#pragma mark - UI

- (void)setDiagnosticTest:(DiagnosticTest*)test
{
	self.diagnosticImages = [test.diagnosticImages allObjects].mutableCopy;
	[self.images reloadData];

	self.typeText.tag = [self.index intValue];
	self.typeText.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.typeText.textField.text = test.name;
	
	self.note.tag = [self.index intValue];
	self.note.delegate = (BaseViewController*)self.cellDelegate;
	self.note.text = test.note;
}

#pragma mark - Actions

- (IBAction)remove:(id)sender
{
	if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(doActionForKey:actionData:)])
		[self.cellDelegate doActionForKey:@"deleteTest" actionData:@{@"index": self.index}];
}

- (IBAction)addPhoto:(id)sender
{
	if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(doActionForKey:actionData:)])
		[self.cellDelegate doActionForKey:@"addPhoto" actionData:@{@"index": self.index}];
}

#pragma mark - UICollectionViewDataSource

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
	return CGSizeMake(90, 90);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
	return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
	return 10;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
	return 10;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
	return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return [self.diagnosticImages count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	ThumbCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[ThumbCollectionCell reuseIdentifier] forIndexPath:indexPath];
	cell.index = [NSNumber numberWithInt:(int)indexPath.row];
	DiagnosticImage *diagnosticImage = self.diagnosticImages[indexPath.row];
	[cell setThumbWithData:diagnosticImage.image];
	
	return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(doActionForKey:actionData:)])
		[self.cellDelegate doActionForKey:@"selectGallery" actionData:@{@"index": self.index, @"photoIndex": indexPath}];
}


@end
