//
//  FatherHistoryCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface AllergyCell : BaseTableViewCell

@property (nonatomic, weak) IBOutlet PickerField *type;
@property (nonatomic, weak) IBOutlet CustomTextField *note;

@end
