//
//  ContactEmailCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "ContactEmailCell.h"
#import "ContactEmail.h"
#import "BaseViewController.h"

@implementation ContactEmailCell

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.contactTypeText.accessibilityIdentifier = [self reuseIdentifier];
	self.contactText.accessibilityIdentifier = [self reuseIdentifier];
}

- (void)setContent:(id)obj
{
	[super setContent:obj];
	
	ContactEmail *contact = (ContactEmail*)obj;
	
	self.contactTypeText.tag = [self.index intValue];
	self.contactTypeText.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.contactTypeText.textField.text = contact.type;
	
	self.contactText.tag = [self.index intValue];
	self.contactText.delegate = (BaseViewController*)self.cellDelegate;
	self.contactText.text = contact.text;
}

- (IBAction)remove:(id)sender
{
	if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(doActionForKey:actionData:)])
		[self.cellDelegate doActionForKey:@"deleteEmail" actionData:@{@"index": self.index}];
}


@end
