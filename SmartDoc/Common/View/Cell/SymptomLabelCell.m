//
//  ContactPhoneCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "SymptomLabelCell.h"
#import "SymptomHistory.h"
#import "BaseViewController.h"

@implementation SymptomLabelCell

- (void)setContent:(id)obj
{
	SymptomHistory *symptomHistory = (SymptomHistory*)obj;
	
	self.symptom.text = symptomHistory.name;
	self.details.text = symptomHistory.details;
	self.note.text = symptomHistory.note;
}

- (void)setSymptomVisit:(Symptom*)symptom
{
	self.symptom.text = symptom.name;
	self.details.text = symptom.details;
	self.note.text = symptom.note;
}

@end
