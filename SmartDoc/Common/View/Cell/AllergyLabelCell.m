//
//  ContactPhoneCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "AllergyLabelCell.h"
#import "Allergy.h"

@implementation AllergyLabelCell

- (void)setContent:(id)obj
{
	Allergy *allergy = (Allergy*)obj;
	
	self.type.text = allergy.name;
	self.note.text = allergy.note;
}

@end
