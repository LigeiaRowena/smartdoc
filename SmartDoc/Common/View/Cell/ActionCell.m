//
//  ContactEmailCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "ActionCell.h"
#import "Action.h"
#import "BaseViewController.h"

@implementation ActionCell

- (void)awakeFromNib
{
	[super awakeFromNib];
}

- (void)setContent:(id)obj
{
	[super setContent:obj];
	
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
	[formatter setDateFormat:@"dd/MM/yyyy"];

	
	self.action = (Action*)obj;
	
	if ([self.action.type isEqualToString:ActionAppointment])
		self.type.image = [UIImage imageNamed:@"Icon_todoappointment.png"];
	else if ([self.action.type isEqualToString:ActionNote])
		self.type.image = [UIImage imageNamed:@"Icon_todonote.png"];
	else if ([self.action.type isEqualToString:ActionTodo])
		self.type.image = [UIImage imageNamed:@"Icon_todoalert.png"];
	
	if (![BaseViewController isEmptyString:self.action.dateString])
		self.dateLabel.text = [self.action.dateString uppercaseString];
	else
		self.dateLabel.text = [formatter stringFromDate:self.action.date];
	
	if ([self.action.type isEqualToString:ActionNote])
		[self.descLabel fitText:self.action.desc];
	else
		[self.descLabel fitText:[self.action.desc uppercaseString]];
}

- (IBAction)remove:(id)sender
{
	if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(doActionForKey:actionData:)])
		[self.cellDelegate doActionForKey:@"deleteAction" actionData:@{@"index": self.index, @"action": self.action}];
}

- (IBAction)edit:(id)sender
{
	if ([self.action.type isEqualToString:ActionNote])
	{
		if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(doActionForKey:actionData:)])
			[self.cellDelegate doActionForKey:@"editNoteAction" actionData:@{@"index": self.index}];
	}
	else
	{
		if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(doActionForKey:actionData:)])
			[self.cellDelegate doActionForKey:@"editAction" actionData:@{@"index": self.index}];
	}
}



@end
