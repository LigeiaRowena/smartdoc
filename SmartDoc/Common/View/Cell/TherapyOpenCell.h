//
//  ContactPhoneCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "ToggleView.h"

@interface TherapyOpenCell : BaseTableViewCell <ToggleViewDelegate>

@property (nonatomic, weak) IBOutlet UIView *bg;
@property (nonatomic, weak) IBOutlet ToggleView *switchSuspend;
@property (nonatomic, weak) IBOutlet PickerField *drugName;
@property (nonatomic, weak) IBOutlet CustomTextField *drugDosageText;
@property (nonatomic, weak) IBOutlet PickerField *drugUnitText;
@property (nonatomic, weak) IBOutlet PickerField *drugFrequencyText;
@property (nonatomic, weak) IBOutlet PickerField *drugRouteText;

@property (nonatomic, strong) NSNumber *section;

@end
