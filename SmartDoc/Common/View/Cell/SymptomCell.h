//
//  FatherHistoryCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "Symptom.h"

@interface SymptomCell : BaseTableViewCell

@property (nonatomic, weak) IBOutlet PickerField *typeText;
@property (nonatomic, weak) IBOutlet PickerField *detail;
@property (nonatomic, weak) IBOutlet CustomTextField *note;

- (void)setSymptomVisit:(Symptom*)symptom;

@end
