//
//  ContactEmailCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface ContactEmailCell : BaseTableViewCell

@property (nonatomic, weak) IBOutlet PickerField *contactTypeText;
@property (nonatomic, weak) IBOutlet CustomTextField *contactText;

@end
