//
//  PatientCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 04/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "PatientCell.h"
#import "NSString+Utility.h"
#import "Patient.h"
#import "PatientData.h"
#import "Dashboard.h"
#import "Action.h"

@implementation PatientCell

- (void)awakeFromNib
{
	[super awakeFromNib];
}

- (void)setContent:(id)obj
{
	[super setContent:obj];
	
	Patient *patient = (Patient*)obj;
	
	// status
	if ([patient.dashboard.active boolValue])
		self.status.image = [UIImage imageNamed:@"Bg_status_green.png"];
	else
		self.status.image = [UIImage imageNamed:@"Bg_status_red.png"];
	 
	// name
	NSString *title = [NSString stringWithFormat:@"%@ %@ %@", patient.patientData.name, patient.patientData.middleName, patient.patientData.lastName];
	if ([title isBlank])
		self.patientName.text = @"";
	else
		self.patientName.text = title;
	
	// actions
	NSArray *actions = [[patient.dashboard actions] allObjects].mutableCopy;
	NSArray *actionsOrdered = [actions sortedArrayUsingComparator: ^(Action *a_1, Action *a_2) {
		NSDate *d_1 = a_1.date;
		NSDate *d_2 = a_2.date;
		return [d_1 compare: d_2];
	}];
	if ([actionsOrdered count] > 0)
	{
		Action *lastAction = [actionsOrdered lastObject];
		self.lastEventIcon.hidden = NO;
		self.lastEvent.hidden = NO;
		NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
		[formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
		[formatter setDateFormat:@"dd/MM/yyyy"];
		if ([lastAction.type isEqualToString:ActionAppointment])
		{
			self.lastEventIcon.image = [UIImage imageNamed:@"Icon_calendar.png"];
			if (lastAction.date != nil)
				self.lastEvent.text = [NSString stringWithFormat:@"LAST APPOINTMENT %@", [formatter stringFromDate:lastAction.date]];
			else
				self.lastEvent.text = @"LAST APPOINTMENT";
		}
		else if ([lastAction.type isEqualToString:ActionTodo])
		{
			self.lastEventIcon.image = [UIImage imageNamed:@"Icon_alert.png"];
			if (![BaseViewController isEmptyString:lastAction.dateString])
				self.lastEvent.text = [NSString stringWithFormat:@"LAST TODO %@", lastAction.dateString];
			else
				self.lastEvent.text = @"LAST TODO";
		}
		else if ([lastAction.type isEqualToString:ActionNote])
		{
			self.lastEventIcon.image = [UIImage imageNamed:@"Icon_note.png"];
			if (lastAction.date != nil)
				self.lastEvent.text = [NSString stringWithFormat:@"LAST NOTE %@", [formatter stringFromDate:lastAction.date]];
			else
				self.lastEvent.text = @"LAST NOTE";
		}
		
	}
	else
	{
		self.lastEventIcon.hidden = YES;
		self.lastEvent.hidden = YES;
	}
}


@end
