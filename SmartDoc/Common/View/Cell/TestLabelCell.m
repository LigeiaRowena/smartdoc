//
//  ContactPhoneCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "TestLabelCell.h"
#import "DiagnosticTest.h"

@implementation TestLabelCell

- (void)awakeFromNib
{
	[super awakeFromNib];
}

- (void)setContent:(id)obj
{
	DiagnosticTest *test = (DiagnosticTest*)obj;
	
	self.type.text = test.name;
	self.note.text = test.note;
}


@end
