//
//  ContactPhoneCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "ContactPhoneCell.h"
#import "ContactPhone.h"
#import "BaseViewController.h"

@implementation ContactPhoneCell

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.contactTypeText.accessibilityIdentifier = [self reuseIdentifier];
	self.contactText.accessibilityIdentifier = [self reuseIdentifier];
}

- (void)setContent:(id)obj
{
	[super setContent:obj];
	
	ContactPhone *contact = (ContactPhone*)obj;
	
	self.contactTypeText.tag = [self.index intValue];
	self.contactTypeText.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.contactTypeText.textField.text = contact.type;
	
	self.contactText.tag = [self.index intValue];
	self.contactText.delegate = (BaseViewController*)self.cellDelegate;
	self.contactText.text = contact.text;
}

- (IBAction)remove:(id)sender
{
	if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(doActionForKey:actionData:)])
		[self.cellDelegate doActionForKey:@"deletePhone" actionData:@{@"index": self.index}];
}

@end
