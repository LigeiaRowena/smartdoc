//
//  FatherHistoryCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface VaccinationCell : BaseTableViewCell

@property (nonatomic, weak) IBOutlet PickerField *typeText;
@property (nonatomic, weak) IBOutlet PickerField *dateText;
@property (nonatomic, weak) IBOutlet CustomTextField *text;

@end
