//
//  FatherHistoryCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "OperationCell.h"
#import "Operation.h"
#import "BaseViewController.h"

@implementation OperationCell

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.operation.accessibilityIdentifier = [self reuseIdentifier];
	self.note.accessibilityIdentifier = [self reuseIdentifier];
}


- (void)setContent:(id)obj
{
	[super setContent:obj];
	
	Operation *op = (Operation*)obj;
	
	self.operation.tag = [self.index intValue];
	self.operation.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.operation.textField.text = op.name;
	
	self.note.tag = [self.index intValue];
	self.note.delegate = (BaseViewController*)self.cellDelegate;
	self.note.text = op.note;
}

- (IBAction)remove:(id)sender
{
	if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(doActionForKey:actionData:)])
		[self.cellDelegate doActionForKey:@"deleteOperation" actionData:@{@"index": self.index}];
}

@end
