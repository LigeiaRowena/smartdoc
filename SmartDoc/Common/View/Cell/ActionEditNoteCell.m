//
//  ContactEmailCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "ActionEditNoteCell.h"
#import "Action.h"
#import "BaseViewController.h"

@implementation ActionEditNoteCell

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.typetext.accessibilityIdentifier = [self reuseIdentifier];
}

- (void)setContent:(id)obj
{
	[super setContent:obj];
	
	self.action = (Action*)obj;
	
	self.type.image = [UIImage imageNamed:@"Icon_todonote.png"];
	self.typetext.tag = [self.index intValue];
	self.typetext.textviewDelegate = (BaseViewController*)self.cellDelegate;
	self.typetext.textView.text = self.action.desc;
}

- (IBAction)save:(id)sender
{
	self.action.desc = self.typetext.textView.text;
	[[DBManager getIstance] saveContext];

	if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(doActionForKey:actionData:)])
		[self.cellDelegate doActionForKey:@"saveActionNote" actionData:@{}];
}



@end
