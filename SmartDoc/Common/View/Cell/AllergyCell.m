//
//  FatherHistoryCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "AllergyCell.h"
#import "Allergy.h"
#import "BaseViewController.h"

@implementation AllergyCell

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.type.accessibilityIdentifier = [self reuseIdentifier];
	self.note.accessibilityIdentifier = [self reuseIdentifier];
}


- (void)setContent:(id)obj
{
	[super setContent:obj];
	
	Allergy *allergy = (Allergy*)obj;
	
	self.type.tag = [self.index intValue];
	self.type.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.type.textField.text = allergy.name;
	
	self.note.tag = [self.index intValue];
	self.note.delegate = (BaseViewController*)self.cellDelegate;
	self.note.text = allergy.note;
}

- (IBAction)remove:(id)sender
{
	if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(doActionForKey:actionData:)])
		[self.cellDelegate doActionForKey:@"deleteAllergy" actionData:@{@"index": self.index}];
}

@end
