//
//  FatherHistoryCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface FatherHistoryLabelCell : BaseTableViewCell

@property (nonatomic, weak) IBOutlet UILabel *type;
@property (nonatomic, weak) IBOutlet UILabel *note;


@end
