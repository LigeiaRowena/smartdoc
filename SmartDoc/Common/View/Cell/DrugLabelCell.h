//
//  ContactPhoneCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface DrugLabelCell : BaseTableViewCell

@property (nonatomic, weak) IBOutlet UILabel *name;
@property (nonatomic, weak) IBOutlet UILabel *dosage;
@property (nonatomic, weak) IBOutlet UILabel *unit;
@property (nonatomic, weak) IBOutlet UILabel *frequency;
@property (nonatomic, weak) IBOutlet UILabel *route;

@end
