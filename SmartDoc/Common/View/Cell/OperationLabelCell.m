//
//  ContactPhoneCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "OperationLabelCell.h"
#import "Operation.h"

@implementation OperationLabelCell

- (void)setContent:(id)obj
{
	Operation *operation = (Operation*)obj;
	
	self.type.text = operation.name;
	self.note.text = operation.note;
}

@end
