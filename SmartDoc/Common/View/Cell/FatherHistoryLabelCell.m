//
//  FatherHistoryCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "FatherHistoryLabelCell.h"
#import "FatherDiseaseHistory.h"

@implementation FatherHistoryLabelCell


- (void)setContent:(id)obj
{
	[super setContent:obj];
	
	FatherDiseaseHistory *fatherDiseaseHistory = (FatherDiseaseHistory*)obj;
	
	self.type.text = fatherDiseaseHistory.type;
	self.note.text = fatherDiseaseHistory.note;
}


@end
