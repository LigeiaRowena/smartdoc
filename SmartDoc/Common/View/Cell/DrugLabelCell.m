//
//  ContactPhoneCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "DrugLabelCell.h"
#import "PrescribedTherapy.h"
#import "BaseViewController.h"

@implementation DrugLabelCell

- (void)setContent:(id)obj
{
	PrescribedTherapy *therapy = (PrescribedTherapy*)obj;
	
	self.name.text = therapy.drugName;
	self.dosage.text = therapy.dosage;
	self.unit.text = therapy.dosageUnit;
	self.frequency.text = therapy.frequency;
	self.route.text = therapy.route;
}


@end
