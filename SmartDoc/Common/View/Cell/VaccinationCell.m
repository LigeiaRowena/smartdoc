//
//  FatherHistoryCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "VaccinationCell.h"
#import "Vaccination.h"
#import "BaseViewController.h"

@implementation VaccinationCell

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.typeText.accessibilityIdentifier = [self reuseIdentifier];
	self.dateText.accessibilityIdentifier = [self reuseIdentifier];
	self.text.accessibilityIdentifier = [self reuseIdentifier];
}


- (void)setContent:(id)obj
{
	[super setContent:obj];
	
	Vaccination *vaccination = (Vaccination*)obj;
	
	self.typeText.tag = [self.index intValue];
	self.typeText.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.typeText.textField.text = vaccination.type;
	
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
	[formatter setDateFormat:@"dd/MM/yyyy"];
	self.dateText.tag = [self.index intValue];
	self.dateText.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.dateText.textField.text = [formatter stringFromDate:vaccination.date];
	
	self.text.tag = [self.index intValue];
	self.text.delegate = (BaseViewController*)self.cellDelegate;
	self.text.text = vaccination.note;
}

- (IBAction)remove:(id)sender
{
	if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(doActionForKey:actionData:)])
		[self.cellDelegate doActionForKey:@"deleteVaccination" actionData:@{@"index": self.index}];
}

@end
