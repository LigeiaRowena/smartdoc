//
//  ContactPhoneCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "TherapyOpenCell.h"
#import "PrescribedTherapy.h"
#import "BaseViewController.h"

@implementation TherapyOpenCell

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.switchSuspend.accessibilityIdentifier = [self reuseIdentifier];
	self.drugName.accessibilityIdentifier = [self reuseIdentifier];
	self.drugDosageText.accessibilityIdentifier = [self reuseIdentifier];
	self.drugUnitText.accessibilityIdentifier = [self reuseIdentifier];
	self.drugFrequencyText.accessibilityIdentifier = [self reuseIdentifier];
	self.drugRouteText.accessibilityIdentifier = [self reuseIdentifier];
	
	[self.switchSuspend commotInitWithFrame:self.switchSuspend.frame toggleBaseType:ToggleBaseTypeChangeImage toggleButtonType:ToggleButtonTypeDefault];
}

- (void)setContent:(id)obj
{
	[super setContent:obj];
	
	PrescribedTherapy *therapy = (PrescribedTherapy*)obj;
	self.switchSuspend.toggleDelegate = self;
	
	if ([therapy.suspended boolValue])
	{
		self.switchSuspend.selectedButton = ToggleButtonSelectedLeft;
		self.bg.backgroundColor = normalRedColor();
	}
	else if ([therapy.active boolValue])
	{
		self.switchSuspend.selectedButton = ToggleButtonSelectedRight;
		self.bg.backgroundColor = lightBlueColor();
	}
	else if ([therapy.prescribed boolValue])
	{
		self.switchSuspend.selectedButton = ToggleButtonSelectedRight;
		self.bg.backgroundColor = normalGreenColor();
	}
	
	self.drugName.tag = [self.index intValue];
	self.drugName.section = [self.section intValue];
	self.drugName.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.drugName.textField.text = therapy.drugName;

	self.drugDosageText.tag = [self.index intValue];
	self.drugDosageText.section = [self.section intValue];
	self.drugDosageText.delegate = (BaseViewController*)self.cellDelegate;
	self.drugDosageText.text = therapy.dosage;
	
	self.drugUnitText.tag = [self.index intValue];
	self.drugUnitText.section = [self.section intValue];
	self.drugUnitText.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.drugUnitText.textField.text = therapy.dosageUnit;
	
	self.drugFrequencyText.tag = [self.index intValue];
	self.drugFrequencyText.section = [self.section intValue];
	self.drugFrequencyText.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.drugFrequencyText.textField.text = therapy.frequency;
	
	self.drugRouteText.tag = [self.index intValue];
	self.drugRouteText.section = [self.section intValue];
	self.drugRouteText.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.drugRouteText.textField.text = therapy.route;
}

- (IBAction)remove:(id)sender
{
	if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(doActionForKey:actionData:)])
		[self.cellDelegate doActionForKey:@"deleteTherapy" actionData:@{@"index": self.index, @"section": self.section}];
}

- (void)selectToggle:(BOOL)on
{
	if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(doActionForKey:actionData:)])
		[self.cellDelegate doActionForKey:@"changeStatusTherapy" actionData:@{@"index": self.index, @"section": self.section}];
}

@end
