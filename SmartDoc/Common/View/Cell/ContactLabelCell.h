//
//  ContactPhoneCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface ContactLabelCell : BaseTableViewCell

@property (nonatomic, weak) IBOutlet UILabel *contactType;
@property (nonatomic, weak) IBOutlet UILabel *contact;

- (void)setContactPhone:(id)obj;
- (void)setContactEmail:(id)obj;

@end
