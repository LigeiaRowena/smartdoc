//
//  ContactPhoneCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface TherapyCell : BaseTableViewCell <CellDelegate>

@property (nonatomic, weak) IBOutlet UIImageView *status;
@property (nonatomic, weak) IBOutlet UILabel *statusLabel;
@property (nonatomic, weak) IBOutlet UILabel *startDate;
@property (nonatomic, weak) IBOutlet UILabel *abstract;

@end
