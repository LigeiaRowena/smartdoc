//
//  ContactPhoneCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "Symptom.h"

@interface SymptomLabelCell : BaseTableViewCell

@property (nonatomic, weak) IBOutlet UILabel *symptom;
@property (nonatomic, weak) IBOutlet UILabel *details;
@property (nonatomic, weak) IBOutlet UILabel *note;

- (void)setSymptomVisit:(Symptom*)symptom;

@end
