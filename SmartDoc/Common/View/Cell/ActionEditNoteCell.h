//
//  ContactEmailCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface ActionEditNoteCell : BaseTableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *type;
@property (nonatomic, weak) IBOutlet CustomTextView *typetext;

@property (nonatomic, strong) Action *action;
@end
