//
//  ContactEmailCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "VisitCell.h"
#import "Visit.h"
#import "BaseViewController.h"

@implementation VisitCell

- (void)awakeFromNib
{
	[super awakeFromNib];
}

- (void)setVisit:(id)obj
{
	[super setContent:obj];
	
	Visit *visit = (Visit*)obj;
	
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
	[formatter setDateFormat:@"dd/MM/yyyy"];
	self.dateLabel.text = [formatter stringFromDate:visit.date];
	self.dateLabel.textColor = lightGrayBlueColor();
	self.dateLabel.alpha = 1;
	self.descLabel.text = [visit.type uppercaseString];
	self.thumb.image = [UIImage imageNamed:@"Icon_visittypeappointment.png"];
}

- (void)setFirstVisit:(NSDate*)date
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
	[formatter setDateFormat:@"dd/MM/yyyy"];
	self.dateLabel.text = [formatter stringFromDate:date];
	self.dateLabel.textColor = [UIColor redColor];
	self.dateLabel.alpha = 0.75;
	self.descLabel.text = [@"FIRST VISIT, REMOTE ANAMNESIS" uppercaseString];
	self.thumb.image = [UIImage imageNamed:@"Icon_firstvisit.png"];
}


@end
