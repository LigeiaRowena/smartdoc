//
//  FatherHistoryCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "MotherHistoryLabelCell.h"
#import "MotherDiseaseHistory.h"

@implementation MotherHistoryLabelCell


- (void)setContent:(id)obj
{
	[super setContent:obj];
	
	MotherDiseaseHistory *motherDiseaseHistory = (MotherDiseaseHistory*)obj;
	
	self.type.text = motherDiseaseHistory.type;
	self.note.text = motherDiseaseHistory.note;
}


@end
