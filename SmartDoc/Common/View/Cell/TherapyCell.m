//
//  ContactPhoneCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "TherapyCell.h"
#import "PrescribedTherapy.h"
#import "BaseViewController.h"

@implementation TherapyCell

- (void)awakeFromNib
{
	[super awakeFromNib];
}

- (void)setContent:(id)obj
{
	[super setContent:obj];
	
	PrescribedTherapy *therapy = (PrescribedTherapy*)obj;
	
	if ([therapy.suspended boolValue])
	{
		self.statusLabel.text = @"SUSPENDED";
		self.statusLabel.textColor = normalRedColor();
		self.status.image = [UIImage imageNamed:@"Icon_suspendedtherapy.png"];
	}
	else if ([therapy.active boolValue])
	{
		self.statusLabel.text = @"ACTIVE";
		self.statusLabel.textColor = lightBlueColor();
		self.status.image = [UIImage imageNamed:@"Icon_activetherapy.png"];
	}
	else if ([therapy.prescribed boolValue])
	{
		self.statusLabel.text = @"ACTIVE";
		self.statusLabel.textColor = normalGreenColor();
		self.status.image = [UIImage imageNamed:@"Icon_previoustherapy.png"];
	}

	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
	[formatter setDateFormat:@"dd/MM/yyyy"];
	self.startDate.text = [formatter stringFromDate:therapy.date];
	
	NSString *name = [NSString stringWithFormat:@"%@, %@ %@, %@", therapy.drugName, therapy.dosage, therapy.dosageUnit, therapy.frequency];
	self.abstract.text = name;
}


@end
