//
//  FatherHistoryCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "FatherHistoryCell.h"
#import "FatherDiseaseHistory.h"
#import "BaseViewController.h"

@implementation FatherHistoryCell

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.fatherTypeText.accessibilityIdentifier = [self reuseIdentifier];
	self.fatherText.accessibilityIdentifier = [self reuseIdentifier];
}


- (void)setContent:(id)obj
{
	[super setContent:obj];
	
	FatherDiseaseHistory *father = (FatherDiseaseHistory*)obj;
	
	self.fatherTypeText.tag = [self.index intValue];
	self.fatherTypeText.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.fatherTypeText.textField.text = father.type;
	
	self.fatherText.tag = [self.index intValue];
	self.fatherText.delegate = (BaseViewController*)self.cellDelegate;
	self.fatherText.text = father.note;
}

- (IBAction)remove:(id)sender
{
	if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(doActionForKey:actionData:)])
		[self.cellDelegate doActionForKey:@"deleteFatherRisk" actionData:@{@"index": self.index}];
}

@end
