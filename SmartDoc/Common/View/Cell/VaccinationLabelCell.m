//
//  ContactPhoneCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "VaccinationLabelCell.h"
#import "Vaccination.h"

@implementation VaccinationLabelCell

- (void)setContent:(id)obj
{
	Vaccination *vaccination = (Vaccination*)obj;
	
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
	[formatter setDateFormat:@"dd/MM/yyyy"];

	self.type.text = vaccination.type;
	self.date.text = [formatter stringFromDate:vaccination.date];
	self.note.text = vaccination.note;
}

@end
