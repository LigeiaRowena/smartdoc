//
//  ContactPhoneCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "ContactLabelCell.h"
#import "ContactPhone.h"
#import "ContactEmail.h"
#import "BaseViewController.h"

@implementation ContactLabelCell

- (void)setContactPhone:(id)obj
{
	ContactPhone *contact = (ContactPhone*)obj;
	
	self.contactType.text = contact.type;
	self.contact.text = contact.text;
}

- (void)setContactEmail:(id)obj
{
	ContactEmail *contact = (ContactEmail*)obj;

	self.contactType.text = contact.type;
	self.contact.text = contact.text;
}


@end
