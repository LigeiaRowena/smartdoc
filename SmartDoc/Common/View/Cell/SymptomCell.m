//
//  FatherHistoryCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "SymptomCell.h"
#import "SymptomHistory.h"
#import "BaseViewController.h"

@implementation SymptomCell

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.typeText.accessibilityIdentifier = [self reuseIdentifier];
	self.typeText.fieldType = SymptomTypeField;
	self.detail.accessibilityIdentifier = [self reuseIdentifier];
	self.detail.fieldType = SymptomDetailsField;
	self.note.accessibilityIdentifier = [self reuseIdentifier];
	self.note.type = SymptomNoteField;
}


- (void)setContent:(id)obj
{
	[super setContent:obj];
	
	SymptomHistory *symptomHistory = (SymptomHistory*)obj;
	
	self.typeText.tag = [self.index intValue];
	self.typeText.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.typeText.textField.text = symptomHistory.name;
	
	self.detail.tag = [self.index intValue];
	self.detail.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.detail.textField.text = symptomHistory.details;
	
	self.note.tag = [self.index intValue];
	self.note.delegate = (BaseViewController*)self.cellDelegate;
	self.note.text = symptomHistory.note;
}

- (void)setSymptomVisit:(Symptom*)symptom
{
	self.typeText.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.typeText.textField.text = symptom.name;
	
	self.detail.tag = [self.index intValue];
	self.detail.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.detail.textField.text = symptom.details;
	
	self.note.tag = [self.index intValue];
	self.note.delegate = (BaseViewController*)self.cellDelegate;
	self.note.text = symptom.note;
}

- (IBAction)remove:(id)sender
{
	if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(doActionForKey:actionData:)])
		[self.cellDelegate doActionForKey:@"deleteSymptom" actionData:@{@"index": self.index}];
}

@end
