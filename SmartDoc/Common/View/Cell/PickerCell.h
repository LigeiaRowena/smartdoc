//
//  PickerCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 20/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface PickerCell : BaseTableViewCell

@property (nonatomic, weak) IBOutlet UILabel *name;
@property (nonatomic, weak) IBOutlet UIImageView *status;

@end
