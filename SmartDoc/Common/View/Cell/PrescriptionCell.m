//
//  ContactPhoneCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "PrescriptionCell.h"
#import "Prescription.h"
#import "BaseViewController.h"

@implementation PrescriptionCell

- (void)awakeFromNib
{
	[super awakeFromNib];
}

- (void)setContent:(id)obj
{
	[super setContent:obj];
	
	Prescription *prescription = (Prescription*)obj;
	
	self.numberLabel.text = [NSString stringWithFormat:@"%i", [self.index intValue] + 1];

	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
	[formatter setDateFormat:@"dd/MM/yyyy"];
	self.startDate.text = [formatter stringFromDate:prescription.startDate];
	
	self.abstract.text = prescription.title;
}


@end
