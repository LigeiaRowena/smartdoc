//
//  FatherHistoryCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "ThumbCollectionCell.h"

@interface TestCell : BaseTableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) IBOutlet PickerField *typeText;
@property (nonatomic, weak) IBOutlet CustomTextField *note;
@property (nonatomic, weak) IBOutlet PickerField *dateText;
@property (nonatomic, weak) IBOutlet UICollectionView *images;
@property (nonatomic, strong) NSMutableArray* diagnosticImages;

@end
