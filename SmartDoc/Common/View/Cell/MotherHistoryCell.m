//
//  FatherHistoryCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "MotherHistoryCell.h"
#import "MotherDiseaseHistory.h"
#import "BaseViewController.h"

@implementation MotherHistoryCell

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.fatherTypeText.accessibilityIdentifier = [self reuseIdentifier];
	self.fatherText.accessibilityIdentifier = [self reuseIdentifier];
}


- (void)setContent:(id)obj
{
	[super setContent:obj];
	
	MotherDiseaseHistory *mother = (MotherDiseaseHistory*)obj;
	
	self.fatherTypeText.tag = [self.index intValue];
	self.fatherTypeText.pickerDelegate = (BaseViewController*)self.cellDelegate;
	self.fatherTypeText.textField.text = mother.type;
	
	self.fatherText.tag = [self.index intValue];
	self.fatherText.delegate = (BaseViewController*)self.cellDelegate;
	self.fatherText.text = mother.note;
}

- (IBAction)remove:(id)sender
{
	if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(doActionForKey:actionData:)])
		[self.cellDelegate doActionForKey:@"deleteMotherRisk" actionData:@{@"index": self.index}];
}

@end
