//
//  PatientCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 04/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface PatientCell : BaseTableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *status;
@property (nonatomic, weak) IBOutlet UILabel *patientName;
@property (nonatomic, weak) IBOutlet UILabel *lastEvent;
@property (nonatomic, weak) IBOutlet UIImageView *lastEventIcon;

@end
