//
//  MenuView.m
//  SmartDoc
//
//  Created by Francesca Corsini on 20/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "MenuView.h"

@implementation MenuView

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{
		self.items = @[].mutableCopy;
		self.index = 0;
		
		for (UIView *view in self.subviews)
		{
			if ([view isKindOfClass:[UIButton class]])
				[self.items addObject:(UIButton*)view];
		}
	}
	return self;
}

- (void)selectItemAtIndex:(int)index
{
	self.index = index;
	UIButton *button = self.items[index];
	[self selectItem:button];
}

- (IBAction)selectItem:(id)sender
{
	UIButton *buttonSelected = (UIButton*)sender;
	for (UIButton *button in self.items)
		[button setSelected:NO];
	[buttonSelected setSelected:YES];
	self.itemSelected = buttonSelected;
	self.index = (int)[self.items indexOfObject:buttonSelected];
	
	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(menuDidSelectItemAtIndex:)])
		[self.delegate menuDidSelectItemAtIndex:self.index];
}


@end
