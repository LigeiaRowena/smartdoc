//
//  UILabel+Fit.m
//  RadioRai
//
//  Created by Francesca Corsini on 10/03/14.
//
//

#import "UILabel+Fit.h"

@implementation FitLabel : UILabel

@end

@implementation FitLabel (Fit)

- (void)setRoundedLabel
{
	self.backgroundColor = [UIColor colorWithRed:(202.0f/255.0f) green:(203.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f];
	self.layer.backgroundColor = [UIColor colorWithRed:(202.0f/255.0f) green:(203.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f].CGColor;
	self.layer.borderColor = [UIColor clearColor].CGColor;
	self.layer.borderWidth = 0.0f;
	self.layer.cornerRadius = 5.0f;
}

- (void)setNormalLabel
{
	self.backgroundColor = [UIColor clearColor];
	self.layer.backgroundColor = [UIColor clearColor].CGColor;
	self.layer.borderColor = [UIColor clearColor].CGColor;
	self.layer.borderWidth = 0.0f;
	self.layer.cornerRadius = 0.0f;
}


- (void)fitText:(NSString*)text
{
	if (self.height == nil)
		self.height = [NSNumber numberWithFloat:self.frame.size.height];
	
    if (text)
        self.text = text;
    else
        self.text = @"";
    
	self.numberOfLines = 0;
	self.frame = [self getLabelRect];
	
	if (self.roundedLabel)
	{
		if (text == nil || [[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] || [text isEqual:[NSNull null]])
		{
			self.frame = (CGRect){self.frame.origin, self.frame.size.width, [self.height floatValue]};
			self.text = @"-";
			//[self setRoundedLabel];
		}
		else
			[self setNormalLabel];
	}
}

- (CGRect)getLabelRect
{
	CGRect rect = CGRectZero;
	NSDictionary *attributes = @{NSFontAttributeName: self.font};
	CGSize size = [self.text boundingRectWithSize:CGSizeMake(self.frame.size.width, [self.height floatValue]) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
	rect = (CGRect){self.frame.origin, self.frame.size.width, size.height};
	return rect;
}

@end



