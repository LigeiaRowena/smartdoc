//
//  RoundedLabel.m
//  SmartDoc
//
//  Created by Francesca Corsini on 13/10/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "RoundedLabel.h"

@implementation RoundedLabel

- (void)setRoundedLabel
{
	self.backgroundColor = [UIColor colorWithRed:(202.0f/255.0f) green:(203.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f];
	self.layer.backgroundColor = [UIColor colorWithRed:(202.0f/255.0f) green:(203.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f].CGColor;
	self.layer.borderColor = [UIColor clearColor].CGColor;
	self.layer.borderWidth = 0.0f;
	self.layer.cornerRadius = 5.0f;
}

- (void)setNormalLabel
{
	self.backgroundColor = [UIColor clearColor];
	self.layer.backgroundColor = [UIColor clearColor].CGColor;
	self.layer.borderColor = [UIColor clearColor].CGColor;
	self.layer.borderWidth = 0.0f;
	self.layer.cornerRadius = 0.0f;
}

- (void)setRoundedText:(NSString*)text
{
	if (text == nil || [[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] || [text isEqual:[NSNull null]])
	{
		//[self setRoundedLabel];
		self.text = @"-";
	}
	else
	{
		[self setNormalLabel];
		self.text = text;
	}
}

@end
