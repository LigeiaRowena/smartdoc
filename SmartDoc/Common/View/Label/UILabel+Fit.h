//
//  UILabel+Fit.h
//  RadioRai
//
//  Created by Francesca Corsini on 10/03/14.
//
//

#import <UIKit/UIKit.h>

@class FitLabel;

@interface FitLabel : UILabel
@property (nonatomic, strong) NSNumber *height;
@property (nonatomic) BOOL roundedLabel;
@end

@interface FitLabel (Fit)
- (void)fitText:(NSString*)text;
- (void)setRoundedLabel;
@end

