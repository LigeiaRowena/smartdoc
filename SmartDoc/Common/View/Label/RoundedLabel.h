//
//  RoundedLabel.h
//  SmartDoc
//
//  Created by Francesca Corsini on 13/10/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoundedLabel : UILabel

- (void)setRoundedLabel;
- (void)setRoundedText:(NSString*)text;

@end
