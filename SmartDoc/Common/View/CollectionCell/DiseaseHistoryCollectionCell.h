//
//  DiseaseHistoryCollectionCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseCollectionViewCell.h"
#import "DiseaseHistory.h"
#import "OtherRiskFactor.h"

@interface DiseaseHistoryCollectionCell : BaseCollectionViewCell <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UILabel *name;
@property (nonatomic, weak) IBOutlet UIButton *status;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;

- (void)setDiseaseHistory:(DiseaseHistory*)diseaseHistory;
- (void)setDiseaseHistoryNotEditable:(DiseaseHistory*)diseaseHistory;
- (void)selOtherRiskFactor:(OtherRiskFactor*)otherRiskFactor;
- (void)selOtherRiskFactorNotEditable:(OtherRiskFactor*)otherRiskFactor;

@end
