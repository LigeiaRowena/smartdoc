//
//  DiseaseHistoryCollectionCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "DiseaseHistoryCollectionCell.h"
#import "BaseViewController.h"


@implementation DiseaseHistoryCollectionCell

- (void)awakeFromNib
{
	[super awakeFromNib];
	self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(action:)];

}

- (void)setContent:(id)obj
{
	[super setContent:obj];
}

- (void)selOtherRiskFactor:(OtherRiskFactor*)otherRiskFactor
{
	self.status.selected = [otherRiskFactor.status boolValue];
	self.name.text = otherRiskFactor.name;
}

- (void)selOtherRiskFactorNotEditable:(OtherRiskFactor*)otherRiskFactor
{
	self.status.selected = [otherRiskFactor.status boolValue];
	self.name.text = otherRiskFactor.name;
	self.name.textColor = darkGrayBlueColor();
	self.name.font = OpenSansBold(17);
	NSDictionary *attributes = @{NSFontAttributeName: OpenSansBold(17)};
	CGSize size = [otherRiskFactor.name boundingRectWithSize:CGSizeMake(160, 40) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
	self.name.frame = CGRectMake(self.name.frame.origin.x, self.name.frame.origin.y, size.width, self.name.frame.size.height);
	self.backgroundColor = [UIColor whiteColor];
}

- (void)setDiseaseHistory:(DiseaseHistory*)diseaseHistory
{
	[self addGestureRecognizer:self.tapGesture];
	
	self.status.selected = [diseaseHistory.status boolValue];
	self.name.text = diseaseHistory.name;
}


- (void)setDiseaseHistoryNotEditable:(DiseaseHistory*)diseaseHistory
{
	self.status.selected = [diseaseHistory.status boolValue];
	self.name.text = diseaseHistory.name;
	self.name.textColor = darkGrayBlueColor();
	self.name.font = OpenSansBold(17);
	
	self.backgroundColor = [UIColor whiteColor];
}

- (IBAction)changeStatus:(id)sender
{
	if (self.cellDelegate != nil)
		self.status.selected = !self.status.selected;
	
	if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(collectionDoActionForKey:actionData:)])
		[self.cellDelegate collectionDoActionForKey:@"changeStatus" actionData:@{@"index": self.index}];
}

- (void)action:(UIGestureRecognizer *)gesture {
	if (self.cellDelegate != nil && [self.cellDelegate respondsToSelector:@selector(collectionDoActionForKey:actionData:)])
		[self.cellDelegate collectionDoActionForKey:@"longPress" actionData:@{@"index": self.index}];
}

@end
