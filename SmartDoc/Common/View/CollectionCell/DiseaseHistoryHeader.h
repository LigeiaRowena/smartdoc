//
//  DiseaseHistoryHeader.h
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseCollectionReusableView.h"

@interface DiseaseHistoryHeader : BaseCollectionReusableView

@property (nonatomic, weak) IBOutlet UILabel *header;

@end
