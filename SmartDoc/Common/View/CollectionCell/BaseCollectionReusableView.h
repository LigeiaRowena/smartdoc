//
//  BaseCollectionReusableView.h
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseCollectionReusableView : UICollectionReusableView

- (void)setContent:(id)obj;
+ (NSString *)reuseIdentifier;

@end
