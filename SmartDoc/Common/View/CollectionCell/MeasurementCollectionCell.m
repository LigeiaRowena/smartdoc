//
//  DiseaseHistoryCollectionCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "MeasurementCollectionCell.h"
#import "BaseViewController.h"
#import "Settings.h"

@implementation MeasurementCollectionCell

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.value.accessibilityIdentifier = [self reuseIdentifier];
}

- (void)setContent:(id)obj
{
	[super setContent:obj];
}

- (void)setContentNotEditable
{
	self.name.textColor = lightBlueColor();
	self.name.font = [UIFont fontWithName:@"OpenSans-Bold" size:16];
	
	self.value.userInteractionEnabled = NO;
	self.value.font = [UIFont fontWithName:@"OpenSans-Bold" size:17];
	self.value.borderStyle = UITextBorderStyleLine;
	self.value.background = nil;
	self.value.textColor = darkGrayBlueColor();
	self.backgroundColor = [UIColor whiteColor];
	
	self.previousVisitLabel.textColor = lightGrayBlueColor();
	self.previousVisitLabel.font = [UIFont fontWithName:@"OpenSans" size:14];
	self.previousVisitLabel.textColor = [UIColor blackColor];
	self.previousVisitLabel.alpha = 0.5f;

	self.previousValue.textColor = lightGrayBlueColor();
	self.previousValue.font = [UIFont fontWithName:@"OpenSans" size:14];
	self.previousValue.textColor = [UIColor blackColor];
	self.previousValue.alpha = 0.5f;
}

- (void)setMeasurement:(Measurement*)measurement previousMeasurements:(NSArray*)previousMeasurements
{
	// Measurement System
	NSString *unit;
	NSDecimalNumber *actualValue;
	Measurement *lastMeasurement;
	if ([previousMeasurements count] > 0)
		lastMeasurement = (Measurement*)previousMeasurements[0];
	else
		lastMeasurement = measurement;
	NSDecimalNumber *prevValue;
	
	if ([[DBManager getIstance] isMetricSystem])
	{
		unit = measurement.unit;
		actualValue = measurement.value;
		prevValue = lastMeasurement.value;
	}
	else
	{
		unit = [[DBManager getIstance] getUnitImperialSystemMeasurement:measurement measurementName:nil];
		actualValue = [[DBManager getIstance] getValueImperialSystemMeasurement:measurement];
		prevValue = [[DBManager getIstance] getValueImperialSystemMeasurement:lastMeasurement];
	}
	
	// nome
	if (![BaseViewController isEmptyString:measurement.unit])
		self.name.text = [[NSString stringWithFormat:@"%@(%@)", measurement.name, unit] uppercaseString];
	else
		self.name.text = [measurement.name uppercaseString];
	
	// valore
	self.value.tag = [self.index intValue];
	self.value.delegate = (BaseViewController*)self.cellDelegate;
	if (![BaseViewController isEmptyNumber:measurement.value])
		self.value.text = [getDecimalFormatter(1) stringFromNumber:actualValue];
	else
		self.value.text = @"";
	self.value.placeholder = unit;
	
	
	// variazione percentuale
	NSString *percentDelta;
	NSDecimalNumber *delta = [measurement.value decimalNumberBySubtracting:lastMeasurement.value];
	if ([measurement.value isEqualToNumber:[NSDecimalNumber zero]] && [lastMeasurement.value isEqualToNumber:[NSDecimalNumber zero]])
		percentDelta = @"";
	else
	{
		if ([delta floatValue] > 0)
			percentDelta = [[NSString stringWithFormat:@"/+%@", [getDecimalFormatter(1) stringFromNumber:delta]] stringByAppendingString:@"%"];
		else if ([delta floatValue] < 0)
			percentDelta = [[NSString stringWithFormat:@"/%@", [getDecimalFormatter(1) stringFromNumber:delta]] stringByAppendingString:@"%"];
		else if ([delta floatValue] == 0)
			percentDelta = @"";
	}
	if (![BaseViewController isEmptyNumber:lastMeasurement.value])
		self.previousValue.text = [[NSString stringWithFormat:@"%@ %@ %@", [getDecimalFormatter(1) stringFromNumber:prevValue], unit, percentDelta] uppercaseString];
	else
		self.previousValue.text = @"";
}


@end
