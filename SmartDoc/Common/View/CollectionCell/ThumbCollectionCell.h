//
//  DiseaseHistoryCollectionCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseCollectionViewCell.h"

@interface ThumbCollectionCell : BaseCollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumb;

- (void)setThumbWithData:(NSData*)data;

@end
