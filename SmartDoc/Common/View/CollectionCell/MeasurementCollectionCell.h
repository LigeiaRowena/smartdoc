//
//  DiseaseHistoryCollectionCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseCollectionViewCell.h"
#import "Measurement.h"

@interface MeasurementCollectionCell : BaseCollectionViewCell

@property (nonatomic, weak) IBOutlet UILabel *name;
@property (nonatomic, weak) IBOutlet CustomTextField *value;
@property (nonatomic, weak) IBOutlet UILabel *previousVisitLabel;
@property (nonatomic, weak) IBOutlet UILabel *previousValue;

- (void)setMeasurement:(Measurement*)measurement previousMeasurements:(NSArray*)previousMeasurements;
- (void)setContentNotEditable;

@end
