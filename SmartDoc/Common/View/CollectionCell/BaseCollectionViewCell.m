//
//  BaseCollectionViewCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseCollectionViewCell.h"

@implementation BaseCollectionViewCell

#pragma mark - Init

- (void)awakeFromNib
{
	[super awakeFromNib];
}

#pragma mark - Identifier

- (NSString *)reuseIdentifier
{
    return [[self class] reuseIdentifier];
}

+ (NSString *)reuseIdentifier
{
    return NSStringFromClass([self class]);
}


#pragma mark - UI cella

- (void)setContent:(id)obj
{
}

@end
