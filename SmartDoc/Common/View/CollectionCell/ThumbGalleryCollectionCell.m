//
//  DiseaseHistoryCollectionCell.m
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "ThumbGalleryCollectionCell.h"

@implementation ThumbGalleryCollectionCell

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.thumb.layer.masksToBounds = YES;
	self.thumb.layer.borderColor = borderGrayColor().CGColor;
	self.thumb.layer.borderWidth = 2.0f;
	self.thumb.layer.cornerRadius = 5.0f;
}

- (void)setSelected:(BOOL)selected
{
	[super setSelected:selected];
	self.selectIcon.hidden = !self.selectIcon.hidden;
}

- (void)setContent:(id)obj
{
	[super setContent:obj];
}

- (void)setThumbWithData:(NSData*)data 
{
	UIImage *image = [UIImage imageWithData:data];
	self.thumb.image = image;
	self.selectIcon.hidden = YES;
}

- (void)setSelectIcon
{
	self.selectIcon.hidden = !self.selectIcon.hidden;
}

@end
