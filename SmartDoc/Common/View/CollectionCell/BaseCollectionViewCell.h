//
//  BaseCollectionViewCell.h
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickerField.h"
#import "CustomTextView.h"
#import "CustomTextField.h"

@protocol CollectionCellDelegate <NSObject>
@optional
- (void)collectionDoActionForKey:(NSString*)actionKey actionData:(id)actionData;
@end

@interface BaseCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, weak) id<CollectionCellDelegate> cellDelegate;
@property (nonatomic, strong) NSNumber *index;

- (void)setContent:(id)obj;
+ (NSString *)reuseIdentifier;

@end
