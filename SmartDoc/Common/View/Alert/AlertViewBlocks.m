//
//  Flickr
//
//  Created by Francesca Corsini on 08/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "AlertViewBlocks.h"

@implementation AlertViewBlocks

#pragma mark - Utility Methods

+ (AlertViewBlocks*)getIstance
{
	static dispatch_once_t onceToken = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&onceToken, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}


- (void)warningConfirmAlertViewWithMessage:(NSString*)message confirmBlock:(AlertViewBlocksConfirm)confirm cancelBlock:(AlertViewBlocksCancel)cancel
{
	// blocks
	confirmBlock = [confirm copy];
	cancelBlock = [cancel copy];
	
	// AlertViewController
	AlertViewController *alertViewController = [[AlertViewController alloc] initWithNibName:@"AlertViewController" bundle:nil];
	
	[self showCustomAlertViewController:alertViewController transitionBlock:^{
		alertViewController.titleLabel.text = @"WARNING";
		alertViewController.messageLabel.text = message;
		alertViewController.delegate = self;
		[alertViewController.cancelButton setImage:[UIImage imageNamed:@"Butt_cancelalert.png"] forState:UIControlStateNormal];
		[alertViewController.actionButton setImage:[UIImage imageNamed:@"Butt_okalert.png"] forState:UIControlStateNormal];
	}];
}

- (void)warningDeleteAlertViewWithMessage:(NSString*)message confirmBlock:(AlertViewBlocksConfirm)confirm cancelBlock:(AlertViewBlocksCancel)cancel
{
	// blocks
	confirmBlock = [confirm copy];
	cancelBlock = [cancel copy];
	
	// AlertViewController
	AlertViewController *alertViewController = [[AlertViewController alloc] initWithNibName:@"AlertViewController" bundle:nil];

	[self showCustomAlertViewController:alertViewController transitionBlock:^{
		alertViewController.titleLabel.text = @"WARNING";
		alertViewController.messageLabel.text = message;
		alertViewController.delegate = self;
		[alertViewController.cancelButton setImage:[UIImage imageNamed:@"Butt_cancelalert.png"] forState:UIControlStateNormal];
		[alertViewController.actionButton setImage:[UIImage imageNamed:@"Butt_deletealert.png"] forState:UIControlStateNormal];
	}];
}

- (void)warningAlertViewWithMessage:(NSString*)message confirmBlock:(AlertViewBlocksConfirm)confirm
{
	// blocks
	confirmBlock = [confirm copy];
	
	// AlertViewController
	AlertViewController *alertViewController = [[AlertViewController alloc] initWithNibName:@"AlertViewController" bundle:nil];
	
	[self showCustomAlertViewController:alertViewController transitionBlock:^{
		[alertViewController hideCancelButton];
		alertViewController.titleLabel.text = @"WARNING";
		alertViewController.messageLabel.text = message;
		alertViewController.delegate = self;
		[alertViewController.actionButton setImage:[UIImage imageNamed:@"Butt_okalert.png"] forState:UIControlStateNormal];
	}];
}

- (void)informationConfirmAlertViewWithmessage:(NSString*)message confirmBlock:(AlertViewBlocksConfirm)confirm cancelBlock:(AlertViewBlocksCancel)cancel
{
	// blocks
	confirmBlock = [confirm copy];
	cancelBlock = [cancel copy];
	
	// AlertViewController
	AlertViewController *alertViewController = [[AlertViewController alloc] initWithNibName:@"AlertViewController" bundle:nil];
	
	[self showCustomAlertViewController:alertViewController transitionBlock:^{
		alertViewController.titleLabel.text = @"INFORMATION";
		alertViewController.messageLabel.text = message;
		alertViewController.delegate = self;
		[alertViewController.cancelButton setImage:[UIImage imageNamed:@"Butt_cancelalert.png"] forState:UIControlStateNormal];
		[alertViewController.actionButton setImage:[UIImage imageNamed:@"Butt_okgreenalert.png"] forState:UIControlStateNormal];
	}];
}

- (void)twoOptionsAlertViewWithmessage:(NSString*)message firstButtonBg:(UIImage*)firstButtonBg secondButtonBg:(UIImage*)secondButtonBg confirmBlock:(AlertViewBlocksConfirm)confirm secondConfirm:(AlertViewBlocksSecondConfirm)secondConfirm
{
	// blocks
	confirmBlock = [confirm copy];
	secondConfirmBlock = [secondConfirm copy];
	
	// AlertViewController
	AlertViewController *alertViewController = [[AlertViewController alloc] initWithNibName:@"AlertViewController" bundle:nil];
	
	[self showCustomAlertViewController:alertViewController transitionBlock:^{
		alertViewController.titleLabel.text = @"INFORMATION";
		alertViewController.messageLabel.text = message;
		alertViewController.delegate = self;
		[alertViewController.actionButton setImage:firstButtonBg forState:UIControlStateNormal];
		[alertViewController.secondActionButton setImage:secondButtonBg forState:UIControlStateNormal];
		[alertViewController.secondActionButton setEnabled: YES];
		[alertViewController.cancelButton setEnabled:NO];
	}];
}

- (void)twoOptionsAlertViewWithmessage:(NSString*)message firstButtonTitle:(NSString *)firstButtonTitle secondButtonTitle:(NSString *)secondButtonTitle confirmBlock:(AlertViewBlocksConfirm)confirm secondConfirm:(AlertViewBlocksSecondConfirm)secondConfirm cancelBlock:(AlertViewBlocksCancel)cancel
{
	// blocks
	confirmBlock = [confirm copy];
	secondConfirmBlock = [secondConfirm copy];
	cancelBlock = [cancelBlock copy];
	
	// BigAlertViewController
	BigAlertViewController *alertViewController = [[BigAlertViewController alloc] initWithNibName:@"BigAlertViewController" bundle:nil];
	
	[self showBigCustomAlertViewController:alertViewController transitionBlock:^{
		alertViewController.titleLabel.text = @"INFORMATION";
		alertViewController.messageLabel.text = message;
		alertViewController.delegate = self;
		alertViewController.actionButton.backgroundColor = tooltipGrayColor();
		[alertViewController.actionButton setTitle:firstButtonTitle forState:UIControlStateNormal];
		[alertViewController.actionButton.titleLabel setNumberOfLines:0];
		alertViewController.secondActionButton.backgroundColor = tooltipGrayColor();
		[alertViewController.secondActionButton setTitle:secondButtonTitle forState:UIControlStateNormal];
		[alertViewController.secondActionButton.titleLabel setNumberOfLines:0];
		[alertViewController.secondActionButton setEnabled: YES];
		[alertViewController.cancelButton setEnabled: YES];
		[alertViewController.cancelButton setImage:[UIImage imageNamed:@"Butt_cancelalert.png"] forState:UIControlStateNormal];
	}];
}

- (void)showEditViewWithItem:(NSString *)item delegate:(id<EditItemViewControllerDelegate>)delegate object:(id)object
{
    EditItemViewController *editItemViewController = [[EditItemViewController alloc] initWithNibName:@"EditItemViewController" bundle:nil];
    editItemViewController.delegate = delegate;
    [self showEditItemViewController:editItemViewController transitionBlock:^{
        editItemViewController.textField.text = item;
        editItemViewController.object = object;
    }];
}

- (void)twoOptionsAlertViewWithmessage:(NSString*)message firstButtonTitle:(NSString*)firstButtonTitle secondButtonTitle:(NSString*)secondButtonTitle confirmBlock:(AlertViewBlocksConfirm)confirm secondConfirm:(AlertViewBlocksSecondConfirm)secondConfirm
{
	// blocks
	confirmBlock = [confirm copy];
	secondConfirmBlock = [secondConfirm copy];
	
	// AlertViewController
	AlertViewController *alertViewController = [[AlertViewController alloc] initWithNibName:@"AlertViewController" bundle:nil];
	
	[self showCustomAlertViewController:alertViewController transitionBlock:^{
		alertViewController.titleLabel.text = @"INFORMATION";
		alertViewController.messageLabel.text = message;
		alertViewController.delegate = self;
		alertViewController.actionButton.backgroundColor = tooltipGrayColor();
		[alertViewController.actionButton setTitle:firstButtonTitle forState:UIControlStateNormal];
		alertViewController.secondActionButton.backgroundColor = tooltipGrayColor();
		[alertViewController.secondActionButton setTitle:secondButtonTitle forState:UIControlStateNormal];
		[alertViewController.secondActionButton setEnabled: YES];
		[alertViewController.cancelButton setEnabled:NO];
	}];
}

- (void)informationAlertViewWithTitle:(NSString*)title message:(NSString*)message confirmBlock:(AlertViewBlocksConfirm)confirm
{
	// blocks
	confirmBlock = [confirm copy];
	
	// AlertViewController
	AlertViewController *alertViewController = [[AlertViewController alloc] initWithNibName:@"AlertViewController" bundle:nil];
	
	[self showCustomAlertViewController:alertViewController transitionBlock:^{
		[alertViewController hideCancelButton];
		alertViewController.titleLabel.text = [title uppercaseString];
		alertViewController.messageLabel.text = message;
		alertViewController.delegate = self;
		[alertViewController.actionButton setImage:[UIImage imageNamed:@"Butt_okgreenalert.png"] forState:UIControlStateNormal];
	}];
}

- (void)informationAlertViewWithMessage:(NSString*)message confirmBlock:(AlertViewBlocksConfirm)confirm
{
	// blocks
	confirmBlock = [confirm copy];
	
	// AlertViewController
	AlertViewController *alertViewController = [[AlertViewController alloc] initWithNibName:@"AlertViewController" bundle:nil];
	
	[self showCustomAlertViewController:alertViewController transitionBlock:^{
		[alertViewController hideCancelButton];
		alertViewController.titleLabel.text = @"INFORMATION";
		alertViewController.messageLabel.text = message;
		alertViewController.delegate = self;
		[alertViewController.actionButton setImage:[UIImage imageNamed:@"Butt_okgreenalert.png"] forState:UIControlStateNormal];
	}];
}

- (void)showEditItemViewController:(EditItemViewController*)editViewController transitionBlock:(AlertViewBlocksTransition)transitionBlock
{
    UIViewController *parent = [self getRootController];
    
    editViewController.editView.transform = CGAffineTransformMakeScale(1.05, 1.05);
    editViewController.view.alpha = 0.8f;
    
    [parent addChildViewController:editViewController];
    [editViewController didMoveToParentViewController:parent];
    [parent.view addSubview:editViewController.view];
    transitionBlock();
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        editViewController.editView.transform = CGAffineTransformIdentity;
        editViewController.view.alpha = 1.0f;
    } completion:^(BOOL finished) {
        editViewController.view.frame = parent.view.bounds;
    }];
}

- (void)showBigCustomAlertViewController:(BigAlertViewController*)alertViewController transitionBlock:(AlertViewBlocksTransition)transitionBlock
{
	UIViewController *parent = [self getRootController];
	
	alertViewController.alert.transform = CGAffineTransformMakeScale(1.05, 1.05);
	alertViewController.view.alpha = 0.8f;
	
	[parent addChildViewController:alertViewController];
	[alertViewController didMoveToParentViewController:parent];
	[parent.view addSubview:alertViewController.view];
	transitionBlock();
	
	[UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
		alertViewController.alert.transform = CGAffineTransformIdentity;
		alertViewController.view.alpha = 1.0f;
	} completion:^(BOOL finished) {
		alertViewController.view.frame = parent.view.bounds;
	}];
}

- (void)showCustomAlertViewController:(AlertViewController*)alertViewController transitionBlock:(AlertViewBlocksTransition)transitionBlock
{
	UIViewController *parent = [self getRootController];
	
	alertViewController.alert.transform = CGAffineTransformMakeScale(1.05, 1.05);
	alertViewController.view.alpha = 0.8f;
	
	[parent addChildViewController:alertViewController];
	[alertViewController didMoveToParentViewController:parent];
	[parent.view addSubview:alertViewController.view];
	transitionBlock();
	
	[UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
		alertViewController.alert.transform = CGAffineTransformIdentity;
		alertViewController.view.alpha = 1.0f;
	} completion:^(BOOL finished) {
		alertViewController.view.frame = parent.view.bounds;
	}];
}

- (UIViewController *)getRootController
{
	UIWindow *win = [[[UIApplication sharedApplication] delegate] window];
	UIViewController *cntr = win.rootViewController;
	if (cntr != nil)
		return cntr;
	else
		return nil;
}

#pragma mark - AlertViewControllerDelegate

- (void)alertViewController:(AlertViewController *)alertViewController clickedButtonAtIndex:(NSInteger)buttonIndex
{
	switch (buttonIndex) {
		case 0: // cancel
			if (cancelBlock != nil)
				cancelBlock();
			break;
		case 1: // ok-delete
			if (confirmBlock != nil)
				confirmBlock();
			break;
		case 2: // second confirm
			if (secondConfirmBlock != nil)
				secondConfirmBlock();
			break;
		default:
			break;
	}
}

#pragma mark - BigAlertViewController

- (void)bigAlertViewController:(AlertViewController *)alertViewController clickedButtonAtIndex:(NSInteger)buttonIndex
{
	switch (buttonIndex) {
		case 0: // cancel
			if (cancelBlock != nil)
				cancelBlock();
			break;
		case 1: // ok-delete
			if (confirmBlock != nil)
				confirmBlock();
			break;
		case 2: // second confirm
			if (secondConfirmBlock != nil)
				secondConfirmBlock();
			break;
		default:
			break;
	}
}

@end
