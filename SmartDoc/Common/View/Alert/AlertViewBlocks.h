//
//  Flickr
//
//  Created by Francesca Corsini on 08/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AlertViewController.h"
#import "BigAlertViewController.h"
#import "EditItemViewController.h"

typedef void(^AlertViewBlocksConfirm)();
typedef void(^AlertViewBlocksSecondConfirm)();
typedef void(^AlertViewBlocksCancel)();
typedef void(^AlertViewBlocksTransition)();

@interface AlertViewBlocks : NSObject <AlertViewControllerDelegate, BigAlertViewControllerDelegate>
{
    AlertViewBlocksConfirm confirmBlock;
	AlertViewBlocksSecondConfirm secondConfirmBlock;
    AlertViewBlocksCancel cancelBlock;
}

// Utility Methods
+ (AlertViewBlocks*)getIstance;

// Warning alert with red OK button and CANCEL button
- (void)warningConfirmAlertViewWithMessage:(NSString*)message confirmBlock:(AlertViewBlocksConfirm)confirm cancelBlock:(AlertViewBlocksCancel)cancel;
// Warning delete alert with red OK button and CANCEL button
- (void)warningDeleteAlertViewWithMessage:(NSString*)message confirmBlock:(AlertViewBlocksConfirm)confirm cancelBlock:(AlertViewBlocksCancel)cancel;
// Warning alert with red OK button
- (void)warningAlertViewWithMessage:(NSString*)message confirmBlock:(AlertViewBlocksConfirm)confirm;

// Information alert with green OK button and CANCEL button
- (void)informationConfirmAlertViewWithmessage:(NSString*)message confirmBlock:(AlertViewBlocksConfirm)confirm cancelBlock:(AlertViewBlocksCancel)cancel;
// Information alert with green OK button and CANCEL button and title customizable
- (void)informationAlertViewWithTitle:(NSString*)title message:(NSString*)message confirmBlock:(AlertViewBlocksConfirm)confirm;
// Information alert with green OK button
- (void)informationAlertViewWithMessage:(NSString*)message confirmBlock:(AlertViewBlocksConfirm)confirm;

// Two-options alert with custom action buttons
- (void)twoOptionsAlertViewWithmessage:(NSString*)message firstButtonBg:(UIImage*)firstButtonBg secondButtonBg:(UIImage*)secondButtonBg confirmBlock:(AlertViewBlocksConfirm)confirm secondConfirm:(AlertViewBlocksSecondConfirm)secondConfirm;
// Two-options alert with custom action buttons and cancel button
- (void)twoOptionsAlertViewWithmessage:(NSString*)message firstButtonTitle:(NSString *)firstButtonTitle secondButtonTitle:(NSString *)secondButtonTitle confirmBlock:(AlertViewBlocksConfirm)confirm secondConfirm:(AlertViewBlocksSecondConfirm)secondConfirm cancelBlock:(AlertViewBlocksCancel)cancel;
// Two-options alert with default action buttons
- (void)twoOptionsAlertViewWithmessage:(NSString*)message firstButtonTitle:(NSString*)firstButtonTitle secondButtonTitle:(NSString*)secondButtonTitle confirmBlock:(AlertViewBlocksConfirm)confirm secondConfirm:(AlertViewBlocksSecondConfirm)secondConfirm;
// Push EditItemViewController from a parent viewController
- (void)showEditViewWithItem:(NSString *)item delegate:(id<EditItemViewControllerDelegate>)delegate object:(id)object;

@end
