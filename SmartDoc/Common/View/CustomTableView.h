//
//  CustomTableView.h
//  SmartDoc
//
//  Created by Francesca Corsini on 08/10/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableView : UITableView

@end
