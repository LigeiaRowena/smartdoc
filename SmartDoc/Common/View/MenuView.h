//
//  MenuView.h
//  SmartDoc
//
//  Created by Francesca Corsini on 20/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MenuViewDelegate <NSObject>
- (void)menuDidSelectItemAtIndex:(int)index;
@end

@interface MenuView : UIView

@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) UIButton *itemSelected;
@property (nonatomic) int index;

@property (nonatomic, weak) IBOutlet id<MenuViewDelegate> delegate;

- (void)selectItemAtIndex:(int)index;

@end
