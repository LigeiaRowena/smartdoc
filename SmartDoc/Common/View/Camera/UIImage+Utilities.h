//
//  UIViewController+RadioRai.h
//  RadioRai
//
//  Created by Francesca Corsini on 10/02/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Utilities)

@property (nonatomic, strong) NSString *note;

@end
