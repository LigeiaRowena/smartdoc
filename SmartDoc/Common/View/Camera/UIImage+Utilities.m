//
//  UIViewController+RadioRai.m
//  RadioRai
//
//  Created by Francesca Corsini on 10/02/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "UIImage+Utilities.h"
#import <objc/runtime.h>

static char noteKey;

@implementation UIImage (Utilities)

@dynamic note;

- (void)setNote:(NSString *)newnNote
{
	[self willChangeValueForKey:@"note"];
	objc_setAssociatedObject(self, &noteKey, newnNote, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
	[self didChangeValueForKey:@"note"];
}

- (NSString*)note
{
	return objc_getAssociatedObject(self, &noteKey);
}

@end
