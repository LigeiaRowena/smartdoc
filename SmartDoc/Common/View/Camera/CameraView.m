//
//  CameraView.m
//  SmartDoc
//
//  Created by Francesca Corsini on 17/10/15.
//  Copyright © 2015 Francesca Corsini. All rights reserved.
//

#import "CameraView.h"

@implementation CameraView

#pragma mark - Init

- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self)
	{
	}
	return self;
}

- (void)awakeFromNib
{
	[super awakeFromNib];
}

- (IBAction)back:(id)sender
{
	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(cancel)])
		[self.delegate cancel];
}

- (IBAction)shootPhoto:(id)sender
{
	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(takePicture)])
		[self.delegate takePicture];
}

@end
