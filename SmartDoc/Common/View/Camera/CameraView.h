//
//  CameraView.h
//  SmartDoc
//
//  Created by Francesca Corsini on 17/10/15.
//  Copyright © 2015 Francesca Corsini. All rights reserved.
//

#import "UIView+NibLoading.h"

@protocol CameraViewDelegate <NSObject>

- (void)takePicture;
- (void)cancel;

@end

@interface CameraView : NibLoadedView

@property (nonatomic, weak) id<CameraViewDelegate> delegate;

@end
