//
//  CustomTableView.m
//  SmartDoc
//
//  Created by Francesca Corsini on 08/10/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "CustomTableView.h"

@implementation CustomTableView

- (void)awakeFromNib
{
	[super awakeFromNib];
}

- (void)setBounds:(CGRect)bounds
{
	[super setBounds:bounds];
}

@end
