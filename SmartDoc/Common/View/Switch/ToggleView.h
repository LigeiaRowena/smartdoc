//
//  ToggleView.h
//  ToggleView
//
//  Created by SOMTD on 12/10/14.
//  Copyright (c) 2012年 somtd.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ToggleButton.h"
#import "ToggleBase.h"

@protocol ToggleViewDelegate <NSObject>
@optional
- (void)selectLeftButton;
- (void)selectRightButton;
- (void)selectToggle:(BOOL)on;
@end

typedef enum{
    ToggleButtonSelectedLeft,
    ToggleButtonSelectedRight,
}ToggleButtonSelected;

@interface ToggleView : UIView <UIGestureRecognizerDelegate>

@property (nonatomic, weak) IBOutlet id<ToggleViewDelegate> toggleDelegate;
@property (nonatomic) ToggleButtonSelected selectedButton;

- (id)initWithFrame:(CGRect)frame toggleBaseType:(ToggleBaseType)baseType toggleButtonType:(ToggleButtonType)buttonType;
- (void)commotInitWithFrame:(CGRect)frame toggleBaseType:(ToggleBaseType)aBaseType toggleButtonType:(ToggleButtonType)aButtonType;

@end
