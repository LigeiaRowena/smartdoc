//
//  ToggleView.m
//  ToggleView
//
//  Created by SOMTD on 12/10/14.
//  Copyright (c) 2012年 somtd.com. All rights reserved.
//

#import "ToggleView.h"

//replace sample image files
NSString *const TOGGLE_BUTTON_IMAGE      = @"Toogle_thumb.png";
NSString *const TOGGLE_BASE_IMAGE        = @"Toogle_background.png";
NSString *const TOGGLE_VIEW_BACKGROUND   = @"Toogle_background.png";

#define LEFT_BUTTON_RECT CGRectMake(0, 0, 72.f, 72.f)
#define RIGHT_BUTTON_RECT CGRectMake(0, 0, 72.f, 72.f)
#define TOGGLE_SLIDE_DULATION 0.1f

@interface ToggleView ()
{
	float _leftEdge;
	float _rightEdge;
	ToggleButton *_toggleButton;
	ToggleBase *_toggleBase;
	UIView *_baseView;
	UIButton *_leftButton;
	UIButton *_rightButton;
}
@end

@implementation ToggleView

- (void)commotInitWithFrame:(CGRect)frame toggleBaseType:(ToggleBaseType)aBaseType toggleButtonType:(ToggleButtonType)aButtonType 
{
	self.frame = frame;
	self.backgroundColor = [UIColor clearColor];
	UIImageView *bgImageView = [[UIImageView alloc]initWithImage:
								[UIImage imageNamed:TOGGLE_VIEW_BACKGROUND]];
	[self addSubview:bgImageView];
	
	//set up toggle base image.
	_toggleBase = [[ToggleBase alloc]initWithImage:[UIImage imageNamed:TOGGLE_BASE_IMAGE] baseType:aBaseType];
	_toggleBase.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
	_toggleBase.userInteractionEnabled = YES;
	
	//set up toggle button image.
	_toggleButton = [[ToggleButton alloc]initWithImage:[UIImage imageNamed:TOGGLE_BUTTON_IMAGE] buttonType:aButtonType];
	_toggleButton.userInteractionEnabled = YES;
	
	CGRect baseViewFrame = CGRectMake(_toggleBase.frame.origin.x + _toggleButton.frame.size.width/2,
									  _toggleBase.frame.origin.y,
									  _toggleBase.frame.size.width - _toggleButton.frame.size.width,
									  _toggleBase.frame.size.height);
	_baseView = [[UIView alloc]initWithFrame:baseViewFrame];
	[self addSubview:_baseView];
	
	//calculate left/right edge
	_leftEdge = _toggleBase.frame.origin.x + _toggleButton.frame.size.width/2;
	_rightEdge = _toggleBase.frame.origin.x + _toggleBase.frame.size.width - _toggleButton.frame.size.width/2;
	_toggleButton.center = CGPointMake(_leftEdge, self.frame.size.height/2);
	[self addSubview:_toggleBase];
	[self addSubview:_toggleButton];
	
	UIPanGestureRecognizer* panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
	
	UITapGestureRecognizer* buttonTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleButtonTapGesture:)];
	
	UITapGestureRecognizer* baseTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleBaseTapGesture:)];
	
	[_toggleButton addGestureRecognizer:panGesture];
	[_toggleButton addGestureRecognizer:buttonTapGesture];
	[_toggleBase addGestureRecognizer:baseTapGesture];
}

- (id)initWithFrame:(CGRect)frame toggleBaseType:(ToggleBaseType)baseType toggleButtonType:(ToggleButtonType)buttonType
{
    self = [super initWithFrame:frame];
    if (self) {
		[self commotInitWithFrame:frame toggleBaseType:baseType toggleButtonType:buttonType];
	}
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		
	}
	return self;
}

- (void)onLeftButton:(id)sender
{
    [UIView animateWithDuration:TOGGLE_SLIDE_DULATION animations:^{
        _toggleButton.center = CGPointMake(_leftEdge, self.frame.size.height/2);
    }];
	
    [_toggleBase selectedLeftToggleBase];
    [_toggleButton selectedLeftToggleButton];
	
	if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectLeftButton)])
		[self.toggleDelegate selectLeftButton];
	
	if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectToggle:)])
		[self.toggleDelegate selectToggle:NO];
}

- (void)onRightButton:(id)sender
{
    [UIView animateWithDuration:TOGGLE_SLIDE_DULATION animations:^{
        _toggleButton.center = CGPointMake(_rightEdge, self.frame.size.height/2);
    }];
	
    [_toggleBase selectedRightToggleBase];
    [_toggleButton selectedRightToggleButton];
	
	if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectRightButton)])
    [self.toggleDelegate selectRightButton];
	
	if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectToggle:)])
		[self.toggleDelegate selectToggle:YES];
}

- (void)setTogglePosition:(float)positonValue ended:(BOOL)isEnded
{
    if (!isEnded)
    {
        if (positonValue == 0.f)
        {
            _toggleButton.center = CGPointMake(_leftEdge, _toggleButton.center.y);
        }
        else if (positonValue == 1.f)
        {
            _toggleButton.center = CGPointMake(_rightEdge, _toggleButton.center.y);
        }
        else
        {
            _toggleButton.center = CGPointMake(_baseView.frame.origin.x + (positonValue * _baseView.frame.size.width), _toggleButton.center.y);
        }
        
    }
    else //isEnded == YES;
    {
        if (positonValue == 0.f)
        {
            _toggleButton.center = CGPointMake(_leftEdge, _toggleButton.center.y);
            [_toggleBase selectedLeftToggleBase];
            [_toggleButton selectedLeftToggleButton];
			
			if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectLeftButton)])
            [self.toggleDelegate selectLeftButton];
			
			if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectToggle:)])
				[self.toggleDelegate selectToggle:NO];
        }
        else if (positonValue == 1.f)
        {
            _toggleButton.center = CGPointMake(_rightEdge, _toggleButton.center.y);
            [_toggleBase selectedRightToggleBase];
            [_toggleButton selectedRightToggleButton];
			
			if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectRightButton)])
				[self.toggleDelegate selectRightButton];
			
			if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectToggle:)])
				[self.toggleDelegate selectToggle:YES];
        }
        else if (positonValue > 0.f && positonValue < 0.5f)
        {
            [UIView animateWithDuration:TOGGLE_SLIDE_DULATION animations:^{
                _toggleButton.center = CGPointMake(_leftEdge, _toggleButton.center.y);
            } completion:^(BOOL finished) {
                [_toggleBase selectedLeftToggleBase];
                [_toggleButton selectedLeftToggleButton];
				if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectLeftButton)])
					[self.toggleDelegate selectLeftButton];
				
				if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectToggle:)])
					[self.toggleDelegate selectToggle:NO];
            }];
        }
        else if (positonValue >= 0.5f && positonValue < 1.f)
        {
            [UIView animateWithDuration:TOGGLE_SLIDE_DULATION animations:^{
                _toggleButton.center = CGPointMake(_rightEdge, _toggleButton.center.y);
            } completion:^(BOOL finished) {
                [_toggleBase selectedRightToggleBase];
                [_toggleButton selectedRightToggleButton];
				
				if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectRightButton)])
					[self.toggleDelegate selectRightButton];
				
				if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectToggle:)])
					[self.toggleDelegate selectToggle:YES];
            }];
        }
    }
}

- (void)handleButtonTapGesture:(UITapGestureRecognizer*) sender {
    
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        if (_toggleButton.center.x == _rightEdge)
        {
            [UIView animateWithDuration:TOGGLE_SLIDE_DULATION animations:^{
                _toggleButton.center = CGPointMake(_leftEdge, _toggleButton.center.y);
            }completion:^(BOOL finished) {
                [_toggleBase selectedLeftToggleBase];
                [_toggleButton selectedLeftToggleButton];
				if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectLeftButton)])
					[self.toggleDelegate selectLeftButton];
				
				if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectToggle:)])
					[self.toggleDelegate selectToggle:NO];
            }];
        }
        else if (_toggleButton.center.x == _leftEdge)
        {
            [UIView animateWithDuration:TOGGLE_SLIDE_DULATION animations:^{
                _toggleButton.center = CGPointMake(_rightEdge, _toggleButton.center.y);
            }completion:^(BOOL finished) {
                [_toggleBase selectedRightToggleBase];
                [_toggleButton selectedRightToggleButton];
				
				if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectRightButton)])
					[self.toggleDelegate selectRightButton];
				
				if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectToggle:)])
					[self.toggleDelegate selectToggle:YES];
            }];
        }
    }
}

- (void)handleBaseTapGesture:(UITapGestureRecognizer*) sender {
    
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        if (_toggleButton.center.x == _rightEdge)
        {
            [UIView animateWithDuration:TOGGLE_SLIDE_DULATION animations:^{
                _toggleButton.center = CGPointMake(_leftEdge, _toggleButton.center.y);
            }completion:^(BOOL finished) {
                [_toggleBase selectedLeftToggleBase];
                [_toggleButton selectedLeftToggleButton];
				if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectLeftButton)])
					[self.toggleDelegate selectLeftButton];
				
				if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectToggle:)])
					[self.toggleDelegate selectToggle:NO];
            }];
        }
        else if (_toggleButton.center.x == _leftEdge)
        {
            [UIView animateWithDuration:TOGGLE_SLIDE_DULATION animations:^{
                _toggleButton.center = CGPointMake(_rightEdge, _toggleButton.center.y);
            }completion:^(BOOL finished) {
                [_toggleBase selectedRightToggleBase];
                [_toggleButton selectedRightToggleButton];
				
				if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectRightButton)])
					[self.toggleDelegate selectRightButton];
				
				if (self.toggleDelegate != nil && [self.toggleDelegate respondsToSelector:@selector(selectToggle:)])
					[self.toggleDelegate selectToggle:YES];
            }];
        }
    }
}


- (void)handlePanGesture:(UIPanGestureRecognizer*) sender {
        
    if (sender.state == UIGestureRecognizerStateBegan)
    {
        CGPoint currentPoint = [sender locationInView:_baseView];
        float position = currentPoint.x;
        float positionValue = position / _baseView.frame.size.width;
        
        if (positionValue < 1.f && positionValue > 0.f)
        {
            [self setTogglePosition:positionValue ended:NO];
        }
    }
    
    if (sender.state == UIGestureRecognizerStateChanged)
    {
        CGPoint currentPoint = [sender locationInView:_baseView];
        float position = currentPoint.x;
        float positionValue = position / _baseView.frame.size.width;
        
        if (positionValue < 1.f && positionValue > 0.f)
        {
            [self setTogglePosition:positionValue ended:NO];
        }
    }
    
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        
        CGPoint currentPoint = [sender locationInView:_baseView];
        float position = currentPoint.x;
        float positionValue = position / _baseView.frame.size.width;
        
        if (positionValue < 1.f && positionValue > 0.f)
        {
            [self setTogglePosition:positionValue ended:YES];
        }
        else if (positionValue >= 1.f)
        {
            [self setTogglePosition:1.f ended:YES];
        }
        else if (positionValue <= 0.f)
        {
            [self setTogglePosition:0.f ended:YES];
        }
    }
}

- (void)setSelectedButton:(ToggleButtonSelected)selectedButton
{
    _selectedButton = selectedButton;
	
	if (_selectedButton == ToggleButtonSelectedLeft)
	{
		[UIView animateWithDuration:TOGGLE_SLIDE_DULATION animations:^{
			_toggleButton.center = CGPointMake(_leftEdge, self.frame.size.height/2);
		}];
		
		[_toggleBase selectedLeftToggleBase];
		[_toggleButton selectedLeftToggleButton];
	}
	
	else if (_selectedButton == ToggleButtonSelectedRight)
	{
		[UIView animateWithDuration:TOGGLE_SLIDE_DULATION animations:^{
			_toggleButton.center = CGPointMake(_rightEdge, self.frame.size.height/2);
		}];
		
		[_toggleBase selectedRightToggleBase];
		[_toggleButton selectedRightToggleButton];
	}
}

@end
