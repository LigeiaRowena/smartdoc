//
//  CustomTextField.m
//  SmartDoc
//
//  Created by Francesca Corsini on 21/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{
	}
	return self;
}

- (void)drawRect:(CGRect)rect
{
	self.autocorrectionType = UITextAutocorrectionTypeNo;
	self.autocapitalizationType = UITextAutocapitalizationTypeNone;
	[self setTintColor:[UIColor blueColor]];
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
	return CGRectMake(bounds.origin.x + 10, bounds.origin.y, bounds.size.width - 10, bounds.size.height);
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
	return CGRectMake(bounds.origin.x + 10, bounds.origin.y, bounds.size.width - 10, bounds.size.height);
}



@end
