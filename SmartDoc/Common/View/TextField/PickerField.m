//
//  PickerTextView.m
//  SmartDoc
//
//  Created by Francesca Corsini on 10/05/13.
//  Copyright (c) 2013 Andrea. All rights reserved.
//

#import "PickerField.h"

#pragma mark - Init PickerField

@implementation PickerField

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{
		if (self.data != nil)
			self.textField.text = self.data;
	}
	return self;
}


- (void)drawRect:(CGRect)rect
{
	if (self.data != nil)
		self.textField.text = self.data;
}
 

#pragma mark - Actions

- (IBAction)selectPicker:(id)sender
{
	if ([self.fieldType isEqualToString:DateField])
	{
		if ([self.pickerDelegate respondsToSelector:@selector(tapOnPickerFieldDateButton:)])
			[self.pickerDelegate tapOnPickerFieldDateButton:self];
	}
	else if ([self.fieldType isEqualToString:DrugNameField])
	{
		if ([self.pickerDelegate respondsToSelector:@selector(tapOnPickerFieldDrugButton:)])
			[self.pickerDelegate tapOnPickerFieldDrugButton:self];
	}
	else
	{
		if ([self.pickerDelegate respondsToSelector:@selector(tapOnPickerFieldButton:)])
			[self.pickerDelegate tapOnPickerFieldButton:self];
	}
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{	
	if ([self.pickerDelegate respondsToSelector:@selector(pickerFieldDidBeginEditing:)])
		[self.pickerDelegate pickerFieldDidBeginEditing:self];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
	[self selectPicker:self.button];
	return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	// mette la prima maiuscola
	if (textField.text.length > 0)
		textField.text = [NSString stringWithFormat:@"%@%@",[[textField.text substringToIndex:1] uppercaseString],[textField.text substringFromIndex:1]];
	
	return YES;
}

@end
