//
//  CustomTextView.h
//  SmartDoc
//
//  Created by Francesca Corsini on 22/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomTextView;

@protocol CustomTextViewDelegate <NSObject>
@optional
- (void)customTextViewDidBeginEditing:(CustomTextView *)textView;
- (void)customViewDidBeginEditing:(UIView *)superView;
- (void)customTextViewDidEndEditing:(CustomTextView *)textView;
- (void)customViewDidEndEditing:(UIView *)superView textView:(CustomTextView*)textView;
- (void)customTextViewDidChange:(CustomTextView *)textView;
@end

@interface CustomTextView : UIView <UITextViewDelegate>

@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, weak) IBOutlet UIImageView *bg;
@property (nonatomic, weak) IBOutlet id<CustomTextViewDelegate> textviewDelegate;
@property (nonatomic, strong) NSNumber *numberOfLines;


@end
