//
//  PickerTextView.h
//  SmartDoc
//
//  Created by Francesca Corsini on 10/05/13.
//  Copyright (c) 2013 Andrea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSString+Utility.h"

@class PickerField;

@protocol PickerFieldDelegate <NSObject>
@optional
- (void)tapOnPickerFieldButton:(PickerField*)pickerField;
- (void)tapOnPickerFieldDateButton:(PickerField*)pickerField;
- (void)tapOnPickerFieldDrugButton:(PickerField*)pickerField;
- (void)pickerFieldDidBeginEditing:(PickerField *)pickerField;
- (void)pickerFieldDidEndEditing:(PickerField *)pickerField;
@end

@interface PickerField : UIView <UITextFieldDelegate>

@property (nonatomic,strong) NSString *fieldType;

@property (nonatomic,weak) IBOutlet UIImageView *bg;
@property (nonatomic,weak) IBOutlet UITextField *textField;
@property (nonatomic,weak) IBOutlet UIButton *button;
@property (nonatomic,weak) IBOutlet id<PickerFieldDelegate> pickerDelegate;
@property (nonatomic,strong) id data;
@property (nonatomic) int section;
@property (nonatomic) int pickerIndex;



@end
