//
//  CustomTextField.h
//  SmartDoc
//
//  Created by Francesca Corsini on 21/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTextField : UITextField

@property (nonatomic) int section;
@property (nonatomic, strong) NSString *type;
@property (nonatomic) BOOL mandatory;

@end
