//
//  CustomTextView.m
//  SmartDoc
//
//  Created by Francesca Corsini on 22/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "CustomTextView.h"

@implementation CustomTextView

#pragma mark - Init

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{
	}
	return self;
}

- (void)drawRect:(CGRect)rect
{
	self.textView.autocorrectionType = UITextAutocorrectionTypeNo;
	self.textView.autocapitalizationType =UITextAutocapitalizationTypeNone;
	[self setTintColor:[UIColor blueColor]];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView
{
	if (self.textviewDelegate != nil && [self.textviewDelegate respondsToSelector:@selector(customTextViewDidChange:)])
		[self.textviewDelegate customTextViewDidChange:self];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
	// mette la prima maiuscola
	/*
	if (textView.text.length > 0)
		textView.text = [NSString stringWithFormat:@"%@%@",[[textView.text substringToIndex:1] uppercaseString],[textView.text substringFromIndex:1]];
	 */
	
	if (self.numberOfLines != nil)
	{
		NSMutableString *t = [self.textView.text mutableCopy];
		[t replaceCharactersInRange: range withString: text];
		
		NSUInteger numberLines = 0;
		for (int i = 0; i < t.length; i++) {
			if ([[NSCharacterSet newlineCharacterSet]
				 characterIsMember: [t characterAtIndex: i]])
				numberLines++;
		}
		return (numberLines < [self.numberOfLines intValue]);
	}
	else
		return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
	
	return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
	if (self.textviewDelegate != nil && [self.textviewDelegate respondsToSelector:@selector(customTextViewDidBeginEditing:)])
		[self.textviewDelegate customTextViewDidBeginEditing:self];
	
	if (self.textviewDelegate != nil && [self.textviewDelegate respondsToSelector:@selector(customViewDidBeginEditing:)])
		[self.textviewDelegate customViewDidBeginEditing:self.superview];

}

- (void)textViewDidEndEditing:(UITextView *)textView
{
	if (self.textviewDelegate != nil && [self.textviewDelegate respondsToSelector:@selector(customTextViewDidEndEditing:)])
		[self.textviewDelegate customTextViewDidEndEditing:self];
	
	if (self.textviewDelegate != nil && [self.textviewDelegate respondsToSelector:@selector(customViewDidEndEditing:textView:)])
		[self.textviewDelegate customViewDidEndEditing:self.superview textView:self];
}



@end
