//
//  RAIRequests.m
//  RaiNews
//
//  Created by Pronk on 12/09/13.
//  Copyright (c) 2013 RaiNet. All rights reserved.
//

#import "AFRequests.h"
#import "SVProgressHUD.h"
#import "Settings.h"

@implementation AFRequests

#pragma mark - Common

+ (AFRequests*) sharedObject
{
    static dispatch_once_t onceToken = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&onceToken, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

+ (NSString*)getValueFromPlist:(NSString*)plist key:(NSString*)key
{
	NSString *path = [[NSBundle mainBundle] pathForResource:[plist stringByDeletingPathExtension] ofType:[plist pathExtension]];
	NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
	
	return dict[key];
}

+ (NSDictionary*)getParamsFromPlist:(NSString*)plist key:(NSString*)key
{
	NSString *path = [[NSBundle mainBundle] pathForResource:[plist stringByDeletingPathExtension] ofType:[plist pathExtension]];
	NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
	
	return dict[key];
}

+ (BOOL)isEmptyObject:(id)object
{
    if (object == nil || [object isEqual:[NSNull null]])
        return YES;
    else if ([object respondsToSelector:@selector(count)])
		if ([object count] == 0)
			return YES;
    return NO;
}

#pragma mark - Requests

+ (void)getRequestUrl:(NSString*)url params:(NSDictionary*)params success:(void (^)(NSHTTPURLResponse* response, id responseObject))successBlock failure:(void (^)(NSHTTPURLResponse* response, NSError *error))failureBlock
{
	AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
	manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"text/html", @"application/json", @"application/javascript", nil];
	
	[manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
		if (![self isEmptyObject:responseObject])
			successBlock(operation.response, responseObject);
		else
			failureBlock(operation.response, [NSError errorWithDomain:@"" code:0 userInfo:@{}]);
		
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		failureBlock(operation.response, error);
	}];
}

+ (void)postRequestUrl:(NSString*)url params:(NSDictionary*)params success:(void (^)(NSHTTPURLResponse* response, id responseObject))successBlock failure:(void (^)(NSHTTPURLResponse* response, NSError *error))failureBlock
{
	AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
	manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"text/html", @"application/json", @"application/javascript", nil];
		
	[manager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
		if (![self isEmptyObject:responseObject])
			successBlock(operation.response, responseObject);
		else
			failureBlock(operation.response, [NSError errorWithDomain:@"" code:0 userInfo:@{}]);
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failureBlock(operation.response, error);
	}];
}

+ (NSURLSessionDataTask *)post:(NSString*)url headers:(NSDictionary*)headers parameters:(NSDictionary*)parameters progress:(NSProgress*)progress constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure progressBlock:(void (^)(NSProgress* pr))progressBlock
{
	AFHTTPSessionManager *session = [[AFHTTPSessionManager alloc] init];
	NSURLSessionDataTask *datatask = [session POST:url headers:headers parameters:parameters progress:progress constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
		if (block)
			block(formData);
		
	} success:^(NSURLSessionDataTask *task, id responseObject) {
		if (success)
			success(task, responseObject);
		
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		if (failure)
			failure(task, error);
		
	} progressBlock:^(NSProgress *pr) {
		if (progressBlock)
			progressBlock(pr);
	}];
	
	return datatask;
}

+ (void)login:(NSString*)email password:(NSString*)password success:(void (^)(NSHTTPURLResponse* response, id responseObject))successBlock failure:(void (^)(NSHTTPURLResponse* response, NSError *error))failureBlock
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
		[SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
		[SVProgressHUD show];
	});
	
	NSMutableDictionary *params = [self getParamsFromPlist:REQUEST key:REQUEST_LOGIN].mutableCopy;
	NSString *basepath = [[self getValueFromPlist:REQUEST key:REQUEST_BASEPATH] stringByAppendingString:params[@"url"]];
	
	[params setValue:email forKey:@"email"];
	[params setValue:password forKey:@"password"];
	[params removeObjectForKey:@"url"];

	[self postRequestUrl:basepath params:params success:^(NSHTTPURLResponse *response, id responseObject) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[SVProgressHUD dismiss];
		});
		if (![self isEmptyObject:responseObject[@"session"]]) {
			NSString *session = responseObject[@"session"];
			NSString *email = responseObject[@"email"];
			[[DBManager getIstance] setSession:session];
			Settings *settings = [[DBManager getIstance] getSettings];
			settings.email = email;
			[[DBManager getIstance] saveContext];
			successBlock(response, responseObject);
		}
		else if (![self isEmptyObject:responseObject[@"ERROR"]]) {
			[[DBManager getIstance] setSession:@""];
			failureBlock(response, [NSError errorWithDomain:@"" code:0 userInfo:@{@"message": responseObject[@"ERROR"][@"MESSAGE"]}]);
		}
		else {
			[[DBManager getIstance] setSession:@""];
			failureBlock(response, [NSError errorWithDomain:@"" code:0 userInfo:@{@"message": @"Connection error"}]);
		}
	} failure:^(NSHTTPURLResponse *response, NSError *error) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[SVProgressHUD dismiss];
		});
		[[DBManager getIstance] setSession:@""];
		failureBlock(response, [NSError errorWithDomain:@"" code:0 userInfo:@{@"message": @"Connection error"}]);
	}];
}

+ (void)sendActivationCode:(NSString*)email success:(void (^)(NSHTTPURLResponse* response, id responseObject))successBlock failure:(void (^)(NSHTTPURLResponse* response, NSError *error))failureBlock
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
		[SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
		[SVProgressHUD show];
	});
	
	NSMutableDictionary *params = [self getParamsFromPlist:REQUEST key:REQUEST_SENDCODE].mutableCopy;
	NSString *basepath = [[self getValueFromPlist:REQUEST key:REQUEST_BASEPATH] stringByAppendingString:params[@"url"]];

	[params setValue:email forKey:@"email"];
	[params removeObjectForKey:@"url"];

	[self postRequestUrl:basepath params:params success:^(NSHTTPURLResponse *response, id responseObject) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[SVProgressHUD dismiss];
		});
		if (![self isEmptyObject:responseObject[@"ERROR"]])
			failureBlock(response, [NSError errorWithDomain:@"" code:0 userInfo:@{@"message": responseObject[@"ERROR"][@"MESSAGE"]}]);
		else
			successBlock(response, responseObject);
	} failure:^(NSHTTPURLResponse *response, NSError *error) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[SVProgressHUD dismiss];
		});
		failureBlock(response, [NSError errorWithDomain:@"" code:0 userInfo:@{@"message": @"Connection error"}]);
	}];
}

+ (void)sendPassword:(NSString*)email success:(void (^)(NSHTTPURLResponse* response, id responseObject))successBlock failure:(void (^)(NSHTTPURLResponse* response, NSError *error))failureBlock
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
		[SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
		[SVProgressHUD show];
	});

	NSMutableDictionary *params = [self getParamsFromPlist:REQUEST key:REQUEST_SENDPASSWORD].mutableCopy;
	NSString *basepath = [[self getValueFromPlist:REQUEST key:REQUEST_BASEPATH] stringByAppendingString:params[@"url"]];

	[params setValue:email forKey:@"email"];
	[params removeObjectForKey:@"url"];

	[self postRequestUrl:basepath params:params success:^(NSHTTPURLResponse *response, id responseObject) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[SVProgressHUD dismiss];
		});
		if (![self isEmptyObject:responseObject[@"ERROR"]])
			failureBlock(response, [NSError errorWithDomain:@"" code:0 userInfo:@{@"message": responseObject[@"ERROR"][@"MESSAGE"]}]);
		else
			successBlock(response, responseObject);
	} failure:^(NSHTTPURLResponse *response, NSError *error) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[SVProgressHUD dismiss];
		});
		failureBlock(response, [NSError errorWithDomain:@"" code:0 userInfo:@{@"message": @"Connection error"}]);
	}];
}


+ (void)registerUser:(NSString*)name email:(NSString*)email password:(NSString*)password passwordConfirm:(NSString*)passwordConfirm specialization:(NSString*)specialization number:(NSString*)number address:(NSString*)address zipcode:(NSString*)zipcode city:(NSString*)city country:(NSString*)country success:(void (^)(NSHTTPURLResponse* response, id responseObject))successBlock failure:(void (^)(NSHTTPURLResponse* response, NSError *error))failureBlock
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
		[SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
		[SVProgressHUD show];
	});
	
	NSMutableDictionary *params = [self getParamsFromPlist:REQUEST key:REQUEST_REGISTER].mutableCopy;
	NSString *basepath = [[self getValueFromPlist:REQUEST key:REQUEST_BASEPATH] stringByAppendingString:params[@"url"]];
	
	[params setValue:name forKey:@"name"];
	[params setValue:email forKey:@"email"];
	[params setValue:password forKey:@"pw"];
	[params setValue:passwordConfirm forKey:@"pw_conf"];
	[params setValue:number forKey:@"wnumber"];
	[params setValue:address forKey:@"address"];
	[params setValue:zipcode forKey:@"zipcode"];
	[params setValue:city forKey:@"city"];
	[params setValue:country forKey:@"country"];
	[params setValue:specialization forKey:@"specialization"];
	[params removeObjectForKey:@"url"];

	[self postRequestUrl:basepath params:params success:^(NSHTTPURLResponse *response, id responseObject) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[SVProgressHUD dismiss];
		});
		if (![self isEmptyObject:responseObject[@"ERROR"]])
			failureBlock(response, [NSError errorWithDomain:@"" code:0 userInfo:@{@"message": responseObject[@"ERROR"][@"MESSAGE"]}]);
		else
			successBlock(response, responseObject);
	} failure:^(NSHTTPURLResponse *response, NSError *error) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[SVProgressHUD dismiss];
		});
		failureBlock(response, [NSError errorWithDomain:@"" code:0 userInfo:@{@"message": @"Connection error"}]);
	}];
}


+ (AFHTTPRequestOperation*)getDrugsPage:(int)page total:(int)total success:(void (^)(id response))successBlock failure:(void (^)(NSError *error))failureBlock
{
	NSString *url = [[AFRequests getParamsFromPlist:REQUEST key:REQUEST_DRUGS][@"url"] stringByReplacingOccurrencesOfString:@"[$PAGE$]" withString:[NSString stringWithFormat:@"%i", page]];
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
	AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
	[operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
		float progress = (float)page/total;
		[SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
		[SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
		[SVProgressHUD showProgress:progress status:@"Loading"];
		
		id response = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
		if (page == total)
			dispatch_async(dispatch_get_main_queue(), ^{
				[SVProgressHUD dismiss];
			});
		successBlock(response);
		
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		NSLog(@"getDrugsPage %i response %@ error %@", page, operation.response, error);
		if (page == total)
			dispatch_async(dispatch_get_main_queue(), ^{
				[SVProgressHUD dismiss];
			});
		failureBlock(nil);
	}];
	
	return operation;
}

+ (void)getDrugsSuccess:(void (^)())successBlock failure:(void (^)(NSString *error))failureBlock progressBlock:(void (^)(id response))progressBlock
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
		[SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
		[SVProgressHUD showProgress:0 status:@"Loading"];
	});
	
	// get drugs header
	NSMutableDictionary *params = [self getParamsFromPlist:REQUEST key:REQUEST_DRUGS].mutableCopy;
	NSString *basepath = [self getParamsFromPlist:REQUEST key:REQUEST_DRUGS][@"urlBase"];
	[params setValue:@"0" forKey:@"page"];
	[params removeObjectForKey:@"urlBase"];
	[params removeObjectForKey:@"url"];
	
	[self getRequestUrl:basepath params:params success:^(NSHTTPURLResponse *response, id responseObject) {
		int totalPages = [responseObject[@"metadata"][@"total_pages"] intValue];
		//int totalPages = 20;
		int currentPage = [responseObject[@"metadata"][@"current_page"] intValue];
		AFHTTPRequestOperation *lastOperation = nil;
		NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
		for (currentPage = 1; currentPage <= totalPages; currentPage++)
		{
			AFHTTPRequestOperation *operation = [AFRequests getDrugsPage:currentPage total:totalPages success:^(id response) {
				float progress = (float)currentPage/totalPages;
				NSDecimalNumber *progressNumber = (NSDecimalNumber*)[NSNumber numberWithFloat:progress*100];
				[SVProgressHUD showProgress:progress status:[NSString stringWithFormat:@"Downloading %@ %%", [getDecimalFormatter(0) stringFromNumber:progressNumber]]];
				progressBlock(response);
				if (currentPage == totalPages)
				{
					dispatch_async(dispatch_get_main_queue(), ^{
						[SVProgressHUD dismiss];
					});
					successBlock();
				}
				
			} failure:^(NSError *error) {
				if (currentPage == totalPages)
				{
					dispatch_async(dispatch_get_main_queue(), ^{
						[SVProgressHUD dismiss];
					});
					successBlock();
				}
			}];
			
			if (lastOperation != nil)
				[operation addDependency:lastOperation];
			[operationQueue addOperations:@[operation] waitUntilFinished:NO];
			lastOperation = operation;
		}
		
	} failure:^(NSHTTPURLResponse *response, NSError *error) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[SVProgressHUD dismiss];
		});
		failureBlock(@"Connection error");
	}];
}


+ (void)uploadDBFile:(NSString*)path success:(void (^)(NSURLResponse *response, id responseObject))successBlock failure:(void (^)(NSString *error))failureBlock
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
		[SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
		[SVProgressHUD showProgress:0 status:@"Loading"];
	});
	
	NSMutableDictionary *params = [self getParamsFromPlist:REQUEST key:REQUEST_UPLOAD].mutableCopy;
	NSString *basepath = [[self getValueFromPlist:REQUEST key:REQUEST_BASEPATH] stringByAppendingString:params[@"url"]];
	NSData *data = [[NSData alloc] initWithContentsOfFile:path];
	[params setValue:data forKey:@"files"];
	[params setValue:[[DBManager getIstance] getSession] forKey:@"session"];
	[params setValue:@"dbFile" forKey:@"title"];
	[params removeObjectForKey:@"url"];
	NSData *datatosend = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:path]];
	NSProgress *progress = [NSProgress progressWithTotalUnitCount:data.length];

	[self post:basepath headers:nil parameters:nil progress:progress constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
		[formData appendPartWithFileData:datatosend name:@"files" fileName:path mimeType:@"application/x-sqlite3"];
		[formData appendPartWithFormData:[@"db" dataUsingEncoding:NSUTF8StringEncoding] name:@"title"];
		NSString *session = [[DBManager getIstance] getSession];
		[formData appendPartWithFormData:[session dataUsingEncoding:NSUTF8StringEncoding] name:@"session"];

	} success:^(NSURLSessionDataTask *task, id responseObject) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[SVProgressHUD dismiss];
		});
		successBlock(nil, responseObject);
		
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[SVProgressHUD dismiss];
		});
		failureBlock([error localizedDescription]);
		
	} progressBlock:^(NSProgress *pr) {
		float progress = (float)pr.completedUnitCount/pr.totalUnitCount;
		NSDecimalNumber *progressNumber = (NSDecimalNumber*)[NSNumber numberWithFloat:progress*100];
		dispatch_async(dispatch_get_main_queue(), ^{
			[SVProgressHUD showProgress:progress status:[NSString stringWithFormat:@"Uploading %@ %%", [getDecimalFormatter(0) stringFromNumber:progressNumber]]];
		});
	}];
}

+ (void)downloadDBFileSuccess:(void (^)(NSURLResponse *response, id responseObject))successBlock failure:(void (^)(NSString *error))failureBlock
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
		[SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
		[SVProgressHUD showProgress:0 status:@"Loading"];
	});
	
	NSMutableDictionary *params = [self getParamsFromPlist:REQUEST key:REQUEST_USER_FILES].mutableCopy;
	[params setValue:[[DBManager getIstance] getSession] forKey:@"session"];
	NSString *basePath = [[self getValueFromPlist:REQUEST key:REQUEST_BASEPATH] stringByAppendingString:params[@"url"]];
	[params removeObjectForKey:@"url"];

	[self getRequestUrl:basePath params:nil success:^(NSHTTPURLResponse *response, id responseObject) {
		NSString *url = responseObject[@"url"];
		[AFRequests downloadFileAtURL:[NSString stringWithFormat:@"http://%@", url] success:^(NSURLResponse *response, id responseObject) {
			dispatch_async(dispatch_get_main_queue(), ^{
				[SVProgressHUD dismiss];
			});
			successBlock(response, responseObject);
			
		} failure:^(NSString *error) {
			dispatch_async(dispatch_get_main_queue(), ^{
				[SVProgressHUD dismiss];
			});
			failureBlock(error);
		}];
		
	} failure:^(NSHTTPURLResponse *response, NSError *error) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[SVProgressHUD dismiss];
		});
		failureBlock([error localizedDescription]);
	}];
	
}


+ (void)downloadFileAtURL:(NSString*)url success:(void (^)(NSURLResponse *response, id responseObject))successBlock failure:(void (^)(NSString *error))failureBlock
{
	NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
	AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
	NSURL *URL = [NSURL URLWithString:url];
	NSURLRequest *request = [NSURLRequest requestWithURL:URL];
	
	NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
		NSURL *filePath = [[DBManager getIstance] modelCloudPathURL];
		return filePath;
	} completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
		if ([[NSFileManager defaultManager] fileExistsAtPath:[[DBManager getIstance] modelPathString]] && !error) {
			successBlock(response, filePath);
		}
		else
		{
			failureBlock(error.localizedDescription);
		}
	}];
	[downloadTask resume];
}


@end
