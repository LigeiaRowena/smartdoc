//
//  RAIRequests.h
//  RaiNews
//
//  Created by Pronk on 12/09/13.
//  Copyright (c) 2013 RaiNet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface AFRequests : NSObject <NSURLSessionDelegate>

// Common
+ (AFRequests*) sharedObject;
+ (NSDictionary*)getParamsFromPlist:(NSString*)plist key:(NSString*)key;

// Requests
+ (void)login:(NSString*)email password:(NSString*)password success:(void (^)(NSHTTPURLResponse* response, id responseObject))successBlock failure:(void (^)(NSHTTPURLResponse* response, NSError *error))failureBlock;
+ (void)sendActivationCode:(NSString*)email success:(void (^)(NSHTTPURLResponse* response, id responseObject))successBlock failure:(void (^)(NSHTTPURLResponse* response, NSError *error))failureBlock;
+ (void)sendPassword:(NSString*)email success:(void (^)(NSHTTPURLResponse* response, id responseObject))successBlock failure:(void (^)(NSHTTPURLResponse* response, NSError *error))failureBlock;
+ (void)registerUser:(NSString*)name email:(NSString*)email password:(NSString*)password passwordConfirm:(NSString*)passwordConfirm specialization:(NSString*)specialization number:(NSString*)number address:(NSString*)address zipcode:(NSString*)zipcode city:(NSString*)city country:(NSString*)country success:(void (^)(NSHTTPURLResponse* response, id responseObject))successBlock failure:(void (^)(NSHTTPURLResponse* response, NSError *error))failureBlock;
+ (void)getDrugsSuccess:(void (^)())successBlock failure:(void (^)(NSString *error))failureBlock progressBlock:(void (^)(id response))progressBlock;
+ (void)uploadDBFile:(NSString*)path success:(void (^)(NSURLResponse *response, id responseObject))successBlock failure:(void (^)(NSString *error))failureBlock;
+ (void)downloadDBFileSuccess:(void (^)(NSURLResponse *response, id responseObject))successBlock failure:(void (^)(NSString *error))failureBlock;

@end
