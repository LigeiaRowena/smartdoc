#import <UIKit/UIKit.h>

@protocol GKLineGraphDataSource;

@interface GKLineGraph : UIView

@property (nonatomic, assign) BOOL optionalLabel;
@property (nonatomic, assign) BOOL animated;
@property (nonatomic, assign) CFTimeInterval animationDuration;

@property (nonatomic, weak) IBOutlet id<GKLineGraphDataSource> dataSource;

@property (nonatomic, assign) CGFloat lineWidth;
@property (nonatomic, assign) CGFloat margin;
@property (nonatomic, assign) NSString *unit;

@property (nonatomic, assign) NSInteger valueLabelCount;

@property (nonatomic, assign) CGFloat *minValue;
@property (nonatomic, assign) BOOL startFromZero;

@property (nonatomic, strong) NSMutableArray *titleLabels;
@property (nonatomic, strong) NSMutableArray *valueLabels;
@property (nonatomic, strong) NSMutableArray *optionalLabels;

- (void)draw;
- (void)reset;

@end

@protocol GKLineGraphDataSource <NSObject>
- (NSInteger)numberOfLines;
- (UIColor *)colorForLineAtIndex:(NSInteger)index;
- (NSArray *)valuesForLineAtIndex:(NSInteger)index;
@optional
- (CFTimeInterval)animationDurationForLineAtIndex:(NSInteger)index;
- (NSString *)titleForLineAtIndex:(NSInteger)index;
@end
