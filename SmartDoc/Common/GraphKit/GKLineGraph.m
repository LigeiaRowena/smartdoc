#import "GKLineGraph.h"
#import "FrameAccessor.h"
#import "NSArray+MK.h"

static CGFloat kDefaultLabelWidth = 60.0;
static CGFloat kDefaultLabelHeight = 12.0;
static NSInteger kDefaultValueLabelCount = 5;

static CGFloat kDefaultLineWidth = 3.0;
static CGFloat kDefaultMargin = 10.0;
static CGFloat kDefaultMarginBottom = 20.0;

static CGFloat kAxisMargin = 80.0;

@interface GKLineGraph ()
@end

@implementation GKLineGraph

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self _init];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self _init];
    }
    return self;
}

- (void)_init {
    self.animated = YES;
	self.optionalLabel = NO;
    self.animationDuration = 1;
    self.lineWidth = kDefaultLineWidth;
    self.margin = kDefaultMargin;
    self.valueLabelCount = kDefaultValueLabelCount;
	self.unit = @"";
    self.clipsToBounds = YES;
	self.optionalLabels = @[].mutableCopy;
	self.titleLabels = @[].mutableCopy;
	self.valueLabels = @[].mutableCopy;
}

- (void)draw {
    NSAssert(self.dataSource, @"GKLineGraph : No data source is assgined.");
	
    [self _constructTitleLabels];
	[self _drawLines];
}

- (BOOL)_hasTitleLabels {
    return ![self.titleLabels mk_isEmpty];
}

- (BOOL)_hasValueLabels {
    return ![self.valueLabels mk_isEmpty];
}

- (void)_constructTitleLabels {
	
    NSInteger count = [[self.dataSource valuesForLineAtIndex:0] count];
    for (NSInteger idx = 0; idx < count; idx++) {
        
        CGRect frame = CGRectMake(0, 0, kDefaultLabelWidth, kDefaultLabelHeight);
        UILabel *item = [[UILabel alloc] initWithFrame:frame];
        item.textAlignment = NSTextAlignmentCenter;
		item.font = [UIFont fontWithName:@"OpenSans" size:11];
        item.textColor = [UIColor whiteColor];
        item.text = [self.dataSource titleForLineAtIndex:idx];
        
        [self.titleLabels addObject:item];
    }
}

- (void)_removeTitleLabels
{
	[self.titleLabels enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
	{
		UILabel *label = (UILabel*)obj;
		if ([label isDescendantOfView:self])
			[label removeFromSuperview];
	}];
	
    [self.titleLabels removeAllObjects];
}

- (void)_positionTitleLabel:(int)idx
{
	CGFloat labelWidth = kDefaultLabelWidth;
	CGFloat labelHeight = kDefaultLabelHeight;
	CGFloat startX = [self _pointXForIndex:idx] - (labelWidth / 2);
	CGFloat startY = (self.height - labelHeight);
	
	UILabel *label = [self.titleLabels objectAtIndex:idx];
	label.x = startX;
	label.y = startY;
	[self addSubview:label];
}


- (CGFloat)_pointXForIndex:(NSInteger)index {
	return kAxisMargin + self.margin + (index * [self _stepX]);
}

- (CGFloat)_stepX {
    id values = [self.dataSource valuesForLineAtIndex:0];
    CGFloat result = ([self _plotWidth] / [values count]);
    return result;
}

- (void)_constructValueLabel:(int)idx
{
	CGRect frame = CGRectMake(0, 0, kDefaultLabelWidth, kDefaultLabelHeight);
	UILabel *item = [[UILabel alloc] initWithFrame:frame];
	item.textAlignment = 1;
	item.font = [UIFont fontWithName:@"OpenSans" size:11];;
	item.textColor = [UIColor whiteColor];
	
	float value = [self _minValue] + (idx * [self _stepValueLabelY]);
	item.centerY = [self _positionYForLineValue:value];
	NSDecimalNumber *decimalNumber = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:value] decimalValue]];
	item.text = [NSString stringWithFormat:@"%@ %@", [getDecimalFormatter(1) stringFromNumber:decimalNumber], self.unit];

	[self.valueLabels addObject:item];
	[self addSubview:item];

}

- (CGFloat)_stepValueLabelY {
    return (([self _maxValue] - [self _minValue]) / (self.valueLabelCount - 1));
}

- (CGFloat)_maxValue {
    NSArray *values = [self _allValues];
    return [[values mk_max] floatValue];
}

- (CGFloat)_minValue {
    if (self.startFromZero) return 0;
    NSArray *values = [self _allValues];
    return [[values mk_min] floatValue];
}

- (NSArray *)_allValues {
    NSInteger count = [self.dataSource numberOfLines];
    id values = [NSMutableArray array];
    for (NSInteger idx = 0; idx < count; idx++) {
        NSArray *item = [self.dataSource valuesForLineAtIndex:idx];
        [values addObjectsFromArray:item];
    }
    return values;
}

- (void)_removeValueLabels
{
	[self.valueLabels enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		UILabel *label = (UILabel*)obj;
		if ([label isDescendantOfView:self])
			[label removeFromSuperview];
	}];
	
    [self.valueLabels removeAllObjects];
}

- (void)_removeOptionalLabels
{
	[self.optionalLabels enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		UILabel *label = (UILabel*)obj;
		if ([label isDescendantOfView:self])
			[label removeFromSuperview];
	}];
	[self.optionalLabels removeAllObjects];
}

- (CGFloat)_plotWidth {
    return (self.width - (2 * self.margin) - kAxisMargin);
}

- (CGFloat)_plotHeight {
    return (self.height - (2 * kDefaultLabelHeight + kDefaultMarginBottom));
}

- (void)_drawLines {
    for (NSInteger idx = 0; idx < [self.dataSource numberOfLines]; idx++) {
        [self _drawLineAtIndex:idx];
    }
}

- (void)_drawLineAtIndex:(NSInteger)index {
    
    // http://stackoverflow.com/questions/19599266/invalid-context-0x0-under-ios-7-0-and-system-degradation
    UIGraphicsBeginImageContext(self.frame.size);
    
    UIBezierPath *path = [self _bezierPathWith:0];
    CAShapeLayer *layer = [self _layerWithPath:path];
    
    layer.strokeColor = [[self.dataSource colorForLineAtIndex:index] CGColor];
    
    [self.layer addSublayer:layer];
    
    NSInteger idx = 0;
    NSArray *values = [self.dataSource valuesForLineAtIndex:index];
    for (NSDecimalNumber *item in values) {

        CGFloat x = [self _pointXForIndex:idx];
        CGFloat y = [self _positionYForLineValue:[item floatValue]];
        CGPoint point = CGPointMake(x, y);
		
		if (self.optionalLabel)
			[self addOptionalLabelAtPoint:point value:[getDecimalFormatter(1) stringFromNumber:item]];
		
		[self _positionTitleLabel:(int)idx];
        
        if (idx != 0) [path addLineToPoint:point];
        [path moveToPoint:point];
        
        idx++;
    }
	
	for (int i = 0; i < self.valueLabelCount; i++)
	{
		[self _constructValueLabel:i];
	}
	
    layer.path = path.CGPath;
    
    if (self.animated) {
        CABasicAnimation *animation = [self _animationWithKeyPath:@"strokeEnd"];
        if ([self.dataSource respondsToSelector:@selector(animationDurationForLineAtIndex:)]) {
            animation.duration = [self.dataSource animationDurationForLineAtIndex:index];
        }
        [layer addAnimation:animation forKey:@"strokeEndAnimation"];
		
		[UIView animateWithDuration:animation.duration animations:^{
			for (UILabel *label in self.optionalLabels)
				label.alpha = 1.0f;
		}];
    }
    
    UIGraphicsEndImageContext();
}

- (void)addOptionalLabelAtPoint:(CGPoint)point value:(NSString*)value
{
	CGRect frame = CGRectMake(point.x-kDefaultLabelWidth/2, point.y-20/2, kDefaultLabelWidth, 18);
	UILabel *item = [[UILabel alloc] initWithFrame:frame];
	item.textAlignment = NSTextAlignmentCenter;
	item.font = [UIFont fontWithName:@"OpenSans" size:11];
	item.textColor = [UIColor colorWithRed:(86.0f/255.0f) green:(93.0f/255.0f) blue:(103.0f/255.0f) alpha:1.0f];
	item.backgroundColor = [UIColor colorWithRed:(229.0f/255.0f) green:(231.0f/255.0f) blue:(234.0f/255.0f) alpha:1.0f];
	item.text = [NSString stringWithFormat:@"%@ %@", value, self.unit];
	item.alpha = 0.0f;
	item.layer.masksToBounds = YES;
	item.layer.backgroundColor = [UIColor colorWithRed:(229.0f/255.0f) green:(231.0f/255.0f) blue:(234.0f/255.0f) alpha:1.0f].CGColor;
	item.layer.borderColor = [UIColor clearColor].CGColor;
	item.layer.borderWidth = 0.0f;
	item.layer.cornerRadius = 5.0f;
	[self.optionalLabels addObject:item];
	[self addSubview:item];
}

- (CGFloat)_positionYForLineValue:(CGFloat)value
{
	CGFloat scale = 1;
	if ([self _maxValue] != [self _minValue])
		scale = (value - [self _minValue]) / ([self _maxValue] - [self _minValue]);
    CGFloat result = [self _plotHeight] * scale;
    result = ([self _plotHeight] -  result);
    result += kDefaultLabelHeight;
    return result;
}

- (UIBezierPath *)_bezierPathWith:(CGFloat)value {
    UIBezierPath *path = [UIBezierPath bezierPath];
    path.lineCapStyle = kCGLineCapRound;
    path.lineJoinStyle = kCGLineJoinRound;
    path.lineWidth = self.lineWidth;
    return path;
}

- (CAShapeLayer *)_layerWithPath:(UIBezierPath *)path {
    CAShapeLayer *item = [CAShapeLayer layer];
    item.fillColor = [[UIColor blackColor] CGColor];
    item.lineCap = kCALineCapRound;
    item.lineJoin  = kCALineJoinRound;
    item.lineWidth = self.lineWidth;
    item.strokeColor = [[UIColor redColor] CGColor];
    item.strokeEnd = 1;
    return item;
}

- (CABasicAnimation *)_animationWithKeyPath:(NSString *)keyPath {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:keyPath];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.duration = self.animationDuration;
    animation.fromValue = @(0);
    animation.toValue = @(1);
    return animation;
}

- (void)reset {
    [self _removeTitleLabels];
    [self _removeValueLabels];
	[self _removeOptionalLabels];
	self.layer.sublayers = nil;
}

@end
