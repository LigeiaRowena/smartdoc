//
//  Utils.m
//  SmartDoc
//
//  Created by Enrico on 10/09/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "Utils.h"

#pragma mark - Utility

@implementation Utils

+ (UIView*)findFirstResponderBeneathView:(UIView*)view {
    // Search recursively for first responder
    for ( UIView *childView in view.subviews ) {
        if ( [childView respondsToSelector:@selector(isFirstResponder)] && [childView isFirstResponder] ) return childView;
        UIView *result = [self findFirstResponderBeneathView:childView];
        if ( result ) return result;
    }
    return nil;
}

+ (void)findAndResignFirstResponderBeneathView:(UIView*)view {
    
    UIView *responderView = [self findFirstResponderBeneathView: view];
    [responderView resignFirstResponder];
}

#pragma mark - Plist Handler

+ (NSString *)getDocumentsDirectory
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}

+ (BOOL)copyPlistToDocumentFolder:(NSString*)fileName
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *documentsDirectory = [self getDocumentsDirectory];
	NSString *bundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:fileName];
	NSString *documentsPath = [documentsDirectory stringByAppendingPathComponent:fileName];
	NSError *error;
	
	if (([fileManager fileExistsAtPath:bundlePath]) && (![fileManager fileExistsAtPath:documentsPath]))
		return [fileManager copyItemAtPath:bundlePath toPath:documentsPath error:&error];
	else
		return FALSE;
}

+ (NSArray*)getArrayFromPlist:(NSString*)fileName
{
	NSString *documentsDirectory = [self getDocumentsDirectory];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:fileName];
	
	return [NSArray arrayWithContentsOfFile:filePath];
}

+ (void)setDictionary:(NSDictionary *)dictionary toPlist:(NSString *)fileName
{
	NSString *documentsDirectory = [self getDocumentsDirectory];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:fileName];
	[dictionary writeToFile: filePath atomically: YES];
}

+ (NSDictionary*)getDictionaryFromPlist:(NSString*)fileName
{
	NSString *documentsDirectory = [self getDocumentsDirectory];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:fileName];
	
	return [NSDictionary dictionaryWithContentsOfFile:filePath];
}

+ (id)getObjectFromPlist:(NSString*)fileName key:(NSString*)key
{
	NSDictionary *dictionary = [self getDictionaryFromPlist:fileName];
	
	return dictionary[key];
}

@end
