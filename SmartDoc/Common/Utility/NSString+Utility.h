//
//  NSString+Utility.h
//  SmartDoc
//
//  Created by Francesca Corsini on 16/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utility)

- (NSString *)stringByTrimmingWhitespace;
- (BOOL)isEqualToStringIgnoringCase:(NSString *)string;
- (BOOL)isBlank;
- (BOOL)isNotBlank;
- (BOOL)containsString:(NSString*)string array:(NSArray*)array;
- (BOOL)isRegularNumber;

@end
