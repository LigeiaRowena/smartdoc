//
//  Reminder.m
//  SmartDoc
//
//  Created by Francesca Corsini on 17/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "Reminder.h"
#import "NSDate-Utilities.h"

@implementation Reminder

static Reminder *instance;


+ (Reminder *) getIstance
{
	@synchronized(self)
	{
		if(instance == nil)
		{
            instance = [[Reminder alloc] init];
            return instance;
		}
	}
	return instance;
}

- (id)init
{
    if (self = [super init])
	{
    }
	return self;
}

- (void)deleteNotificationWithDate:(NSDate*)date info:(NSString*)info
{	
	NSArray *notifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
	for (UILocalNotification *notification in notifications)
	{
		if ([notification.userInfo[@"message"] isEqualToString:info] && [notification.fireDate isEqualToDateIgnoringTime:date])
			[[UIApplication sharedApplication] cancelLocalNotification:notification];
	}
}


- (void)addLocalNotificationWithDate:(NSDate*)date info:(NSString*)info
{
	//[self deleteNotificationWithDate:date info:info];
	[[UIApplication sharedApplication] cancelAllLocalNotifications];
	
	UILocalNotification *notification = [[UILocalNotification alloc] init];
	notification.fireDate = date;
	notification.timeZone = [NSTimeZone defaultTimeZone];
	notification.alertBody = @"Action to do";
	notification.alertAction = @"Show me";
	notification.soundName = UILocalNotificationDefaultSoundName;
	notification.applicationIconBadgeNumber = 0;
	notification.repeatInterval = 0;
	NSDictionary *userDict = @{@"message": info};
	notification.userInfo = userDict;
	
	[[UIApplication sharedApplication] scheduleLocalNotification:notification];
}


@end
