//
//  NSString+Utility.m
//  SmartDoc
//
//  Created by Francesca Corsini on 16/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "NSString+Utility.h"

@implementation NSString (Utility)

- (NSString *)stringByTrimmingWhitespace
{
	return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (BOOL)isEqualToStringIgnoringCase:(NSString *)string
{
	return [self compare:string options:NSCaseInsensitiveSearch] == NSOrderedSame;
}

- (BOOL)isBlank
{
	if (self == nil)
		return YES;
	if ([[self stringByTrimmingWhitespace] isEqualToString:@""])
		return YES;
	return NO;
}

- (BOOL)isNotBlank
{
	return ![self isBlank];
}

- (BOOL)containsString:(NSString*)string array:(NSArray*)array
{
	BOOL contains = FALSE;
	for (NSString *elem in array)
		if ([elem isEqualToString:string])
			contains = TRUE;
	return contains;
}

- (BOOL)isRegularNumber
{
    NSString *regex = @"^[0-9]*\\.?[0-9]+$";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [test evaluateWithObject:self];
}

@end
