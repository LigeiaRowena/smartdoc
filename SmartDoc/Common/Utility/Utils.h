//
//  Utils.h
//  SmartDoc
//
//  Created by Enrico on 10/09/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+ (UIView*)findFirstResponderBeneathView:(UIView*)view;
+ (void)findAndResignFirstResponderBeneathView:(UIView*)view;

+ (NSString *)getDocumentsDirectory;
+ (BOOL)copyPlistToDocumentFolder:(NSString*)fileName;
+ (NSArray*)getArrayFromPlist:(NSString*)fileName;
+ (NSDictionary*)getDictionaryFromPlist:(NSString*)fileName;
+ (id)getObjectFromPlist:(NSString*)fileName key:(NSString*)key;
+ (void)setDictionary:(NSDictionary *)dictionary toPlist:(NSString *)fileName;

@end
