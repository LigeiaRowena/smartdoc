//
//  Reminder.h
//  SmartDoc
//
//  Created by Francesca Corsini on 17/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Reminder : NSObject

+ (Reminder *) getIstance;
- (void)deleteNotificationWithDate:(NSDate*)date info:(NSString*)info;
- (void)addLocalNotificationWithDate:(NSDate*)date info:(NSString*)info;

@end
