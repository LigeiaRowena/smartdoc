//
//  EditItemViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 15/11/16.
//  Copyright © 2016 Francesca Corsini. All rights reserved.
//

#import "EditItemViewController.h"

@interface EditItemViewController ()

@end

@implementation EditItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
}

- (IBAction)edit:(id)sender
{
    [self parent:self.parentViewController removeViewController:self animateWithDuration:0.2];

    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(dismissEditView:object:)])
        [self.delegate dismissEditView:self.textField.text object:self.object];
}

#pragma mark - UITextFieldDelegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    // mette la prima maiuscola
    if (textField.text.length > 0)
        textField.text = [NSString stringWithFormat:@"%@%@",[[textField.text substringToIndex:1] uppercaseString],[textField.text substringFromIndex:1]];
    return YES;
}

@end
