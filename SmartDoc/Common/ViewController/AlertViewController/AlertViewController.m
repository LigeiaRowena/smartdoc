//
//  AlertViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 10/11/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "AlertViewController.h"

@interface AlertViewController ()

@end

@implementation AlertViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
}

- (void)hideCancelButton
{
	self.cancelButton.hidden = YES;
	self.actionButton.frame = CGRectMake(101, 180, self.actionButton.frame.size.width, self.actionButton.frame.size.height);
}

- (IBAction)selectCancelButton:(id)sender
{
	[self parent:self.parentViewController removeViewController:self animateWithDuration:0.2];

	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(alertViewController:clickedButtonAtIndex:)])
		[self.delegate alertViewController:self clickedButtonAtIndex:0];
}

- (IBAction)selectActionButton:(id)sender
{
	[self parent:self.parentViewController removeViewController:self animateWithDuration:0.2];

	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(alertViewController:clickedButtonAtIndex:)])
		[self.delegate alertViewController:self clickedButtonAtIndex:1];
}

- (IBAction)selectSecondActionButton:(id)sender
{
	[self parent:self.parentViewController removeViewController:self animateWithDuration:0.2];
	
	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(alertViewController:clickedButtonAtIndex:)])
		[self.delegate alertViewController:self clickedButtonAtIndex:2];
}

@end
