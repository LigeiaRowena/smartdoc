//
//  AlertViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 10/11/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"

@class AlertViewController;

@protocol AlertViewControllerDelegate <NSObject>
@optional
- (void)alertViewController:(AlertViewController *)alertViewController clickedButtonAtIndex:(NSInteger)buttonIndex;
@end

@interface AlertViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UIView *alert;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *messageLabel;
@property (nonatomic, weak) IBOutlet UIButton *cancelButton;
@property (nonatomic, weak) IBOutlet UIButton *actionButton;
@property (nonatomic, weak) IBOutlet UIButton *secondActionButton;

@property (nonatomic, weak) id<AlertViewControllerDelegate> delegate;

- (void)hideCancelButton;

@end
