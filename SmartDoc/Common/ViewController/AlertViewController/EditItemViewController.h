//
//  EditItemViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 15/11/16.
//  Copyright © 2016 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"
#import "CustomTextField.h"

@protocol EditItemViewControllerDelegate <NSObject>
- (void)dismissEditView:(NSString*)text object:(id)object;
@end

@interface EditItemViewController : BaseViewController

@property (nonatomic, weak) IBOutlet id<EditItemViewControllerDelegate> delegate;
@property (nonatomic, weak) IBOutlet CustomTextField *textField;
@property (nonatomic, strong) id object;
@property (nonatomic, weak) IBOutlet UIView *editView;


@end
