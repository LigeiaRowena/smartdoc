//
//  InfoViewController.h
//  SmartDoc
//
//  Created by Enrico on 11/09/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"
#import "Settings.h"
#import "PickerViewController.h"
#import "MenuView.h"

@interface SettingsViewController : BaseViewController <PickerViewControllerDelegate, MenuViewDelegate>

@property (nonatomic, strong) Settings *settings;


@end
