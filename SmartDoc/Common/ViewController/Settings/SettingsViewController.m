//
//  InfoViewController.m
//  SmartDoc
//
//  Created by Enrico on 11/09/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "SettingsViewController.h"
#import "NSString+Utility.h"
#import "DBManager.h"
#import "CustomTextField.h"

@interface SettingsViewController ()
{
	UIPopoverController *popover;
	NSNumberFormatter *formatter;
}

@property (nonatomic, weak) IBOutlet UILabel *infoLabel;
@property (nonatomic, weak) IBOutlet UILabel *descLabel;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *firtnameLabel;
@property (nonatomic, weak) IBOutlet UILabel *lastnameLabel;
@property (nonatomic, weak) IBOutlet UILabel *orgLabel;
@property (nonatomic, weak) IBOutlet UILabel *phoneworkLabel;
@property (nonatomic, weak) IBOutlet UILabel *phonemobileLabel;
@property (nonatomic, weak) IBOutlet UILabel *emailLabel;
@property (nonatomic, weak) IBOutlet UILabel *addressLabel;
@property (nonatomic, weak) IBOutlet UILabel *webLabel;
@property (nonatomic, weak) IBOutlet UILabel *specLabel;
@property (weak, nonatomic) IBOutlet PickerField *titleText;
@property (weak, nonatomic) IBOutlet CustomTextField *firtnameText;
@property (weak, nonatomic) IBOutlet CustomTextField *lastnameText;
@property (weak, nonatomic) IBOutlet CustomTextField *orgText;
@property (weak, nonatomic) IBOutlet CustomTextField *phoneworkText;
@property (weak, nonatomic) IBOutlet CustomTextField *phonemobileText;
@property (weak, nonatomic) IBOutlet CustomTextField *emailText;
@property (weak, nonatomic) IBOutlet CustomTextField *addressText;
@property (weak, nonatomic) IBOutlet CustomTextField *webText;
@property (weak, nonatomic) IBOutlet PickerField *specText;
@property (weak, nonatomic) IBOutlet PickerField *appointmentText;
@property (weak, nonatomic) IBOutlet MenuView *systemMetricsMenu;

@end

#pragma mark - Init SettingsViewController

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	// menu system metrics
	if ([[DBManager getIstance] isMetricSystem])
		[self.systemMetricsMenu selectItemAtIndex:0];
	if ([[DBManager getIstance] isImperialSystem])
		[self.systemMetricsMenu selectItemAtIndex:1];
	else
	{
		[[DBManager getIstance] setMetricSystem:MetricSystem];
		[self.systemMetricsMenu selectItemAtIndex:0];
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
	formatter = [[NSNumberFormatter alloc] init];
	
	// data
	self.settings = [[DBManager getIstance] getSettings];
	
	// textfields
	self.titleText.textField.text = self.settings.title;
	self.firtnameText.text = self.settings.firstName;
	self.lastnameText.text = self.settings.lastName;
	self.orgText.text = self.settings.organization;
	self.phoneworkText.text = self.settings.phoneWork;
	self.phonemobileText.text = self.settings.phoneMobile;
	self.emailText.text = self.settings.email;
	self.addressText.text = self.settings.address;
	self.webText.text = self.settings.web;
	self.specText.textField.text = self.settings.speciality;
	self.appointmentText.textField.text = [NSString stringWithFormat:@"%@ min", [formatter stringFromNumber:self.settings.appointmentDuration]];
}

#pragma mark - MenuViewDelegate

- (void)menuDidSelectItemAtIndex:(int)index
{
	if (index == 0)
	{
		[[DBManager getIstance] setMetricSystem:MetricSystem];

	}
	else if (index == 1)
	{
		[[DBManager getIstance] setMetricSystem:ImperialSystem];
	}
}


#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	if (![self.view.gestureRecognizers containsObject:self.tap]) {
		[self.view addGestureRecognizer:self.tap];
	}
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	
	if ([self.view.gestureRecognizers containsObject:self.tap]) {
		[self.view removeGestureRecognizer:self.tap];
	}
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	if (textField == self.emailText || textField == self.webText)
		return YES;
	else
		return [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
}

#pragma mark - PickerViewControllerDelegate Methods

- (void)pickerViewControllerSetText:(NSString*)text pickerField:(PickerField*)pickerField
{
	pickerField.textField.text = text;
}

#pragma mark - PickerFieldDelegate Methods

- (void)tapOnPickerFieldButton:(PickerField*)pickerField
{
	PickerViewController *pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
	pickerViewController.pickerField = pickerField;
	pickerViewController.delegate = self;
	
	if (pickerField == self.titleText)
	{
		NSArray *list = [self getArrayFromPlist:PLIST_FILE key:@"Title"];
		for(NSString *title in list)
			[pickerViewController.data addObject:title];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = StaticField;
	}
	
	else if (pickerField == self.specText)
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:SpecData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = SpecData;
	}
	
	if (pickerField == self.appointmentText)
	{
		NSArray *list = [self getArrayDurations];
		for(NSString *title in list)
			[pickerViewController.data addObject:title];
		[pickerViewController.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		pickerViewController.pickerType = StaticField;
	}
	
	popover = [[UIPopoverController alloc] initWithContentViewController:pickerViewController];
	[popover setPopoverContentSize:pickerViewController.view.frame.size];
	[pickerViewController setPopover:popover];
	[popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma mark - Actions


- (IBAction)save:(id)sender
{
	self.settings.title = self.titleText.textField.text;
	self.settings.firstName = self.firtnameText.text;
	self.settings.lastName = self.lastnameText.text;
	self.settings.organization = self.orgText.text;
	self.settings.phoneWork = self.phoneworkText.text;
	self.settings.phoneMobile = self.phonemobileText.text;
	self.settings.email = self.emailText.text;
	self.settings.address = self.addressText.text;
	self.settings.web = self.webText.text;
	self.settings.speciality = self.specText.textField.text;
	if (self.appointmentText) {
		int intero = [self.appointmentText.textField.text intValue];
		self.settings.appointmentDuration = [NSNumber numberWithInt:intero];
	}
	
	[[DBManager getIstance] saveContext];
	
	if ((self.firtnameText.mandatory && [BaseViewController isEmptyString:self.firtnameText.text]) || (self.lastnameText.mandatory && [BaseViewController isEmptyString:self.lastnameText.text]) || (self.emailText.mandatory && [BaseViewController isEmptyString:self.emailText.text]))
	{
		[[AlertViewBlocks getIstance] warningAlertViewWithMessage:@"Please fill all the mandatory fields before saving." confirmBlock:^{
		}];
	}
	else
	{
		[[DBManager getIstance] getSettings].firstLaunch = @NO;
		[[DBManager getIstance] saveContext];
		[self parent:self.parentViewController removeViewController:self animateWithDuration:0.2];
	}
}

- (IBAction)cancel:(id)sender
{
	[self parent:self.parentViewController removeViewController:self animateWithDuration:0.2];
}

- (IBAction)syncDB:(id)sender
{
	//TODO: cambiare stile bottone da photoshop (icona cloud e testo)
	NSString *filePath = [[DBManager getIstance] modelPathString];
	[[AlertViewBlocks getIstance] twoOptionsAlertViewWithmessage:@"Send DB to the cloud or sync DB from the cloud?" firstButtonTitle:@"Backup local app data to the cloud" secondButtonTitle:@"Download backup data from the cloud" confirmBlock:^{
		// send db to the cloud
		[AFRequests uploadDBFile:filePath success:^(NSURLResponse *response, id responseObject) {
			[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Backup local app data to the cloud successful" confirmBlock:^{
			}];
		} failure:^(NSString *error) {
			[[AlertViewBlocks getIstance] warningAlertViewWithMessage:error confirmBlock:^{
			}];
		}];
		
	} secondConfirm:^{
		// download db from the cloud
		[self recoverDBFromCloud];
		
	} cancelBlock:^{
	}];
}

@end
