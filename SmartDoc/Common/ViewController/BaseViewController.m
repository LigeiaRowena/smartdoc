//
//  SmartDoc
//
//  Created by Francesca Corsini on 18/05/13.
//  Copyright (c) 2013 Andrea. All rights reserved.
//

#import "BaseViewController.h"
#import "Settings.h"
#import "PDFExporter.h"

#pragma mark - Init BaseViewController

#define kBorderInset 20.0
#define kMarginInset 10.0
#define kPDFFileName @"file.pdf"

@implementation BaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		self.tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		self.tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// set the background color
	self.view.backgroundColor = lightGrayBgColor();
	self.mainscroll.backgroundColor = lightGrayBgColor();
	
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.frame.size.width, self.mainscroll.frame.size.height + 30);
	
	self.formatter = [[NSDateFormatter alloc] init] ;
	[self.formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
	[self.formatter setDateFormat:@"dd/MM/yyyy"];
	
	// UIImagePickerController
	self.imagePickerController = [[UIImagePickerController alloc] init];
	self.imagePickerController.modalPresentationStyle = UIModalPresentationFullScreen;
	self.imagePickerController.allowsEditing = NO;
	self.imagePickerController.delegate = self;
	self.imagePickerController.videoQuality = UIImagePickerControllerQualityTypeHigh;
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	[self setup];
}

- (void)setup
{
}

- (void)dismissKeyboard:(id)sender {
	
	[self.view endEditing:YES];
	if ([self.view.gestureRecognizers containsObject:self.tap]) {
		[self.view removeGestureRecognizer:self.tap];
	}
}


#pragma mark - Utility

+ (BOOL)isEmptyObject:(id)object
{
    if (object == nil || [object isEqual:[NSNull null]] || [object count] == 0)
        return YES;
    else
        return NO;
}

+ (BOOL)isEmptyNumber:(NSDecimalNumber*)number
{
	if ([number isEqualToNumber:[NSNumber numberWithInt:0]])
		return YES;
	else if ([number isEqual:[NSNull null]])
		return YES;
	else
		return NO;
}

+ (BOOL)isEmptyString:(NSString *)string
{
    if (string == nil || [string isEqual:[NSNull null]])
        return YES;
	else if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""])
		return YES;
    else
        return NO;
}

+ (BOOL)isValidNumber:(NSString*)number
{
	NSString *numberRegEx = @"^[0-9]+$";
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
	return [predicate evaluateWithObject:number];
}



- (NSArray*)getArrayFromPlist:(NSString*)plist key:(NSString*)key
{
	NSString *path = [[NSBundle mainBundle] pathForResource:[plist stringByDeletingPathExtension] ofType:[plist pathExtension]];
	return [NSDictionary dictionaryWithContentsOfFile:path][key];
}

- (NSArray*)getArrayDurations
{
	NSMutableArray *durations = @[].mutableCopy;
	for (int duration = 15; duration <= 60*24; duration = duration+15)
	{
		[durations addObject:[NSString stringWithFormat:@"%i min", duration]];
	}
	return durations;
}

#pragma mark - CustomTextViewDelegate Methods

- (void)customTextViewDidChange:(CustomTextView *)textView
{
	// mette la prima maiuscola
	if (textView.textView.text.length > 0)
		textView.textView.text = [NSString stringWithFormat:@"%@%@",[[textView.textView.text substringToIndex:1] uppercaseString],[textView.textView.text substringFromIndex:1]];
}

- (void)customViewDidBeginEditing:(UIView *)superView
{
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + KEYBOARD_HEIGHT);
	if (![self itemIsVisible:superView])
		[self.mainscroll setContentOffset:CGPointMake(self.mainscroll.contentOffset.x, self.mainscroll.contentOffset.y + KEYBOARD_HEIGHT) animated:YES];
	
	if (![self.view.gestureRecognizers containsObject:self.tap]) {
		[self.view addGestureRecognizer:self.tap];
	}
}

- (void)customTextViewDidBeginEditing:(CustomTextView *)textView
{
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + KEYBOARD_HEIGHT);
	if (![self itemIsVisible:textView])
		[self.mainscroll setContentOffset:CGPointMake(self.mainscroll.contentOffset.x, self.mainscroll.contentOffset.y + KEYBOARD_HEIGHT) animated:YES];

	if (![self.view.gestureRecognizers containsObject:self.tap]) {
		[self.view addGestureRecognizer:self.tap];
	}
}

- (void)customTextViewDidEndEditing:(CustomTextView *)textView
{
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height - KEYBOARD_HEIGHT);

	[textView resignFirstResponder];
	
	if ([self.view.gestureRecognizers containsObject:self.tap]) {
		[self.view removeGestureRecognizer:self.tap];
	}
}

- (void)customViewDidEndEditing:(UIView *)superView textView:(CustomTextView*)textView
{
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height - KEYBOARD_HEIGHT);
	
	[textView resignFirstResponder];
	
	if ([self.view.gestureRecognizers containsObject:self.tap]) {
		[self.view removeGestureRecognizer:self.tap];
	}
}


#pragma mark - PickerFieldDelegate Methods

- (void)pickerFieldDidBeginEditing:(PickerField *)pickerField
{
	if (![self.view.gestureRecognizers containsObject:self.tap])
		[self.view addGestureRecognizer:self.tap];
}

- (void)pickerFieldDidEndEditing:(PickerField *)pickerField
{
	if ([self.view.gestureRecognizers containsObject:self.tap])
		[self.view removeGestureRecognizer:self.tap];
}


#pragma mark - UITextFieldDelegate Methods

- (BOOL)itemIsVisible:(UIView*)item
{
	BOOL isVisible = NO;
	
	float beginY = self.mainscroll.contentOffset.y;
	float endY = self.mainscroll.contentOffset.y + self.mainscroll.frame.size.height - KEYBOARD_HEIGHT;
	if (item.frame.origin.y >= beginY && item.frame.origin.y + 150 <= endY)
		isVisible = YES;
	
	return isVisible;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	if (self.mainscroll != nil)
	{
		self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height + KEYBOARD_HEIGHT);
		UIView *superView = [self getSuperViewFromTextField:textField];
		if (![self itemIsVisible:superView])
			[self.mainscroll setContentOffset:CGPointMake(self.mainscroll.contentOffset.x, self.mainscroll.contentOffset.y + KEYBOARD_HEIGHT) animated:YES];
	}
	
	if (![self.view.gestureRecognizers containsObject:self.tap]) {
		[self.view addGestureRecognizer:self.tap];
	}
}

- (UIView*)getSuperViewFromTextField:(UITextField*)textField
{
	UIView *superView = textField;
	if ([superView.superview isEqual:self.mainscroll])
		return superView;
	do {
		superView = superView.superview;
	}
	while (![superView.superview isEqual:self.mainscroll]);	
	return superView;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	self.mainscroll.contentSize = CGSizeMake(self.mainscroll.contentSize.width, self.mainscroll.contentSize.height - KEYBOARD_HEIGHT);
	
	if (!CGPointEqualToPoint(self.mainscroll.contentOffset, CGPointZero))
		[self.mainscroll setContentOffset:CGPointMake(self.mainscroll.contentOffset.x, self.mainscroll.contentOffset.y - KEYBOARD_HEIGHT) animated:YES];


	if ([self.view.gestureRecognizers containsObject:self.tap]) {
		[self.view removeGestureRecognizer:self.tap];
	}
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    
    [textField resignFirstResponder];
	
	if ([self.view.gestureRecognizers containsObject:self.tap]) {
		[self.view removeGestureRecognizer:self.tap];
	}
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
	
	if ([self.view.gestureRecognizers containsObject:self.tap]) {
		[self.view removeGestureRecognizer:self.tap];
	}
	
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	
	// mette la prima maiuscola
	if (textField.text.length > 0)
		textField.text = [NSString stringWithFormat:@"%@%@",[[textField.text substringToIndex:1] uppercaseString],[textField.text substringFromIndex:1]];

	return YES;
}

#pragma mark - Email Methods

- (void)sendPDFFromText:(NSString*)body title:(NSString*)title
{
	Settings *settings = [[DBManager getIstance] getSettings];
	if (![MFMailComposeViewController canSendMail])
	{
		[[AlertViewBlocks getIstance] warningAlertViewWithMessage:@"Your device is not able to send emails." confirmBlock:^{
		}];
	}
	if ([BaseViewController isEmptyString:settings.email])
	{
		[[AlertViewBlocks getIstance] warningAlertViewWithMessage:@"You have to set a proper email in the Settings section." confirmBlock:^{
		}];
	}
	if ([MFMailComposeViewController canSendMail] && ![BaseViewController isEmptyString:settings.email])
	{
		MFMailComposeViewController *mcController = [[MFMailComposeViewController alloc] init];
		mcController.mailComposeDelegate = self;
		if (![BaseViewController isEmptyString:body])
			[mcController setMessageBody:body isHTML:NO];
		[mcController setSubject: title];
		NSArray *elencoRicezione = @[settings.email];
		[mcController setToRecipients:elencoRicezione];
		[self clearDocuments];
		NSString *fileName = [self generatePDF:body];
		[mcController addAttachmentData:[NSData dataWithContentsOfFile:fileName] mimeType:@"application/pdf" fileName:kPDFFileName];
		mcController.mailComposeDelegate = self;
		[self presentViewController:mcController animated:YES completion:nil];
	}
}

- (void)sendPDFFromOCRtitle:(NSString*)title
{
	Settings *settings = [[DBManager getIstance] getSettings];
	if (![MFMailComposeViewController canSendMail])
	{
		[[AlertViewBlocks getIstance] warningAlertViewWithMessage:@"Your device is not able to send emails." confirmBlock:^{
		}];
	}
	if ([BaseViewController isEmptyString:settings.email])
	{
		[[AlertViewBlocks getIstance] warningAlertViewWithMessage:@"You have to set a proper email in the Settings section." confirmBlock:^{
		}];
	}
	if ([MFMailComposeViewController canSendMail] && ![BaseViewController isEmptyString:settings.email])
	{
		MFMailComposeViewController *mcController = [[MFMailComposeViewController alloc] init];
		mcController.mailComposeDelegate = self;
		[mcController setSubject: title];
		NSArray *elencoRicezione = @[settings.email];
		[mcController setToRecipients:elencoRicezione];
		[self clearDocuments];
		NSString *fileName = [self generatePDFFromScreenshot];
		[mcController addAttachmentData:[NSData dataWithContentsOfFile:fileName] mimeType:@"application/pdf" fileName:kPDFFileName];
		mcController.mailComposeDelegate = self;
		[self presentViewController:mcController animated:YES completion:nil];
	}
}

- (void)sendPDFtitle:(NSString*)title path:(NSString*)path
{
	Settings *settings = [[DBManager getIstance] getSettings];
	if (![MFMailComposeViewController canSendMail])
	{
		[[AlertViewBlocks getIstance] warningAlertViewWithMessage:@"Your device is not able to send emails." confirmBlock:^{
		}];
	}
	if ([BaseViewController isEmptyString:settings.email])
	{
		[[AlertViewBlocks getIstance] warningAlertViewWithMessage:@"You have to set a proper email in the Settings section." confirmBlock:^{
		}];
	}
	if ([MFMailComposeViewController canSendMail] && ![BaseViewController isEmptyString:settings.email])
	{
		MFMailComposeViewController *mcController = [[MFMailComposeViewController alloc] init];
		mcController.mailComposeDelegate = self;
		[mcController setSubject: title];
		NSArray *elencoRicezione = @[settings.email];
		[mcController setToRecipients:elencoRicezione];
		[mcController addAttachmentData:[NSData dataWithContentsOfFile:path] mimeType:@"application/pdf" fileName:kPDFFileName];
		[self presentViewController:mcController animated:YES completion:nil];
	}
}

#pragma mark - PDF Methods

- (void)clearDocuments
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pdfFileName = [documentsDirectory stringByAppendingPathComponent:kPDFFileName];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error = nil;
	if ([fileManager fileExistsAtPath:pdfFileName])
		[fileManager removeItemAtPath:pdfFileName error:&error];
}

- (NSString*)generatePDF:(NSString*)body
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pdfFileName = [documentsDirectory stringByAppendingPathComponent:kPDFFileName];
	
	UIGraphicsBeginPDFContextToFile(pdfFileName, CGRectZero, nil);
	CGSize stringSize = [self getSizeText:body];
    BOOL done = NO;
    do
    {
        UIGraphicsBeginPDFPageWithInfo(CGRectMake(-(kBorderInset + kMarginInset)/2, kBorderInset + kMarginInset, stringSize.width + 4*(kBorderInset + kMarginInset), stringSize.height), nil);
		[self drawText:body];
        done = YES;
    }
    while (!done);
    UIGraphicsEndPDFContext();
	
	return pdfFileName;
}

- (CGSize)getSizeText:(NSString*)text
{
	NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:12.0]};
	CGSize stringSize = [text boundingRectWithSize:CGSizeMake(612 - 2*kBorderInset-2*kMarginInset, MAXFLOAT - 2*kBorderInset - 2*kMarginInset) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
	return stringSize;
}

- (void)drawText:(NSString*)text
{
    CGContextRef  currentContext = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(currentContext, 0.0, 0.0, 0.0, 1.0);
    CGSize stringSize = [self getSizeText:text];
    CGRect renderingRect = [self getFrame:stringSize];
	
	NSMutableParagraphStyle* style = NSMutableParagraphStyle.defaultParagraphStyle.mutableCopy;
	style.alignment = NSTextAlignmentLeft;
	NSDictionary* attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:12.0], NSForegroundColorAttributeName: [UIColor blackColor], NSParagraphStyleAttributeName: style};
	[text drawInRect:renderingRect withAttributes:attributes];
}

- (NSString*)generatePDFFromScreenshot
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pdfFileName = [documentsDirectory stringByAppendingPathComponent:kPDFFileName];
	
	UIGraphicsBeginPDFContextToFile(pdfFileName, CGRectZero, nil);
	CGSize scrollSize = self.mainscroll.contentSize;
	int pages = scrollSize.height / [UIScreen mainScreen].bounds.size.height + 1;
	for (int i = 0; i < pages; i++)
	{
		UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height), nil);
		UIImage * demoImage = [self generateUIImage];
		[demoImage drawInRect:CGRectMake(0, kMarginInset, demoImage.size.width, demoImage.size.height - kMarginInset*2)];
		[self.mainscroll setContentOffset:CGPointMake(self.mainscroll.contentOffset.x, self.mainscroll.contentOffset.y + [UIScreen mainScreen].bounds.size.height)];
	}
	UIGraphicsEndPDFContext();
	
	
	return pdfFileName;
}

- (UIImage*)generateUIImage
{
	UIGraphicsBeginImageContext(self.mainscroll.contentSize);
	[self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
	
	return viewImage;
}

- (CGRect)getFrame:(CGSize)size
{
    CGRect renderingRect = CGRectMake(kBorderInset + kMarginInset, kBorderInset + kMarginInset, size.width + 2*(kBorderInset + kMarginInset), size.height + 2*(kBorderInset + kMarginInset) + 500);
	return renderingRect;
}

#pragma mark - Print Methods


- (void)printPDFWithActivity:(NSString*)path anchor:(UIButton*)anchor
{
	NSURL *url = [NSURL fileURLWithPath:path];
	NSArray *activityItems = @[url];
	UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
	activityViewController.excludedActivityTypes = nil;
	
	UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:activityViewController];
	[popover presentPopoverFromRect:anchor.bounds inView:anchor permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)printPDF:(NSString*)path anchor:(UIView*)anchor
{
	NSData *myData = [NSData dataWithContentsOfFile:path];

	UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
	pic.delegate = self;
	
	if (pic && [UIPrintInteractionController canPrintData: myData])
	{		
		UIPrintInfo *printInfo = [UIPrintInfo printInfo];
		printInfo.outputType = UIPrintInfoOutputGeneral;
		printInfo.jobName = @"SmartApp";
		pic.printInfo = printInfo;

		printInfo.duplex = UIPrintInfoDuplexLongEdge;
		pic.printInfo = printInfo;
		pic.showsPageRange = YES;
		pic.printingItem = myData;
		
		[pic presentFromRect:anchor.bounds inView:anchor animated:YES completionHandler:^(UIPrintInteractionController *printInteractionController, BOOL completed, NSError *error) {
			if (!completed && error)
				NSLog(@"Printing could not complete because of error: %@", error);
		}];
	}
}

- (void)printText:(NSString*)text anchor:(UIView*)anchor
{
	UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    pic.delegate = self;
	
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    printInfo.jobName = @"SmartApp";
    pic.printInfo = printInfo;
	
    UISimpleTextPrintFormatter *textFormatter = [[UISimpleTextPrintFormatter alloc] initWithText:text];
    textFormatter.startPage = 0;
    textFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0);
    textFormatter.maximumContentWidth = 6 * 72.0;
    pic.printFormatter = textFormatter;
    pic.showsPageRange = YES;
	
	[pic presentFromRect:anchor.bounds inView:anchor animated:YES completionHandler:^(UIPrintInteractionController *printInteractionController, BOOL completed, NSError *error) {
		if (!completed && error)
			NSLog(@"Printing could not complete because of error: %@", error);
	}];
}


#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
	[self dismissViewControllerAnimated:YES completion:nil];
	[self.mainscroll setContentOffset:CGPointMake(0, 0)];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
	[self dismissViewControllerAnimated:YES completion:nil];
	[self.mainscroll setContentOffset:CGPointMake(0, 0)];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
	if (!error)
		[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Image successfully saved in Photos app" confirmBlock:^{
		}];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
	[self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - CameraViewDelegate

- (void)showCamera:(NSNumber*)index
{
	// mostra imagePickerController come camera
 	self.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
	self.imagePickerController.showsCameraControls = NO;
	self.overlayView = [[CameraView alloc] initWithFrame:self.imagePickerController.cameraOverlayView.frame];
	self.overlayView.delegate = self;
	self.imagePickerController.cameraOverlayView = self.overlayView;
	self.overlayView = nil;
	self.imagePickerController.index = index;
	[self presentViewController:self.imagePickerController animated:YES completion:nil];
}

- (void)showPhotos:(NSNumber*)index
{
	// mostra imagePickerController come photos
	self.imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
	self.imagePickerController.index = index;
	[self presentViewController:self.imagePickerController animated:YES completion:nil];
}

- (void)takePicture
{
	// scatta foto dalla camera
	[self.imagePickerController takePicture];
}

- (void)cancel
{
	// esci dalla camera
	[self dismissViewControllerAnimated:YES completion:nil];
}


- (void)recoverDBFromCloud
{
	// download the db from the cloud
	[AFRequests downloadDBFileSuccess:^(NSURLResponse *response, id responseObject) {
		[[AlertViewBlocks getIstance] informationConfirmAlertViewWithmessage:@"The local app data will be overwritten with the data from the cloud" confirmBlock:^{
			// delete the local db
			NSURL *filePath = [[DBManager getIstance] modelPathURL];
			if ([[NSFileManager defaultManager] fileExistsAtPath:filePath.path]) {
				NSError *error = nil;
				[[NSFileManager defaultManager] removeItemAtURL:filePath error:&error];
				if (error)
					NSLog(@"error %@", error);
			}
			// delete the local shm file
			NSURL *shmFilePath = [[DBManager getIstance] shmPathURL];
			if ([[NSFileManager defaultManager] fileExistsAtPath:shmFilePath.path]) {
				NSError *error = nil;
				[[NSFileManager defaultManager] removeItemAtURL:shmFilePath error:&error];
				if (error)
					NSLog(@"error %@", error);
			}
			// delete the local wal file
			NSURL *walFilePath = [[DBManager getIstance] walPathURL];
			if ([[NSFileManager defaultManager] fileExistsAtPath:walFilePath.path]) {
				NSError *error = nil;
				[[NSFileManager defaultManager] removeItemAtURL:walFilePath error:&error];
				if (error)
					NSLog(@"error %@", error);
			}
			// overwrite the local db with the db from the cloud
			NSURL *fileCloudPath = [[DBManager getIstance] modelCloudPathURL];
			if ([[NSFileManager defaultManager] fileExistsAtPath:fileCloudPath.path]) {
				NSError *error = nil;
				[[NSFileManager defaultManager] moveItemAtURL:fileCloudPath toURL:filePath error:&error];
				if (error)
					NSLog(@"error %@", error);
			}
		} cancelBlock:^{
			NSURL *filePath = [[DBManager getIstance] modelCloudPathURL];
			if ([[NSFileManager defaultManager] fileExistsAtPath:filePath.path]) {
				NSError *error = nil;
				[[NSFileManager defaultManager] removeItemAtURL:filePath error:&error];
				if (error)
					NSLog(@"error %@", error);
			}
		}];
	} failure:^(NSString *error) {
		[[AlertViewBlocks getIstance] warningAlertViewWithMessage:error confirmBlock:^{
		}];
	}];
}


@end
