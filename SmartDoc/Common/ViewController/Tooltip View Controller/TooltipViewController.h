//
//  TooltipViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 26/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"

@interface TooltipViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UILabel *infoLabel;
@property (nonatomic, strong) NSString *info;

@end
