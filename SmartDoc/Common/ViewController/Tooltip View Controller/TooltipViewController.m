//
//  TooltipViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 26/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "TooltipViewController.h"
#import "PopoverBackgroundView.h"

@interface TooltipViewController ()
@end

@implementation TooltipViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.view.backgroundColor = tooltipGrayColor();
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	// popover
	self.popover.backgroundColor = [UIColor clearColor];
	self.popover.popoverLayoutMargins = UIEdgeInsetsZero;
	self.popover.popoverBackgroundViewClass = [PopoverBackgroundView class];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	self.infoLabel.text = self.info;
}


@end
