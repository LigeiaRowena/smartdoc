//
//  UIViewController+RadioRai.m
//  RadioRai
//
//  Created by Francesca Corsini on 10/02/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "UIViewController+Transitions.h"
#import <objc/runtime.h>


#define kStatusBar 20

@implementation UIViewController (Transitions)


#pragma mark - IOS7 Utility

- (float)getDelta
{
	return kStatusBar;
}

#pragma mark - Navigation UIViewController

// get the root view controller
- (UIViewController *)getRootController
{
	UIWindow *win = [[[UIApplication sharedApplication] delegate] window];
	UIViewController *cntr = win.rootViewController;
	if (cntr != nil)
		return cntr;
	else
		return nil;
}

// update the frame of the mainview of the child viewcontroller
// with autolayout or autoresizemask
- (void)updateFrameContentView:(UIView*)contentView containerView:(UIView*)containerView position:(CGPoint)position
{
	contentView.frame = containerView.bounds;
}


// remove a child view controller with animation
- (void)parent:(UIViewController*)parent removeViewController:(UIViewController*)viewController animateWithDuration:(float)duration
{
	if (viewController != nil)
	{
		[self removeSubchildsFrom:viewController];
		[viewController willMoveToParentViewController:nil];
		[UIView animateWithDuration:0.35 animations:^{
			viewController.view.alpha = 0;
		} completion:^(BOOL finished) {
			[viewController.view removeFromSuperview];
			[viewController removeFromParentViewController];
		}];
	}
}


// remove a child view controller with animation of pop
- (void)parent:(UIViewController*)parent popViewController:(UIViewController*)viewController animateWithDuration:(float)duration
{
	if (viewController != nil)
	{
		[self removeSubchildsFrom:viewController];
		CATransition* transition = [CATransition animation];
		transition.duration = 0.5;
		transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
		transition.type = kCATransitionPush;
		transition.subtype = kCATransitionFromLeft;
		[viewController willMoveToParentViewController:nil];
		[viewController.view removeFromSuperview];
		[viewController removeFromParentViewController];
		[parent.view.layer addAnimation:transition forKey:kCATransition];
	}
}

// remove a child view controller without animation
- (void)parent:(UIViewController*)parent removeViewController:(UIViewController*)viewController
{
	if (viewController != nil)
	{
		[self removeSubchildsFrom:viewController];
		[viewController willMoveToParentViewController:nil];
		[viewController.view removeFromSuperview];
		[viewController removeFromParentViewController];
	}
}

// remove all subchilds from a view controller
- (void)removeSubchildsFrom:(UIViewController*)viewController
{
	for (UIViewController* child in viewController.childViewControllers)
	{
		[child willMoveToParentViewController:nil];
		[child.view removeFromSuperview];
        [child removeFromParentViewController];
	}
}

// add a child view controller with animation
- (void)parent:(UIViewController*)parent addChildViewController:(UIViewController*)viewController superView:(UIView*)superView animateWithDuration:(float)duration
{
	if (viewController != nil && parent != nil)
	{
		[parent addChildViewController:viewController];
		[viewController didMoveToParentViewController:parent];
		viewController.view.alpha = 0;
		[superView addSubview:viewController.view];
		[UIView animateWithDuration:0.35 animations:^{
			viewController.view.alpha = 1;
		}];
		[self updateFrameContentView:viewController.view containerView:superView position:CGPointZero];
	}
}

// add a child view controller without animation with a particular position
- (void)parent:(UIViewController*)parent addChildViewController:(UIViewController*)viewController superView:(UIView*)superView position:(CGPoint)position
{
	if (viewController != nil && parent != nil)
	{
		viewController.view.frame = superView.bounds;
		[parent addChildViewController:viewController];
		[viewController didMoveToParentViewController:parent];
		[superView addSubview:viewController.view];
		[self updateFrameContentView:viewController.view containerView:superView position:position];
	}
}

// add a child view controller without animation
- (void)parent:(UIViewController*)parent addChildViewController:(UIViewController*)viewController superView:(UIView*)superView
{
	if (viewController != nil && parent != nil)
	{
		viewController.view.frame = superView.bounds;
		[parent addChildViewController:viewController];
		[viewController didMoveToParentViewController:parent];
		[superView addSubview:viewController.view];
		[self updateFrameContentView:viewController.view containerView:superView position:CGPointZero];
	}
}

// push a child view controller
- (void)parent:(UIViewController*)parent pushViewController:(UIViewController*)viewController superView:(UIView*)superView animateWithDuration:(float)duration
{
	if (viewController != nil && parent != nil)
	{
		viewController.view.frame = superView.bounds;
		CATransition* transition = [CATransition animation];
		transition.duration = 0.5;
		transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
		transition.type = kCATransitionPush;
		transition.subtype = kCATransitionFromRight;
		[parent addChildViewController:viewController];
		[viewController didMoveToParentViewController:parent];
		[superView addSubview:viewController.view];
		[parent.view.layer addAnimation:transition forKey:kCATransition];
		[self updateFrameContentView:viewController.view containerView:superView position:CGPointZero];
	}
}


// transition from 2 view controllers moving from right to left side (push style)
- (void)parent:(UIViewController*)parent pushToViewController:(UIViewController*)destination fromViewController:(UIViewController*)source superView:(UIView*)superView animateWithDuration:(float)duration
{
	if (parent != nil && source != nil && destination != nil)
	{
		destination.view.frame = superView.bounds;
		[destination.view setTransform:CGAffineTransformMakeTranslation(source.view.frame.size.width, 0)];
		[source willMoveToParentViewController:nil];
		[parent addChildViewController:destination];
		
		[parent transitionFromViewController: source toViewController: destination duration: 0.35 options:UIViewAnimationOptionCurveEaseIn animations:^{
			[destination.view setTransform:CGAffineTransformIdentity];
			[source.view setTransform:CGAffineTransformMakeTranslation(-source.view.frame.size.width, 0)];
		} completion:^(BOOL finished)
		 {
			 [self updateFrameContentView:destination.view containerView:superView position:CGPointZero];
             [self removeSubchildsFrom:source];
			 [source removeFromParentViewController];
			 [destination didMoveToParentViewController:parent];
		 }];
	}
}

// transition from 2 view controllers moving from left to right side (pop style)
- (void)parent:(UIViewController*)parent popToViewController:(UIViewController*)destination fromViewController:(UIViewController*)source superView:(UIView*)superView animateWithDuration:(float)duration
{
	if (parent != nil && source != nil && destination != nil)
	{
		destination.view.frame = superView.bounds;
		[destination.view setTransform:CGAffineTransformMakeTranslation(- destination.view.frame.size.width, 0)];
		[source willMoveToParentViewController:nil];
		[parent addChildViewController:destination];
		
		[parent transitionFromViewController: source toViewController: destination duration: 0.35 options:UIViewAnimationOptionCurveEaseIn animations:^{
			[destination.view setTransform:CGAffineTransformIdentity];
		} completion:^(BOOL finished)
		 {
			 [self updateFrameContentView:destination.view containerView:superView position:CGPointZero];
             [self removeSubchildsFrom:source];
			 [source removeFromParentViewController];
			 [destination didMoveToParentViewController:parent];
		 }];
	}
}

// transition from 2 view controllers with a fadein-fadeout transition
- (void)parent:(UIViewController*)parent fadeToViewController:(UIViewController*)destination fromViewController:(UIViewController*)source superView:(UIView*)superView animateWithDuration:(float)duration
{
	if (parent != nil && source != nil && destination != nil)
	{
		destination.view.frame = superView.bounds;
		destination.view.alpha = 0.0f;
		source.view.alpha = 1.0f;
		[source willMoveToParentViewController:nil];
		[parent addChildViewController:destination];
		
		[parent transitionFromViewController: source toViewController: destination duration: 0.35 options:UIViewAnimationOptionCurveEaseIn animations:^{
			destination.view.alpha = 1.0f;
			source.view.alpha = 0.0f;
		} completion:^(BOOL finished)
		 {
			 [self updateFrameContentView:destination.view containerView:superView position:CGPointZero];
             [self removeSubchildsFrom:source];
			 [source removeFromParentViewController];
			 [destination didMoveToParentViewController:parent];
		 }];
	}
}


// get a child from a parent
- (UIViewController*)getChildName:(NSString*)classString fromParent:(UIViewController*)parent
{
	Class class = NSClassFromString(classString);
	UIViewController *vc = nil;
	for (UIViewController *child in parent.childViewControllers)
	{
		if ([child isKindOfClass:class])
			vc = child;
	}
	return vc;
}

- (UIViewController*)getLastChildFromParent:(UIViewController*)parent
{
	UIViewController *child = nil;
	if ([parent.childViewControllers count] > 0)
		child = [parent.childViewControllers lastObject];
	return child;
}

- (UIImageView*)getScreenShot
{
	UIImageView *imageView;
	UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
	UIGraphicsBeginImageContext(window.bounds.size);
	[window.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	imageView = [[UIImageView alloc] initWithImage:image];
	
	return imageView;
}




@end
