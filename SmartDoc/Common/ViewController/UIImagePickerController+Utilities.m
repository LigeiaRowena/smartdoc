//
//  UIViewController+RadioRai.m
//  RadioRai
//
//  Created by Francesca Corsini on 10/02/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "UIImagePickerController+Utilities.h"
#import <objc/runtime.h>

static char indexKey;

@implementation UIImagePickerController (Utilities)

@dynamic index;

- (void)setIndex:(NSNumber *)newIndex
{
	[self willChangeValueForKey:@"index"];
	objc_setAssociatedObject(self, &indexKey, newIndex, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
	[self didChangeValueForKey:@"index"];
}

- (NSNumber*)index
{
	return objc_getAssociatedObject(self, &indexKey);
}

- (BOOL)shouldAutorotate
{
	return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
	return UIInterfaceOrientationPortrait;
}

@end
