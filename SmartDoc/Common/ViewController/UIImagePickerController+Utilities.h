//
//  UIViewController+RadioRai.h
//  RadioRai
//
//  Created by Francesca Corsini on 10/02/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImagePickerController (Utilities)

@property (nonatomic, strong) NSNumber *index;

@end
