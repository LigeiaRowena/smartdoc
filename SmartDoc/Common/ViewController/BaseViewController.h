//
//  SmartDoc
//
//  Created by Francesca Corsini on 18/05/13.
//  Copyright (c) 2013 Andrea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Patient.h"
#import "PickerField.h"
#import "CustomTextField.h"
#import "CustomTextView.h"
#import "NSString+Utility.h"
#import "DBManager.h"
#import "MessageUI/MessageUI.h"
#import "AlertViewBlocks.h"
#import "UIViewController+Transitions.h"
#import "UIImagePickerController+Utilities.h"
#import "UIImage+Utilities.h"
#import "CameraView.h"
#import "Settings.h"

#define KEYBOARD_HEIGHT 308

@interface BaseViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate, PickerFieldDelegate, CustomTextViewDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate, UIPrintInteractionControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CameraViewDelegate>

@property (nonatomic, weak) IBOutlet UIScrollView *mainscroll;
@property (strong, nonatomic) UIPopoverController *popover;
@property (strong, nonatomic) UITapGestureRecognizer *tap;
@property (strong, nonatomic) Patient *patient;
@property (nonatomic, strong) NSDateFormatter *formatter;
@property (nonatomic, strong) UIImagePickerController *imagePickerController;
@property (nonatomic, strong) CameraView *overlayView;

- (NSArray*)getArrayFromPlist:(NSString*)plist key:(NSString*)key;
- (NSArray*)getArrayDurations;
+ (BOOL)isEmptyObject:(id)object;
+ (BOOL)isEmptyString:(NSString *)string;
+ (BOOL)isEmptyNumber:(NSDecimalNumber*)number;
+ (BOOL)isValidNumber:(NSString*)number;

- (void)sendPDFFromText:(NSString*)body title:(NSString*)title;
- (void)sendPDFFromOCRtitle:(NSString*)title;
- (void)sendPDFtitle:(NSString*)title path:(NSString*)path;
- (void)printPDFWithActivity:(NSString*)path anchor:(UIButton*)anchor;
- (void)printPDF:(NSString*)path anchor:(UIView*)anchor;
- (void)printText:(NSString*)text anchor:(UIView*)anchor;

- (void)setup;

- (void)showCamera:(NSNumber*)index;
- (void)showPhotos:(NSNumber*)index;

- (void)recoverDBFromCloud;

@end
