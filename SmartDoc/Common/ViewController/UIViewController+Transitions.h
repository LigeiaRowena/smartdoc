//
//  UIViewController+RadioRai.h
//  RadioRai
//
//  Created by Francesca Corsini on 10/02/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Transitions)


///////////////
//IOS7 Utility
///////////////
- (float)getDelta;

///////////////
//Navigation UIViewController
///////////////

// get the root view controller
- (UIViewController *)getRootController;

// remove a child view controller with animation
- (void)parent:(UIViewController*)parent removeViewController:(UIViewController*)viewController animateWithDuration:(float)duration;

// remove a child view controller without animation
- (void)parent:(UIViewController*)parent removeViewController:(UIViewController*)viewController;

// remove a child view controller with animation of pop
- (void)parent:(UIViewController*)parent popViewController:(UIViewController*)viewController animateWithDuration:(float)duration;

// remove all subchilds from a view controller
- (void)removeSubchildsFrom:(UIViewController*)viewController;

// add a child view controller with animation
- (void)parent:(UIViewController*)parent addChildViewController:(UIViewController*)viewController superView:(UIView*)superView animateWithDuration:(float)duration;

// add a child view controller without animation
- (void)parent:(UIViewController*)parent addChildViewController:(UIViewController*)viewController superView:(UIView*)superView;

// add a child view controller without animation with a particular position
- (void)parent:(UIViewController*)parent addChildViewController:(UIViewController*)viewController superView:(UIView*)superView position:(CGPoint)position;

// push a child view controller
- (void)parent:(UIViewController*)parent pushViewController:(UIViewController*)viewController superView:(UIView*)superView animateWithDuration:(float)duration;


// transition from 2 view controllers moving from right to left side (push style)
- (void)parent:(UIViewController*)parent pushToViewController:(UIViewController*)destination fromViewController:(UIViewController*)source superView:(UIView*)superView animateWithDuration:(float)duration;

// transition from 2 view controllers moving from left to right side (pop style)
- (void)parent:(UIViewController*)parent popToViewController:(UIViewController*)destination fromViewController:(UIViewController*)source superView:(UIView*)superView animateWithDuration:(float)duration;

// transition from 2 view controllers with a fadein-fadeout transition
- (void)parent:(UIViewController*)parent fadeToViewController:(UIViewController*)destination fromViewController:(UIViewController*)source superView:(UIView*)superView animateWithDuration:(float)duration;

// get a child from a parent
- (UIViewController*)getChildName:(NSString*)classString fromParent:(UIViewController*)parent;

// get the last child from a parent
- (UIViewController*)getLastChildFromParent:(UIViewController*)parent;

- (UIImageView*)getScreenShot;

@end
