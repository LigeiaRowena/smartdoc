//
//  PickerViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 12/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"
#import "PickerField.h"
#import "PickerEditView.h"
#import "PickerAddView.h"

@protocol PickerViewControllerDelegate <NSObject>
@optional
- (void)pickerViewControllerSetText:(NSString*)text pickerField:(PickerField*)pickerField;
@end

@interface PickerViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, UIPopoverControllerDelegate, PickerEditViewDelegate, PickerAddViewDelegate>

@property (nonatomic, strong) NSMutableArray *data;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UIView *buttonsView;
@property (weak, nonatomic) IBOutlet PickerEditView *pickerEditView;
@property (weak, nonatomic) IBOutlet PickerAddView *pickerAddView;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, weak) PickerField *pickerField;
@property (nonatomic, strong) NSString *pickerType;
@property (nonatomic, weak) id<PickerViewControllerDelegate> delegate;
@property (nonatomic, strong) NSString *dataType;

- (void)selectItem:(NSString*)text;


@end
