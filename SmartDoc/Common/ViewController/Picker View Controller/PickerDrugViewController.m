//
//  PickerDrugViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 26/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "PickerDrugViewController.h"
#import "DBManager.h"
#import "PickerCell.h"
#import "PopoverBackgroundView.h"

@interface PickerDrugViewController ()
@end

@implementation PickerDrugViewController

#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		self.data = @[].mutableCopy;
		self.searchData = @[].mutableCopy;
		self.search = NO;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		self.data = @[].mutableCopy;
		self.searchData = @[].mutableCopy;
		self.search = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.view.backgroundColor = [UIColor colorWithRed:(238.0f/255.0f) green:(238.0f/255.0f) blue:(238.0f/255.0f) alpha:1.0f];
	
	// register cell
	UINib *cellNib = [UINib nibWithNibName:@"PickerCell" bundle:nil];
	[self.table registerNib:cellNib forCellReuseIdentifier:[PickerCell reuseIdentifier]];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	// popover
	self.popover.delegate = self;
	self.popover.backgroundColor = [UIColor clearColor];
	self.popover.popoverLayoutMargins = UIEdgeInsetsZero;
	self.popover.popoverBackgroundViewClass = [PopoverBackgroundView class];
	
	// filter by active
	[self filterByActive:nil];
}

#pragma mark - UITextFieldDelegate Methods

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	[self search:nil];
	[super textFieldDidEndEditing:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[self search:nil];
	return [super textFieldShouldReturn:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	if (![BaseViewController isEmptyString:self.textfield.text])
	{
		self.search = YES;
		[self.searchData removeAllObjects];
		for (NSString *data in self.data)
		{
			if (stringContainsOccurenceOfString(data, self.textfield.text))
				[self.searchData addObject: data];
		}
		[self.searchData sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
	}
	else
		self.search = NO;
	
	[self.table reloadData];
	return [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
}

#pragma mark - Actions

- (IBAction)search:(id)sender
{
	[self.textfield resignFirstResponder];
	[super textFieldDidEndEditing:self.textfield];

	if (![BaseViewController isEmptyString:self.textfield.text])
	{
		self.search = YES;
		[self.searchData removeAllObjects];
		for (NSString *data in self.data)
		{
			if (stringContainsOccurenceOfString(data, self.textfield.text))
				[self.searchData addObject: data];
		}
		[self.searchData sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
	}
	else
		self.search = NO;
	
    [self.table reloadData];
}

- (IBAction)filterByName:(id)sender
{
	self.ratioName.selected = YES;
	self.ratioActive.selected = NO;
	self.indexPath = nil;
	
	if (self.search)
	{
		[self.searchData removeAllObjects];
		NSArray *list = [[DBManager getIstance] getDinamicDataArray:DrugNameData text:@""];
		for (DinamicData *drugName in list)
			[self.searchData addObject:[drugName text]];
		[self.searchData sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		[self.table reloadData];
		NSString *text = self.pickerField.textField.text;
		if (![text isBlank] && [self.searchData containsObject:text])
		{
			self.indexPath = [NSIndexPath indexPathForRow:[self.searchData indexOfObject:text] inSection:0];
			[self.table reloadRowsAtIndexPaths:@[self.indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
			[self.table scrollToRowAtIndexPath:self.indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
		}
	}
	else
	{
		[self.data removeAllObjects];
		NSArray *list = [[DBManager getIstance] getDinamicDataArray:DrugNameData text:@""];
		for(DinamicData *drugName in list)
			[self.data addObject:[drugName text]];
		[self.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		[self.table reloadData];
		NSString *text = self.pickerField.textField.text;
		if (![text isBlank] && [self.data containsObject:text])
		{
			self.indexPath = [NSIndexPath indexPathForRow:[self.data indexOfObject:text] inSection:0];
			[self.table reloadRowsAtIndexPaths:@[self.indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
			[self.table scrollToRowAtIndexPath:self.indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
		}
	}
}

- (IBAction)filterByActive:(id)sender
{
	self.ratioName.selected = NO;
	self.ratioActive.selected = YES;
	self.indexPath = nil;
	
	if (self.search)
	{
		[self.searchData removeAllObjects];
		NSArray *list = [[DBManager getIstance] getDinamicDataArray:DrugActiveData text:@""];
		for (DinamicData *drugActive in list)
			[self.searchData addObject:[drugActive text]];
		[self.searchData sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		[self.table reloadData];
		NSString *text = self.pickerField.textField.text;
		if (![text isBlank] && [self.searchData containsObject:text])
		{
			self.indexPath = [NSIndexPath indexPathForRow:[self.searchData indexOfObject:text] inSection:0];
			[self.table reloadRowsAtIndexPaths:@[self.indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
			[self.table scrollToRowAtIndexPath:self.indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
		}
	}
	else
	{
		[self.data removeAllObjects];
		NSArray *list = [[DBManager getIstance] getDinamicDataArray:DrugActiveData text:@""];
		for (DinamicData *drugActive in list)
			[self.data addObject:[drugActive text]];
		[self.data sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		[self.table reloadData];
		NSString *text = self.pickerField.textField.text;
		if (![text isBlank] && [self.data containsObject:text])
		{
			self.indexPath = [NSIndexPath indexPathForRow:[self.data indexOfObject:text] inSection:0];
			[self.table reloadRowsAtIndexPaths:@[self.indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
			[self.table scrollToRowAtIndexPath:self.indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
		}
	}
}

- (void)dismissPicker
{
	self.pickerField.pickerIndex = (int)self.indexPath.row;
	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(pickerDrugViewControllerSetText:pickerField:)] && self.indexPath != nil)
	{
		if (self.search)
			[self.delegate pickerDrugViewControllerSetText:self.searchData[self.indexPath.row] pickerField:self.pickerField];
		else
			[self.delegate pickerDrugViewControllerSetText:self.data[self.indexPath.row] pickerField:self.pickerField];
	}
	[self.popover dismissPopoverAnimated:YES];
}


#pragma mark - UITableViewDelegate/UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (self.search)
		return [self.searchData count];
	else
		return [self.data count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	PickerCell *cell = [tableView dequeueReusableCellWithIdentifier:[PickerCell reuseIdentifier]];
	if (self.search)
		[cell setContent:self.searchData[indexPath.row]];
	else
		[cell setContent:self.data[indexPath.row]];
	if ([indexPath isEqual:self.indexPath])
		cell.status.hidden = NO;
	else
		cell.status.hidden = YES;
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	self.indexPath = indexPath;
	[self.table reloadData];
	[self dismissPicker];
}


#pragma mark - UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
	[self.table reloadData];
	[self dismissPicker];
}


@end
