//
//  PickerDateViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "PickerDateViewController.h"
#import "PopoverBackgroundView.h"

@interface PickerDateViewController ()

@end

@implementation PickerDateViewController

#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.view.backgroundColor = [UIColor whiteColor];
	
	// date formatter
	self.formatter = [[NSDateFormatter alloc] init] ;
	[self.formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
	[self.formatter setDateFormat:@"dd/MM/yyyy"];
	if (self.date != nil)
		self.picker.date = self.date;
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	// popover
	self.popover.delegate = self;
	self.popover.backgroundColor = [UIColor clearColor];
	self.popover.popoverLayoutMargins = UIEdgeInsetsZero;
	self.popover.popoverBackgroundViewClass = [PopoverBackgroundView class];
}


#pragma mark - UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
	[self dismissPicker];
}

#pragma mark - Actions

- (void)dismissPicker
{
	if ([self.delegate respondsToSelector:@selector(pickerDateViewControllerSetDate:pickerField:)])
		[self.delegate pickerDateViewControllerSetDate:self.picker.date pickerField:self.pickerField];
	[self.popover dismissPopoverAnimated:YES];
}


@end
