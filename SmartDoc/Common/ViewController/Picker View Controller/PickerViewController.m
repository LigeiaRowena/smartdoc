//
//  PickerViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 12/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "PickerViewController.h"
#import "DBManager.h"
#import "PickerCell.h"
#import "PopoverBackgroundView.h"
#import "DinamicData.h"

#pragma mark - Init PickerViewController

@implementation PickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		self.data = @[].mutableCopy;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		self.data = @[].mutableCopy;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.view.backgroundColor = [UIColor whiteColor];

	// register cell
	UINib *cellNib = [UINib nibWithNibName:@"PickerCell" bundle:nil];
	[self.table registerNib:cellNib forCellReuseIdentifier:[PickerCell reuseIdentifier]];
	[self.table reloadData];
}

- (void)selectItem:(NSString*)text
{
	if (![text isBlank] && [[DBManager getIstance] dbContainsDinamicDataType:self.dataType text:text])
	{
		self.indexPath = [NSIndexPath indexPathForRow:[self indexOfDinamicData:text] inSection:0];
		[self.table reloadRowsAtIndexPaths:@[self.indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
		[self.table scrollToRowAtIndexPath:self.indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
	}
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	// edit button
	NSString *text = self.pickerField.textField.text;
	if ([self.table numberOfRowsInSection:0] == 0 || [text isBlank])
		self.editBtn.enabled = NO;
	if ([self.pickerType isEqualToString:StaticField] || [self.pickerType isEqualToString:DrugUnitField] || [self.pickerType isEqualToString:DrugFrequencyField])
	{
		self.editBtn.enabled = NO;
		self.addBtn.enabled = NO;
		self.editBtn.hidden = YES;
		self.addBtn.hidden = YES;
	}
	
	// popover
	self.popover.delegate = self;
	self.popover.backgroundColor = [UIColor clearColor];
	self.popover.popoverLayoutMargins = UIEdgeInsetsZero;
	self.popover.popoverBackgroundViewClass = [PopoverBackgroundView class];
}

- (int)indexOfDinamicData:(NSString*)text
{
	__block int index = 0;
	[self.data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
		if ([[(DinamicData*)obj text] isEqualToString:text])
		{
			index = (int)idx;
			*stop = YES;
		}
	}];
	return index;
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	// datasource
	NSString *text = self.pickerField.textField.text;
	if (![text isBlank] && [[DBManager getIstance] dbContainsDinamicDataType:self.dataType text:text])
	{		
		self.indexPath = [NSIndexPath indexPathForRow:[self indexOfDinamicData:text] inSection:0];
		[self.table reloadRowsAtIndexPaths:@[self.indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
		[self.table scrollToRowAtIndexPath:self.indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
	}
	if (![text isBlank] && [self.data containsObject:text])
	{
		self.indexPath = [NSIndexPath indexPathForRow:[self.data indexOfObject:text] inSection:0];
		[self.table reloadRowsAtIndexPaths:@[self.indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
		[self.table scrollToRowAtIndexPath:self.indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
	}
}

#pragma mark - PickerEditViewDelegate

- (void)dismissPickerEditView:(NSString*)text
{
	self.buttonsView.hidden = NO;
	self.pickerEditView.hidden = YES;
	[UIView animateWithDuration:0.2 animations:^{
		self.buttonsView.alpha = 1.0f;
	} completion:^(BOOL finished) {
		[self pickerViewControllerEditText:text];
	}];
}

- (void)pickerViewControllerEditText:(NSString*)text
{
	if (![self.data[self.indexPath.row] isKindOfClass:[DinamicData class]])
		return;
	
	DinamicData *data = self.data[self.indexPath.row];
	data.text = text;
	[[DBManager getIstance] saveContext];
	[self.data removeAllObjects];
	self.data = [[DBManager getIstance] getDinamicDataArray:self.dataType text:@""].mutableCopy;
	[self.table reloadData];
}

#pragma mark - PickerAddViewDelegate

- (void)dismissPickerAddView:(NSString*)text
{
	self.buttonsView.hidden = NO;
	self.pickerAddView.hidden = YES;
	[UIView animateWithDuration:0.2 animations:^{
		self.buttonsView.alpha = 1.0f;
	} completion:^(BOOL finished) {
		[self pickerViewControllerAddText:text];
	}];
}

- (void)pickerViewControllerAddText:(NSString*)text
{
	if ([BaseViewController isEmptyString:text] || [[DBManager getIstance] dbContainsDinamicDataType:self.dataType text:text])
		return;
	
	[self.data addObject:[[DBManager getIstance] createDinamicDataType:self.dataType text:text]];
	[self.table reloadData];
}

#pragma mark - Actions

- (IBAction)add:(id)sender
{
	self.pickerAddView.textfield.text = @"";
	self.pickerAddView.frame = CGRectMake(0, self.view.frame.size.height - self.pickerAddView.frame.size.height, self.pickerAddView.frame.size.width, self.pickerAddView.frame.size.height);

	[UIView animateWithDuration:0.2 animations:^{
		self.buttonsView.alpha = 0.0f;
	} completion:^(BOOL finished) {
		self.buttonsView.hidden = YES;
		self.pickerAddView.hidden = NO;
		[self.pickerAddView.textfield setNeedsDisplay];
	}];
}

- (IBAction)edit:(id)sender
{
	self.pickerEditView.frame = CGRectMake(0, self.view.frame.size.height - self.pickerEditView.frame.size.height, self.pickerEditView.frame.size.width, self.pickerEditView.frame.size.height);
	
	[UIView animateWithDuration:0.2 animations:^{
		self.buttonsView.alpha = 0.0f;
	} completion:^(BOOL finished) {
		self.buttonsView.hidden = YES;
		self.pickerEditView.hidden = NO;
		DinamicData *data = self.data[self.indexPath.row];
		self.pickerEditView.textfield.text = data.text;
		[self.pickerEditView.textfield setNeedsDisplay];
	}];
}


- (void)dismissPicker
{
	NSString *text = nil;
	if (self.indexPath.row > [self.data count] - 1)
		text = self.pickerField.textField.text;
	else
	{
		self.pickerField.pickerIndex = (int)self.indexPath.row;
		id object = self.data[self.indexPath.row];
		text = ([object isKindOfClass:[DinamicData class]]) ? [(DinamicData*)object text] : object;
	}

	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(pickerViewControllerSetText:pickerField:)] && self.indexPath != nil)
		[self.delegate pickerViewControllerSetText:text pickerField:self.pickerField];
	[self.popover dismissPopoverAnimated:YES];
}

#pragma mark - UITableViewDelegate/UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	if ([self.pickerType isEqualToString:StaticField] || [self.pickerType isEqualToString:DrugUnitField] || [self.pickerType isEqualToString:DrugFrequencyField])
		return NO;
	else
		return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle==UITableViewCellEditingStyleDelete)
    {
		if (![self.data[indexPath.row] isKindOfClass:[DinamicData class]])
			return;
		
		DinamicData *data = self.data[indexPath.row];
		[[DBManager getIstance] deleteObject:data];
		if ([indexPath isEqual:self.indexPath])
			self.indexPath = nil;
		[self.data removeObjectAtIndex:indexPath.row];
		[self.table reloadData];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  	return [self.data count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	PickerCell *cell = [tableView dequeueReusableCellWithIdentifier:[PickerCell reuseIdentifier]];
	[cell setContent:self.data[indexPath.row]];
	if ([indexPath isEqual:self.indexPath])
		cell.status.hidden = NO;
	else
		cell.status.hidden = YES;
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	self.indexPath = indexPath;
	[self.table reloadData];
	[self dismissPicker];
}

#pragma mark - UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
	[self.table reloadData];
	[self dismissPicker];
}

@end
