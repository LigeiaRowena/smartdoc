//
//  PickerDrugViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 26/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"
#import "PickerField.h"
#import "DinamicData.h"

@protocol PickerDrugViewControllerDelegate <NSObject>
@optional
- (void)pickerDrugViewControllerSetText:(NSString*)text pickerField:(PickerField*)pickerField;
@end

@interface PickerDrugViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, UIPopoverControllerDelegate>

@property (nonatomic, strong) NSMutableArray *data;
@property (nonatomic, strong) NSMutableArray *searchData;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet CustomTextField *textfield;
@property (weak, nonatomic) IBOutlet UIButton *ratioName;
@property (weak, nonatomic) IBOutlet UIButton *ratioActive;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, weak) PickerField *pickerField;
@property (nonatomic, strong) NSString *pickerType;
@property (nonatomic, weak) id<PickerDrugViewControllerDelegate> delegate;
@property (nonatomic) BOOL search;

@property (strong, nonatomic) DinamicData *dinamicData;

@end
