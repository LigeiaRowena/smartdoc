//
//  PickerDateViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"
#import "PickerField.h"

@protocol PickerDateViewControllerDelegate <NSObject>
@optional
- (void)pickerDateViewControllerSetDate:(NSDate*)date pickerField:(PickerField*)pickerField;
@end

@interface PickerDateViewController : BaseViewController <UIPopoverControllerDelegate>

@property (weak, nonatomic) IBOutlet UIDatePicker *picker;
@property (nonatomic, weak) PickerField *pickerField;
@property (nonatomic, weak) id<PickerDateViewControllerDelegate> delegate;
@property (nonatomic, strong) NSDate *date;

@end
