//
//  PickerEditView.m
//  SmartDoc
//
//  Created by Francesca Corsini on 22/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "PickerEditView.h"

@implementation PickerEditView

#pragma mark - Init

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
	{
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
}

#pragma mark - UITextFieldDelegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
	return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	
	// mette la prima maiuscola
	if (textField.text.length > 0)
		textField.text = [NSString stringWithFormat:@"%@%@",[[textField.text substringToIndex:1] uppercaseString],[textField.text substringFromIndex:1]];
	
	return YES;
}

#pragma mark - Actions

- (IBAction)edit:(id)sender
{
	if (self.delegate != nil && [self.delegate respondsToSelector:@selector(dismissPickerEditView:)])
		[self.delegate dismissPickerEditView:self.textfield.text];
}

@end
