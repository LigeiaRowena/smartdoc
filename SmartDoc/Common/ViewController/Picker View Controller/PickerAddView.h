//
//  PickerAddView.h
//  SmartDoc
//
//  Created by Francesca Corsini on 22/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "UIView+NibLoading.h"
#import "CustomTextField.h"

@protocol PickerAddViewDelegate <NSObject>
- (void)dismissPickerAddView:(NSString*)text;
@end

@interface PickerAddView : NibLoadedView

@property (nonatomic, weak) IBOutlet CustomTextField *textfield;
@property (nonatomic, weak) IBOutlet id<PickerAddViewDelegate> delegate;

@end
