//
//  AppDelegate.h
//  SmartDoc
//
//  Created by Francesca Corsini on 01/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RootViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) RootViewController *viewController;

@end
