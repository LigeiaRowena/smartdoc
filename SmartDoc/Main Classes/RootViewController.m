//
//  RootViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 19/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "RootViewController.h"
#import "Utils.h"
#import "LoginViewController.h"
#import "CalendarManager.h"
#import "PatientListViewController.h"
#import "SVProgressHUD.h"

@interface RootViewController ()

@end

@implementation RootViewController

#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	//[self setWantsFullScreenLayout:YES];
	[self setNeedsStatusBarAppearanceUpdate];
	
	// copy Questions.plist to Documents folder
	[Utils copyPlistToDocumentFolder:QUESTIONS_PLIST];
	
	// copy LocalSettings.plist to Documents folder
	[Utils copyPlistToDocumentFolder:LOCAL_SETTINGS];
	
	// sync local db with iCloud
	[[DBManager getIstance] beginSyncWithDelegate:self firstLaunch:YES];
	
	// create Settings on the db if doesn't exist
	if (![[DBManager getIstance] getSettings])
		[[DBManager getIstance] createSettings];
}

- (void)dealloc
{
	[[DBManager getIstance] stopSync];
}

#pragma mark - DBManagerSyncDelegate

- (void)didBeginSyncDB
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
		[SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
		[SVProgressHUD showWithStatus:@"Syncing the local db: please wait"];
	});
}

- (void)didFinishSyncDB
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[SVProgressHUD dismiss];
	});
		
	// refresh local Calendar with data taken from local db
	[[CalendarManager getIstance] updateCalendarConfirmBlock:^(NSString *success) {
	} cancelBlock:^(NSError *error) {
	}];
}

- (void)didFailSyncDBWithError:(NSError*)error
{
	NSLog(@"didFailSyncDBWithError: %@", error);
	
	[[AlertViewBlocks getIstance] warningAlertViewWithMessage:@"There was an error while syncing the db" confirmBlock:^{
	}];
}

#pragma mark - Actions

- (IBAction)enter:(id)sender
{
	[[DBManager getIstance] stopSync];

#ifdef STAGING
	PatientListViewController *vc = [[PatientListViewController alloc] initWithNibName:@"PatientListViewController" bundle:nil];
	[self parent:self pushViewController:vc superView:self.view animateWithDuration:0.2];
#else
	// already logged, go directly to patient list view
	if ([[DBManager getIstance] getRemember] && ![BaseViewController isEmptyString:[[DBManager getIstance] getSession]])
	{
		PatientListViewController *vc = [[PatientListViewController alloc] initWithNibName:@"PatientListViewController" bundle:nil];
		[self parent:self pushViewController:vc superView:self.view animateWithDuration:0.2];
	}
	// you have to login
	else
	{
		LoginViewController *vc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
		[self parent:self pushViewController:vc superView:self.view animateWithDuration:0.2];
	}
#endif
}


@end
