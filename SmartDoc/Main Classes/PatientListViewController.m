//
//  PatientsListViewController.m
//  SmartDoc
//
//  Created by Andrea on 05/10/12.
//  Copyright (c) 2012 Andrea. All rights reserved.
//

#import "PatientListViewController.h"
#import "PatientCell.h"
#import "Patient.h"
#import "SettingsViewController.h"
#import "PatientViewController.h"
#import "PatientFirstVisitViewController.h"
#import "PatientRootViewController.h"
#import "Visit.h"
#import "Measurement.h"
#import "DBManager.h"
#import "PatientData.h"
#import "Settings.h"
#import "SVProgressHUD.h"

@interface PatientListViewController ()

@property (weak, nonatomic) IBOutlet UITextField *searchText;
@property (weak, nonatomic) IBOutlet PickerField *categoryText;
@property (strong, nonatomic) NSMutableArray *patients;

@end

#pragma mark - Init PatientListViewController

@implementation PatientListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// load PatientCell NIB file
	UINib *cellNib = [UINib nibWithNibName:@"PatientCell" bundle:nil];
	[self.tableView registerNib:cellNib forCellReuseIdentifier:[PatientCell reuseIdentifier]];
	
	// init properties
	self.categoryText.textField.text = @"Standard";
	self.patients = @[].mutableCopy;
	
	// show settings and load Dinamic Datas only at first launch
	if ([[[[DBManager getIstance] getSettings] firstLaunch] boolValue])
	{
		[[DBManager getIstance] loadDinamicDatas];
		SettingsViewController *settingsViewController = [[SettingsViewController alloc] initWithNibName:@"SettingsBeginViewController" bundle:nil];
		[self parent:self addChildViewController:settingsViewController superView:self.view animateWithDuration:0.2];
	}
	
	// check if there is a db from the cloud
	if ([[[[DBManager getIstance] getSettings] firstLaunch] boolValue])
	{
		[[AlertViewBlocks getIstance] informationConfirmAlertViewWithmessage:@"Do you want to recover app data from the cloud?" confirmBlock:^{
			[self recoverDBFromCloud];
		} cancelBlock:^{
		}];
	}
}


- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	// show all patients
	[self checkBlankPatients];
	[self.menu selectItemAtIndex:0];
	[self.tableView reloadData];
}


#pragma mark - MenuViewDelegate

- (void)menuDidSelectItemAtIndex:(int)index
{
	self.searchText.text = @"";
	if (index == 0)
		[self showAllPatient];
	else if (index == 1)
		[self showRecentPatient];
	else if (index == 2)
		[self showStarredPatient];
}


#pragma mark - PickerViewControllerDelegate Methods

- (void)pickerViewControllerSetText:(NSString*)text pickerField:(PickerField*)pickerField
{
	pickerField.textField.text = text;
	self.searchText.text = @"";
	[self.menu selectItemAtIndex:self.menu.index];
}

#pragma mark - PickerFieldDelegate Methods

- (void)tapOnPickerFieldButton:(PickerField*)pickerField
{
	PickerViewController *pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
	pickerViewController.pickerField = pickerField;
	pickerViewController.delegate = self;
	
	if (pickerField == self.categoryText)
	{
		pickerViewController.data = [[DBManager getIstance] getDinamicDataArray:CategoryData text:@""].mutableCopy;
		pickerViewController.pickerType = EditableField;
		pickerViewController.dataType = CategoryData;
	}
	
	self.popover = [[UIPopoverController alloc] initWithContentViewController:pickerViewController];
	[self.popover setPopoverContentSize:pickerViewController.view.frame.size];
	[pickerViewController setPopover:self.popover];
	[self.popover presentPopoverFromRect:pickerField.button.bounds inView:pickerField.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}


#pragma mark - UITextFieldDelegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
	[self.menu selectItemAtIndex:0];
    return [super textFieldShouldBeginEditing:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	[super textFieldDidEndEditing:textField];
	if ([textField.text length] == 0) {
		[self.menu selectItemAtIndex:0];
	}
	[self.tableView reloadData];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	[self.patients removeAllObjects];
	NSString *searchString = [NSString stringWithFormat:@"%@%@", textField.text, string];
	if ([searchString length] > 0) {
		NSArray *fo = [[DBManager getIstance] getPatientsFavourites:NO];
        for (Patient *patient in fo) {
			BOOL nameContainsText = stringContainsOccurenceOfString([patient.patientData name], searchString);
			BOOL lastNameContainsText = stringContainsOccurenceOfString([patient.patientData lastName], searchString);
			BOOL middleNameContainsText = stringContainsOccurenceOfString([patient.patientData middleName], searchString);
			if (nameContainsText || lastNameContainsText || middleNameContainsText)
				[self.patients addObject:patient];
        }
    }
	self.patients = [self.patients sortedArrayUsingComparator: ^(id id_1, id id_2) {
		Patient *m_1 = (Patient*) id_1;
		Patient *m_2 = (Patient*) id_2;
		NSString *d_1 = m_1.patientData.name;
		NSString *d_2 = m_2.patientData.name;
		return [d_1 compare: d_2];
	}].mutableCopy;
    [self.tableView reloadData];
	return YES;
}

#pragma mark - Actions

- (void)showRecentPatient
{
	[self reloadPatientsAlphabetically:NO onlyFavourites:NO];
	[self.tableView reloadData];
}

- (void)showAllPatient
{
	[self reloadPatientsAlphabetically:YES onlyFavourites:NO];
	[self.tableView reloadData];
}

- (void)showStarredPatient
{
	[self reloadPatientsAlphabetically:YES onlyFavourites:YES];
	[self.tableView reloadData];
}

- (IBAction)newPatient:(id)sender
{
	Patient *patient = [[DBManager getIstance] createPatientIsFavourite:NO];
	patient.category = @"Standard";
	PatientViewController *newPatientViewController = [[PatientViewController alloc] initWithNibName:@"PatientViewController" bundle:nil];
	newPatientViewController.patient = patient;
	[self parent:self pushViewController:newPatientViewController superView:self.view animateWithDuration:0.2];
}

- (IBAction)viewSettings:(id)sender
{
	SettingsViewController *settingsViewController = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
	[self parent:self addChildViewController:settingsViewController superView:self.view animateWithDuration:0.2];
}

- (void)pushMedicalHistory:(BaseViewController*)sourceViewController patient:(Patient*)patient
{
	PatientFirstVisitViewController *patientMedicalHistoryViewController = [[PatientFirstVisitViewController alloc] initWithNibName:@"PatientFirstVisitViewController" bundle:nil];
	patientMedicalHistoryViewController.patient = patient;
	
	if (sourceViewController == self)
		[self parent:self pushViewController:patientMedicalHistoryViewController superView:self.view animateWithDuration:0.2];
	else
		[self parent:self pushToViewController:patientMedicalHistoryViewController fromViewController:sourceViewController superView:self.view animateWithDuration:0.2];
}

- (void)pushPatientRoot:(BaseViewController*)sourceViewController patient:(Patient*)patient
{
	PatientRootViewController *patientRootViewController = [[PatientRootViewController alloc] initWithNibName:@"PatientRootViewController" bundle:nil];
	patientRootViewController.patient = patient;
	
	if (sourceViewController == self)
		[self parent:self pushViewController:patientRootViewController superView:self.view animateWithDuration:0.2];
	else
		[self parent:self pushToViewController:patientRootViewController fromViewController:sourceViewController superView:self.view animateWithDuration:0.2];
}

#pragma mark - Model Methods

- (void)checkBlankPatients
{
	NSArray *fo = [[DBManager getIstance] getPatientsFavourites:NO];
	for (Patient *patient in fo)
	{
		if ([BaseViewController isEmptyString:patient.patientData.name] && [BaseViewController isEmptyString:patient.patientData.lastName])
			[[DBManager getIstance] deleteObject:patient];
	}
}

- (void)reloadPatientsAlphabetically:(BOOL)alphabetically onlyFavourites:(BOOL)onlyFavourites
{
	// query
	[self.patients removeAllObjects];
	NSArray *fo = [[DBManager getIstance] getPatientsFavourites:onlyFavourites];
	
	// sort by category
	for (Patient *patient in fo) {
		if ([patient.category isEqualToString:self.categoryText.textField.text])
			[self.patients addObject:patient];
    }
	
	if (alphabetically) {
		// order alphabetically
		self.patients = [self.patients sortedArrayUsingComparator: ^(id id_1, id id_2) {
			Patient *m_1 = (Patient*) id_1;
			Patient *m_2 = (Patient*) id_2;
			NSString *d_1 = m_1.patientData.name;
			NSString *d_2 = m_2.patientData.name;
			return [d_1 compare: d_2];
		}].mutableCopy;
	} else {
		// order by date
		self.patients = [self.patients sortedArrayUsingComparator: ^(id id_1, id id_2) {
			Patient *m_1 = (Patient*) id_1;
			Patient *m_2 = (Patient*) id_2;
			NSDate *d_1 = m_1.creationDate;
			NSDate *d_2 = m_2.creationDate;
			return [d_2 compare: d_1];
		}].mutableCopy;
	}
}

#pragma mark - UITableViewDelegate/UITableViewDataSource Methods

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
		[[AlertViewBlocks getIstance] warningDeleteAlertViewWithMessage:@"You are going to delete this information: are you sure?"  confirmBlock:^{
			Patient *patient = self.patients[indexPath.row];
			[[DBManager getIstance] deleteObject:patient];
			[self.menu selectItemAtIndex:0];
		} cancelBlock:^{
		}];
	}
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.patients count];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 768, 34)];
	view.backgroundColor = lightGrayBgColor();
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 768-20, 34)];
	label.backgroundColor = [UIColor clearColor];
	label.font = OpenSansBold(18);
	label.textColor = grayBlueColor();
	label.text = nil;
	[view addSubview:label];
	return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PatientCell *cell = [tableView dequeueReusableCellWithIdentifier:[PatientCell reuseIdentifier]];
	Patient *patient = self.patients[indexPath.row];
	[cell setContent:patient];
		 
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	Patient *patient = self.patients[indexPath.row];
	
	if ([patient.hasPatientData boolValue] && [patient.hasFirstVisit boolValue])
		[self pushPatientRoot:self patient:patient];
	
	else if ([patient.hasPatientData boolValue] && ![patient.hasFirstVisit boolValue])
		[self pushMedicalHistory:self patient:patient];

	else
	{
		PatientViewController *newPatientViewController = [[PatientViewController alloc] initWithNibName:@"PatientViewController" bundle:nil];
		newPatientViewController.patient = patient;
		[self parent:self pushViewController:newPatientViewController superView:self.view animateWithDuration:0.2];
	}
}

@end
