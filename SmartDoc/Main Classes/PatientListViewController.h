//
//  PatientsListViewController.h
//  SmartDoc
//
//  Created by Andrea on 05/10/12.
//  Copyright (c) 2012 Andrea. All rights reserved.
//

#import "BaseViewController.h"
#import "MenuView.h"
#import "PickerViewController.h"

@interface PatientListViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, MenuViewDelegate, PickerViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet MenuView *menu;

- (void)pushMedicalHistory:(BaseViewController*)sourceViewController patient:(Patient*)patient;
- (void)pushPatientRoot:(BaseViewController*)sourceViewController patient:(Patient*)patient;
- (void)showAllPatient;

@end
