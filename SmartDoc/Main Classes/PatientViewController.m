//
//  NewPatientViewController.m
//  SmartDoc
//
//  Created by Francesca Corsini on 21/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "PatientViewController.h"
#import "SettingsViewController.h"
#import "PatientDataEditViewController.h"
#import "PatientListViewController.h"
#import "PatientData.h"

@interface PatientViewController ()

@property (nonatomic, weak) IBOutlet UIView *mainView;

@end

@implementation PatientViewController

#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	PatientDataEditViewController *patientDataViewController = [[PatientDataEditViewController alloc] initWithNibName:@"PatientDataEditViewController" bundle:nil];
	patientDataViewController.patient = self.patient;
	[self parent:self addChildViewController:patientDataViewController superView:self.mainView];
}

#pragma mark - Actions

- (IBAction)saveAndGoBack:(id)sender
{
	if (![BaseViewController isEmptyString:self.patient.patientData.name] || ![BaseViewController isEmptyString:self.patient.patientData.lastName])
	{
		PatientListViewController *parent = (PatientListViewController*)self.parentViewController;
		[parent.menu selectItemAtIndex:0];
		self.patient.hasPatientData = [NSNumber numberWithBool:YES];
		[[DBManager getIstance] saveContext];
		[self parent:parent popViewController:self animateWithDuration:0.2];
	}
	else if ([BaseViewController isEmptyString:self.patient.patientData.name] && [BaseViewController isEmptyString:self.patient.patientData.lastName])
	{
		[[AlertViewBlocks getIstance] informationConfirmAlertViewWithmessage:@"Please insert patient name or surname!" confirmBlock:^{
		} cancelBlock:^{
			[[DBManager getIstance] deleteObject:self.patient];
			PatientListViewController *parent = (PatientListViewController*)self.parentViewController;
			[parent.menu selectItemAtIndex:0];
			[self parent:parent popViewController:self animateWithDuration:0.2];
		}];
	}
}

- (IBAction)createMedicalHistory:(id)sender
{
	PatientListViewController *parent = (PatientListViewController*)self.parentViewController;
	[parent.menu selectItemAtIndex:0];
	
	if ([BaseViewController isEmptyString:self.patient.patientData.name] && [BaseViewController isEmptyString:self.patient.patientData.lastName])
	{
		[[AlertViewBlocks getIstance] informationConfirmAlertViewWithmessage:@"Please insert patient name or surname!" confirmBlock:^{
		} cancelBlock:^{
			[[DBManager getIstance] deleteObject:self.patient];
			PatientListViewController *parent = (PatientListViewController*)self.parentViewController;
			[parent.menu selectItemAtIndex:0];
			[self parent:parent popViewController:self animateWithDuration:0.2];
		}];
		return;
	}
	
	self.patient.hasPatientData = [NSNumber numberWithBool:YES];
	[[DBManager getIstance] saveContext];
	
	if ([self.patient.hasFirstVisit boolValue])
	{
		PatientListViewController *patientListViewController = (PatientListViewController*)self.parentViewController;
		[patientListViewController pushPatientRoot:self patient:self.patient];
	}
	else
	{
		PatientListViewController *patientListViewController = (PatientListViewController*)self.parentViewController;
		[patientListViewController pushMedicalHistory:self patient:self.patient];
	}
}

- (IBAction)back:(id)sender
{
	[[DBManager getIstance] deleteObject:self.patient];
	PatientListViewController *parent = (PatientListViewController*)self.parentViewController;
	[parent.menu selectItemAtIndex:0];
	[self parent:parent popViewController:self animateWithDuration:0.2];
	

	/*
	if (![BaseViewController isEmptyString:self.patient.patientData.name] || ![BaseViewController isEmptyString:self.patient.patientData.lastName])
	{
		PatientListViewController *parent = (PatientListViewController*)self.parentViewController;
		[parent.menu selectItemAtIndex:0];
		self.patient.hasPatientData = [NSNumber numberWithBool:YES];
		[[DBManager getIstance] saveContext];
		[self parent:parent popViewController:self animateWithDuration:0.2];
	}
	else if ([BaseViewController isEmptyString:self.patient.patientData.name] && [BaseViewController isEmptyString:self.patient.patientData.lastName])
	{
		[[AlertViewBlocks getIstance] customConfirmAlertViewWithTitle:@"Warning" message:@"Please insert patient name or surname!" confirmBlock:^{
		} cancelBlock:^{
			[[DBManager getIstance] deleteObject:self.patient];
			PatientListViewController *parent = (PatientListViewController*)self.parentViewController;
			[parent.menu selectItemAtIndex:0];
			[self parent:parent popViewController:self animateWithDuration:0.2];
		}];
	}	
	 */
}

- (IBAction)viewSettings:(id)sender
{
	SettingsViewController *settingsViewController = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
	[self parent:self addChildViewController:settingsViewController superView:self.view animateWithDuration:0.2];
}



@end
