//
//  AppDelegate.m
//  SmartDoc
//
//  Created by Francesca Corsini on 01/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "AppDelegate.h"
#import "RootViewController.h"
#import "CalendarManager.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
/*
	// local notification
	UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
	if (notification)
	{
		NSString *message = notification.userInfo[@"message"];
		[[AlertViewBlocks getIstance] customConfirmAlertViewWithTitle:@"Action to do" message:message confirmBlock:^{
		} cancelBlock:^{
		}];
	}
	application.applicationIconBadgeNumber = 0;
	*/
	
	// rootViewController
	self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.viewController = [[RootViewController alloc] initWithNibName:@"RootViewController" bundle:nil];
	self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    
	return YES;
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
/*
	if (notification)
	{
		NSString *message = notification.userInfo[@"message"];
		[[AlertViewBlocks getIstance] customConfirmAlertViewWithTitle:@"Action to do" message:message confirmBlock:^{
		} cancelBlock:^{
		}];
	}
	application.applicationIconBadgeNumber = 0;
	*/
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{	
	//application.applicationIconBadgeNumber = 0;
	
	// refresh local Calendar with data taken from local db
	[[CalendarManager getIstance] updateCalendarConfirmBlock:^(NSString *success) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[[AlertViewBlocks getIstance] informationAlertViewWithMessage:@"Succeeded to sync the Calendar with appointments of the app." confirmBlock:^{
			}];
		});
		
	} cancelBlock:^(NSError *error) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[[AlertViewBlocks getIstance] warningAlertViewWithMessage:@"Failed to sync the Calendar with appointments of the app." confirmBlock:^{
			}];
		});
		
	}];
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
