//
//  RootViewController.h
//  SmartDoc
//
//  Created by Francesca Corsini on 19/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "BaseViewController.h"

@interface RootViewController : BaseViewController <DBManagerSyncDelegate>

@end
