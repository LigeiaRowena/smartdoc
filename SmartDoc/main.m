//
//  main.m
//  SmartDoc
//
//  Created by Francesca Corsini on 01/07/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
