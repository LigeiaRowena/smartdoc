//
//  DBManager.h
//  SmartDoc
//
//  Created by Francesca Corsini on 18/05/13.
//  Copyright (c) 2013 Andrea. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDB.h"

@protocol DBManagerSyncDelegate <NSObject>
- (void)didBeginSyncDB;
- (void)didFinishSyncDB;
- (void)didFailSyncDBWithError:(NSError*)error;
@end

extern NSString * const SpecData;
extern NSString * const AppointmentData;
extern NSString * const ToDoData;
extern NSString * const DrugNameData;
extern NSString * const DrugActiveData;
extern NSString * const RouteData;
extern NSString * const InsuranceTypeData;
extern NSString * const WorkData;
extern NSString * const CategoryData;
extern NSString * const CittaData;
extern NSString * const CountryData;
extern NSString * const StateData;
extern NSString * const ZipcodeData;
extern NSString * const RiskFactorData;
extern NSString * const OperationData;
extern NSString * const DeliveryData;
extern NSString * const VaccinationData;
extern NSString * const SmokingData;
extern NSString * const AllergyData;
extern NSString * const VisitTypeData;
extern NSString * const SymptomData;
extern NSString * const DiagnosticTestsData;
extern NSString * const ReferrerData;
extern NSString * const MetricSystem;
extern NSString * const ImperialSystem;

@class Patient, ContactPhone, ContactEmail, Action, Operation, Visit, Measurement, PreviousMeasurement, Symptom, DiagnosticTest, PrescribedTherapy, DiseaseHistory, RemoteAnamnesis, ProximalAnamnesis, Dashboard, PatientData, ActualRiskFactor, OtherRiskFactor, Prescription,Settings, SymptomHistory, FatherDiseaseHistory, MotherDiseaseHistory, Vaccination, Allergy, DiagnosticImage, DinamicData;

@interface DBManager : NSObject
{
	NSManagedObjectContext *managedObjectContext;
	NSManagedObjectModel *managedObjectModel;
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
}

@property (nonatomic, strong, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, assign) id <DBManagerSyncDelegate> delegate;

// GENERAL
+ (DBManager *)getIstance;
- (void)beginSyncWithDelegate:(id <DBManagerSyncDelegate>)delegate firstLaunch:(BOOL)firstLaunch;
- (void)stopSync;
- (BOOL)saveContext;
- (void)deleteObject:(NSManagedObject*)object;
- (NSManagedObject*)createEntity:(NSString*)entityName;
- (NSArray*)getEntityArray:(NSString*)entityName;
- (NSURL *)modelPathURL;
- (NSString *)modelPathString;
- (NSURL *)shmPathURL;
- (NSURL *)walPathURL;
- (NSURL *)modelCloudPathURL;
- (NSString *)modelCloudPathString;
// DinamicData
- (DinamicData*)createDinamicDataType:(NSString*)type text:(NSString*)text;
- (NSArray*)getDinamicDataArray:(NSString*)type text:(NSString*)text;
- (BOOL)dbContainsDinamicDataType:(NSString*)type text:(NSString*)text;
- (void)loadDinamicDatas;


// FMDB
- (void)saveDB:(NSString*)fileName;
- (void)readDB:(NSString*)fileName;

// SETTINGS
// Specialization Data
- (NSArray*)createSpecFromPlist;
// Settings
- (Settings*)createSettings;
- (Settings*)getSettings;
- (BOOL)isMetricSystem;
- (BOOL)isImperialSystem;
- (void)setMetricSystem:(NSString*)system;
- (void)setUsername:(NSString *)username;
- (void)setPassword:(NSString *)password;
- (void)setRemember:(BOOL)remember;
- (void)setSession:(NSString *)session;
- (NSString *)getUsername;
- (NSString *)getPassword;
- (BOOL)getRemember;
- (NSString *)getSession;

// PATIENT LIST
// Patient
- (Patient*)createPatientIsFavourite:(BOOL)isFavourite;
- (NSArray*)getPatientsWithCategory:(NSString*)category;
- (NSArray*)getPatientsFavourites:(BOOL)favourites;

// DASHBOARD
- (Dashboard*)createDashboardForPatient:(Patient*)patient;
// Action
- (Action*)createActionForDashboard:(Dashboard*)dashboard dateString:(NSString*)dateString date:(NSDate*)date type:(NSString*)type desc:(NSString*)desc time:(NSString*)time eventIdentifier:(NSString*)eventIdentifier;
// AppointmentData Data
- (NSArray*)createAppointmentTypeFromPlist;
// ToDo Data
- (NSArray*)createToDoFromPlist;

// DRUGS
// Drug Name
- (NSArray*)createDrugNameFromPlist;
// Drug Active
- (NSArray*)createDrugActiveFromPlist;
// Route Data
- (NSArray*)createRouteFromPlist;

// DIAGNOSTIC TESTS
// Diagnostic Tests for Visit
- (DiagnosticTest*)createDiagnosticTestVisitForVisit:(Visit*)visit name:(NSString*)name note:(NSString*)note;
// Diagnostic Tests for Prescription
- (DiagnosticTest*)createDiagnosticTestForPrescription:(Prescription*)prescription name:(NSString*)name note:(NSString*)note;
// Diagnostic Tests for ProximalAnamnesis
- (DiagnosticTest*)createDiagnosticTestForProximalAnamnesis:(ProximalAnamnesis*)proximalAnamnesis type:(NSString*)type note:(NSString*)note date:(NSDate*)date;
// Diagnostic Image
- (DiagnosticImage*)createDiagnosticImageForTest:(DiagnosticTest*)test image:(NSData*)image note:(NSString*)note;

// PATIENT DATA
- (PatientData*)createPatientDataForPatient:(Patient*)patient;
// Contact Phone
- (ContactPhone*)createContactPhoneForPatientData:(PatientData*)patientData text:(NSString*)text type:(NSString*)type;
// Contact Email
- (ContactEmail*)createContactEmailForPatientData:(PatientData*)patientData text:(NSString*)text type:(NSString*)type;
// Insurance Type Data
- (NSArray*)createInsuranceTypesFromPlist;
// Work Data
- (NSArray*)createWorksFromPlist;
// Category Data
- (NSArray*)createCategoryFromPlist;
// Citta Data
- (NSArray*)createCittaFromPlist;
// Country Data
- (NSArray*)createCountriesFromPlist;
// State Data
- (NSArray*)createStatesFromPlist;
// Zipcode Data
- (NSArray*)createZipcodesFromPlist;

// REMOTE ANAMNESIS
- (RemoteAnamnesis*)createRemoteAnamnesisForPatient:(Patient*)patient;
// Father Disease History
- (FatherDiseaseHistory*)createFatherDiseaseHistoryForRemoteAnamnesis:(RemoteAnamnesis*)remoteAnamnesis note:(NSString*)note type:(NSString*)type;
// Mother Disease History
- (MotherDiseaseHistory*)createMotherDiseaseHistoryForRemoteAnamnesis:(RemoteAnamnesis*)remoteAnamnesis note:(NSString*)note type:(NSString*)type;
// Vaccination
- (Vaccination*)createVaccinationForRemoteAnamnesis:(RemoteAnamnesis*)remoteAnamnesis note:(NSString*)note type:(NSString*)type date:(NSDate*)date;
// Operation
- (Operation*)createOperationForRemoteAnamnesis:(RemoteAnamnesis*)remoteAnamnesis name:(NSString*)name note:(NSString*)note;
// Disease History
- (DiseaseHistory*)createDiseaseHistoryForRemoteAnamnesis:(RemoteAnamnesis*)remoteAnamnesis name:(NSString*)name status:(NSNumber*)status;
// Risk Factor Data
- (NSArray*)createRiskFactorsFromPlist;
// Operation Data
- (NSArray*)createOperationFromPlist;
// Delivery Data
- (NSArray*)createDeliveriesFromPlist;
// Vaccination Data
- (NSArray*)createVaccinationsFromPlist;

// PROXIMAL ANAMNESIS
- (ProximalAnamnesis*)createProximalAnamnesisForPatient:(Patient*)patient;
// Symptom History
- (SymptomHistory*)createSymptomHistoryForProximalAnamnesis:(ProximalAnamnesis*)proximalAnamnesis name:(NSString*)name details:(NSString*)details note:(NSString*)note;


// ACTUAL RISK FACTORS
- (ActualRiskFactor*)createActualRiskFactorForPatient:(Patient*)patient;
// Smoking Data
- (NSArray*)createSmokingsFromPlist;
// Allergy Data
- (NSArray*)createAllergyDataFromPlist;
// Allergy
- (Allergy*)createAllergyForActualRiskFactor:(ActualRiskFactor*)actualRiskFactor name:(NSString*)name note:(NSString*)note;


// VISITS
- (Visit*)createVisitForPatient:(Patient*)patient date:(NSDate*)date finalConsideration:(NSString*)finalConsideration info:(NSString*)info type:(NSString*)type;
// Measurement
- (Measurement*)getMeasurementWithName:(NSString*)name visit:(Visit*)visit;
- (NSArray*)getAllMeasurementsWithName:(NSString*)name patient:(Patient*)patient;
- (NSArray*)getMeasurementsSortedWithNameForVisit:(Visit*)visit;
- (NSArray*)getMeasurementsSortedWithIndexForVisit:(Visit*)visit;
// Measurement Imperial System
- (NSDecimalNumber*)getValueImperialSystemMeasurement:(Measurement*)measurement;
- (NSDecimalNumber*)getValueMetricSystemMeasurementName:(NSString*)measurementName measurementValue:(NSDecimalNumber*)measurementValue;
- (NSString*)getUnitImperialSystemMeasurement:(Measurement*)measurement measurementName:(NSString*)measurementName;
- (NSString*)getUnitMetricSystemMeasurement:(Measurement*)measurement measurementName:(NSString*)measurementName;
// Symptom
- (Symptom*)createSymptomForVisit:(Visit*)visit name:(NSString*)name details:(NSString*)details note:(NSString*)note;
// Visit Type Data
- (NSArray*)createVisitTypesFromPlist;
// Symptom Data
- (NSArray*)createSymptomsFromPlist;
// Test Type Data
- (NSArray*)createTestTypesFromPlist;


// HOME THERAPY
// Prescribed Therapy
- (PrescribedTherapy*)createPrescribedTherapyForPatient:(Patient*)patient date:(NSDate*)date drugName:(NSString*)drugName dosage:(NSString*)dosage frequency:(NSString*)frequency route:(NSString*)route active:(BOOL)active suspended:(BOOL)suspended prescribed:(BOOL)prescribed dosageUnit:(NSString*)dosageUnit visit:(Visit*)visit prescription:(Prescription*)prescription;
- (NSArray*)getPrescribedTherapiesFromPatient:(Patient*)patient;
- (NSArray*)getSuspendedTherapiesFromPatient:(Patient*)patient;
- (NSArray*)getTherapiesFromPatient:(Patient*)patient suspended:(BOOL)suspended prescribed:(BOOL)prescribed active:(BOOL)active;
- (NSArray*)getTherapiesFromVisit:(Visit*)visit;
- (NSArray*)getTherapiesFromPrescription:(Prescription*)prescription;

// PRESCRIPTIONS
- (Prescription*)createPrescriptionForPatient:(Patient*)patient date:(NSDate*)date;
// Referrer Data
- (NSArray*)createReferrersPlist;

@end
