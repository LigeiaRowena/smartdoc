//
//  DBManager.m
//  SmartDoc
//
//  Created by Francesca Corsini on 18/05/13.
//  Copyright (c) 2013 Andrea. All rights reserved.
//

#import "DBManager.h"
#import "Utils.h"
#import "Patient.h"
#import "ContactPhone.h"
#import "ContactEmail.h"
#import "Action.h"
#import "Operation.h"
#import "Visit.h"
#import "Measurement.h"
#import "Symptom.h"
#import "DiagnosticTest.h"
#import "NSDate-Utilities.h"
#import "PrescribedTherapy.h"
#import "DiseaseHistory.h"
#import "RemoteAnamnesis.h"
#import "ProximalAnamnesis.h"
#import "Dashboard.h"
#import "PatientData.h"
#import "ActualRiskFactor.h"
#import "OtherRiskFactor.h"
#import "Prescription.h"
#import "Settings.h"
#import "SymptomHistory.h"
#import "FatherDiseaseHistory.h"
#import "MotherDiseaseHistory.h"
#import "Vaccination.h"
#import "Allergy.h"
#import "DiagnosticImage.h"
#import "DinamicData.h"

NSString * const SpecData = @"SpecData";
NSString * const AppointmentData = @"AppointmentData";
NSString * const ToDoData = @"ToDoData";
NSString * const DrugNameData = @"DrugNameData";
NSString * const DrugActiveData = @"DrugActiveData";
NSString * const RouteData = @"RouteData";
NSString * const InsuranceTypeData = @"InsuranceTypeData";
NSString * const WorkData = @"WorkData";
NSString * const CategoryData = @"CategoryData";
NSString * const CittaData = @"CittaData";
NSString * const CountryData = @"CountryData";
NSString * const StateData = @"StateData";
NSString * const ZipcodeData = @"ZipcodeData";
NSString * const RiskFactorData = @"RiskFactorData";
NSString * const OperationData = @"OperationData";
NSString * const DeliveryData = @"DeliveryData";
NSString * const VaccinationData = @"VaccinationData";
NSString * const SmokingData = @"SmokingData";
NSString * const AllergyData = @"AllergyData";
NSString * const VisitTypeData = @"VisitTypeData";
NSString * const SymptomData = @"SymptomData";
NSString * const DiagnosticTestsData = @"DiagnosticTestsData";
NSString * const ReferrerData = @"ReferrerData";
NSString * const MetricSystem = @"MetricSystem";
NSString * const ImperialSystem = @"ImperialSystem";

#pragma mark - CORE DATA

@implementation DBManager

static DBManager *instance;

@synthesize managedObjectContext;

#pragma mark - GENERAL

+ (DBManager *) getIstance
{
	@synchronized(self)
	{
		if(instance == nil)
		{
            instance = [[DBManager alloc] init];
            return instance;
		}
	}
	return instance;
}

- (id)init
{
    if (self = [super init])
	{
    }
	return self;
}


- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

- (NSString *)modelPathString
{
	NSURL *url = [self modelPathURL];
	return url.path;
}

- (NSURL *)modelPathURL
{
	return [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"SmartDocModel.sqlite"]];
}

- (NSURL *)shmPathURL
{
	return [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"SmartDocModel.sqlite-shm"]];
}

- (NSURL *)walPathURL
{
	return [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"SmartDocModel.sqlite-wal"]];
}

- (NSURL *)modelCloudPathURL
{
	return [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"SmartDocModel_cloud.sqlite"]];
}

- (NSString *)modelCloudPathString
{
	NSURL *url = [self modelCloudPathURL];
	return url.path;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (managedObjectModel != nil)
	{
        return managedObjectModel;
    }
    managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    return managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
	if (persistentStoreCoordinator != nil)
	{
		return persistentStoreCoordinator;
	}
	
	// Adding an Persistent Store to Core Data
	persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
	
	// Notification sent after the iCloud (“ubiquity”) identity has changed
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector (iCloudAccountAvailabilityChanged:) name: NSUbiquityIdentityDidChangeNotification object: nil];
	
	//NSURL *iCloudURL = [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];
	
	return [self localPersistentStoreCoordinator];
	//TODO: enable iCloud
	/*
	if (iCloudURL)
		return [self iCloudPersistentStoreCoordinator];
	else
		return [self localPersistentStoreCoordinator];
	 */
}

- (NSPersistentStoreCoordinator *)iCloudPersistentStoreCoordinator
{
	[self addSyncObservers];
	
	NSURL *storeUrl = [self modelPathURL];
	NSError *error = nil;
	NSDictionary *storeOptions = @{
			NSPersistentStoreUbiquitousContentNameKey: @"MyAppCloudStore",
			NSMigratePersistentStoresAutomaticallyOption: [NSNumber numberWithBool:YES],
			NSInferMappingModelAutomaticallyOption: [NSNumber numberWithBool:YES],
			NSPersistentStoreRebuildFromUbiquitousContentOption: [NSNumber numberWithBool:YES]
								   };
	[persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:storeOptions error:&error];
	
	return persistentStoreCoordinator;
}

- (NSPersistentStoreCoordinator *)localPersistentStoreCoordinator
{
	NSURL *storeUrl = [self modelPathURL];
	NSError *error = nil;
	NSDictionary *storeOptions = @{
			NSMigratePersistentStoresAutomaticallyOption: [NSNumber numberWithBool:YES],
			NSInferMappingModelAutomaticallyOption: [NSNumber numberWithBool:YES],
			NSPersistentStoreFileProtectionKey: NSFileProtectionComplete
								   };
	[persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:storeOptions error:&error];

	return persistentStoreCoordinator;
}

- (void)initCoreData
{
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	if (coordinator != nil)
	{
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
	else
		if (self.delegate != nil)
			[self.delegate didFailSyncDBWithError:nil];
	
	// No iCloud sync
	if (![[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil])
	{
		if (self.delegate != nil)
			[self.delegate didFinishSyncDB];
	}
}

- (void)beginSyncWithDelegate:(id <DBManagerSyncDelegate>)delegate firstLaunch:(BOOL)firstLaunch
{
	if (self.delegate)
		self.delegate = nil;
	self.delegate = delegate;
	
	if (firstLaunch)
		[self initCoreData];
	else
		[self addSyncObservers];
}

- (void)stopSync
{
	self.delegate = nil;
	[self removeSyncObservers];
}

- (NSArray*)getArrayFromPlist:(NSString*)plist key:(NSString*)key
{
	NSString *path = [[NSBundle mainBundle] pathForResource:[plist stringByDeletingPathExtension] ofType:[plist pathExtension]];
	return [NSDictionary dictionaryWithContentsOfFile:path][key];
}

- (NSArray*)getArrayFromPlist:(NSString*)plist
{
	NSString *path = [[NSBundle mainBundle] pathForResource:[plist stringByDeletingPathExtension] ofType:[plist pathExtension]];
	return [NSArray arrayWithContentsOfFile:path];
}


-(BOOL) saveContext
{
    NSError *error;
    if (![managedObjectContext save:&error])
	{
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		return NO;
	}
	return YES;
}

- (void)deleteObject:(NSManagedObject*)object
{
	[managedObjectContext deleteObject:object];
	[self saveContext];
}

- (BOOL)isEmptyString:(NSString*)string
{
	if (string != nil && ![(NSNull*)string isEqual:[NSNull null]] && ![string isEqualToString:@""])
		return NO;
	else
		return YES;
}

- (NSManagedObject*)createEntity:(NSString*)entityName
{
	NSManagedObject *entity = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext: managedObjectContext];
	[self saveContext];
	return entity;
}

- (NSArray*)getEntityArray:(NSString*)entityName
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
	[fetchRequest setEntity:entity];
	
	NSError *error;
	NSArray *foo = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
	if (foo == nil || [foo count] == 0)
		return @[];
	else
		return foo;
}

#pragma mark iCloud Notifications

- (void)addSyncObservers
{
	[self removeSyncObservers];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(storesDidChange:) name:NSPersistentStoreCoordinatorStoresDidChangeNotification object:persistentStoreCoordinator];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(persistentStoreDidImportUbiquitousContentChanges:) name:NSPersistentStoreDidImportUbiquitousContentChangesNotification object:persistentStoreCoordinator];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(storesWillChange:) name:NSPersistentStoreCoordinatorStoresWillChangeNotification object:persistentStoreCoordinator];
}

- (void)removeSyncObservers
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:NSPersistentStoreCoordinatorStoresDidChangeNotification object:persistentStoreCoordinator];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:NSPersistentStoreDidImportUbiquitousContentChangesNotification object:persistentStoreCoordinator];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:NSPersistentStoreCoordinatorStoresWillChangeNotification object:persistentStoreCoordinator];
}

- (void)storesDidChange:(NSNotification*)notification
{
	// at first launch of the app this key refers to an array with all the persistent stores added
	id addedPersistentStore = [notification.userInfo objectForKey:NSAddedPersistentStoresKey];
	if ([addedPersistentStore count] > 0)
		NSLog(@"A persistent store was added");
	
	// this key refers to an array with all the persistent stores removed
	id removedPersistentStore = [notification.userInfo objectForKey:NSRemovedPersistentStoresKey];
	if ([removedPersistentStore count] > 0)
		NSLog(@"A persistent store was removed");
	
	// this key refers to an array with all the persistent stores changed
	id changedPersistentStore = [notification.userInfo objectForKey:NSUUIDChangedPersistentStoresKey];
	if ([changedPersistentStore count] > 0)
		NSLog(@"A persistent store changed");
	
	// this key refers to the type of event
	NSNumber *storeUbiquitousTransition = [notification.userInfo objectForKey:NSPersistentStoreUbiquitousTransitionTypeKey];
	switch (storeUbiquitousTransition.integerValue) {
		case NSPersistentStoreUbiquitousTransitionTypeAccountAdded: {
			NSLog(@"storesDidChange: Account Added");
			// account was added
		}
			break;
		case NSPersistentStoreUbiquitousTransitionTypeAccountRemoved: {
			NSLog(@"storesDidChange: Account Removed");
			// account was removed
		}
			break;
		case NSPersistentStoreUbiquitousTransitionTypeContentRemoved: {
			NSLog(@"storesDidChange: Content Removed");
			// content was removed
		}
			break;
		case NSPersistentStoreUbiquitousTransitionTypeInitialImportCompleted: {
			NSLog(@"storesDidChange: Initial Import:");
			if (self.delegate != nil)
				[self.delegate didFinishSyncDB];
			// initial import
		}
			break;
			
		default:
			if (self.delegate != nil)
				[self.delegate didBeginSyncDB];
			break;
	}
}

- (void)persistentStoreDidImportUbiquitousContentChanges:(NSNotification*)notification
{
	// Received and merge updates from iCloud
	[self.managedObjectContext mergeChangesFromContextDidSaveNotification:notification];
	NSDictionary *userInfo = [notification userInfo];
	if ([userInfo[@"updated"] count] > 0 || [userInfo[@"inserted"] count] > 0)
	{
		if (self.delegate != nil)
			[self.delegate didFinishSyncDB];
	}
}

- (void)storesWillChange:(NSNotification*)notification
{
	if ([self.managedObjectContext hasChanges])
	{
		NSError *err = nil;
		[self.managedObjectContext save:&err];
		if (err)
		{
			if (self.delegate != nil)
				[self.delegate didFailSyncDBWithError:err];
		}
		else
		{
			if (self.delegate != nil)
				[self.delegate didBeginSyncDB];
		}
	}
	else
	{
		[self.managedObjectContext reset];
		if (self.delegate != nil)
			[self.delegate didBeginSyncDB];
	}
}

- (void)iCloudAccountAvailabilityChanged:(NSNotification*)notification
{
	//NSLog(@"iCloudAccountAvailabilityChanged: ubiquityIdentityToken=%@", [[NSFileManager defaultManager] ubiquityIdentityToken]);
}

#pragma mark DinamicData

- (DinamicData*)createDinamicDataType:(NSString*)type text:(NSString*)text
{
	DinamicData *entity = [NSEntityDescription insertNewObjectForEntityForName:@"DinamicData" inManagedObjectContext: managedObjectContext];
	entity.type = type;
	if (![self isEmptyString:text])
		entity.text = text;
	[self saveContext];
	return entity;
}

- (NSArray*)getDinamicDataArray:(NSString*)type text:(NSString*)text
{
	// get all the dinamic datas
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"DinamicData" inManagedObjectContext:managedObjectContext];
	[fetchRequest setEntity:entity];
	NSPredicate *typePredicate = [NSPredicate predicateWithFormat: @"(type==%@)", type];
	NSPredicate *textPredicate = [NSPredicate predicateWithFormat: @"(text==%@)", text];
	if (![self isEmptyString:text])
		[fetchRequest setPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:@[typePredicate, textPredicate]]];
	else
		[fetchRequest setPredicate:typePredicate];
	NSError *error;
	NSMutableArray *array = [managedObjectContext executeFetchRequest:fetchRequest error:&error].mutableCopy;
	
	// detect any blank object
	[array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		DinamicData *object = (DinamicData*)obj;
		if ([self isEmptyString:object.text] || [self isEmptyString:object.type])
		{
			[[DBManager getIstance] deleteObject:object];
			[array removeObject:object];
		}
	}];
	
	// order alphabetically
	NSArray *orderedArray = [array sortedArrayUsingComparator: ^(id id_1, id id_2) {
		DinamicData *m_1 = (DinamicData*) id_1;
		DinamicData *m_2 = (DinamicData*) id_2;
		NSString *d_1 = m_1.text;
		NSString *d_2 = m_2.text;
		return [d_1 compare: d_2];
	}].mutableCopy;

	if ([orderedArray count] > 0)
		return orderedArray;
	else
		return @[];
}

- (BOOL)dbContainsDinamicDataType:(NSString*)type text:(NSString*)text
{
	NSArray *foo = [self getDinamicDataArray:type text:text];
	if ([foo count] > 0)
		return TRUE;
	else
		return FALSE;
}

- (void)loadDinamicDatas
{	
	// Specialization Data
	[self createSpecFromPlist];
	
	// AppointmentData Data
	[self createAppointmentTypeFromPlist];
	
	// ToDo Data
	[self createToDoFromPlist];
		
	// Route Data
	[self createRouteFromPlist];
	
	// Insurance Type Data
	[self createInsuranceTypesFromPlist];
	
	// Work Data
	[self createWorksFromPlist];
	
	// Category Data
	[self createCategoryFromPlist];
	
	// Citta Data
	[self createCittaFromPlist];
	
	// Country Data
	[self createCountriesFromPlist];
	
	// State Data
	[self createStatesFromPlist];
	
	// Zipcode Data
	[self createZipcodesFromPlist];
	
	// Risk Factor Data
	[self createRiskFactorsFromPlist];
	
	// Operation Data
	[self createOperationFromPlist];
	
	// Delivery Data
	[self createDeliveriesFromPlist];
	
	// Vaccination Data
	[self createVaccinationsFromPlist];
	
	// Smoking Data
	[self createSmokingsFromPlist];
	
	// Allergy Data
	[self createAllergyDataFromPlist];
	
	// Visit Type Data
	[self createVisitTypesFromPlist];
	
	// Symptom Data
	[self createSymptomsFromPlist];
	
	// Test Type Data
	[self createTestTypesFromPlist];
	
	// Referrer Data
	[self createReferrersPlist];
	
	// Drug Name
	//[self createDrugNameFromPlist];
	
	// Drug Active
	[self createDrugActiveFromPlist];
}

#pragma mark - FMDB

- (void)saveDB:(NSString*)fileName
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [[NSString alloc] initWithString:[paths objectAtIndex:0]];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:fileName];
	NSString *bundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:fileName];
	
	//se già esiste il db in Documents non sovrascriverlo
	if (![fileManager fileExistsAtPath:filePath]) {
		NSError *error;
		[fileManager copyItemAtPath:bundlePath toPath:filePath error:&error];
	}
}

- (void)readDB:(NSString*)fileName
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [[NSString alloc] initWithString:[paths objectAtIndex:0]];
	NSString *defaultDBPath = [documentsDirectory stringByAppendingPathComponent:fileName];
	
	FMDatabase *db = [FMDatabase databaseWithPath:defaultDBPath];
	if (![db open])
	{
		NSLog(@"Could not open db.");
		return;
	}
	
	// seleziona ogni riga della tabella
	FMResultSet *s = [db executeQuery:@"SELECT * FROM comuni"];
	while ([s next]) {
		// count di tutte le colonne della tabella
		//int columnCount = [s columnCount];
		
		// indice della colonna "cap"
		//int capColumnIndex = [s columnIndexForName:@"cap"];
		
		// nome della colonna numero 5
		//NSString *capColumnName = [s columnNameForIndex:5];
		
		// valore della riga selezionata della colonna "cap"
		//NSString *value = [s stringForColumn:@"cap"];
		
		// dictionary con tutti i valori della riga selezionata
		//NSDictionary *dictionary = [s resultDictionary];
	}
	
	
	// seleziona solo le colonne "comune" e "cap" dalla tabella
	FMResultSet *rs = [db executeQuery:@"select comune, cap from comuni"];
	while ([rs next]) {
		// valore della riga selezionata della colonna "cap"
		//NSString *value = [rs stringForColumn:@"cap"];
		
		// dictionary con tutti i valori della riga selezionata
		//NSDictionary *dictionary = [rs resultDictionary];
	}
	
	[db close];
}

#pragma mark - SETTINGS

#pragma mark Specialization Data

- (NSArray*)createSpecFromPlist
{
	NSDictionary *dict = [Utils getDictionaryFromPlist:QUESTIONS_PLIST];
	NSArray *array = [[dict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:SpecData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:SpecData text:@""];
}

#pragma mark Settings

- (Settings*)createSettings
{	
	Settings *settings = [NSEntityDescription insertNewObjectForEntityForName:@"Settings" inManagedObjectContext: managedObjectContext];
	settings.address = @"";
	settings.phoneWork = @"";
	settings.phoneMobile = @"";
	settings.email = @"";
	settings.speciality = @"";
	settings.title = @"";
	settings.firstName = @"";
	settings.lastName = @"";
	settings.organization = @"";
	settings.web = @"";
	settings.systemMeasurement = MetricSystem;
	settings.firstLaunch = @YES;
	settings.firstDashboardLaunch = @YES;
	settings.appointmentDuration = @30;
	[self saveContext];
	return settings;
}

- (Settings*)getSettings
{
	return [[self getEntityArray:@"Settings"] lastObject];
}

- (BOOL)isMetricSystem
{
	if ([[self getSettings].systemMeasurement isEqualToString:MetricSystem])
		return YES;
	else
		return NO;
}

- (BOOL)isImperialSystem
{
	if ([[self getSettings].systemMeasurement isEqualToString:ImperialSystem])
		return YES;
	else
		return NO;
}

- (void)setMetricSystem:(NSString*)system
{
	Settings *settings = [self getSettings];
	settings.systemMeasurement = system;
	[self saveContext];
}

- (void)setUsername:(NSString *)username
{
	NSMutableDictionary *dict = [Utils getDictionaryFromPlist:LOCAL_SETTINGS].mutableCopy;
	[dict setValue:username forKey:@"username"];
	[Utils setDictionary:dict toPlist:LOCAL_SETTINGS];
}

- (void)setPassword:(NSString *)password
{
	NSMutableDictionary *dict = [Utils getDictionaryFromPlist:LOCAL_SETTINGS].mutableCopy;
	[dict setValue:password forKey:@"password"];
	[Utils setDictionary:dict toPlist:LOCAL_SETTINGS];
}

- (void)setRemember:(BOOL)remember
{
	NSMutableDictionary *dict = [Utils getDictionaryFromPlist:LOCAL_SETTINGS].mutableCopy;
	[dict setValue:[NSNumber numberWithBool:remember] forKey:@"remember"];
	[Utils setDictionary:dict toPlist:LOCAL_SETTINGS];
}

- (void)setSession:(NSString *)session
{
	NSMutableDictionary *dict = [Utils getDictionaryFromPlist:LOCAL_SETTINGS].mutableCopy;
	[dict setValue:session forKey:@"session"];
	[Utils setDictionary:dict toPlist:LOCAL_SETTINGS];
}

- (NSString *)getUsername
{
	return [Utils getObjectFromPlist:LOCAL_SETTINGS key:@"username"];
}

- (NSString *)getPassword
{
	return [Utils getObjectFromPlist:LOCAL_SETTINGS key:@"password"];
}

- (BOOL)getRemember
{
	return [[Utils getObjectFromPlist:LOCAL_SETTINGS key:@"remember"] boolValue];
}

- (NSString *)getSession
{
	return [Utils getObjectFromPlist:LOCAL_SETTINGS key:@"session"];
}


#pragma mark - PATIENT LIST

#pragma mark Patient

- (Patient*)createPatientIsFavourite:(BOOL)isFavourite
{
	Patient *patient = [NSEntityDescription insertNewObjectForEntityForName:@"Patient" inManagedObjectContext: managedObjectContext];
	patient.hasFirstVisit = [NSNumber numberWithBool:NO];
	patient.hasPatientData = [NSNumber numberWithBool:NO];
	patient.remoteAnamnesis = [self createRemoteAnamnesisForPatient:patient];
	patient.proximalAnamnesis = [self createProximalAnamnesisForPatient:patient];
	patient.actualRiskFactor = [self createActualRiskFactorForPatient:patient];
	patient.dashboard = [self createDashboardForPatient:patient];
	patient.patientData = [self createPatientDataForPatient:patient];
	patient.patientData.name = @"";
	patient.patientData.middleName = @"";
	patient.patientData.lastName = @"";
	patient.creationDate = [NSDate date];
	patient.favourite = [NSNumber numberWithBool:isFavourite];
	NSArray *diseases = [self getArrayFromPlist:PLIST_FILE key:@"DiseaseHistory"];
	for (NSString *disease in diseases)
		[self createDiseaseHistoryForRemoteAnamnesis:patient.remoteAnamnesis name:disease status:[NSNumber numberWithBool:NO]];
	NSArray *otherRiskFactors = [self getArrayFromPlist:PLIST_FILE key:@"OtherRiskFactor"];
	for (NSString *risk in otherRiskFactors)
		[self createOtherRiskFactorForActualRiskFactor:patient.actualRiskFactor name:risk];

	[self saveContext];
	return patient;
}

- (NSArray*)getPatientsWithCategory:(NSString*)category
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Patient"inManagedObjectContext:managedObjectContext];
	[fetchRequest setEntity:entity];
	if (category != nil && ![category isEqual:[NSNull null]] && ![category isEqualToString:@""])
	{
		NSPredicate *predicate = [NSPredicate predicateWithFormat: @"(category==%@)", category];
		[fetchRequest setPredicate:predicate];
	}
	NSError *error;
	NSArray *array = [managedObjectContext executeFetchRequest:fetchRequest error:&error];;
	return array;
}

- (NSArray*)getPatientsFavourites:(BOOL)favourites
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Patient"inManagedObjectContext:managedObjectContext];
	[fetchRequest setEntity:entity];
	if (favourites)
	{
		NSPredicate *predicate = [NSPredicate predicateWithFormat: @"(favourite==%d)", favourites];
		[fetchRequest setPredicate:predicate];
	}
	NSError *error;
	return [managedObjectContext executeFetchRequest:fetchRequest error:&error];
}



#pragma mark - DASHBOARD

- (Dashboard*)createDashboardForPatient:(Patient*)patient
{
	Dashboard *dashboard = [NSEntityDescription insertNewObjectForEntityForName:@"Dashboard" inManagedObjectContext: managedObjectContext];
	dashboard.active = [NSNumber numberWithBool:YES];
	patient.dashboard = dashboard;
	[self saveContext];
	return dashboard;
}

#pragma mark Action

- (Action*)createActionForDashboard:(Dashboard*)dashboard dateString:(NSString*)dateString date:(NSDate*)date type:(NSString*)type desc:(NSString*)desc time:(NSString*)time eventIdentifier:(NSString*)eventIdentifier
{
	Action *action = [NSEntityDescription insertNewObjectForEntityForName:@"Action" inManagedObjectContext: managedObjectContext];
	action.dateString = dateString;
	action.type = type;
	if (date != nil)
		action.date = date;
	action.desc = desc;
	action.time = time;
	action.eventIdentifier = eventIdentifier;
	[dashboard addActionsObject:action];
	[self saveContext];
	return action;
}

- (NSDate*)getDateFromSpecificDate:(NSString*)dateString
{
	NSDate *date;
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
	[formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"]];
	[formatter setDateFormat:@"dd/MM/yyyy"];
	
	if ([dateString isEqualToString:@"ASAP"])
		date = [NSDate dateTomorrow];
	else if ([dateString isEqualToString:@"1 Week"])
		date = [[NSDate date] dateByAddingDays:7];
	else if ([dateString isEqualToString:@"2 Weeks"])
		date = [[NSDate date] dateByAddingDays:14];
	else if ([dateString isEqualToString:@"1 Month"])
		date = [[NSDate date] dateByAddingMonths:1];
	else
		date = [formatter dateFromString:dateString];
	
	return date;
}

#pragma mark AppointmentData Data

- (NSArray*)createAppointmentTypeFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:AppointmentData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:AppointmentData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:AppointmentData text:@""];
}

#pragma mark ToDo Data

- (NSArray*)createToDoFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:ToDoData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:ToDoData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:AppointmentData text:@""];
}

#pragma mark - DRUGS

#pragma mark Drug Name

- (NSArray*)createDrugNameFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:DrugNameData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:DrugNameData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:DrugNameData text:@""];
}

#pragma mark Drug Active

- (NSArray*)createDrugActiveFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:DrugActiveData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:DrugActiveData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:DrugActiveData text:@""];
}

#pragma mark Route Data

- (NSArray*)createRouteFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:RouteData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:RouteData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:RouteData text:@""];
}

#pragma mark - PATIENT DATA

- (PatientData*)createPatientDataForPatient:(Patient*)patient
{
	PatientData *patientData = [NSEntityDescription insertNewObjectForEntityForName:@"PatientData" inManagedObjectContext: managedObjectContext];
	patient.patientData = patientData;
	[self saveContext];
	return patientData;
}

#pragma mark Contact Phone

- (ContactPhone*)createContactPhoneForPatientData:(PatientData*)patientData text:(NSString*)text type:(NSString*)type
{
	ContactPhone *contact = [NSEntityDescription insertNewObjectForEntityForName:@"ContactPhone" inManagedObjectContext: managedObjectContext];
	contact.text = text;
	contact.type = type;
	[patientData addPhoneContactsObject:contact];
	[self saveContext];
	return contact;
}

#pragma mark Contact Email

- (ContactEmail*)createContactEmailForPatientData:(PatientData*)patientData text:(NSString*)text type:(NSString*)type
{
	ContactEmail *contact = [NSEntityDescription insertNewObjectForEntityForName:@"ContactEmail" inManagedObjectContext: managedObjectContext];
	contact.text = text;
	contact.type = type;
	[patientData addEmailContactsObject:contact];
	[self saveContext];
	return contact;
}

#pragma mark Insurance Type Data

- (NSArray*)createInsuranceTypesFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:InsuranceTypeData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:InsuranceTypeData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:InsuranceTypeData text:@""];
}

#pragma mark Work Data

- (NSArray*)createWorksFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:WorkData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:WorkData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:WorkData text:@""];
}

#pragma mark Category Data

- (NSArray*)createCategoryFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:CategoryData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:CategoryData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:CategoryData text:@""];
}

#pragma mark Citta Data

- (NSArray*)createCittaFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:CittaData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:CittaData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:CittaData text:@""];
}

#pragma mark Country Data

- (NSArray*)createCountriesFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:CountryData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:CountryData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:CountryData text:@""];
}

#pragma mark State Data

- (NSArray*)createStatesFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:StateData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:StateData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:StateData text:@""];
}

#pragma mark Zipcode Data

- (NSArray*)createZipcodesFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:ZipcodeData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:ZipcodeData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:ZipcodeData text:@""];
}

#pragma mark - REMOTE ANAMNESIS

- (RemoteAnamnesis*)createRemoteAnamnesisForPatient:(Patient*)patient
{
	RemoteAnamnesis *remoteAnamnesis = [NSEntityDescription insertNewObjectForEntityForName:@"RemoteAnamnesis" inManagedObjectContext: managedObjectContext];
	patient.remoteAnamnesis = remoteAnamnesis;
	[self saveContext];
	return remoteAnamnesis;
}

#pragma mark Father Disease History

- (FatherDiseaseHistory*)createFatherDiseaseHistoryForRemoteAnamnesis:(RemoteAnamnesis*)remoteAnamnesis note:(NSString*)note type:(NSString*)type
{
	FatherDiseaseHistory *history = [NSEntityDescription insertNewObjectForEntityForName:@"FatherDiseaseHistory" inManagedObjectContext: managedObjectContext];
	history.type = type;
	history.note = note;
	[remoteAnamnesis addFatherDiseasesObject:history];
	[self saveContext];
	return history;
}

#pragma mark Mother Disease History

- (MotherDiseaseHistory*)createMotherDiseaseHistoryForRemoteAnamnesis:(RemoteAnamnesis*)remoteAnamnesis note:(NSString*)note type:(NSString*)type
{
	MotherDiseaseHistory *history = [NSEntityDescription insertNewObjectForEntityForName:@"MotherDiseaseHistory" inManagedObjectContext: managedObjectContext];
	history.type = type;
	history.note = note;
	[remoteAnamnesis addMotherDiseasesObject:history];
	[self saveContext];
	return history;
}

#pragma mark Vaccination

- (Vaccination*)createVaccinationForRemoteAnamnesis:(RemoteAnamnesis*)remoteAnamnesis note:(NSString*)note type:(NSString*)type date:(NSDate*)date
{
	Vaccination *vaccination = [NSEntityDescription insertNewObjectForEntityForName:@"Vaccination" inManagedObjectContext: managedObjectContext];
	vaccination.type = type;
	vaccination.note = note;
	vaccination.date = date;
	[remoteAnamnesis addVaccinationsObject:vaccination];
	[self saveContext];
	return vaccination;
}

#pragma mark Operation

- (Operation*)createOperationForRemoteAnamnesis:(RemoteAnamnesis*)remoteAnamnesis name:(NSString*)name note:(NSString*)note
{
	Operation *operation = [NSEntityDescription insertNewObjectForEntityForName:@"Operation" inManagedObjectContext: managedObjectContext];
	operation.name = name;
	operation.note = note;
	[remoteAnamnesis addOperationsObject:operation];
	[self saveContext];
	return operation;
}

#pragma mark Disease History

- (DiseaseHistory*)createDiseaseHistoryForRemoteAnamnesis:(RemoteAnamnesis*)remoteAnamnesis name:(NSString*)name status:(NSNumber*)status
{
	DiseaseHistory *diseaseHistory = [NSEntityDescription insertNewObjectForEntityForName:@"DiseaseHistory" inManagedObjectContext: managedObjectContext];
	diseaseHistory.name = name;
	diseaseHistory.status = status;
	diseaseHistory.date = [NSDate date];
	[remoteAnamnesis addDiseaseHistoryObject:diseaseHistory];
	[self saveContext];
	return diseaseHistory;
}

#pragma mark Risk Factor Data

- (NSArray*)createRiskFactorsFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:RiskFactorData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:RiskFactorData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:RiskFactorData text:@""];
}

#pragma mark Operation Data

- (NSArray*)createOperationFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:OperationData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:OperationData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:OperationData text:@""];
}

#pragma mark Delivery Data

- (NSArray*)createDeliveriesFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:DeliveryData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:DeliveryData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:DeliveryData text:@""];
}

#pragma mark Vaccination Data

- (NSArray*)createVaccinationsFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:VaccinationData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:VaccinationData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:VaccinationData text:@""];
}

#pragma mark - PROXIMAL ANAMNESIS

- (ProximalAnamnesis*)createProximalAnamnesisForPatient:(Patient*)patient
{
	ProximalAnamnesis *proximalAnamnesis = [NSEntityDescription insertNewObjectForEntityForName:@"ProximalAnamnesis" inManagedObjectContext: managedObjectContext];
	patient.proximalAnamnesis = proximalAnamnesis;
	[self saveContext];
	return proximalAnamnesis;
}

#pragma mark Symptom History

- (SymptomHistory*)createSymptomHistoryForProximalAnamnesis:(ProximalAnamnesis*)proximalAnamnesis name:(NSString*)name details:(NSString*)details note:(NSString*)note
{
	SymptomHistory *symptomHistory = [NSEntityDescription insertNewObjectForEntityForName:@"SymptomHistory" inManagedObjectContext: managedObjectContext];
	symptomHistory.name = name;
	symptomHistory.details = details;
	symptomHistory.note = note;
	[proximalAnamnesis addSymptomsHistoryObject:symptomHistory];
	[self saveContext];
	return symptomHistory;
}

#pragma mark - ACTUAL RISK FACTORS

- (ActualRiskFactor*)createActualRiskFactorForPatient:(Patient*)patient
{
	ActualRiskFactor *actualRiskFactor = [NSEntityDescription insertNewObjectForEntityForName:@"ActualRiskFactor" inManagedObjectContext: managedObjectContext];
	patient.actualRiskFactor = actualRiskFactor;
	[self saveContext];
	return actualRiskFactor;
}

#pragma mark Smoking Data

- (NSArray*)createSmokingsFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:SmokingData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:SmokingData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:SmokingData text:@""];
}

#pragma mark Other Risk Factor

- (OtherRiskFactor*)createOtherRiskFactorForActualRiskFactor:(ActualRiskFactor*)actualRiskFactor name:(NSString*)name
{
	OtherRiskFactor *otherRiskFactor = [NSEntityDescription insertNewObjectForEntityForName:@"OtherRiskFactor" inManagedObjectContext: managedObjectContext];
	otherRiskFactor.name = name;
	otherRiskFactor.status = [NSNumber numberWithBool:NO];
	[actualRiskFactor addOtherRiskFactorsObject:otherRiskFactor];
	[self saveContext];
	return otherRiskFactor;
}

#pragma mark Allergy Data

- (NSArray*)createAllergyDataFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:AllergyData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:AllergyData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:AllergyData text:@""];
}

#pragma mark Allergy

- (Allergy*)createAllergyForActualRiskFactor:(ActualRiskFactor*)actualRiskFactor name:(NSString*)name note:(NSString*)note
{
	Allergy *allergy = [NSEntityDescription insertNewObjectForEntityForName:@"Allergy" inManagedObjectContext: managedObjectContext];
	allergy.name = name;
	allergy.note = note;
	[actualRiskFactor addAllergiesObject:allergy];
	[self saveContext];
	return allergy;
}

#pragma mark - VISITS

- (Visit*)createVisitForPatient:(Patient*)patient date:(NSDate*)date finalConsideration:(NSString*)finalConsideration info:(NSString*)info type:(NSString*)type 
{
	Visit *visit = [NSEntityDescription insertNewObjectForEntityForName:@"Visit" inManagedObjectContext: managedObjectContext];
	visit.date = date;
	visit.finalConsideration = finalConsideration;
	visit.note = info;
	visit.type = type;
	NSArray *measurements = [self getArrayFromPlist:PLIST_FILE key:@"Measurement"];
	[measurements enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		NSDictionary *measurement = (NSDictionary*)obj;
		[self createMeasurementForVisit:visit name:measurement[@"name"] actualValue:[NSDecimalNumber zero] date:date unit:measurement[@"unit"] index:(int)idx];
	}];
	[patient addVisitsObject:visit];
	[self saveContext];
	return visit;
}

#pragma mark Measurement

- (Measurement*)createMeasurementForVisit:(Visit*)visit name:(NSString*)name actualValue:(NSDecimalNumber*)actualValue date:(NSDate*)date unit:(NSString*)unit index:(int)index
{
	Measurement *measurement = [NSEntityDescription insertNewObjectForEntityForName:@"Measurement" inManagedObjectContext: managedObjectContext];
	measurement.name = name;
	if (actualValue)
		measurement.value = actualValue;
	else
		measurement.value = [NSDecimalNumber zero];
	measurement.index = [NSNumber numberWithInt:index];
	if (date != nil)
		measurement.date = date;
	measurement.unit = unit;
	[visit addMeasurementsObject:measurement];
	[self saveContext];
	return measurement;
}

- (Measurement*)getMeasurementWithName:(NSString*)name visit:(Visit*)visit
{
	Measurement *measurement;
	for (Measurement *m in [visit.measurements allObjects])
	{
		if ([m.name isEqualToString:name])
			measurement = m;
	}
	return measurement;
}

- (NSArray*)getAllMeasurementsWithName:(NSString*)name patient:(Patient*)patient
{
	// prendo tutte le misure con quel nome per quel paziente
	NSMutableArray *measurementsSorted = @[].mutableCopy;
	for (Visit *visit in [patient.visits allObjects])
	{
		for (Measurement *m in [visit.measurements allObjects])
		{
			if ([m.name isEqualToString:name])
				[measurementsSorted addObject:m];
		}
	}
	
	// filtro le misure per data dalla più recente
	NSArray *measurementsOrdered = [measurementsSorted sortedArrayUsingComparator: ^(Measurement *d_1, Measurement *d_2) {
		NSDate *n_1 = d_1.date;
		NSDate *n_2 = d_2.date;
		return [n_1 compare: n_2];
	}];

	return measurementsOrdered;
}

- (NSArray*)getMeasurementsSortedWithNameForVisit:(Visit*)visit
{
	NSArray *measurements = [visit.measurements allObjects];
	NSArray *measurementsOrdered = [measurements sortedArrayUsingComparator: ^(id id_1, id id_2) {
		Measurement *m_1 = (Measurement*) id_1;
		Measurement *m_2 = (Measurement*) id_2;
		NSString *d_1 = m_1.name;
		NSString *d_2 = m_2.name;
		return [d_1 compare: d_2];
	}];
	return measurementsOrdered;
}

- (NSArray*)getMeasurementsSortedWithIndexForVisit:(Visit*)visit
{
	NSArray *measurements = [visit.measurements allObjects];
	NSArray *measurementsOrdered = [measurements sortedArrayUsingComparator: ^(id id_1, id id_2) {
		Measurement *m_1 = (Measurement*) id_1;
		Measurement *m_2 = (Measurement*) id_2;
		NSNumber *d_1 = m_1.index;
		NSNumber *d_2 = m_2.index;
		return [d_1 compare: d_2];
	}];
	return measurementsOrdered;
}


#pragma mark Measurement Imperial System

- (NSDecimalNumber*)getValueImperialSystemMeasurement:(Measurement*)measurement
{
	NSDecimalNumber *value = [NSDecimalNumber zero];
	float valueFloat = 0;
	
	if ([measurement.name isEqualToString:@"Height"])
	{
		valueFloat = [measurement.value floatValue] * 0.3937f;
		value = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:valueFloat] decimalValue]];
	}
	
	else if ([measurement.name isEqualToString:@"Weight"])
	{
		valueFloat = [measurement.value floatValue] * 2.2046f;
		value = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:valueFloat] decimalValue]];
	}
	
	else if ([measurement.name isEqualToString:@"Waist C."])
	{
		valueFloat = [measurement.value floatValue] * 0.3937f;
		value = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:valueFloat] decimalValue]];
	}
	
	else if ([measurement.name isEqualToString:@"Blood Max"])
	{
		value = measurement.value;
	}
	
	else if ([measurement.name isEqualToString:@"Blood Min"])
	{
		value = measurement.value;
	}
	
	else if ([measurement.name isEqualToString:@"Cardiac F."])
	{
		value = measurement.value;
	}
	
	return value;
}

- (NSDecimalNumber*)getValueMetricSystemMeasurementName:(NSString*)measurementName measurementValue:(NSDecimalNumber*)measurementValue
{
	NSDecimalNumber *value = [NSDecimalNumber zero];
	float valueFloat = 0;
	
	if ([measurementName isEqualToString:@"Height"])
	{
		valueFloat = [measurementValue floatValue] / 0.3937f;
		value = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:valueFloat] decimalValue]];
	}
	
	else if ([measurementName isEqualToString:@"Weight"])
	{
		valueFloat = [measurementValue floatValue] / 2.2046f;
		value = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:valueFloat] decimalValue]];
	}
	
	else if ([measurementName isEqualToString:@"Waist C."])
	{
		valueFloat = [measurementValue floatValue] / 0.3937f;
		value = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:valueFloat] decimalValue]];
	}
	
	else if ([measurementName isEqualToString:@"Blood Max"])
	{
		value = measurementValue;
	}
	
	else if ([measurementName isEqualToString:@"Blood Min"])
	{
		value = measurementValue;
	}
	
	else if ([measurementName isEqualToString:@"Cardiac F."])
	{
		value = measurementValue;
	}
	
	return value;
}

- (NSString*)getUnitImperialSystemMeasurement:(Measurement*)measurement measurementName:(NSString*)measurementName
{
	NSString *unit;
	NSString *name;
	if (measurement != nil)
		name = measurement.name;
	else
		name = measurementName;
	
	if ([name isEqualToString:@"Height"])
	{
		unit = @"in";
	}
	
	else if ([name isEqualToString:@"Weight"])
	{
		unit = @"lb";
	}
	
	else if ([name isEqualToString:@"Waist C."])
	{
		unit = @"in";
	}
	
	else if ([name isEqualToString:@"Blood Max"])
	{
		unit = @"";
	}
	
	else if ([name isEqualToString:@"Blood Min"])
	{
		unit = @"";
	}
	
	else if ([name isEqualToString:@"Cardiac F."])
	{
		unit = @"";
	}
	
	return unit;
}

- (NSString*)getUnitMetricSystemMeasurement:(Measurement*)measurement measurementName:(NSString*)measurementName
{
	NSString *unit;
	NSString *name;
	if (measurement != nil)
		name = measurement.name;
	else
		name = measurementName;
	
	if ([name isEqualToString:@"Height"])
	{
		unit = @"cm";
	}
	
	else if ([name isEqualToString:@"Weight"])
	{
		unit = @"kg";
	}
	
	else if ([name isEqualToString:@"Waist C."])
	{
		unit = @"cm";
	}
	
	else if ([name isEqualToString:@"Blood Max"])
	{
		unit = @"";
	}
	
	else if ([name isEqualToString:@"Blood Min"])
	{
		unit = @"";
	}
	
	else if ([name isEqualToString:@"Cardiac F."])
	{
		unit = @"";
	}
	
	return unit;
}


#pragma mark Symptom

- (Symptom*)createSymptomForVisit:(Visit*)visit name:(NSString*)name details:(NSString*)details note:(NSString*)note
{
	Symptom *symptom = [NSEntityDescription insertNewObjectForEntityForName:@"Symptom" inManagedObjectContext: managedObjectContext];
	symptom.name = name;
	symptom.details = details;
	symptom.note = note;
	symptom.note = note;
	[visit addSymptomsObject:symptom];
	[self saveContext];
	return symptom;
}

#pragma mark Visit Type Data

- (NSArray*)createVisitTypesFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:VisitTypeData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:VisitTypeData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:VisitTypeData text:@""];
}

#pragma mark Symptom Data

- (NSArray*)createSymptomsFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:SymptomData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:SymptomData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:SymptomData text:@""];
}

#pragma mark Test Type Data

- (NSArray*)createTestTypesFromPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:DiagnosticTestsData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:DiagnosticTestsData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:DiagnosticTestsData text:@""];
}

#pragma mark - DIAGNOSTIC TESTS

#pragma mark Diagnostic Tests for Visit

- (DiagnosticTest*)createDiagnosticTestVisitForVisit:(Visit*)visit name:(NSString*)name note:(NSString*)note
{
	DiagnosticTest *diagnosticTest = [NSEntityDescription insertNewObjectForEntityForName:@"DiagnosticTest" inManagedObjectContext: managedObjectContext];
	diagnosticTest.name = name;
	diagnosticTest.note = note;
	diagnosticTest.date = nil;
	diagnosticTest.type = nil;
	diagnosticTest.prescription = nil;
	diagnosticTest.proximalAnamnesis = nil;
	[visit addTestsObject:diagnosticTest];
	[self saveContext];
	return diagnosticTest;
}

#pragma mark Diagnostic Tests for Prescription

- (DiagnosticTest*)createDiagnosticTestForPrescription:(Prescription*)prescription name:(NSString*)name note:(NSString*)note
{
	DiagnosticTest *diagnosticTest = [NSEntityDescription insertNewObjectForEntityForName:@"DiagnosticTest" inManagedObjectContext: managedObjectContext];
	diagnosticTest.name = name;
	diagnosticTest.note = note;
	diagnosticTest.date = nil;
	diagnosticTest.type = nil;
	diagnosticTest.visit = nil;
	diagnosticTest.proximalAnamnesis = nil;
	[prescription addTestsObject:diagnosticTest];
	[self saveContext];
	return diagnosticTest;
}

#pragma mark Diagnostic Tests for ProximalAnamnesis

- (DiagnosticTest*)createDiagnosticTestForProximalAnamnesis:(ProximalAnamnesis*)proximalAnamnesis type:(NSString*)type note:(NSString*)note date:(NSDate*)date
{
	DiagnosticTest *diagnosticTest = [NSEntityDescription insertNewObjectForEntityForName:@"DiagnosticTest" inManagedObjectContext: managedObjectContext];
	diagnosticTest.name = nil;
	diagnosticTest.type = type;
	diagnosticTest.note = note;
	diagnosticTest.date = date;
	diagnosticTest.visit = nil;
	diagnosticTest.prescription = nil;
	[proximalAnamnesis addTestsObject:diagnosticTest];
	[self saveContext];
	return diagnosticTest;
}

#pragma mark Diagnostic Image

- (DiagnosticImage*)createDiagnosticImageForTest:(DiagnosticTest*)test image:(NSData*)image note:(NSString*)note
{
	DiagnosticImage *diagnosticImage = [NSEntityDescription insertNewObjectForEntityForName:@"DiagnosticImage" inManagedObjectContext: managedObjectContext];
	diagnosticImage.image = image;
	diagnosticImage.note = note;
	[test addDiagnosticImagesObject:diagnosticImage];
	[self saveContext];
	return diagnosticImage;
}

#pragma mark - HOME THERAPY

#pragma mark Prescribed Therapy

- (PrescribedTherapy*)createPrescribedTherapyForPatient:(Patient*)patient date:(NSDate*)date drugName:(NSString*)drugName dosage:(NSString*)dosage frequency:(NSString*)frequency route:(NSString*)route active:(BOOL)active suspended:(BOOL)suspended prescribed:(BOOL)prescribed dosageUnit:(NSString*)dosageUnit visit:(Visit*)visit prescription:(Prescription*)prescription
{
	PrescribedTherapy *prescribedTherapy = [NSEntityDescription insertNewObjectForEntityForName:@"PrescribedTherapy" inManagedObjectContext: managedObjectContext];
	prescribedTherapy.date = date;
	prescribedTherapy.drugName = drugName;
	prescribedTherapy.dosage = dosage;
	prescribedTherapy.frequency = frequency;
	prescribedTherapy.route = route;
	prescribedTherapy.dosageUnit = dosageUnit;
	prescribedTherapy.active = [NSNumber numberWithBool:active];
	prescribedTherapy.suspended = [NSNumber numberWithBool:suspended];
	prescribedTherapy.prescribed = [NSNumber numberWithBool:prescribed];
	if (visit != nil)
		prescribedTherapy.visit = visit;
	if (prescription != nil)
		prescribedTherapy.prescription = prescription;
	[patient addPrescribedTherapiesObject:prescribedTherapy];
	[self saveContext];
	return prescribedTherapy;
}

- (NSArray*)getPrescribedTherapiesFromPatient:(Patient*)patient
{
	NSArray *therapies = [patient.prescribedTherapies allObjects];
	NSMutableArray *prescribedTherapies = @[].mutableCopy;
	for (PrescribedTherapy *prescribedTherapy in therapies)
		if (![prescribedTherapy.suspended boolValue])
			[prescribedTherapies addObject:prescribedTherapy];
	return prescribedTherapies;
}

- (NSArray*)getSuspendedTherapiesFromPatient:(Patient*)patient
{
	NSArray *therapies = [patient.prescribedTherapies allObjects];
	NSMutableArray *prescribedTherapies = @[].mutableCopy;
	for (PrescribedTherapy *prescribedTherapy in therapies)
		if ([prescribedTherapy.suspended boolValue])
			[prescribedTherapies addObject:prescribedTherapy];
	return prescribedTherapies;
}


- (NSArray*)getTherapiesFromPatient:(Patient*)patient suspended:(BOOL)suspended prescribed:(BOOL)prescribed active:(BOOL)active
{
	NSArray *therapies = [patient.prescribedTherapies allObjects];
	NSMutableArray *patientTherapies = @[].mutableCopy;
	for (PrescribedTherapy *prescribedTherapy in therapies)
		if ([prescribedTherapy.suspended boolValue]==suspended && [prescribedTherapy.prescribed boolValue]==prescribed && [prescribedTherapy.active boolValue]==active)
			[patientTherapies addObject:prescribedTherapy];
	return patientTherapies;
}

- (NSArray*)getTherapiesFromVisit:(Visit*)visit
{
	NSArray *allDrugs = [self getTherapiesFromPatient:visit.patient suspended:NO prescribed:NO active:YES];
	NSMutableArray *drugs = @[].mutableCopy;
	for (PrescribedTherapy *drug in allDrugs)
	{
		if (drug.visit == visit)
			[drugs addObject:drug];
	}
	return drugs;
}

- (NSArray*)getTherapiesFromPrescription:(Prescription*)prescription
{
	NSArray *allDrugs = [self getTherapiesFromPatient:prescription.patient suspended:NO prescribed:NO active:YES];
	NSMutableArray *drugs = @[].mutableCopy;
	for (PrescribedTherapy *drug in allDrugs)
	{
		if (drug.prescription == prescription)
			[drugs addObject:drug];
	}
	return drugs;
}

#pragma mark - PRESCRIPTIONS

- (Prescription*)createPrescriptionForPatient:(Patient*)patient date:(NSDate*)date
{
	Prescription *prescription = [NSEntityDescription insertNewObjectForEntityForName:@"Prescription" inManagedObjectContext: managedObjectContext];
	prescription.startDate = date;
	prescription.title = @"";
	[patient addPrescriptionsObject:prescription];
	[self saveContext];
	return prescription;
}

#pragma mark Referrer Data

- (NSArray*)createReferrersPlist
{
	NSArray *array = [self getArrayFromPlist:PLIST_FILE key:ReferrerData];
	for (int i = 0; i < [array count]; i++)
	{
		[self createDinamicDataType:ReferrerData text:array[i]];
		[self saveContext];
	}
	return [self getDinamicDataArray:ReferrerData text:@""];
}

@end
