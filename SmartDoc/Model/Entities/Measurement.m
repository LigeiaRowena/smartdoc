//
//  Measurement.m
//  SmartDoc
//
//  Created by Francesca Corsini on 04/08/15.
//  Copyright (c) 2015 Francesca Corsini. All rights reserved.
//

#import "Measurement.h"
#import "Visit.h"


@implementation Measurement

@dynamic date;
@dynamic index;
@dynamic name;
@dynamic unit;
@dynamic value;
@dynamic visit;

@end
