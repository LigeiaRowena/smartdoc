//
//  SymptomHistory.m
//  SmartDoc
//
//  Created by Francesca Corsini on 26/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "SymptomHistory.h"
#import "ProximalAnamnesis.h"


@implementation SymptomHistory

@dynamic details;
@dynamic name;
@dynamic note;
@dynamic proximalAnamnesis;

@end
