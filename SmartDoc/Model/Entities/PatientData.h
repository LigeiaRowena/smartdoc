//
//  PatientData.h
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ContactEmail, ContactPhone, Patient;

@interface PatientData : NSManagedObject

@property (nonatomic, retain) NSString * actualWork;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * birthCity;
@property (nonatomic, retain) NSDate * birthDate;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * fiscalCode;
@property (nonatomic, retain) NSString * healthCode;
@property (nonatomic, retain) NSString * insuranceNote;
@property (nonatomic, retain) NSString * insuranceType;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * levelEducation;
@property (nonatomic, retain) NSString * middleName;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * previousWork;
@property (nonatomic, retain) NSString * sex;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * workNote;
@property (nonatomic, retain) NSString * zipCode;
@property (nonatomic, retain) NSSet *phoneContacts;
@property (nonatomic, retain) Patient *patient;
@property (nonatomic, retain) NSSet *emailContacts;
@end

@interface PatientData (CoreDataGeneratedAccessors)

- (void)addPhoneContactsObject:(ContactPhone *)value;
- (void)removePhoneContactsObject:(ContactPhone *)value;
- (void)addPhoneContacts:(NSSet *)values;
- (void)removePhoneContacts:(NSSet *)values;

- (void)addEmailContactsObject:(ContactEmail *)value;
- (void)removeEmailContactsObject:(ContactEmail *)value;
- (void)addEmailContacts:(NSSet *)values;
- (void)removeEmailContacts:(NSSet *)values;

@end
