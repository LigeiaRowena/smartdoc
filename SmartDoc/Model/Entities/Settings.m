//
//  Settings.m
//  SmartDoc
//
//  Created by Francesca Corsini on 27/01/16.
//  Copyright © 2016 Francesca Corsini. All rights reserved.
//

#import "Settings.h"

@implementation Settings

@dynamic email;
@dynamic address;
@dynamic speciality;
@dynamic title;
@dynamic firstName;
@dynamic lastName;
@dynamic organization;
@dynamic phoneWork;
@dynamic phoneMobile;
@dynamic web;
@dynamic systemMeasurement;
@dynamic firstLaunch;
@dynamic firstDashboardLaunch;
@dynamic appointmentDuration;

@end
