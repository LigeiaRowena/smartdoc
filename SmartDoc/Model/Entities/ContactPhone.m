//
//  ContactPhone.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "ContactPhone.h"
#import "PatientData.h"


@implementation ContactPhone

@dynamic text;
@dynamic type;
@dynamic patientData;

@end
