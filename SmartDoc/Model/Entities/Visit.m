//
//  Visit.m
//  SmartDoc
//
//  Created by Francesca Corsini on 16/10/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "Visit.h"
#import "DiagnosticTest.h"
#import "Measurement.h"
#import "Patient.h"
#import "PrescribedTherapy.h"
#import "Symptom.h"


@implementation Visit

@dynamic date;
@dynamic finalConsideration;
@dynamic note;
@dynamic objectiveExamination;
@dynamic type;
@dynamic tests;
@dynamic measurements;
@dynamic patient;
@dynamic symptoms;
@dynamic drug;

@end
