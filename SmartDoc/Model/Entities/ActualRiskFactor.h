//
//  ActualRiskFactor.h
//  SmartDoc
//
//  Created by Francesca Corsini on 27/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Allergy, OtherRiskFactor, Patient;

@interface ActualRiskFactor : NSManagedObject

@property (nonatomic, retain) NSString * other;
@property (nonatomic, retain) NSString * otherRiskFactorNote;
@property (nonatomic, retain) NSString * smoking;
@property (nonatomic, retain) NSString * smokingFrequency;
@property (nonatomic, retain) NSString * smokingNote;
@property (nonatomic, retain) NSString * smokingTime;
@property (nonatomic, retain) NSString * specialDrugs;
@property (nonatomic, retain) NSSet *otherRiskFactors;
@property (nonatomic, retain) Patient *patient;
@property (nonatomic, retain) NSSet *allergies;
@end

@interface ActualRiskFactor (CoreDataGeneratedAccessors)

- (void)addOtherRiskFactorsObject:(OtherRiskFactor *)value;
- (void)removeOtherRiskFactorsObject:(OtherRiskFactor *)value;
- (void)addOtherRiskFactors:(NSSet *)values;
- (void)removeOtherRiskFactors:(NSSet *)values;

- (void)addAllergiesObject:(Allergy *)value;
- (void)removeAllergiesObject:(Allergy *)value;
- (void)addAllergies:(NSSet *)values;
- (void)removeAllergies:(NSSet *)values;

@end
