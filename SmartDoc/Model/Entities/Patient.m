//
//  Patient.m
//  SmartDoc
//
//  Created by Francesca Corsini on 24/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "Patient.h"
#import "ActualRiskFactor.h"
#import "Dashboard.h"
#import "PatientData.h"
#import "PrescribedTherapy.h"
#import "Prescription.h"
#import "ProximalAnamnesis.h"
#import "RemoteAnamnesis.h"
#import "Visit.h"


@implementation Patient

@dynamic category;
@dynamic creationDate;
@dynamic favourite;
@dynamic hasPatientData;
@dynamic hasFirstVisit;
@dynamic actualRiskFactor;
@dynamic dashboard;
@dynamic patientData;
@dynamic prescribedTherapies;
@dynamic prescriptions;
@dynamic proximalAnamnesis;
@dynamic remoteAnamnesis;
@dynamic visits;

@end
