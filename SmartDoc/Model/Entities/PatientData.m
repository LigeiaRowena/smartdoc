//
//  PatientData.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "PatientData.h"
#import "ContactEmail.h"
#import "ContactPhone.h"
#import "Patient.h"


@implementation PatientData

@dynamic actualWork;
@dynamic address;
@dynamic birthCity;
@dynamic birthDate;
@dynamic city;
@dynamic country;
@dynamic fiscalCode;
@dynamic healthCode;
@dynamic insuranceNote;
@dynamic insuranceType;
@dynamic lastName;
@dynamic levelEducation;
@dynamic middleName;
@dynamic name;
@dynamic previousWork;
@dynamic sex;
@dynamic state;
@dynamic title;
@dynamic workNote;
@dynamic zipCode;
@dynamic phoneContacts;
@dynamic patient;
@dynamic emailContacts;

@end
