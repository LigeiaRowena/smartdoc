//
//  DiagnosticTest.h
//  SmartDoc
//
//  Created by Francesca Corsini on 17/11/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Prescription, DiagnosticImage, Visit, ProximalAnamnesis;

@interface DiagnosticTest : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) Prescription *prescription;
@property (nonatomic, retain) Visit *visit;
@property (nonatomic, retain) ProximalAnamnesis *proximalAnamnesis;
@property (nonatomic, retain) NSSet *diagnosticImages;
@end

@interface DiagnosticTest (CoreDataGeneratedAccessors)

- (void)addDiagnosticImagesObject:(DiagnosticImage *)value;
- (void)removeDiagnosticImagesObject:(DiagnosticImage *)value;
- (void)addDiagnosticImages:(NSSet<DiagnosticImage *> *)values;
- (void)removeDiagnosticImages:(NSSet<DiagnosticImage *> *)values;
	
@end
