//
//  Operation.m
//  SmartDoc
//
//  Created by Francesca Corsini on 13/11/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "Operation.h"
#import "RemoteAnamnesis.h"


@implementation Operation

@dynamic name;
@dynamic note;
@dynamic remoteAnamnesis;

@end
