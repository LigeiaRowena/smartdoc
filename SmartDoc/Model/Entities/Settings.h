//
//  Settings.h
//  SmartDoc
//
//  Created by Francesca Corsini on 27/01/16.
//  Copyright © 2016 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface Settings : NSManagedObject

@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *address;
@property (nonatomic, retain) NSString *speciality;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *firstName;
@property (nonatomic, retain) NSString *lastName;
@property (nonatomic, retain) NSString *organization;
@property (nonatomic, retain) NSString *phoneWork;
@property (nonatomic, retain) NSString *phoneMobile;
@property (nonatomic, retain) NSString *web;
@property (nonatomic, retain) NSString *systemMeasurement;
@property (nonatomic, retain) NSNumber *firstLaunch;
@property (nonatomic, retain) NSNumber *firstDashboardLaunch;
@property (nonatomic, retain) NSNumber *appointmentDuration;

@end
