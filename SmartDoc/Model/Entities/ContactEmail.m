//
//  ContactEmail.m
//  SmartDoc
//
//  Created by Francesca Corsini on 23/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "ContactEmail.h"
#import "PatientData.h"


@implementation ContactEmail

@dynamic text;
@dynamic type;
@dynamic patientData;

@end
