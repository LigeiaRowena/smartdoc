//
//  DiagnosticImage.m
//  SmartDoc
//
//  Created by Francesca Corsini on 10/10/15.
//  Copyright © 2015 Francesca Corsini. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DiagnosticImage.h"
#import "DiagnosticTest.h"

@implementation DiagnosticImage 

@dynamic image;
@dynamic note;
@dynamic diagnosticTest;

@end
