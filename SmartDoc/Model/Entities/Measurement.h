//
//  Measurement.h
//  SmartDoc
//
//  Created by Francesca Corsini on 04/08/15.
//  Copyright (c) 2015 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Visit;

@interface Measurement : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * index;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * unit;
@property (nonatomic, retain) NSDecimalNumber * value;
@property (nonatomic, retain) Visit *visit;

@end
