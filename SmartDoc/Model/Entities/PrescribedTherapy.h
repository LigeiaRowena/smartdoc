//
//  PrescribedTherapy.h
//  SmartDoc
//
//  Created by Francesca Corsini on 16/10/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Patient, Prescription, Visit;

@interface PrescribedTherapy : NSManagedObject

@property (nonatomic, retain) NSNumber * active;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * dosage;
@property (nonatomic, retain) NSString * dosageUnit;
@property (nonatomic, retain) NSString * drugName;
@property (nonatomic, retain) NSString * frequency;
@property (nonatomic, retain) NSNumber * prescribed;
@property (nonatomic, retain) NSString * route;
@property (nonatomic, retain) NSNumber * suspended;
@property (nonatomic, retain) Patient *patient;
@property (nonatomic, retain) Visit *visit;
@property (nonatomic, retain) Prescription *prescription;

@end
