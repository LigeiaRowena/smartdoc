//
//  Visit.h
//  SmartDoc
//
//  Created by Francesca Corsini on 16/10/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DiagnosticTest, Measurement, Patient, PrescribedTherapy, Symptom;

@interface Visit : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * finalConsideration;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSString * objectiveExamination;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSSet *tests;
@property (nonatomic, retain) NSSet *measurements;
@property (nonatomic, retain) Patient *patient;
@property (nonatomic, retain) NSSet *symptoms;
@property (nonatomic, retain) PrescribedTherapy *drug;
@end

@interface Visit (CoreDataGeneratedAccessors)

- (void)addTestsObject:(DiagnosticTest *)value;
- (void)removeTestsObject:(DiagnosticTest *)value;
- (void)addTests:(NSSet<DiagnosticTest *> *)values;
- (void)removeTests:(NSSet<DiagnosticTest *> *)values;

- (void)addMeasurementsObject:(Measurement *)value;
- (void)removeMeasurementsObject:(Measurement *)value;
- (void)addMeasurements:(NSSet *)values;
- (void)removeMeasurements:(NSSet *)values;

- (void)addSymptomsObject:(Symptom *)value;
- (void)removeSymptomsObject:(Symptom *)value;
- (void)addSymptoms:(NSSet *)values;
- (void)removeSymptoms:(NSSet *)values;

@end
