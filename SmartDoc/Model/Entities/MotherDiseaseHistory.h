//
//  MotherDiseaseHistory.h
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RemoteAnamnesis;

@interface MotherDiseaseHistory : NSManagedObject

@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) RemoteAnamnesis *remoteAnamnesis;

@end
