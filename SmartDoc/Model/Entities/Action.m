//
//  Action.m
//  SmartDoc
//
//  Created by Francesca Corsini on 05/07/15.
//  Copyright (c) 2015 Francesca Corsini. All rights reserved.
//

#import "Action.h"
#import "Dashboard.h"


@implementation Action

@dynamic date;
@dynamic dateString;
@dynamic desc;
@dynamic time;
@dynamic type;
@dynamic eventIdentifier;
@dynamic dashboard;

@end
