//
//  Dashboard.h
//  SmartDoc
//
//  Created by Francesca Corsini on 13/11/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Action, Patient;

@interface Dashboard : NSManagedObject

@property (nonatomic, retain) NSNumber * active;
@property (nonatomic, retain) Patient *patient;
@property (nonatomic, retain) NSSet *actions;
@end

@interface Dashboard (CoreDataGeneratedAccessors)

- (void)addActionsObject:(Action *)value;
- (void)removeActionsObject:(Action *)value;
- (void)addActions:(NSSet *)values;
- (void)removeActions:(NSSet *)values;

@end
