//
//  ActualRiskFactor.m
//  SmartDoc
//
//  Created by Francesca Corsini on 27/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "ActualRiskFactor.h"
#import "Allergy.h"
#import "OtherRiskFactor.h"
#import "Patient.h"


@implementation ActualRiskFactor

@dynamic other;
@dynamic otherRiskFactorNote;
@dynamic smoking;
@dynamic smokingFrequency;
@dynamic smokingNote;
@dynamic smokingTime;
@dynamic specialDrugs;
@dynamic otherRiskFactors;
@dynamic patient;
@dynamic allergies;

@end
