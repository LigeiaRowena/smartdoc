//
//  MotherDiseaseHistory.m
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "MotherDiseaseHistory.h"
#import "RemoteAnamnesis.h"


@implementation MotherDiseaseHistory

@dynamic type;
@dynamic note;
@dynamic remoteAnamnesis;

@end
