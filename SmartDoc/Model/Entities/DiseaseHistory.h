//
//  DiseaseHistory.h
//  SmartDoc
//
//  Created by Francesca Corsini on 15/10/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RemoteAnamnesis;

@interface DiseaseHistory : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) RemoteAnamnesis *remoteAnamnesis;

@end
