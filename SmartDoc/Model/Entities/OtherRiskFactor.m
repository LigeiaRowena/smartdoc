//
//  OtherRiskFactor.m
//  SmartDoc
//
//  Created by Francesca Corsini on 27/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "OtherRiskFactor.h"
#import "ActualRiskFactor.h"


@implementation OtherRiskFactor

@dynamic name;
@dynamic status;
@dynamic actualRiskFactor;

@end
