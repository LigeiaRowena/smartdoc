//
//  ProximalAnamnesis.h
//  SmartDoc
//
//  Created by Francesca Corsini on 28/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Patient, SymptomHistory, DiagnosticTest;

@interface ProximalAnamnesis : NSManagedObject

@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSString * onSetDate;
@property (nonatomic, retain) NSString * onSetPlace;
@property (nonatomic, retain) NSString * previousClinicalVisit;
@property (nonatomic, retain) NSString * previousHospitalization;
@property (nonatomic, retain) NSString * symptomsDescription;
@property (nonatomic, retain) Patient *patient;
@property (nonatomic, retain) NSSet *symptomsHistory;
@property (nonatomic, retain) NSSet *tests;
@end

@interface ProximalAnamnesis (CoreDataGeneratedAccessors)

- (void)addSymptomsHistoryObject:(SymptomHistory *)value;
- (void)removeSymptomsHistoryObject:(SymptomHistory *)value;
- (void)addSymptomsHistory:(NSSet *)values;
- (void)removeSymptomsHistory:(NSSet *)values;

- (void)addTestsObject:(DiagnosticTest *)value;
- (void)removeTestsObject:(DiagnosticTest *)value;
- (void)addTests:(NSSet<DiagnosticTest *> *)values;
- (void)removeTests:(NSSet<DiagnosticTest *> *)values;

@end
