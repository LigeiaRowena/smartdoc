//
//  DiseaseHistory.m
//  SmartDoc
//
//  Created by Francesca Corsini on 15/10/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "DiseaseHistory.h"
#import "RemoteAnamnesis.h"


@implementation DiseaseHistory

@dynamic name;
@dynamic status;
@dynamic date;
@dynamic remoteAnamnesis;

@end
