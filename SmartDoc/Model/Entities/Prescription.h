//
//  Prescription.h
//  SmartDoc
//
//  Created by Francesca Corsini on 16/10/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DiagnosticTest, Patient, PrescribedTherapy;

@interface Prescription : NSManagedObject

@property (nonatomic, retain) NSString * details;
@property (nonatomic, retain) NSString * physicalPrescription;
@property (nonatomic, retain) NSString * referral;
@property (nonatomic, retain) NSString * referralNote;
@property (nonatomic, retain) NSDate * startDate;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSSet *tests;
@property (nonatomic, retain) Patient *patient;
@property (nonatomic, retain) PrescribedTherapy *drug;
@end

@interface Prescription (CoreDataGeneratedAccessors)

- (void)addTestsObject:(DiagnosticTest *)value;
- (void)removeTestsObject:(DiagnosticTest *)value;
- (void)addTests:(NSSet<DiagnosticTest *> *)values;
- (void)removeTests:(NSSet<DiagnosticTest *> *)values;


@end
