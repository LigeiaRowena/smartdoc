//
//  Allergy.m
//  SmartDoc
//
//  Created by Francesca Corsini on 27/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "Allergy.h"
#import "ActualRiskFactor.h"


@implementation Allergy

@dynamic name;
@dynamic note;
@dynamic actualRiskFactor;

@end
