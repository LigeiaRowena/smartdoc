//
//  Symptom.m
//  SmartDoc
//
//  Created by Francesca Corsini on 29/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "Symptom.h"
#import "Visit.h"


@implementation Symptom

@dynamic details;
@dynamic name;
@dynamic note;
@dynamic visit;

@end
