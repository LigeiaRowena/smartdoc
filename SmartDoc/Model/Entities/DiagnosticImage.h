//
//  DiagnosticImage.h
//  SmartDoc
//
//  Created by Francesca Corsini on 10/10/15.
//  Copyright © 2015 Francesca Corsini. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DiagnosticTest;

@interface DiagnosticImage : NSManagedObject

@property (nonatomic, retain) NSData *image;
@property (nonatomic, retain) NSString *note;
@property (nonatomic, retain) DiagnosticTest *diagnosticTest;

@end
