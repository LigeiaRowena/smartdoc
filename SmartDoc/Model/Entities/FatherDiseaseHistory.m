//
//  FatherDiseaseHistory.m
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "FatherDiseaseHistory.h"
#import "RemoteAnamnesis.h"


@implementation FatherDiseaseHistory

@dynamic note;
@dynamic type;
@dynamic remoteAnamnesis;

@end
