//
//  ProximalAnamnesis.m
//  SmartDoc
//
//  Created by Francesca Corsini on 28/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "ProximalAnamnesis.h"
#import "Patient.h"
#import "SymptomHistory.h"
#import "DiagnosticTest.h"


@implementation ProximalAnamnesis

@dynamic creationDate;
@dynamic note;
@dynamic onSetDate;
@dynamic onSetPlace;
@dynamic previousClinicalVisit;
@dynamic previousHospitalization;
@dynamic symptomsDescription;
@dynamic patient;
@dynamic symptomsHistory;
@dynamic tests;

@end
