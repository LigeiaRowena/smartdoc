//
//  SymptomHistory.h
//  SmartDoc
//
//  Created by Francesca Corsini on 26/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ProximalAnamnesis;

@interface SymptomHistory : NSManagedObject

@property (nonatomic, retain) NSString * details;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) ProximalAnamnesis *proximalAnamnesis;

@end
