//
//  RemoteAnamnesis.m
//  SmartDoc
//
//  Created by Francesca Corsini on 28/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "RemoteAnamnesis.h"
#import "DiseaseHistory.h"
#import "FatherDiseaseHistory.h"
#import "MotherDiseaseHistory.h"
#import "Operation.h"
#import "Patient.h"
#import "Vaccination.h"


@implementation RemoteAnamnesis

@dynamic delivery;
@dynamic deliveryNote;
@dynamic lifeBirds;
@dynamic note;
@dynamic creationDate;
@dynamic diseaseHistory;
@dynamic fatherDiseases;
@dynamic motherDiseases;
@dynamic operations;
@dynamic patient;
@dynamic vaccinations;

@end
