//
//  PrescribedTherapy.m
//  SmartDoc
//
//  Created by Francesca Corsini on 16/10/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "PrescribedTherapy.h"
#import "Patient.h"
#import "Prescription.h"
#import "Visit.h"


@implementation PrescribedTherapy

@dynamic active;
@dynamic date;
@dynamic dosage;
@dynamic dosageUnit;
@dynamic drugName;
@dynamic frequency;
@dynamic prescribed;
@dynamic route;
@dynamic suspended;
@dynamic patient;
@dynamic visit;
@dynamic prescription;

@end
