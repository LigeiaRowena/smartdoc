//
//  Operation.h
//  SmartDoc
//
//  Created by Francesca Corsini on 13/11/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RemoteAnamnesis;

@interface Operation : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) RemoteAnamnesis *remoteAnamnesis;

@end
