//
//  DiagnosticTest.m
//  SmartDoc
//
//  Created by Francesca Corsini on 17/11/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "DiagnosticTest.h"
#import "Prescription.h"
#import "DiagnosticImage.h"
#import "Visit.h"
#import "ProximalAnamnesis.h"

@implementation DiagnosticTest

@dynamic name;
@dynamic note;
@dynamic date;
@dynamic type;
@dynamic prescription;
@dynamic visit;
@dynamic proximalAnamnesis;
@dynamic diagnosticImages;

@end
