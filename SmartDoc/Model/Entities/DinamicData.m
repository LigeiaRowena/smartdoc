//
//  DinamicData.m
//  SmartDoc
//
//  Created by Francesca Corsini on 27/01/16.
//  Copyright © 2016 Francesca Corsini. All rights reserved.
//

#import "DinamicData.h"

@implementation DinamicData

@dynamic type;
@dynamic text;

@end
