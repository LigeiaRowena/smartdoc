//
//  Dashboard.m
//  SmartDoc
//
//  Created by Francesca Corsini on 13/11/13.
//  Copyright (c) 2013 Francesca Corsini. All rights reserved.
//

#import "Dashboard.h"
#import "Action.h"
#import "Patient.h"


@implementation Dashboard

@dynamic active;
@dynamic patient;
@dynamic actions;

@end
