//
//  RemoteAnamnesis.h
//  SmartDoc
//
//  Created by Francesca Corsini on 28/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DiseaseHistory, FatherDiseaseHistory, MotherDiseaseHistory, Operation, Patient, Vaccination;

@interface RemoteAnamnesis : NSManagedObject

@property (nonatomic, retain) NSString * delivery;
@property (nonatomic, retain) NSString * deliveryNote;
@property (nonatomic, retain) NSNumber * lifeBirds;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) NSSet *diseaseHistory;
@property (nonatomic, retain) NSSet *fatherDiseases;
@property (nonatomic, retain) NSSet *motherDiseases;
@property (nonatomic, retain) NSSet *operations;
@property (nonatomic, retain) Patient *patient;
@property (nonatomic, retain) NSSet *vaccinations;
@end

@interface RemoteAnamnesis (CoreDataGeneratedAccessors)

- (void)addDiseaseHistoryObject:(DiseaseHistory *)value;
- (void)removeDiseaseHistoryObject:(DiseaseHistory *)value;
- (void)addDiseaseHistory:(NSSet *)values;
- (void)removeDiseaseHistory:(NSSet *)values;

- (void)addFatherDiseasesObject:(FatherDiseaseHistory *)value;
- (void)removeFatherDiseasesObject:(FatherDiseaseHistory *)value;
- (void)addFatherDiseases:(NSSet *)values;
- (void)removeFatherDiseases:(NSSet *)values;

- (void)addMotherDiseasesObject:(MotherDiseaseHistory *)value;
- (void)removeMotherDiseasesObject:(MotherDiseaseHistory *)value;
- (void)addMotherDiseases:(NSSet *)values;
- (void)removeMotherDiseases:(NSSet *)values;

- (void)addOperationsObject:(Operation *)value;
- (void)removeOperationsObject:(Operation *)value;
- (void)addOperations:(NSSet *)values;
- (void)removeOperations:(NSSet *)values;

- (void)addVaccinationsObject:(Vaccination *)value;
- (void)removeVaccinationsObject:(Vaccination *)value;
- (void)addVaccinations:(NSSet *)values;
- (void)removeVaccinations:(NSSet *)values;

@end
