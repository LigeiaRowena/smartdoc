//
//  Vaccination.m
//  SmartDoc
//
//  Created by Francesca Corsini on 25/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "Vaccination.h"
#import "RemoteAnamnesis.h"


@implementation Vaccination

@dynamic type;
@dynamic date;
@dynamic note;
@dynamic remoteAnamnesis;

@end
