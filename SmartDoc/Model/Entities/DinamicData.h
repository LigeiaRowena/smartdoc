//
//  DinamicData.h
//  SmartDoc
//
//  Created by Francesca Corsini on 27/01/16.
//  Copyright © 2016 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DinamicData : NSManagedObject

@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) NSString *text;

@end

