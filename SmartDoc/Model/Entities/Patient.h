//
//  Patient.h
//  SmartDoc
//
//  Created by Francesca Corsini on 24/07/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ActualRiskFactor, Dashboard, PatientData, PrescribedTherapy, Prescription, ProximalAnamnesis, RemoteAnamnesis, Visit;

@interface Patient : NSManagedObject

@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) NSNumber * favourite;
@property (nonatomic, retain) NSNumber * hasPatientData;
@property (nonatomic, retain) NSNumber * hasFirstVisit;
@property (nonatomic, retain) ActualRiskFactor *actualRiskFactor;
@property (nonatomic, retain) Dashboard *dashboard;
@property (nonatomic, retain) PatientData *patientData;
@property (nonatomic, retain) NSSet *prescribedTherapies;
@property (nonatomic, retain) NSSet *prescriptions;
@property (nonatomic, retain) ProximalAnamnesis *proximalAnamnesis;
@property (nonatomic, retain) RemoteAnamnesis *remoteAnamnesis;
@property (nonatomic, retain) NSSet *visits;
@end

@interface Patient (CoreDataGeneratedAccessors)

- (void)addPrescribedTherapiesObject:(PrescribedTherapy *)value;
- (void)removePrescribedTherapiesObject:(PrescribedTherapy *)value;
- (void)addPrescribedTherapies:(NSSet *)values;
- (void)removePrescribedTherapies:(NSSet *)values;

- (void)addPrescriptionsObject:(Prescription *)value;
- (void)removePrescriptionsObject:(Prescription *)value;
- (void)addPrescriptions:(NSSet *)values;
- (void)removePrescriptions:(NSSet *)values;

- (void)addVisitsObject:(Visit *)value;
- (void)removeVisitsObject:(Visit *)value;
- (void)addVisits:(NSSet *)values;
- (void)removeVisits:(NSSet *)values;

@end
