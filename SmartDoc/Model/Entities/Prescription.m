//
//  Prescription.m
//  SmartDoc
//
//  Created by Francesca Corsini on 16/10/14.
//  Copyright (c) 2014 Francesca Corsini. All rights reserved.
//

#import "Prescription.h"
#import "DiagnosticTest.h"
#import "Patient.h"
#import "PrescribedTherapy.h"


@implementation Prescription

@dynamic details;
@dynamic physicalPrescription;
@dynamic referral;
@dynamic referralNote;
@dynamic startDate;
@dynamic title;
@dynamic tests;
@dynamic patient;
@dynamic drug;

@end
