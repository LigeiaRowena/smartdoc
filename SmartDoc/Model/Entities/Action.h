//
//  Action.h
//  SmartDoc
//
//  Created by Francesca Corsini on 05/07/15.
//  Copyright (c) 2015 Francesca Corsini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Dashboard;

@interface Action : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * dateString;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSString * time;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * eventIdentifier;
@property (nonatomic, retain) Dashboard *dashboard;

@end
